#!/bin/bash

set -e
export IMAGE_NAME="guildion/guildion-web:$BRANCH_NAME"
if [[ $IMAGE_NAME == "guildion/guildion-web:stable" ]] ; then
  IMAGE_NAME="guildion/guildion-web:latest"
fi
docker login --username=$DOCKER_USER --password=$DOCKER_PASS
docker-compose build
docker-compose up
docker push $IMAGE_NAME

sudo docker rm -v $(docker ps -a -q -f status=exited) || true
