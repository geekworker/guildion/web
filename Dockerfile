FROM node:8.9 AS denpendency

MAINTAINER zakimiii

ARG SOURCE_COMMIT
ENV SOURCE_COMMIT ${SOURCE_COMMIT}
ARG DOCKER_TAG
ENV DOCKER_TAG ${DOCKER_TAG}

RUN apt-get -y -qq update

RUN echo "mysql-server mysql-server/root_password password root" | debconf-set-selections && \
    echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections && \
    apt-get -y install mysql-server

RUN npm install -g yarn

WORKDIR /var/app
RUN mkdir -p /var/app
ADD package.json yarn.lock /var/app/
RUN yarn install --non-interactive --frozen-lockfile

FROM denpendency AS builder

WORKDIR /var/app
COPY . /var/app

WORKDIR /var/app/scripts

RUN chmod +x docker-server-entrypoint.sh

WORKDIR /var/app/

RUN mkdir dist \
  && mkdir tmp \
  && yarn dbuild \
  && yarn build

FROM mhart/alpine-node:10.23 AS scratch

RUN apk add git

RUN npm install -g sequelize-cli \
  && yarn global add babel-cli

WORKDIR /var/app
RUN mkdir -p /var/app

COPY --from=builder /var/app/lib /var/app/lib/
COPY --from=builder /var/app/tmp /var/app/tmp/
COPY --from=builder /var/app/dist /var/app/dist/
COPY --from=builder /var/app/webpack /var/app/webpack/
COPY --from=builder /var/app/package.json /var/app/
COPY --from=builder /var/app/yarn.lock /var/app/
COPY --from=builder /var/app/.babelrc /var/app/
COPY --from=builder /var/app/scripts/ /usr/local/bin/
COPY --from=builder /var/app/.env.backend.production /var/app/
COPY --from=builder /var/app/.env.backend.development /var/app/

EXPOSE 8070

ENTRYPOINT [ "/usr/local/bin/docker-server-entrypoint.sh" ]

