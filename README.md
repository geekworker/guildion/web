# Guildion

##### Community building service for movie watchers ( https://guildion.us )

## Installation

#### Docker Compose

```
docker-compose up -d
```

#### Build Docker

````
git clone https://gitlab.com/Zakimiii/cloudee-ios guildion-web
cd guildion-web
docker build . -t guildion:guldion-web:[tagname]
````

## Useage

#### Clone the repository and make a tmp folder

````
git clone https://gitlab.com/Zakimiii/cloudee-ios guildion-web
cd guildion-web
mkdir tmp
````

### Install dependencies on Javascript

````
nvm install v8.7
npm install -g yarn
yarn global add babel-cli
yarn install --frozen-lockfile
yarn run build
````

### Install mysql server(I recommend mysql -v >= 5.7)

````
brew update
brew doctor
brew upgrade
brew install mysql@5.7
brew link mysql@5.7 --force
mysql.server restart
````

### Setting mysql

```
sudo mysql -u root # you success this command, mysql successfully installed
DROP USER 'root'@'localhost';
CREATE USER 'root'@'%' IDENTIFIED BY '';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
FLUSH PRIVILEGES;
```

### Database migrations

````
sudo npm install sequelize-cli -g
cd src/db
sudo sequelize db:drop && sudo sequelize db:create && sudo sequelize db:migrate && sudo sequelize db:seed:all
````

### To run Guildion in production mode

```
sudo yarn run build
sudo yarn run production
```

### To run Guildion in development mode

```
sudo yarn run start
```

