const webpack = require('webpack');
const git = require('git-rev-sync');
const baseConfig = require('./base.config');
const startKoa = require('./utils/start-koa');
const path = require('path');

// var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// baseConfig.plugins.push(new BundleAnalyzerPlugin());

const dotenv = {}
for (let key in require('dotenv').config({ path: ".env.frontend.development" }).parsed) {
    if (
        key != 'NODE_ENV'
    ) {
        dotenv[key] = JSON.stringify(require('dotenv').config({ path: ".env.frontend.development" }).parsed[key]);
    }
}

const processENV = {};
for (let key in process.env) {
    if (
        key != 'NODE_ENV' &&
        !process.env[key].startsWith("\"")
    ) {
        processENV[key] = JSON.stringify(process.env[key]);
    }
}

module.exports = {
    ...baseConfig,
    devtool: 'cheap-module-eval-source-map',
    output: {
        ...baseConfig.output,
        publicPath: '/assets/'
    },
    module: {
        ...baseConfig.module,
        rules: [
            ...baseConfig.module.rules,
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                //...processENV,
                ...dotenv,
                BROWSER: JSON.stringify(true),
                NODE_ENV: JSON.stringify('development'),
                VERSION: JSON.stringify(git.long()),
            }
        }),
        ...baseConfig.plugins,
        function () {
            console.log("Please wait for app server startup (~60s)" +
                " after webpack server startup...");
            this.plugin('done', startKoa);
        }
    ]
};
