require('../utils/String');

const max_decimal_range = 65;
const min_decimal_range = 4;

module.exports = {
    up: function(queryInterface, Sequelize) {
        return queryInterface.sequelize
            .query(
                `ALTER DATABASE ${queryInterface.sequelize.config.database}
            CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;`
            )
            .then(() => {
                return queryInterface.createTable(
                    'users',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        nickname: {
                            type: Sequelize.TEXT('long'),
                        },
                        username: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        description: {
                            type: Sequelize.STRING(255),
                        },
                        picture_small: {
                            type: Sequelize.STRING(255),
                        },
                        picture_large: {
                            type: Sequelize.STRING(255),
                        },
                        locale: {
                            type: Sequelize.STRING(255),
                        },
                        country_code: {
                            type: Sequelize.STRING(255),
                        },
                        timezone: {
                            type: Sequelize.STRING(255),
                        },
                        size_byte: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        admin: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        verified: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        active: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        guild_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        message_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        file_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        movie_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        folder_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        playlist_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        role_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        stream_duration: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        file_movie_stream_duration: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        actived_at: {
                            type: Sequelize.DATE,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `users` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                //This Table is for OAuth.
                return queryInterface.createTable(
                    'identities',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        username: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        mail_notification_token: {
                            type: Sequelize.STRING(255),
                        },
                        enable_mail_notification: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        email_is_verified: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        last_attempt_verify_email: {
                            type: Sequelize.DATE,
                        },
                        verify_email_attempts: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        token: {
                            type: Sequelize.STRING(255),
                        },
                        token_hash: {
                            type: Sequelize.STRING(255),
                        },
                        email: {
                            type: Sequelize.STRING(255),
                        },
                        token: {
                            type: Sequelize.STRING(255),
                        },
                        last_attempt_sign_in: {
                            type: Sequelize.DATE,
                        },
                        sign_in_attempts: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        password_hash: {
                            type: Sequelize.STRING(255),
                        },
                        password: {
                            type: Sequelize.STRING(255),
                        },
                        last_attempt_delete_password: {
                            type: Sequelize.DATE,
                        },
                        delete_password_attempts: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        delete_password_token: {
                            type: Sequelize.STRING(255),
                        },
                        verified: {
                            type: Sequelize.BOOLEAN,
                        },
                        is_deleted: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `identities` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'devises',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        udid: {
                            type: Sequelize.STRING(255),
                        },
                        os: {
                            type: Sequelize.STRING(255),
                        },
                        model: {
                            type: Sequelize.STRING(255),
                        },
                        locale: {
                            type: Sequelize.STRING(255),
                        },
                        country_code: {
                            type: Sequelize.STRING(255),
                        },
                        notification_id: {
                            type: Sequelize.STRING(255),
                        },
                        app_version: {
                            type: Sequelize.STRING(255),
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `devises` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'userDevises',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        devise_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'devises',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_sign_in: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `userDevises` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'accessTokens',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        identity_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'identities',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        devise_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'devises',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                            allowNull: true,
                        },
                        token: {
                            type: Sequelize.STRING(255),
                        },
                        expired_at: {
                            type: Sequelize.DATE,
                        },
                        is_one_time: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `accessTokens` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'developers',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        api_key: {
                            type: Sequelize.STRING(126),
                            allowNull: false,
                            unique: true,
                        },
                        email: {
                            type: Sequelize.STRING(126),
                            allowNull: false,
                            unique: true,
                        },
                        token: {
                            type: Sequelize.STRING(255),
                        },
                        email_is_verified: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        last_attempt_verify_email: {
                            type: Sequelize.DATE,
                        },
                        verify_email_attempts: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        username: {
                            type: Sequelize.STRING(255),
                        },
                        last_attempt_sign_in: {
                            type: Sequelize.DATE,
                        },
                        sign_in_attempts: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        password_hash: {
                            type: Sequelize.STRING(255),
                        },
                        password: {
                            type: Sequelize.STRING(255),
                            allowNull: true,
                        },
                        country_code: {
                            type: Sequelize.STRING(255),
                        },
                        verified: {
                            type: Sequelize.BOOLEAN,
                        },
                        locale: {
                            type: Sequelize.STRING(255),
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `developers` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'notifications',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        notificatable_id: {
                            type: Sequelize.INTEGER,
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                            allowNull: false,
                        },
                        notificatable_type: {
                            type: Sequelize.STRING(256),
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'set null',
                        },
                        template: {
                            type: Sequelize.STRING(255),
                        },
                        url: {
                            type: Sequelize.STRING(255),
                        },
                        is_checked: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `notifications` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'categories',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        ja_name: {
                            type: Sequelize.TEXT('long'),
                        },
                        en_name: {
                            type: Sequelize.TEXT('long'),
                        },
                        ja_groupname: {
                            type: Sequelize.STRING(255),
                        },
                        en_groupname: {
                            type: Sequelize.STRING(255),
                        },
                        guild_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `categories` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'tags',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.TEXT('long'),
                        },
                        locale: {
                            type: Sequelize.STRING(255),
                        },
                        country_code: {
                            type: Sequelize.STRING(255),
                        },
                        guild_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        is_nsfw: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `tags` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'guilds',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        category_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'categories',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'set null',
                            allowNull: true,
                        },
                        owner_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'set null',
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.TEXT('long'),
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        picture_small: {
                            type: Sequelize.STRING(255),
                        },
                        picture_large: {
                            type: Sequelize.STRING(255),
                        },
                        active_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        member_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        channel_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        file_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        size_byte: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        stream_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        movie_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        folder_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        playlist_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        role_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        locale: {
                            type: Sequelize.STRING(255),
                        },
                        country_code: {
                            type: Sequelize.STRING(255),
                        },
                        is_nsfw: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_public: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        bumped_at: {
                            type: Sequelize.DATE,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `guilds` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'members',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        nickname: {
                            type: Sequelize.TEXT('long'),
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        picture_small: {
                            type: Sequelize.STRING(255),
                        },
                        picture_large: {
                            type: Sequelize.STRING(255),
                        },
                        index: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        messages_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        mentions_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        message_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        file_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        stream_duration: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        file_movie_stream_duration: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        size_byte: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        movie_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        folder_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        playlist_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        role_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        everyone_mentions_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        temporary: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `members` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'uploads',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.TEXT('long'),
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        thumbnail: {
                            type: Sequelize.STRING(255),
                        },
                        url: {
                            type: Sequelize.STRING(255),
                        },
                        format: {
                            type: Sequelize.STRING(255),
                        },
                        duration: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        pixel_width: {
                            type: Sequelize.DECIMAL(
                                max_decimal_range,
                                min_decimal_range
                            ),
                            defaultValue: 0,
                        },
                        pixel_height: {
                            type: Sequelize.DECIMAL(
                                max_decimal_range,
                                min_decimal_range
                            ),
                            defaultValue: 0,
                        },
                        size_byte: {
                            type: Sequelize.INTEGER,
                        },
                        provider: {
                            type: Sequelize.STRING(255),
                        },
                        is_deleted: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        view_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        downloadable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: true,
                        },
                        is_uploaded: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: true,
                        },
                        is_upload_failed: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `uploads` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'files',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        member_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        upload_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'uploads',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                            allowNull: true,
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.TEXT('long'),
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        thumbnail: {
                            type: Sequelize.STRING(255),
                        },
                        url: {
                            type: Sequelize.STRING(255),
                        },
                        format: {
                            type: Sequelize.STRING(255),
                        },
                        duration: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        pixel_width: {
                            type: Sequelize.DECIMAL(
                                max_decimal_range,
                                min_decimal_range
                            ),
                            defaultValue: 0,
                        },
                        pixel_height: {
                            type: Sequelize.DECIMAL(
                                max_decimal_range,
                                min_decimal_range
                            ),
                            defaultValue: 0,
                        },
                        size_byte: {
                            type: Sequelize.INTEGER,
                        },
                        provider: {
                            type: Sequelize.STRING(255),
                        },
                        is_deleted: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        view_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        downloadable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: true,
                        },
                        is_uploaded: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: true,
                        },
                        is_upload_failed: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `files` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'sectionChannels',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.TEXT('long'),
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        index: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        channel_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        is_dm: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `sectionChannels` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'streamChannels',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        section_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'sectionChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'set null',
                            allowNull: true,
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.TEXT('long'),
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        message_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        member_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        stream_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        max_participant_count: {
                            type: Sequelize.INTEGER,
                        },
                        index: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        read_only: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        temporary: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_nsfw: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_default: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_dm: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_static: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_im: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_loop: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_system: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        read_only: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        voice_only: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        movie_only: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `streamChannels` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'textChannels',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        section_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'sectionChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'set null',
                            allowNull: true,
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.TEXT('long'),
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        message_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        member_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        index: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        read_only: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_nsfw: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_default: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `textChannels` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'dmChannels',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        section_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'sectionChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'set null',
                            allowNull: true,
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.TEXT('long'),
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        index: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        message_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        member_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        is_static: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_im: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_system: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `dmChannels` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'folders',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        member_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.TEXT('long'),
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        thumbnail: {
                            type: Sequelize.STRING(255),
                        },
                        index: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        file_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        size_byte: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        movie_count: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        read_only: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        temporary: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_playlist: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_nsfw: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `folders` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'fileReferences',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        folder_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'folders',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        file_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'files',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        index: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `fileReferences` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'streamChannelEntries',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        member_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        channel_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'streamChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        messages_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        mentions_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        everyone_mentions_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `streamChannelEntries` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'textChannelEntries',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        member_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        channel_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'textChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        messages_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        mentions_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        everyone_mentions_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `textChannelEntries` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'dmChannelEntries',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        member_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        channel_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'dmChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        messages_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        mentions_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        everyone_mentions_mute: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `dmChannelEntries` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'streams',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        streamable_id: {
                            type: Sequelize.INTEGER,
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                            allowNull: false,
                        },
                        streamable_type: {
                            type: Sequelize.STRING(256),
                        },
                        channel_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'streamChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        delivery_start_at: {
                            type: Sequelize.DATE,
                        },
                        delivery_end_at: {
                            type: Sequelize.DATE,
                        },
                        duration: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        is_live: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `streams` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'guildTags',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        tag_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'tags',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `guildTags` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'memberRequests',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        voter_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        is_accepted: {
                            type: Sequelize.BOOLEAN,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `memberRequests` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'participants',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        member_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        stream_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'streams',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        delivery_start_at: {
                            type: Sequelize.DATE,
                        },
                        delivery_end_at: {
                            type: Sequelize.DATE,
                        },
                        camera_enable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        mic_enable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_live: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `participants` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'messages',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        messageable_id: {
                            type: Sequelize.INTEGER,
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                            allowNull: false,
                        },
                        sender_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                            allowNull: true,
                        },
                        messageable_type: {
                            type: Sequelize.STRING(256),
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        text: {
                            type: Sequelize.TEXT('long'),
                        },
                        is_log: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_static: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `messages` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'channelogs',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        message_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'messages',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                            allowNull: false,
                        },
                        channelogable_id: {
                            type: Sequelize.INTEGER,
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                            allowNull: false,
                        },
                        channelogable_type: {
                            type: Sequelize.STRING(256),
                        },
                        thumbnail: {
                            type: Sequelize.STRING(256),
                        },
                        template: {
                            type: Sequelize.STRING(256),
                        },
                        action: {
                            type: Sequelize.STRING(256),
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `channelogs` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'reads',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        message_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'messages',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        member_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        complete: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `reads` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'attachments',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        message_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'messages',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        file_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'files',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `attachments` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'reactions',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        member_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        message_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'messages',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        data: {
                            type: Sequelize.STRING(255),
                        },
                        is_private: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `reactions` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'userBlocks',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        receiver_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `userBlocks` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'userMutes',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        receiver_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `userMutes` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'userReports',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        receiver_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `userReports` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'guildBlocks',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `guildBlocks` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'guildReports',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'users',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        description: {
                            type: Sequelize.TEXT('long'),
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `guildReports` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'roles',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        guild_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'guilds',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        name: {
                            type: Sequelize.STRING,
                        },
                        color: {
                            type: Sequelize.STRING,
                        },
                        priority: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        is_default: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        is_everyone: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        mentionable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        admin: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        guild_managable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        roles_managable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        channels_managable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        channels_invitable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        kickable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        banable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        member_acceptable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        member_managable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        members_managable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        channels_viewable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        message_sendable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        messages_managable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        message_embeddable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        message_attachable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        messages_readable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        message_mentionable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        message_reactionable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_connectable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_speekable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_livestreamable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        movie_selectable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_mutable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_deafenable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_movable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        folders_viewable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        folder_creatable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        folders_managable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        files_viewable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        file_creatable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        files_managable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        files_downloadable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `roles` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'memberRoles',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        member_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'members',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        role_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'roles',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        position: {
                            type: Sequelize.TEXT('long'),
                        },
                        priority: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `memberRoles` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'streamChannelRoles',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        role_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'roles',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        channel_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'streamChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        roles_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        channel_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        channel_members_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        kickable: {
                            type: Sequelize.BOOLEAN,
                        },
                        invitable: {
                            type: Sequelize.BOOLEAN,
                        },
                        channel_viewable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_sendable: {
                            type: Sequelize.BOOLEAN,
                        },
                        messages_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_embeddable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_attachable: {
                            type: Sequelize.BOOLEAN,
                        },
                        messages_readable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_mentionable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_reactionable: {
                            type: Sequelize.BOOLEAN,
                        },
                        stream_connectable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_speekable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_livestreamable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        movie_selectable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_mutable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_deafenable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        stream_movable: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `streamChannelRoles` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'textChannelRoles',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        role_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'roles',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        channel_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'textChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        roles_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        channel_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        channel_members_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        kickable: {
                            type: Sequelize.BOOLEAN,
                        },
                        invitable: {
                            type: Sequelize.BOOLEAN,
                        },
                        channel_viewable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_sendable: {
                            type: Sequelize.BOOLEAN,
                        },
                        messages_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_embeddable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_attachable: {
                            type: Sequelize.BOOLEAN,
                        },
                        messages_readable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_mentionable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_reactionable: {
                            type: Sequelize.BOOLEAN,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `textChannelRoles` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'dmChannelRoles',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        role_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'roles',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        channel_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'dmChannels',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        roles_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        channel_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        channel_members_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        kickable: {
                            type: Sequelize.BOOLEAN,
                        },
                        invitable: {
                            type: Sequelize.BOOLEAN,
                        },
                        channel_viewable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_sendable: {
                            type: Sequelize.BOOLEAN,
                        },
                        messages_managable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_embeddable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_attachable: {
                            type: Sequelize.BOOLEAN,
                        },
                        messages_readable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_mentionable: {
                            type: Sequelize.BOOLEAN,
                        },
                        message_reactionable: {
                            type: Sequelize.BOOLEAN,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `dmChannelRoles` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'documentGroups',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        groupname: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        ja_title: {
                            type: Sequelize.TEXT('long'),
                        },
                        en_title: {
                            type: Sequelize.TEXT('long'),
                        },
                        template: {
                            type: Sequelize.STRING(255),
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `documentGroups` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'documentSections',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        group_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'documentGroups',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        section_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'documentSections',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                            allowNull: true,
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        ja_title: {
                            type: Sequelize.TEXT('long'),
                        },
                        en_title: {
                            type: Sequelize.TEXT('long'),
                        },
                        template: {
                            type: Sequelize.STRING(255),
                        },
                        index: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `documentSections` ROW_FORMAT=DYNAMIC;'
                );
            })
            .then(function() {
                return queryInterface.createTable(
                    'documents',
                    {
                        id: {
                            allowNull: false,
                            autoIncrement: true,
                            primaryKey: true,
                            type: Sequelize.INTEGER,
                        },
                        section_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'documentSections',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        group_id: {
                            type: Sequelize.INTEGER,
                            references: {
                                model: 'documentGroups',
                                key: 'id',
                            },
                            onUpdate: 'cascade',
                            onDelete: 'cascade',
                        },
                        uid: {
                            type: Sequelize.STRING(126),
                            unique: true,
                        },
                        ja_title: {
                            type: Sequelize.TEXT('long'),
                        },
                        en_title: {
                            type: Sequelize.TEXT('long'),
                        },
                        ja_html: {
                            type: Sequelize.TEXT('long'),
                        },
                        en_html: {
                            type: Sequelize.TEXT('long'),
                        },
                        ja_thumbnail: {
                            type: Sequelize.STRING(255),
                        },
                        en_thumbnail: {
                            type: Sequelize.STRING(255),
                        },
                        template: {
                            type: Sequelize.STRING(255),
                        },
                        version: {
                            type: Sequelize.STRING(255),
                        },
                        index: {
                            type: Sequelize.INTEGER,
                            defaultValue: 0,
                        },
                        permission: {
                            type: Sequelize.BOOLEAN,
                            defaultValue: false,
                        },
                        created_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                        updated_at: {
                            allowNull: false,
                            type: Sequelize.DATE,
                        },
                    },
                    {
                        engine: 'InnoDB ROW_FORMAT=DYNAMIC',
                    }
                );
            })
            .then(function() {
                return queryInterface.sequelize.query(
                    'ALTER TABLE `documents` ROW_FORMAT=DYNAMIC;'
                );
            })
            .catch(e => {
                if (e) throw e;
            });
    },
    down: function(queryInterface, Sequelize) {
        throw new Error('The initial migration is not revertable');
    },
};

/** MEMO: billing scheme stash
.then(function() {
    return queryInterface.createTable(
        'customers',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            user_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            default_card: {
                type: Sequelize.STRING(255),
            },
            email: {
                type: Sequelize.STRING(255),
            },
            description: {
                type: Sequelize.STRING(255),
            },
            is_live: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        },
        {
            engine: 'InnoDB ROW_FORMAT=DYNAMIC',
        }
    );
})
.then(function() {
    return queryInterface.sequelize.query(
        'ALTER TABLE `customers` ROW_FORMAT=DYNAMIC;'
    );
})
.then(function() {
    return queryInterface.createTable(
        'paymentCards',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            customer_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'customers',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            name: {
                type: Sequelize.STRING(255),
            },
            last4: {
                type: Sequelize.STRING(255),
            },
            exp_month: {
                type: Sequelize.INTEGER,
            },
            exp_year: {
                type: Sequelize.INTEGER,
            },
            brand: {
                type: Sequelize.STRING(255),
            },
            cvc_check: {
                type: Sequelize.STRING(255),
            },
            fingerprint: {
                type: Sequelize.STRING(255),
            },
            address_state: {
                type: Sequelize.STRING(255),
            },
            address_city: {
                type: Sequelize.STRING(255),
            },
            address_line1: {
                type: Sequelize.STRING(255),
            },
            address_line2: {
                type: Sequelize.STRING(255),
            },
            address_zip: {
                type: Sequelize.STRING(255),
            },
            address_zip_check: {
                type: Sequelize.STRING(255),
            },
            country: {
                type: Sequelize.STRING(255),
            },
            is_private: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        },
        {
            engine: 'InnoDB ROW_FORMAT=DYNAMIC',
        }
    );
})
.then(function() {
    return queryInterface.sequelize.query(
        'ALTER TABLE `paymentCards` ROW_FORMAT=DYNAMIC;'
    );
})
.then(function() {
    return queryInterface.createTable(
        'paymentCardTokens',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            card_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'paymentCards',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            token: {
                type: Sequelize.STRING(255),
            },
            is_used: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            is_live: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        },
        {
            engine: 'InnoDB ROW_FORMAT=DYNAMIC',
        }
    );
})
.then(function() {
    return queryInterface.sequelize.query(
        'ALTER TABLE `paymentCardTokens` ROW_FORMAT=DYNAMIC;'
    );
})
.then(function() {
    return queryInterface.createTable(
        'plans',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            amount: {
                type: Sequelize.INTEGER,
            },
            currency: {
                type: Sequelize.STRING(255),
            },
            interval: {
                type: Sequelize.STRING(255),
            },
            name: {
                type: Sequelize.STRING(255),
            },
            trial_days: {
                type: Sequelize.INTEGER,
            },
            billing_day: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            last4: {
                type: Sequelize.STRING(255),
            },
            locale: {
                type: Sequelize.STRING(255),
            },
            country_code: {
                type: Sequelize.STRING(255),
            },
            is_live: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        },
        {
            engine: 'InnoDB ROW_FORMAT=DYNAMIC',
        }
    );
})
.then(function() {
    return queryInterface.sequelize.query(
        'ALTER TABLE `plans` ROW_FORMAT=DYNAMIC;'
    );
})
.then(function() {
    return queryInterface.createTable(
        'subscriptions',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            customer_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'customers',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            plan_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'customers',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            ios_product_id: {
                type: Sequelize.STRING(255),
            },
            status: {
                type: Sequelize.STRING(255),
            },
            started_at: {
                type: Sequelize.DATE,
            },
            current_period_start_at: {
                type: Sequelize.DATE,
            },
            trial_start_at: {
                type: Sequelize.DATE,
            },
            trial_end_at: {
                type: Sequelize.DATE,
            },
            paused_at: {
                type: Sequelize.DATE,
            },
            canceled_at: {
                type: Sequelize.DATE,
            },
            resumed_at: {
                type: Sequelize.DATE,
            },
            expired_at: {
                type: Sequelize.DATE,
            },
            platform: {
                type: Sequelize.STRING(255),
            },
            is_in_retry_billing: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            is_autorenew_enabled: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            prorate: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            is_live: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        },
        {
            engine: 'InnoDB ROW_FORMAT=DYNAMIC',
        }
    );
})
.then(function() {
    return queryInterface.sequelize.query(
        'ALTER TABLE `subscriptions` ROW_FORMAT=DYNAMIC;'
    );
})
.then(function() {
    return queryInterface.createTable(
        'transfers',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            amount: {
                type: Sequelize.INTEGER,
            },
            currency: {
                type: Sequelize.STRING(255),
            },
            status: {
                type: Sequelize.STRING(255),
            },
            description: {
                type: Sequelize.STRING(255),
            },
            scheduled_at: {
                type: Sequelize.DATE,
            },
            term_start_at: {
                type: Sequelize.DATE,
            },
            term_end_at: {
                type: Sequelize.DATE,
            },
            transfer_amount: {
                type: Sequelize.INTEGER,
            },
            transfer_at: {
                type: Sequelize.DATE,
            },
            transfer_amount: {
                type: Sequelize.INTEGER,
            },
            carried_balance: {
                type: Sequelize.INTEGER,
            },
            charge_count: {
                type: Sequelize.INTEGER,
            },
            charge_fee: {
                type: Sequelize.INTEGER,
            },
            charge_gross: {
                type: Sequelize.INTEGER,
            },
            net: {
                type: Sequelize.INTEGER,
            },
            refund_amount: {
                type: Sequelize.INTEGER,
            },
            refund_count: {
                type: Sequelize.INTEGER,
            },
            is_live: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        },
        {
            engine: 'InnoDB ROW_FORMAT=DYNAMIC',
        }
    );
})
.then(function() {
    return queryInterface.sequelize.query(
        'ALTER TABLE `transfers` ROW_FORMAT=DYNAMIC;'
    );
})
.then(function() {
    return queryInterface.createTable(
        'charges',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            customer_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'customers',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            subscription_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'subscriptions',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
                allowNull: true,
            },
            card_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'paymentCards',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            amount: {
                type: Sequelize.INTEGER,
            },
            amount_refunded: {
                type: Sequelize.INTEGER,
            },
            captured: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            captured_at: {
                type: Sequelize.DATE,
            },
            currency: {
                type: Sequelize.STRING(255),
            },
            description: {
                type: Sequelize.STRING(255),
            },
            expired_at: {
                type: Sequelize.DATE,
            },
            failure_code: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            failure_message: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            is_paid: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            refund_reason: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            is_refunded: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            is_live: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        },
        {
            engine: 'InnoDB ROW_FORMAT=DYNAMIC',
        }
    );
})
.then(function() {
    return queryInterface.sequelize.query(
        'ALTER TABLE `charges` ROW_FORMAT=DYNAMIC;'
    );
})
.then(function() {
    return queryInterface.createTable(
        'transferCharges',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            transfer_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'transfers',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            charge_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'charges',
                    key: 'id',
                },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        },
        {
            engine: 'InnoDB ROW_FORMAT=DYNAMIC',
        }
    );
})
.then(function() {
    return queryInterface.sequelize.query(
        'ALTER TABLE `transferCharges` ROW_FORMAT=DYNAMIC;'
    );
})
**/
