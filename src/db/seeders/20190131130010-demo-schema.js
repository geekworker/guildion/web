'use strict';
/*
Use this command:
sudo sequelize db:drop && sudo sequelize db:create &&  sudo sequelize db:migrate &&  sudo sequelize db:seed:all --debug
*/
const data = require('../test_data');
const uuidv4 = require('uuid/v4');
let results;
// const crawler = require('../utils/crawler');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return data({
            users_limit: 100,
            developers_limit: 1,
            notifications_limit: 0,
            categories_limit: 100,
            tags_limit: 0,
            files_limit: 2000,
            guilds_limit: 100,
            members_limit: 500,
            sectionChannels_limit: 300,
            streamChannels_limit: 300,
            textChannels_limit: 300,
            dmChannels_limit: 10,
            folders_limit: 1000,
            fileReferences_limit: 200,
            streamChannelEntries_limit: 500,
            textChannelEntries_limit: 500,
            dmChannelEntries_limit: 500,
            streams_limit: 0,
            guildTags_limit: 0,
            memberRequests_limit: 200,
            participants_limit: 0,
            messages_limit: 1000,
            reads_limit: 0,
            attachments_limit: 0,
            reactions_limit: 0,
            documents_limit: 100,
            documentGroups_limit: 100,
            documentSections_limit: 100,
        }).then(val => {
            results = val;
            return (
                queryInterface
                    .bulkInsert('users', results['users'], {})
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'identities',
                            results['identities'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'developers',
                            results['developers'],
                            {}
                        );
                    })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'notifications',
                    //         results['notifications'],
                    //         {}
                    //     );
                    // })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'categories',
                            results['categories'],
                            {}
                        );
                    })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'tags',
                    //         results['tags'],
                    //         {}
                    //     );
                    // })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'guilds',
                            results['guilds'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'members',
                            results['members'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'files',
                            results['files'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'sectionChannels',
                            results['sectionChannels'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'streamChannels',
                            results['streamChannels'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'textChannels',
                            results['textChannels'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'dmChannels',
                            results['dmChannels'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'folders',
                            results['folders'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'fileReferences',
                            results['fileReferences'],
                            {}
                        );
                    })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'streamChannelEntries',
                    //         results['streamChannelEntries'],
                    //         {}
                    //     );
                    // })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'textChannelEntries',
                    //         results['textChannelEntries'],
                    //         {}
                    //     );
                    // })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'dmChannelEntries',
                    //         results['dmChannelEntries'],
                    //         {}
                    //     );
                    // })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'streams',
                    //         results['streams'],
                    //         {}
                    //     );
                    // })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'guildTags',
                    //         results['guildTags'],
                    //         {}
                    //     );
                    // })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'memberRequests',
                            results['memberRequests'],
                            {}
                        );
                    })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'participants',
                    //         results['participants'],
                    //         {}
                    //     );
                    // })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'messages',
                            results['messages'],
                            {}
                        );
                    })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'reads',
                    //         results['reads'],
                    //         {}
                    //     );
                    // })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'attachments',
                    //         results['attachments'],
                    //         {}
                    //     );
                    // })
                    // .then(() => {
                    //     return queryInterface.bulkInsert(
                    //         'reactions',
                    //         results['reactions'],
                    //         {}
                    //     );
                    // });
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'documentGroups',
                            results['documentGroups'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'documentSections',
                            results['documentSections'],
                            {}
                        );
                    })
                    .then(() => {
                        return queryInterface.bulkInsert(
                            'documents',
                            results['documents'],
                            {}
                        );
                    })
            );
        });
    },

    down: (queryInterface, Sequelize) => {
        throw new Error('The demo seeders are not revertable');
    },
};
