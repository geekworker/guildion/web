const casual = require('casual');
const times = require('../utils/times');

const memberRequest = (users_limit, guilds_limit) => {
    return {
        voter_id: casual.integer((from = 1), (to = users_limit)),
        guild_id: casual.integer((from = 1), (to = guilds_limit)),
        is_private: false,
        is_accepted: casual.bool,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function memberRequests(limit = 30, users_number = 30, guilds_number = 30) {
    let memberRequests_array = [];
    await times(limit)(() => {
        memberRequests_array.push(memberRequest(users_number, guilds_number));
    });
    return memberRequests_array;
}

module.exports = memberRequests;
