
// MEMO: This should generate by API


const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const category = (key) => {
    return {
        uid: String.prototype.getUniqueString(),
        ja_name: casual.word,
        en_name: casual.word,
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function categories(limit = 30) {
    let categories_array = [];
    await times(limit)(() => {
        categories_array.push(category(categories_array.length));
    });
    return categories_array;
}

module.exports = categories;
