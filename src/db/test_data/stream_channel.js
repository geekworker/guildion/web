const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const streamChannel = (key, guilds_limit, sections_limit) => {
    let section_id = casual.integer((from = 1), (to = sections_limit))
    return {
        guild_id: section_id % guilds_limit == 0 ? guilds_limit : section_id % guilds_limit,
        section_id,
        uid: String.prototype.string_to_utf8_hex_string('St') + String.prototype.getUniqueString(),
        name: casual.word,
        description: casual.sentences((n = 3)),
        max_participant_count: casual.integer((from = 1), (to = 30)),
        index: casual.integer((from = 1), (to = key)),
        read_only: casual.boolean,
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function streamChannels(limit = 30, guilds_number = 30, sections_number = 30) {
    let streamChannels_array = [];
    await times(limit)(() => {
        streamChannels_array.push(streamChannel(streamChannels_array.length, guilds_number, sections_number));
    });
    return streamChannels_array;
}

module.exports = streamChannels;
