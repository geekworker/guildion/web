const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const guild = (key, users_limit, categories_limit) => {
    return {
        owner_id: key % users_limit + 1,
        category_id: casual.integer((from = 1), (to = categories_limit)),
        uid: String.prototype.getUniqueString(),
        name: casual.word,
        description: casual.sentences((n = 3)),
        picture_small:
            'https://i0.wp.com/sk-imedia.com/wp-content/uploads/2015/05/osyarega1-e1430436385100.jpg?zoom=2&fit=580%2C387&ssl=1',
        picture_large:
            'https://i0.wp.com/sk-imedia.com/wp-content/uploads/2015/05/osyarega1-e1430436385100.jpg?zoom=2&fit=580%2C387&ssl=1',
        locale: 'ja',
        country_code: 'JP',
        is_public: casual.boolean,
        is_private: casual.boolean,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function guilds(limit = 30, users_number = 30, categories_number = 30) {
    let guilds_array = [];
    await times(limit)(() => {
        guilds_array.push(guild(guilds_array.length, users_number, categories_number));
    });
    return guilds_array;
}

module.exports = guilds;
