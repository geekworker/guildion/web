const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const getChannel = (channels, members) => {

    return channel
}

const message = (key, members, text_channels, dm_channels, stream_channels) => {
    let isDM,
        isStream,
        channel,
        messageable_id,
        member,
        sender_id;
    while (!channel || !messageable_id) {
        isDM = casual.boolean
        isStream = casual.boolean
        const channels = isDM ? dm_channels : (isStream ? stream_channels : text_channels);
        sender_id = Math.floor(Math.random() * members.length) + 1;
        member = members[sender_id - 1];
        const guild_channels = channels.filter(channel => channel.guild_id == member.guild_id);
        channel = guild_channels[Math.floor(Math.random() * guild_channels.length)];
        if (channel) {
            channels.map((c, offset) => {
                if (c.uid == channel.uid) {
                    messageable_id = offset + 1;
                }
            })
        }
    }
    return {
        messageable_id,
        messageable_type: isDM ? 'DMChannel' : (isStream ? 'StreamChannel' : 'TextChannel'),
        sender_id,
        uid: String.prototype.getUniqueString(),
        text: casual.sentences((n = 3)),
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function messages(limit = 30, members = [], text_channels = [], dm_channels = [], stream_channels = []) {
    let messages_array = [];
    await times(limit)(() => {
        messages_array.push(message(messages_array.length, members, text_channels, dm_channels, stream_channels));
    });
    return messages_array;
}

module.exports = messages;
