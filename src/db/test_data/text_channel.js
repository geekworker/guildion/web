const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const textChannel = (key, guilds_limit, sections_limit) => {
    let section_id = casual.integer((from = 1), (to = sections_limit))
    return {
        guild_id: section_id % guilds_limit == 0 ? guilds_limit : section_id % guilds_limit,
        section_id,
        uid: String.prototype.string_to_utf8_hex_string('Te') + String.prototype.getUniqueString(),
        name: casual.word,
        description: casual.sentences((n = 3)),
        index: casual.integer((from = 1), (to = key)),
        read_only: casual.boolean,
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function textChannels(limit = 30, guilds_number = 30, sections_number = 30) {
    let textChannels_array = [];
    await times(limit)(() => {
        textChannels_array.push(textChannel(textChannels_array.length, guilds_number, sections_number));
    });
    return textChannels_array;
}

module.exports = textChannels;
