const casual = require('casual'); //.ja_JP;
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const stream = (key, files_limit, fileReferences_limit, streamChannels_limit) => {
    return {
        // file_id: casual.integer((from = 1), (to = files_limit)),
        // reference_id: casual.integer((from = 1), (to = fileReferences_limit)),
        channel_id: casual.integer((from = 1), (to = streamChannels_limit)),
        delivery_start_at: new Date(),
        delivery_end_at: new Date(),
        is_live: casual.boolean,
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function streams(limit = 30, files_number = 30, fileReferences_number = 30, streamChannels_number = 30) {
    let streams_array = [];
    await times(limit)(() => {
        streams_array.push(stream(streams_array.length, files_number, fileReferences_number, streamChannels_number));
    });
    return streams_array;
}

module.exports = streams;
