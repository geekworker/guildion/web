const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const document = (key, documentGroups_limit = 30, documentSections_limit = 30) => {
    let group_id = casual.integer((from = 1), (to = documentGroups_limit))
    let section_id = casual.integer((from = 1), (to = documentSections_limit))
    return {
        group_id,
        section_id,
        uid: String.prototype.string_to_utf8_hex_string('DO') + String.prototype.getUniqueString(),
        ja_title: casual.word,
        en_title: casual.word,
        ja_html: casual.sentences((n = Math.floor(Math.random() * 20))),
        en_html: casual.sentences((n = Math.floor(Math.random() * 20))),
        ja_thumbnail: 'https://i0.wp.com/sk-imedia.com/wp-content/uploads/2015/05/osyarega1-e1430436385100.jpg?zoom=2&fit=580%2C387&ssl=1',
        en_thumbnail: 'https://i0.wp.com/sk-imedia.com/wp-content/uploads/2015/05/osyarega1-e1430436385100.jpg?zoom=2&fit=580%2C387&ssl=1',
        index: casual.integer((from = 1), (to = key)),
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function documents(limit = 30, documentGroups_limit = 30, documentSections_limit = 30) {
    let documents_array = [];
    await times(limit)(() => {
        documents_array.push(document(documents_array.length, documentGroups_limit, documentSections_limit));
    });
    return documents_array;
}

module.exports = documents;
