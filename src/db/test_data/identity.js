const casual = require('casual'); //.ja_JP;
const gender = ['mail', 'femail'];
const times = require('../utils/times');

const identity = (key, users_limit) => {
    return {
        user_id: key + 1,
        username: String.prototype.getUniqueString(),
        email: casual.email,
        enable_mail_notification: true,
        email_is_verified: true /*casual.boolean*/,
        last_attempt_verify_email: new Date(),
        verify_email_attempts: 0,
        token: '',
        token_hash:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoiY29uZmlybV9lbWFpbCIsImVtYWlsIjoidGgyNzg3NjMxNEBpY2xvdWQuY29tIiwiaWF0IjoxNTUwNzIxODY5fQ.xCQCe1Dmh1Hp4yOcSgzjdot7hgauF4gIMAWaqfJtn6A',
        delete_password_token:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoiY29uZmlybV9lbWFpbCIsImVtYWlsIjoidGgyNzg3NjMxNEBpY2xvdWQuY29tIiwiaWF0IjoxNTUwNzIxODY5fQ.xCQCe1Dmh1Hp4yOcSgzjdot7hgauF4gIMAWaqfJtn6A',
        is_deleted: false /*casual.boolean*/,
        // MEMO: enable to login with password: "password"
        password_hash:
            '$2a$10$KSLrgTunyqxKlTKLeP.32e971hD2cJ.tUGuR9Cubf.3/gFNzKCOBe',
        password: '',
        verified: true,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function identities(limit = 30) {
    let identities_array = [];
    await times(limit)(() => {
        identities_array.push(identity(identities_array.length, limit));
    });
    return identities_array;
}

module.exports = identities;
