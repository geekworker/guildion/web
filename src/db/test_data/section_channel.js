const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const sectionChannel = (key, guilds_limit) => {
    return {
        guild_id: key % guilds_limit + 1,
        uid: String.prototype.string_to_utf8_hex_string('Se') + String.prototype.getUniqueString(),
        name: casual.word,
        description: casual.sentences((n = 3)),
        index: casual.integer((from = 1), (to = key)),
        is_dm: false,
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function sectionChannels(limit = 30, guilds_number = 30) {
    let sectionChannels_array = [];
    await times(limit)(() => {
        sectionChannels_array.push(sectionChannel(sectionChannels_array.length, guilds_number));
    });
    return sectionChannels_array;
}

module.exports = sectionChannels;
