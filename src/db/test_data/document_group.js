const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const documentGroup = (key) => {
    return {
        uid: String.prototype.string_to_utf8_hex_string('DG') + String.prototype.getUniqueString(),
        groupname: String.prototype.string_to_utf8_hex_string('DG') + String.prototype.getUniqueString(),
        ja_title: casual.word,
        en_title: casual.word,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function documentGroups(limit = 30) {
    let documentGroups_array = [];
    await times(limit)(() => {
        documentGroups_array.push(documentGroup(documentGroups_array.length));
    });
    return documentGroups_array;
}

module.exports = documentGroups;
