const casual = require('casual');
const times = require('../utils/times');

const participant = (members_limit, streams_limit) => {
    return {
        member_id: casual.integer((from = 1), (to = members_limit)),
        stream_id: casual.integer((from = 1), (to = streams_limit)),
        delivery_start_at: new Date(),
        delivery_end_at: new Date(),
        camera_enable: casual.boolean,
        mic_enable: casual.boolean,
        is_live: true,
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function participants(
    limit = 30,
    members_number = 30,
    streams_number = 30
) {
    let participants_array = [];
    await times(limit)(() => {
        participants_array.push(
            participant(members_number, streams_number)
        );
    });
    return participants_array;
}

module.exports = participants;
