const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const documentSection = (key, documentGroups_limit = 30, documentSections_limit = 30) => {
    let group_id = casual.integer((from = 1), (to = documentGroups_limit))
    // let section_id = casual.integer((from = 1), (to = documentSections_limit))
    return {
        group_id,
        uid: String.prototype.string_to_utf8_hex_string('DO') + String.prototype.getUniqueString(),
        ja_title: casual.word,
        en_title: casual.word,
        index: casual.integer((from = 1), (to = key)),
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function documentSections(limit = 30, documentGroups_limit = 30) {
    let documentSections_array = [];
    await times(limit)(() => {
        documentSections_array.push(documentSection(documentSections_array.length, documentGroups_limit, limit));
    });
    return documentSections_array;
}

module.exports = documentSections;
