const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const member = (key, guilds_limit, users_limit) => {
    let user_id = casual.integer((from = 1), (to = users_limit))
    let guild_id = casual.integer((from = 1), (to = guilds_limit))
    return {
        user_id,
        guild_id,
        uid: String.prototype.getUniqueString(),
        nickname: casual.username,
        description: casual.sentences((n = 3)),
        picture_small:
            'https://i0.wp.com/sk-imedia.com/wp-content/uploads/2015/05/osyarega1-e1430436385100.jpg?zoom=2&fit=580%2C387&ssl=1',
        picture_large:
            'https://i0.wp.com/sk-imedia.com/wp-content/uploads/2015/05/osyarega1-e1430436385100.jpg?zoom=2&fit=580%2C387&ssl=1',
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function members(limit = 30, guilds_number = 30, sections_number = 30) {
    let members_array = [];
    await times(limit)(() => {
        members_array.push(member(members_array.length, guilds_number, sections_number));
    });
    return members_array;
}

module.exports = members;
