const casual = require('casual'); //.ja_JP;
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const fileReference = (key, folders = [], files = [], members = []) => {
    let file_id, folder_id;
    while (
        !!!file_id ||
        !!!folder_id ||
        file_id == undefined ||
        folder_id == undefined
    ) {
        folder_id = casual.integer((from = 0), (to = folders.length - 1));
        const folder = folders[folder_id];
        const guild_members = members.filter(
            member => member.guild_id == folder.guild_id
        );
        const guild_member_uids = guild_members.map(member => member.uid);
        let member_uid_ids = {};
        members.map((member, i) => {
            member_uid_ids[member.uid] = i + 1;
        });
        const guild_member_ids = Object.keys(member_uid_ids)
            .filter(uid => guild_member_uids.includes(uid))
            .map(uid => member_uid_ids[uid]);
        const guild_files = files.filter(file =>
            guild_member_ids.includes(file.member_id)
        );
        const file =
            guild_files[
                casual.integer((from = 0), (to = guild_files.length - 1))
            ];
        if (!file) continue;
        let file_uid_ids = {};
        files.map((f, i) => {
            file_uid_ids[f.uid] = i + 1;
        });
        file_id = file_uid_ids[file.uid];
    }
    return {
        file_id,
        folder_id,
        index: casual.integer((from = 1), (to = key)),
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function fileReferences(
    limit = 30,
    folders = [],
    files = [],
    members = []
) {
    let fileReferences_array = [];
    await times(limit)(() => {
        fileReferences_array.push(
            fileReference(fileReferences_array.length, folders, files, members)
        );
    });
    return fileReferences_array;
}

module.exports = fileReferences;
