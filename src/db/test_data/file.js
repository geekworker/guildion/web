const casual = require('casual'); //.ja_JP;
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const file = (key, members_limit = 30) => {
    return {
        member_id: casual.integer((from = 1), (to = members_limit)),
        uid: String.prototype.getUniqueString(),
        name: casual.word,
        description: casual.sentences((n = 3)),
        thumbnail:
            'https://d2gbds6weyduti.cloudfront.net/media/2020/03/66/IMG-1747-600x1067.png',
        url: 'https://www.youtube.com/watch?v=AMaUK1cY9N0',
        format: 'youtube',
        provider: 'youtube',
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function files(limit = 30, members_limit = 30) {
    let files_array = [];
    await times(limit)(() => {
        files_array.push(file(files_array.length, members_limit));
    });
    return files_array;
}

module.exports = files;
