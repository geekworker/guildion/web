const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const dmChannel = (key, sectionChannels_limit = 30, dmSectionChannels = []) => {
    let section_id = casual.integer((from = 0), (to = dmSectionChannels.length - 1));
    let dmSectionChannel = dmSectionChannels[section_id];
    return {
        guild_id: dmSectionChannel.guild_id - 1,
        section_id: sectionChannels_limit + section_id,
        uid: String.prototype.string_to_utf8_hex_string('DM') + String.prototype.getUniqueString(),
        name: casual.word,
        description: casual.sentences((n = 3)),
        index: casual.integer((from = 1), (to = key)),
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function dmChannels(limit = 30, sectionChannels_limit = 30, dmSectionChannels = []) {
    let dmChannels_array = [];
    await times(limit)(() => {
        dmChannels_array.push(dmChannel(dmChannels_array.length, sectionChannels_limit, dmSectionChannels));
    });
    return dmChannels_array;
}

module.exports = dmChannels;
