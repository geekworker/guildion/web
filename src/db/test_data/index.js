try {
    const users = require('./user');
    const identities = require('./identity');
    const developers = require('./developer');
    const notifications = require('./notification');
    const categories = require('./category');
    const tags = require('./tag');
    const files = require('./file');
    const guilds = require('./guild');
    const members = require('./member');
    const sectionChannels = require('./section_channel');
    const dmSectionChannels = require('./dm_section_channel');
    const streamChannels = require('./stream_channel');
    const textChannels = require('./text_channel');
    const dmChannels = require('./dm_channel');
    const folders = require('./folder');
    const fileReferences = require('./file_reference');
    const streamChannelEntries = require('./stream_channel_entry');
    const textChannelEntries = require('./text_channel_entry');
    const dmChannelEntries = require('./dm_channel_entry');
    const streams = require('./stream');
    const guildTags = require('./guild_tag');
    const memberRequests = require('./member_request');
    const participants = require('./participant');
    const messages = require('./message');
    const reads = require('./read');
    const attachments = require('./attachment');
    const reactions = require('./reaction');
    const documents = require('./document');
    const documentGroups = require('./document_group');
    const documentSections = require('./document_section');

    require('../utils/String');

    async function data({
        users_limit,
        developers_limit,
        notifications_limit,
        categories_limit,
        tags_limit,
        files_limit,
        guilds_limit,
        members_limit,
        sectionChannels_limit,
        streamChannels_limit,
        textChannels_limit,
        dmChannels_limit,
        folders_limit,
        fileReferences_limit,
        streamChannelEntries_limit,
        textChannelEntries_limit,
        dmChannelEntries_limit,
        streams_limit,
        guildTags_limit,
        memberRequests_limit,
        participants_limit,
        messages_limit,
        reads_limit,
        attachments_limit,
        reactions_limit,
        documents_limit,
        documentGroups_limit,
        documentSections_limit,
    }) {
        const datum = await Promise.all([
            users(users_limit),
            identities(users_limit),
            developers(developers_limit),
            // notifications(notifications_limit),
            categories(categories_limit),
            // tags(tags_limit),
            files(files_limit, members_limit),
            guilds(guilds_limit, categories_limit),
            members(members_limit, guilds_limit, users_limit),
            sectionChannels(sectionChannels_limit, guilds_limit),
            dmSectionChannels(guilds_limit),
            streamChannels(
                streamChannels_limit,
                guilds_limit,
                sectionChannels_limit
            ),
            textChannels(
                textChannels_limit,
                guilds_limit,
                sectionChannels_limit
            ),
            folders(folders_limit, members_limit, guilds_limit),
            // streamChannelEntries(streamChannelEntries_limit),
            // textChannelEntries(textChannelEntries_limit),
            // dmChannelEntries(dmChannelEntries_limit),
            streams(
                streams_limit,
                files_limit,
                fileReferences_limit,
                streamChannels_limit
            ),
            // guildTags(guildTags_limit),
            memberRequests(memberRequests_limit, guilds_limit),
            participants(participants_limit, members_limit, streams_limit),
            // reads(reads_limit),
            // attachments(attachments_limit),
            // reactions(reactions_limit),
            documents(
                documents_limit,
                documentGroups_limit,
                documentSections_limit
            ),
            documentGroups(documentGroups_limit),
            documentSections(
                documentSections_limit,
                documentGroups_limit,
                documentSections_limit
            ),
        ]);

        const [
            users_data,
            identities_data,
            developers_data,
            // notifications_data,
            categories_data,
            // tags_data,
            files_data,
            guilds_data,
            members_data,
            sectionChannels_data,
            dmSectionChannels_data,
            streamChannels_data,
            textChannels_data,
            folders_data,
            // streamChannelEntries_data,
            // textChannelEntries_data,
            // dmChannelEntries_data,
            streams_data,
            // guildTags_data,
            memberRequests_data,
            participants_data,
            // reads_data,
            // attachments_data,
            // reactions_data,
            documents_data,
            documentGroups_data,
            documentSections_data,
        ] = datum;

        const secondDatum = await Promise.all([
            dmChannels(
                dmChannels_limit,
                sectionChannels_limit,
                dmSectionChannels_data
            ),
        ]);

        const [dmChannels_data] = secondDatum;

        const thirdDatum = await Promise.all([
            messages(
                messages_limit,
                members_data,
                textChannels_data,
                dmChannels_data,
                streamChannels_data
            ),
            fileReferences(
                fileReferences_limit,
                folders_data,
                files_data,
                members_data
            ),
        ]);

        const [messages_data, fileReferences_data] = thirdDatum;

        return {
            users: users_data,
            identities: identities_data,
            developers: developers_data,
            // notifications: notifications_data,
            categories: categories_data,
            // tags: tags_data,
            files: files_data,
            guilds: guilds_data,
            members: members_data,
            sectionChannels: sectionChannels_data.concat(
                dmSectionChannels_data
            ),
            streamChannels: streamChannels_data,
            textChannels: textChannels_data,
            dmChannels: dmChannels_data,
            folders: folders_data,
            fileReferences: fileReferences_data,
            // streamChannelEntries: streamChannelEntries_data,
            // textChannelEntries: textChannelEntries_data,
            // dmChannelEntries: dmChannelEntries_data,
            // streams: streams_data,
            // guildTags: guildTags_data,
            memberRequests: memberRequests_data,
            participants: participants_data,
            messages: messages_data,
            // reads: reads_data,
            // attachments: attachments_data,
            // reactions: reactions_data,
            documents: documents_data,
            documentGroups: documentGroups_data,
            documentSections: documentSections_data,
        };
    }

    module.exports = data;
} catch (e) {
    console.log(e);
}
