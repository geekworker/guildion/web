const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const dmSectionChannel = (key, guilds_limit) => {
    return {
        guild_id: key + 1,
        uid: String.prototype.string_to_utf8_hex_string('Se') + String.prototype.getUniqueString(),
        name: "DIRECT MESSAGES",
        description: "",
        index: 1000,//casual.integer((from = 1), (to = key)),
        is_dm: true,
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function dmSectionChannels(guilds_number = 30) {
    let dmSectionChannels_array = [];
    await times(guilds_number)(() => {
        dmSectionChannels_array.push(dmSectionChannel(dmSectionChannels_array.length, guilds_number));
    });
    return dmSectionChannels_array;
}

module.exports = dmSectionChannels;
