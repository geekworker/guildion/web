const casual = require('casual');
const times = require('../utils/times');
const uuidv4 = require('uuid/v4');

const folder = (key, members_limit = 30, guilds_limit = 30) => {
    const guild_id = casual.integer((from = 1), (to = guilds_limit));
    const member_id = casual.integer((from = 1), (to = members_limit));
    return {
        guild_id,
        member_id,
        uid:
            String.prototype.string_to_utf8_hex_string('Ga') +
            String.prototype.getUniqueString(),
        thumbnail:
            'https://d2gbds6weyduti.cloudfront.net/media/2020/03/66/IMG-1747-600x1067.png',
        name: casual.word,
        description: casual.sentences((n = 3)),
        index: casual.integer((from = 1), (to = key)),
        read_only: casual.boolean,
        is_playlist: casual.boolean,
        is_private: false,
        permission: true,
        created_at: new Date(),
        updated_at: new Date(),
    };
};

async function folders(limit = 30, members_limit = 30, guilds_limit = 30) {
    let folders_array = [];
    await times(limit)(() => {
        folders_array.push(
            folder(folders_array.length, members_limit, guilds_limit)
        );
    });
    return folders_array;
}

module.exports = folders;
