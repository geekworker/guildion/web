import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Devise = sequelize.define(
        'Devise',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            udid: {
                type: DataTypes.STRING(255),
            },
            os: {
                type: DataTypes.STRING(255),
                defaultValue: '',
            },
            model: {
                type: DataTypes.STRING(255),
                defaultValue: '',
            },
            locale: {
                type: DataTypes.STRING(255),
                defaultValue: '',
            },
            country_code: {
                type: DataTypes.STRING(255),
                defaultValue: '',
            },
            notification_id: {
                type: DataTypes.STRING(255),
                defaultValue: '',
            },
            app_version: {
                type: DataTypes.STRING(255),
                defaultValue: '',
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'devises',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Devise),
                associate: function(models) {
                    Devise.belongsToMany(models.User, {
                        as: 'Users',
                        through: 'UserDevise',
                        foreignKey: 'devise_id',
                        otherKey: 'user_id',
                    });
                    Devise.hasMany(models.UserDevise, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                        },
                    });
                    Devise.hasMany(models.AccessToken, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: true,
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Devise;
};
