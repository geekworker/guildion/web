import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var TextChannel = sequelize.define(
        'TextChannel',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                field: 'guild_id',
            },
            SectionId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'sectionChannels',
                    key: 'id',
                },
                allowNull: true,
                field: 'section_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.TEXT('long'),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            message_count: {
                type: DataTypes.INTEGER,
            },
            member_count: {
                type: DataTypes.INTEGER,
            },
            index: {
                type: DataTypes.INTEGER,
            },
            is_default: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_nsfw: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            read_only: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'textChannels',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(TextChannel),
                associate: function(models) {
                    TextChannel.belongsTo(models.Guild, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    TextChannel.belongsTo(models.SectionChannel, {
                        constraints: false,
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                    TextChannel.hasMany(models.Message, {
                        foreignKey: 'messageable_id',
                        constraints: false,
                        scope: {
                            messageable_type: 'TextChannel',
                        },
                    });
                    TextChannel.hasMany(models.TextChannelEntry, {
                        foreignKey: {
                            name: 'channel_id',
                        },
                    });
                    TextChannel.belongsToMany(models.Member, {
                        as: 'Members',
                        through: 'TextChannelEntry',
                        foreignKey: 'channel_id',
                        otherKey: 'member_id',
                    });
                    TextChannel.hasMany(models.TextChannelRole, {
                        foreignKey: {
                            name: 'channel_id',
                        },
                    });
                    TextChannel.belongsToMany(models.Role, {
                        as: 'Roles',
                        through: 'TextChannelRole',
                        foreignKey: 'channel_id',
                        otherKey: 'role_id',
                    });
                    TextChannel.belongsToMany(models.File, {
                        as: 'TextFiles',
                        through: 'Text',
                        foreignKey: 'channel_id',
                        otherKey: 'file_id',
                    });
                    TextChannel.belongsToMany(models.FileReference, {
                        as: 'TextFileReferences',
                        through: 'Text',
                        foreignKey: 'channel_id',
                        otherKey: 'reference_id',
                    });
                },
                index_include: (models, { user_id }) => [
                    {
                        as: 'Section',
                        model: models.SectionChannel,
                        required: false,
                    },
                    {
                        as: 'Messages',
                        model: models.Message,
                        attributes: ['id'],
                        required: false,
                        include: [
                            {
                                as: 'Reads',
                                model: models.Read,
                                required: false,
                                where: {
                                    complete: false,
                                },
                            },
                            {
                                model: models.TextChannelEntry,
                            },
                        ],
                    },
                    {
                        as: 'Members',
                        model: models.Member,
                        through: { attributes: [] },
                        required: true,
                        include: [
                            {
                                as: 'User',
                                required: true,
                                model: models.User,
                                where: {
                                    id: Number(user_id) || 0,
                                },
                            },
                            {
                                model: models.TextChannelEntry,
                            },
                        ],
                    },
                ],
                show_include: models => [
                    {
                        as: 'Members',
                        model: models.Member,
                        through: { attributes: [] },
                        required: false,
                        include: [
                            {
                                as: 'User',
                                model: models.User,
                            },
                            {
                                model: models.TextChannelEntry,
                            },
                        ],
                    },
                    {
                        model: models.TextChannelRole,
                        required: false,
                        include: [
                            {
                                model: models.Role,
                            },
                        ],
                    },
                ],
                guild_show_include: (models, { user_id, member_id }) => [
                    {
                        model: models.SectionChannel,
                        required: false,
                    },
                    {
                        as: 'Members',
                        model: models.Member,
                        through: { attributes: [] },
                        required: false,
                        include: [
                            {
                                as: 'User',
                                model: models.User,
                                where: {
                                    id: Number(user_id) || 0,
                                },
                                required: true,
                            },
                            {
                                model: models.TextChannelEntry,
                            },
                        ],
                    },
                ],
                generate_uid: () => {
                    return (
                        String.prototype.string_to_utf8_hex_string('Te') +
                        String.prototype.getUniqueString()
                    );
                },
            },
            instanceMethods: {
                ...instanceMethods,
                sectionify: function(models) {
                    const section_channel = this.SectionChannel;
                    if (!section_channel) return;
                    section_channel.setValue('TextChannels', [this]);
                    this.removeValue('SectionChannel');
                    return section_channel;
                },
            },
        }
    );

    return TextChannel;
};
