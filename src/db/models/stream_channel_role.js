import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var StreamChannelRole = sequelize.define(
        'StreamChannelRole',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            RoleId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'roles',
                    key: 'id',
                },
                field: 'role_id',
            },
            ChannelId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'streamChannels',
                    key: 'id',
                },
                field: 'channel_id',
                allowNull: false,
            },
            roles_managable: {
                type: DataTypes.BOOLEAN,
            },
            channel_managable: {
                type: DataTypes.BOOLEAN,
            },
            channel_members_managable: {
                type: DataTypes.BOOLEAN,
            },
            invitable: {
                type: DataTypes.BOOLEAN,
            },
            kickable: {
                type: DataTypes.BOOLEAN,
            },
            channel_viewable: {
                type: DataTypes.BOOLEAN,
            },
            message_sendable: {
                type: DataTypes.BOOLEAN,
            },
            messages_managable: {
                type: DataTypes.BOOLEAN,
            },
            message_embeddable: {
                type: DataTypes.BOOLEAN,
            },
            message_attachable: {
                type: DataTypes.BOOLEAN,
            },
            messages_readable: {
                type: DataTypes.BOOLEAN,
            },
            message_mentionable: {
                type: DataTypes.BOOLEAN,
            },
            message_reactionable: {
                type: DataTypes.BOOLEAN,
            },
            stream_connectable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_speekable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_livestreamable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            movie_selectable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_mutable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_deafenable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
            },
        },
        {
            tableName: 'streamChannelRoles',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(StreamChannelRole),
                associate: function(models) {
                    StreamChannelRole.belongsTo(models.Role, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'role_id',
                            allowNull: false,
                        },
                    });
                    StreamChannelRole.belongsTo(models.StreamChannel, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'channel_id',
                            allowNull: false,
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return StreamChannelRole;
};
