import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Channelog = sequelize.define(
        'Channelog',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            MessageId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'messages',
                    key: 'id',
                },
                field: 'message_id',
                allowNull: false,
            },
            ChannelogableId: {
                type: DataTypes.INTEGER,
                field: 'channelogable_id',
                allowNull: false,
            },
            channelogable_type: {
                type: DataTypes.STRING(256),
            },
            thumbnail: {
                type: DataTypes.STRING(256),
            },
            template: {
                type: DataTypes.STRING(256),
            },
            action: {
                type: DataTypes.STRING(256),
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'channelogs',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Message),
                associate: function(models) {
                    Channelog.belongsTo(models.Stream, {
                        foreignKey: 'channelogable_id',
                        constraints: false,
                    });
                    Channelog.belongsTo(models.Message, {
                        as: 'Message',
                        foreignKey: {
                            name: 'message_id',
                            allowNull: false,
                        },
                    });
                },
                show_include: models => [
                    {
                        model: models.Stream,
                        required: false,
                        include: [
                            {
                                model: models.File,
                                required: false,
                                include: [
                                    {
                                        model: models.Member,
                                        required: false,
                                        include: [
                                            {
                                                model: models.User,
                                                required: false,
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                model: models.FileReference,
                                required: false,
                                include: [
                                    {
                                        model: models.File,
                                        required: false,
                                        include: [
                                            {
                                                model: models.Member,
                                                required: false,
                                                include: [
                                                    {
                                                        model: models.User,
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    Channelog.addHook('afterFind', findResult => {
        if (!findResult) return;
        if (!Array.isArray(findResult)) findResult = [findResult];
        for (const instance of findResult) {
            if (
                instance.channelogable_type === 'Stream' &&
                instance.Stream !== undefined
            ) {
                const stream = instance.Stream;
                if (
                    stream.streamable_type === 'File' &&
                    stream.File !== undefined
                ) {
                    stream.setValue('Streamable', stream.File);
                } else if (
                    stream.streamable_type === 'FileReference' &&
                    stream.FileReference !== undefined
                ) {
                    stream.setValue('Streamable', stream.FileReference);
                }
                delete stream.File;
                delete stream.dataValues.File;
                delete stream.FileReference;
                delete stream.dataValues.FileReference;

                instance.setValue('Channelogable', stream);
            }
            delete instance.Stream;
            delete instance.dataValues.Stream;
        }
    });

    return Channelog;
};
