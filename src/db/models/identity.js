import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');
const data_config = require('@constants/data_config');

const hashPasswordCreateHook = (identity, options, callback) => {
    if (identity.get('password') == '') return callback(null, options);
    bcrypt.hash(identity.get('password'), 10, (err, hash) => {
        if (err) return callback(err);
        identity.set('password_hash', hash);
        identity.set('password', '');
        return callback(null, options);
    });
};

const hashPasswordUpdateHook = (identity, options, callback) => {
    if (identity.get('password') == '') return callback(null, options);
    bcrypt.hash(identity.get('password'), 10, (err, hash) => {
        if (err) return callback(err);
        identity
            .update({
                password_hash: hash,
                password: '',
            })
            .then(() => {
                return callback(null, options);
            });
    });
};

const hashTokenCreateHook = (identity, options, callback) => {
    if (identity.get('token') == '') return callback(null, options);
    bcrypt.hash(identity.get('token'), 10, (err, hash) => {
        if (err) return callback(err);
        identity.set('token_hash', hash);
        identity.set('token', '');
        return callback(null, options);
    });
};

const hashTokenUpdateHook = (identity, options, callback) => {
    if (identity.get('token') == '') return callback(null, options);
    bcrypt.hash(identity.get('token'), 10, (err, hash) => {
        if (err) return callback(err);
        identity
            .update({
                token_hash: hash,
                token: '',
            })
            .then(() => {
                return callback(null, options);
            });
        return callback(null, options);
    });
};

const beforeCreateHook = (identity, options, callback) => {
    hashPasswordCreateHook(identity, options, callback);
    hashTokenCreateHook(identity, options, callback);
};

const beforeUpdateHook = (identity, options, callback) => {
    hashPasswordUpdateHook(identity, options, callback);
    hashTokenUpdateHook(identity, options, callback);
};

module.exports = function(sequelize, DataTypes) {
    var Identity = sequelize.define(
        'Identity',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            username: {
                type: DataTypes.STRING(126),
            },
            mail_notification_token: {
                type: DataTypes.STRING(255),
            },
            enable_mail_notification: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            email_is_verified: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            last_attempt_verify_email: {
                type: DataTypes.DATE,
            },
            verify_email_attempts: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            token: {
                type: DataTypes.STRING(255),
            },
            token_hash: {
                type: DataTypes.STRING(255),
            },
            email: {
                type: DataTypes.STRING(126),
                allowNull: false,
            },
            last_attempt_sign_in: {
                type: DataTypes.DATE,
            },
            sign_in_attempts: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            password_hash: {
                type: DataTypes.STRING(255),
            },
            password: {
                type: DataTypes.STRING(255),
            },
            last_attempt_delete_password: {
                type: DataTypes.DATE,
            },
            delete_password_attempts: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            delete_password_token: {
                type: DataTypes.STRING(255),
            },
            verified: {
                type: DataTypes.BOOLEAN,
            },
            is_deleted: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'identities',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            hooks: {
                beforeCreate: beforeCreateHook,
                beforeUpdate: beforeUpdateHook,
            },
            classMethods: {
                // ...classMethods(Identity),
                associate: function(models) {
                    Identity.belongsTo(models.User, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: true,
                        },
                    });
                    Identity.hasMany(models.AccessToken, {
                        as: 'AccessTokens',
                        foreignKey: {
                            name: 'identity_id',
                            allowNull: false,
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
                authenticate: async password => {
                    const isValid = await bcrypt.compare(
                        password,
                        this.password_hash
                    );
                    return isValid;
                },
                authenticateToken: async (self, code) => {
                    return await bcrypt.compare(code, self.token_hash);
                },
            },
        }
    );

    return Identity;
};
