import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var TextChannelEntry = sequelize.define(
        'TextChannelEntry',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            MemberId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'members',
                    key: 'id',
                },
                field: 'member_id',
            },
            ChannelId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'textChannels',
                    key: 'id',
                },
                field: 'channel_id',
                allowNull: false,
            },
            mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            messages_mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            mentions_mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            everyone_mentions_mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'textChannelEntries',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(TextChannelEntry),
                associate: function(models) {
                    TextChannelEntry.belongsTo(models.Member, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'member_id',
                            allowNull: false,
                        },
                    });
                    TextChannelEntry.belongsTo(models.TextChannel, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'channel_id',
                            allowNull: false,
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return TextChannelEntry;
};
