import { Map } from 'immutable';
import data_config from '@constants/data_config';
import CONTENT_TYPE from '@constants/content_type';
import PROVIDER_TYPE from '@constants/provider_type';
import { ApiError } from '@extension/Error';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Upload = sequelize.define(
        'Upload',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.TEXT('long'),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            thumbnail: {
                type: DataTypes.STRING(255),
            },
            url: {
                type: DataTypes.STRING(255),
            },
            format: {
                type: DataTypes.STRING(255),
            },
            provider: {
                type: DataTypes.STRING(255),
            },
            size_byte: {
                type: DataTypes.INTEGER,
            },
            duration: {
                type: DataTypes.INTEGER,
            },
            pixel_width: {
                type: DataTypes.DECIMAL(
                    data_config.max_decimal_range,
                    data_config.min_decimal_range
                ),
            },
            pixel_height: {
                type: DataTypes.DECIMAL(
                    data_config.max_decimal_range,
                    data_config.min_decimal_range
                ),
            },
            is_deleted: {
                type: DataTypes.BOOLEAN,
            },
            view_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            downloadable: {
                type: DataTypes.BOOLEAN,
                defaultValue: true,
            },
            is_uploaded: {
                type: DataTypes.BOOLEAN,
                defaultValue: true,
            },
            is_upload_failed: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'uploads',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Upload),
                associate: function(models) {
                    Upload.belongsTo(models.User, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'user_id',
                            allowNull: false,
                        },
                    });
                    Upload.hasMany(models.File, {
                        foreignKey: {
                            name: 'upload_id',
                            allowNull: false,
                        },
                    });
                },
                file_formats: {
                    mp4: CONTENT_TYPE.mp4,
                    mov: CONTENT_TYPE.mov,
                    mp3: CONTENT_TYPE.mp3,
                    jpg: CONTENT_TYPE.jpg,
                    jpeg: CONTENT_TYPE.jpeg,
                    png: CONTENT_TYPE.png,
                },
                movie_where: models => {
                    return {
                        $or: [
                            ...Object.keys(models.Upload.file_formats)
                                .filter(
                                    key =>
                                        models.Upload.file_formats[key].is_movie
                                )
                                .map(
                                    key => models.Upload.file_formats[key].value
                                ),
                        ],
                    };
                },
                image_where: models => {
                    return {
                        $or: [
                            ...Object.keys(models.Upload.file_formats)
                                .filter(
                                    key =>
                                        models.Upload.file_formats[key].is_image
                                )
                                .map(
                                    key => models.Upload.file_formats[key].value
                                ),
                        ],
                    };
                },
                sound_where: models => {
                    return {
                        $or: [
                            ...Object.keys(models.Upload.file_formats)
                                .filter(
                                    key =>
                                        models.Upload.file_formats[key].is_sound
                                )
                                .map(
                                    key => models.Upload.file_formats[key].value
                                ),
                        ],
                    };
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    const formatCreateHook = (upload, options, callback) => {
        if (upload.get('format') == '')
            return callback(
                new ApiError({
                    error: new Error('is_not_correct'),
                    tt_key: 'is_not_correct',
                    tt_params: {
                        data: 'upload format',
                    },
                }),
                options
            );
        if (
            Object.keys(Upload.file_formats).filter(
                key => Upload.file_formats[key].value == upload.get('format')
            ).length > 0
        ) {
            return callback(null, options);
        } else {
            return callback(
                new ApiError({
                    error: new Error('is_not_correct'),
                    tt_key: 'is_not_correct',
                    tt_params: {
                        data: 'upload format',
                    },
                }),
                options
            );
        }
    };

    const formatUpdateHook = (upload, options, callback) => {
        if (upload.get('format') == '')
            return callback(
                new ApiError({
                    error: new Error('is_not_correct'),
                    tt_key: 'is_not_correct',
                    tt_params: {
                        data: 'upload format',
                    },
                }),
                options
            );
        if (
            Object.keys(Upload.file_formats).filter(
                key => Upload.file_formats[key].value == upload.get('format')
            ).length > 0
        ) {
            return callback(null, options);
        } else {
            return callback(
                new ApiError({
                    error: new Error('is_not_correct'),
                    tt_key: 'is_not_correct',
                    tt_params: {
                        data: 'upload format',
                    },
                }),
                options
            );
        }
    };

    const beforeCreateHook = (upload, options, callback) => {
        formatCreateHook(upload, options, callback);
    };

    const beforeUpdateHook = (upload, options, callback) => {
        formatUpdateHook(upload, options, callback);
    };

    Upload.hook('beforeCreate', beforeCreateHook);
    Upload.hook('beforeUpdate', beforeUpdateHook);

    return Upload;
};
