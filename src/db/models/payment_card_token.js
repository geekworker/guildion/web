import data_config from '@constants/data_config';
import autobind from 'class-autobind';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var PaymentCardToken = sequelize.define(
        'PaymentCardToken',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            CardId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'paymentCards',
                    key: 'id',
                },
                field: 'card_id',
            },
            token: {
                type: DataTypes.STRING(255),
            },
            is_used: {
                type: DataTypes.BOOLEAN,
            },
            is_live: {
                type: DataTypes.BOOLEAN,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
            },
            permission: {
                type: DataTypes.BOOLEAN,
            },
        },
        {
            tableName: 'paymentCardTokens',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(PaymentCardToken),
                associate: function(models) {
                    PaymentCardToken.belongsTo(models.PaymentCard, {
                        as: 'Tokens',
                        foreignKey: 'card_id',
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return PaymentCardToken;
};
