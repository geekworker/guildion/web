var path = require('path');
var Sequelize = require('sequelize');
var basename = path.basename(module.filename);
var env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
var db = {};

let sequelize;
if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
    sequelize = new Sequelize(
        config.database,
        config.username,
        config.password,
        config
    );
}

const models = [
    require('./access_token')(sequelize, Sequelize),
    require('./attachment')(sequelize, Sequelize),
    require('./category')(sequelize, Sequelize),
    require('./channelog')(sequelize, Sequelize),
    require('./charge')(sequelize, Sequelize),
    require('./customer')(sequelize, Sequelize),
    require('./devise')(sequelize, Sequelize),
    require('./document')(sequelize, Sequelize),
    require('./document_group')(sequelize, Sequelize),
    require('./document_section')(sequelize, Sequelize),
    require('./file')(sequelize, Sequelize),
    require('./stream_channel')(sequelize, Sequelize),
    require('./text_channel')(sequelize, Sequelize),
    require('./guild')(sequelize, Sequelize),
    require('./guild_tag')(sequelize, Sequelize),
    require('./developer')(sequelize, Sequelize),
    require('./stream_channel_entry')(sequelize, Sequelize),
    require('./text_channel_entry')(sequelize, Sequelize),
    require('./dm_channel_entry')(sequelize, Sequelize),
    require('./stream_channel_role')(sequelize, Sequelize),
    require('./text_channel_role')(sequelize, Sequelize),
    require('./dm_channel_role')(sequelize, Sequelize),
    require('./identity')(sequelize, Sequelize),
    require('./member')(sequelize, Sequelize),
    require('./guild_block')(sequelize, Sequelize),
    require('./guild_report')(sequelize, Sequelize),
    require('./member_request')(sequelize, Sequelize),
    require('./member_role')(sequelize, Sequelize),
    require('./message')(sequelize, Sequelize),
    require('./notification')(sequelize, Sequelize),
    require('./participant')(sequelize, Sequelize),
    require('./payment_card')(sequelize, Sequelize),
    require('./payment_card_token')(sequelize, Sequelize),
    require('./plan')(sequelize, Sequelize),
    require('./folder')(sequelize, Sequelize),
    require('./file_reference')(sequelize, Sequelize),
    require('./reaction')(sequelize, Sequelize),
    require('./read')(sequelize, Sequelize),
    require('./role')(sequelize, Sequelize),
    require('./dm_channel')(sequelize, Sequelize),
    require('./section_channel')(sequelize, Sequelize),
    require('./stream')(sequelize, Sequelize),
    require('./subscription')(sequelize, Sequelize),
    require('./tag')(sequelize, Sequelize),
    require('./transfer')(sequelize, Sequelize),
    require('./transfer_charge')(sequelize, Sequelize),
    require('./upload')(sequelize, Sequelize),
    require('./user')(sequelize, Sequelize),
    require('./user_block')(sequelize, Sequelize),
    require('./user_devise')(sequelize, Sequelize),
    require('./user_mute')(sequelize, Sequelize),
    require('./user_report')(sequelize, Sequelize),
];

models.forEach(model => {
    db[model.name] = model;
});

models.forEach(model => {
    if (db[model.name].associate) {
        // console.log('entered', model.name);
        db[model.name].associate(db);
    }
});

const classMethods = require('./class_methods');
models.forEach(model => {
    Object.keys(classMethods(model)).forEach(key => {
        model[key] = classMethods(model)[key];
    });
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

if (env === 'development' || env === 'staging') {
    // in dev, sync all table schema automatically for convenience
    sequelize.sync();
}

function esc(value, max_length = 256) {
    if (!value) return '';
    if (typeof value === 'number') return value;
    if (typeof value === 'boolean') return value;
    if (typeof value !== 'string') return '(object)';
    let res = value
        .substring(0, max_length - max_length * 0.2)
        .replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function(char) {
            switch (char) {
                case '\0':
                    return '\\0';
                case '\x08':
                    return '\\b';
                case '\x09':
                    return '\\t';
                case '\x1a':
                    return '\\z';
                case '\n':
                    return '\\n';
                case '\r':
                    return '\\r';
                // case '\'':
                // case "'":
                // case '"':
                // case '\\':
                // case '%':
                //     return '\\' + char; // prepends a backslash to backslash, percent, and double/single quotes
            }
            return '-';
        });
    return res.length < max_length ? res : '-';
}

db.esc = esc;

db.escAttrs = function(attrs) {
    const res = {};
    Object.keys(attrs).forEach(key => (res[key] = esc(attrs[key])));
    return res;
};

module.exports = db;
