import { Map } from 'immutable';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var SectionChannel = sequelize.define(
        'SectionChannel',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                field: 'guild_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.TEXT('long'),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            index: {
                type: DataTypes.INTEGER,
            },
            channel_count: {
                type: DataTypes.INTEGER,
            },
            is_dm: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'sectionChannels',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(SectionChannel),
                associate: function(models) {
                    SectionChannel.belongsTo(models.Guild, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    SectionChannel.hasMany(models.StreamChannel, {
                        constraints: false,
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                    SectionChannel.hasMany(models.DMChannel, {
                        constraints: false,
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                    SectionChannel.hasMany(models.TextChannel, {
                        constraints: false,
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                },
                generate_uid: () => {
                    return String.prototype.string_to_utf8_hex_string('Se') + String.prototype.getUniqueString()
                },
                sectionifies: (sections) => {
                    var uniques = [];
                    sections.map(section => {
                        const unique = uniques.filter(val => val.id == section.id)[0]
                        if (!!unique) {
                            unique.setValue('TextChannels', (unique.dataValues.TextChannels || []).concat(section.dataValues.TextChannels || []))
                            unique.setValue('DMChannels', (unique.dataValues.DMChannels || []).concat(section.dataValues.DMChannels || []))
                            unique.setValue('StreamChannels', (unique.dataValues.StreamChannels || []).concat(section.dataValues.StreamChannels || []))
                        } else {
                            uniques.push(section)
                        }
                    })
                    return uniques
                }
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return SectionChannel;
};
