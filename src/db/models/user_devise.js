import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var UserDevise = sequelize.define(
        'UserDevise',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            DeviseId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'devises',
                    key: 'id',
                },
                field: 'devise_id',
            },
            is_sign_in: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'userDevises',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Devise),
                associate: function(models) {
                    UserDevise.belongsTo(models.User, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: true,
                        },
                    });
                    UserDevise.belongsTo(models.Devise, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: true,
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return UserDevise;
};
