import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var StreamChannel = sequelize.define(
        'StreamChannel',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                field: 'guild_id',
            },
            SectionId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'sectionChannels',
                    key: 'id',
                },
                allowNull: true,
                field: 'section_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.TEXT('long'),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            message_count: {
                type: DataTypes.INTEGER,
            },
            member_count: {
                type: DataTypes.INTEGER,
            },
            stream_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            max_participant_count: {
                type: DataTypes.INTEGER,
            },
            index: {
                type: DataTypes.INTEGER,
            },
            temporary: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_dm: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_static: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_default: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_nsfw: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_im: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_loop: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_system: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            voice_only: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            movie_only: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            read_only: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'streamChannels',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(StreamChannel),
                associate: function(models) {
                    StreamChannel.belongsTo(models.Guild, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    StreamChannel.belongsTo(models.SectionChannel, {
                        constraints: false,
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                    StreamChannel.hasMany(models.Message, {
                        foreignKey: 'messageable_id',
                        constraints: false,
                        scope: {
                            messageable_type: 'StreamChannel',
                        },
                    });
                    StreamChannel.hasMany(models.StreamChannelEntry, {
                        foreignKey: {
                            name: 'channel_id',
                        },
                    });
                    StreamChannel.belongsToMany(models.Member, {
                        as: 'Members',
                        through: 'StreamChannelEntry',
                        foreignKey: 'channel_id',
                        otherKey: 'member_id',
                    });
                    StreamChannel.hasMany(models.Stream, {
                        foreignKey: {
                            name: 'channel_id',
                            allowNull: false,
                        },
                    });
                    StreamChannel.hasMany(models.StreamChannelRole, {
                        foreignKey: {
                            name: 'channel_id',
                        },
                    });
                    StreamChannel.belongsToMany(models.Role, {
                        as: 'Roles',
                        through: 'StreamChannelRole',
                        foreignKey: 'channel_id',
                        otherKey: 'role_id',
                    });
                    // StreamChannel.belongsToMany(models.File, {
                    //     as: 'StreamFiles',
                    //     through: 'Stream',
                    //     foreignKey: 'streamable_id',
                    //     otherKey: 'file_id',
                    // });
                    // StreamChannel.belongsToMany(models.FileReference, {
                    //     as: 'StreamFileReferences',
                    //     through: 'Stream',
                    //     foreignKey: 'streamable_id',
                    //     otherKey: 'reference_id',
                    // });
                },
                index_include: (models, { user_id }) => [
                    {
                        model: models.Stream,
                        where: {
                            is_live: true,
                        },
                        required: false,
                        include: [
                            {
                                model: models.Participant,
                                where: {
                                    is_live: true,
                                },
                                include: [
                                    {
                                        model: models.Member,
                                        include: [
                                            {
                                                model: models.User,
                                            },
                                        ],
                                    },
                                ],
                            },
                            { model: models.File },
                            {
                                model: models.FileReference,
                                include: [
                                    {
                                        model: models.Folder,
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        as: 'Section',
                        model: models.SectionChannel,
                        required: false,
                    },
                    {
                        as: 'Messages',
                        model: models.Message,
                        attributes: ['id'],
                        required: false,
                        include: [
                            {
                                as: 'Reads',
                                model: models.Read,
                                required: false,
                                where: {
                                    complete: false,
                                },
                            },
                        ],
                    },
                    {
                        as: 'Members',
                        model: models.Member,
                        through: { attributes: [] },
                        required: true,
                        include: [
                            {
                                as: 'User',
                                required: true,
                                model: models.User,
                                where: {
                                    id: Number(user_id) || 0,
                                },
                            },
                            {
                                model: models.StreamChannelEntry,
                            },
                        ],
                    },
                ],
                show_include: models => [
                    {
                        model: models.Stream,
                        where: {
                            is_live: true,
                        },
                        required: false,
                        include: [
                            {
                                model: models.Participant,
                                where: {
                                    is_live: true,
                                },
                                required: false,
                                include: [
                                    {
                                        model: models.Member,
                                        include: [
                                            {
                                                model: models.User,
                                            },
                                        ],
                                    },
                                ],
                            },
                            { model: models.File },
                            {
                                model: models.FileReference,
                                include: [
                                    {
                                        model: models.Folder,
                                    },
                                    {
                                        model: models.File,
                                    },
                                ],
                            },
                        ],
                    },
                ],
                guild_show_include: (models, { user_id, member_id }) => [
                    {
                        model: models.SectionChannel,
                        required: false,
                    },
                    {
                        as: 'Members',
                        model: models.Member,
                        through: { attributes: [] },
                        required: false,
                        include: [
                            {
                                as: 'User',
                                model: models.User,
                                where: {
                                    id: Number(user_id) || 0,
                                },
                                required: true,
                            },
                            {
                                model: models.StreamChannelEntry,
                            },
                        ],
                    },
                    {
                        model: models.Stream,
                        where: {
                            is_live: true,
                        },
                        required: false,
                        include: [
                            {
                                model: models.Participant,
                                where: {
                                    is_live: true,
                                },
                                required: false,
                                include: [
                                    {
                                        model: models.Member,
                                        include: [
                                            {
                                                model: models.User,
                                            },
                                        ],
                                    },
                                ],
                            },
                            { model: models.File },
                            {
                                model: models.FileReference,
                                include: [
                                    {
                                        model: models.File,
                                    },
                                ],
                            },
                        ],
                    },
                ],
                generate_uid: () => {
                    return (
                        String.prototype.string_to_utf8_hex_string('St') +
                        String.prototype.getUniqueString()
                    );
                },
            },
            instanceMethods: {
                ...instanceMethods,
                sectionify: function(models) {
                    const section_channel = this.SectionChannel;
                    if (!section_channel) return;
                    section_channel.setValue('StreamChannels', [this]);
                    this.removeValue('SectionChannel');
                    return section_channel;
                },
            },
        }
    );

    StreamChannel.addHook('afterFind', findResult => {
        if (!findResult) return;
        if (!Array.isArray(findResult)) findResult = [findResult];
        for (const instance of findResult) {
            if (!instance.Streams) break;
            for (const stream of instance.Streams) {
                // stream.setStreamable.bind();
                if (
                    stream.streamable_type === 'File' &&
                    stream.File !== undefined
                ) {
                    stream.setValue('Streamable', stream.File);
                } else if (
                    stream.streamable_type === 'FileReference' &&
                    stream.FileReference !== undefined
                ) {
                    stream.setValue('Streamable', stream.FileReference);
                }
                delete stream.File;
                delete stream.dataValues.File;
                delete stream.FileReference;
                delete stream.dataValues.FileReference;
            }
        }
    });

    return StreamChannel;
};
