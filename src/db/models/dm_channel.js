import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var DMChannel = sequelize.define(
        'DMChannel',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            SectionId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'sectionChannels',
                    key: 'id',
                },
                allowNull: true,
                field: 'section_id',
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                allowNull: true,
                field: 'guild_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.TEXT('long'),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            index: {
                type: DataTypes.INTEGER,
            },
            member_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            message_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            is_static: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_im: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_system: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'dmChannels',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(DMChannel),
                associate: function(models) {
                    DMChannel.hasMany(models.Message, {
                        foreignKey: 'messageable_id',
                        constraints: false,
                        scope: {
                            messageable_type: 'DMChannel',
                        },
                    });
                    DMChannel.belongsTo(models.Guild, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    DMChannel.belongsTo(models.SectionChannel, {
                        constraints: false,
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                    DMChannel.hasMany(models.DMChannelEntry, {
                        foreignKey: {
                            name: 'channel_id',
                        },
                    });
                    DMChannel.belongsToMany(models.Member, {
                        as: 'Members',
                        through: 'DMChannelEntry',
                        foreignKey: 'channel_id',
                        otherKey: 'member_id',
                    });
                    DMChannel.hasMany(models.DMChannelRole, {
                        foreignKey: {
                            name: 'channel_id',
                        },
                    });
                    DMChannel.belongsToMany(models.Role, {
                        as: 'Roles',
                        through: 'DMChannelRole',
                        foreignKey: 'channel_id',
                        otherKey: 'role_id',
                    });
                },
                index_include: (models, { user_id }) => [
                    {
                        as: 'Messages',
                        model: models.Message,
                        attributes: ['id'],
                        required: false,
                        include: [
                            {
                                as: 'Reads',
                                model: models.Read,
                                required: false,
                                where: {
                                    complete: false,
                                },
                            },
                        ],
                    },
                    {
                        as: 'Members',
                        model: models.Member,
                        through: { attributes: [] },
                        required: true,
                        include: [
                            {
                                as: 'User',
                                model: models.User,
                                required: true,
                                where: {
                                    id: Number(user_id) || 0,
                                },
                            },
                            {
                                model: models.DMChannelEntry,
                            },
                        ],
                    },
                    {
                        as: 'Section',
                        model: models.SectionChannel,
                        required: false,
                    },
                ],
                show_include: models => [
                    {
                        as: 'Members',
                        model: models.Member,
                        through: { attributes: [] },
                        required: false,
                        include: [
                            {
                                as: 'User',
                                model: models.User,
                            },
                            {
                                model: models.DMChannelEntry,
                            },
                        ],
                    },
                    {
                        model: models.DMChannelRole,
                        required: false,
                        include: [
                            {
                                model: models.Role,
                            },
                        ],
                    },
                ],
                guild_show_include: (models, { user_id, member_id }) => [
                    {
                        model: models.SectionChannel,
                        required: false,
                    },
                    {
                        as: 'Members',
                        model: models.Member,
                        through: { attributes: [] },
                        required: false,
                        include: [
                            {
                                as: 'User',
                                model: models.User,
                                where: {
                                    id: Number(user_id) || 0,
                                },
                                required: true,
                            },
                            {
                                model: models.DMChannelEntry,
                            },
                        ],
                    },
                ],
                guild_show_include: (models, { user_id, member_id }) => [
                    {
                        model: models.SectionChannel,
                        required: false,
                    },
                    {
                        as: 'Members',
                        model: models.Member,
                        through: { attributes: [] },
                        required: false,
                        include: [
                            {
                                as: 'User',
                                model: models.User,
                                where: {
                                    id: Number(user_id) || 0,
                                },
                                required: true,
                            },
                            {
                                model: models.DMChannelEntry,
                            },
                        ],
                    },
                ],
                generate_uid: () => {
                    return (
                        String.prototype.string_to_utf8_hex_string('DM') +
                        String.prototype.getUniqueString()
                    );
                },
            },
            instanceMethods: {
                ...instanceMethods,
                sectionify: function(models) {
                    const section_channel = this.SectionChannel;
                    if (!section_channel) return;
                    section_channel.setValue('DMChannels', [this]);
                    this.removeValue('SectionChannel');
                    return section_channel;
                },
            },
        }
    );

    return DMChannel;
};
