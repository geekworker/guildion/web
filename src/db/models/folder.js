import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Folder = sequelize.define(
        'Folder',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                field: 'guild_id',
            },
            MemberId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'members',
                    key: 'id',
                },
                field: 'member_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.TEXT('long'),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            thumbnail: {
                type: DataTypes.STRING(255),
            },
            index: {
                type: DataTypes.INTEGER,
            },
            file_count: {
                type: DataTypes.INTEGER,
            },
            size_byte: {
                type: DataTypes.INTEGER,
            },
            movie_count: {
                type: DataTypes.INTEGER,
            },
            read_only: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            temporary: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_playlist: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_nsfw: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'folders',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Folder),
                associate: function(models) {
                    Folder.hasMany(models.FileReference, {
                        foreignKey: {
                            name: 'folder_id',
                            allowNull: false,
                        },
                    });
                    Folder.belongsTo(models.Guild, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Folder.belongsTo(models.Member, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'member_id',
                            allowNull: false,
                        },
                    });
                    Folder.belongsToMany(models.File, {
                        as: 'Files',
                        through: 'FileReference',
                        foreignKey: 'folder_id',
                        otherKey: 'file_id',
                    });
                },
                index_include: models => [
                    {
                        as: 'FileReferences',
                        model: models.FileReference,
                        required: false,
                        include: [
                            {
                                model: models.File,
                                include: [
                                    {
                                        model: models.Member,
                                    },
                                ],
                            },
                        ],
                    },
                ],
                show_include: models => [
                    {
                        model: models.Member,
                        include: [
                            {
                                model: models.User,
                            },
                        ],
                    },
                    {
                        as: 'FileReferences',
                        model: models.FileReference,
                        required: false,
                        include: [
                            {
                                model: models.File,
                                include: [
                                    {
                                        model: models.Member,
                                    },
                                ],
                            },
                        ],
                    },
                ],
                generate_uid: () => {
                    return (
                        String.prototype.string_to_utf8_hex_string('Ga') +
                        String.prototype.getUniqueString()
                    );
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Folder;
};
