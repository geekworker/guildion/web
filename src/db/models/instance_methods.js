module.exports = {
    setValue: function(key, value) {
        this.setDataValue(key, value)
        if (!!this['$options'].includeNames) {
            this['$options'].includeNames.push(key)
        } else {
            this['$options'].includeNames = []
            this['$options'].includeNames.push(key)
        }

        this[key] = value
    },
    removeValue: function(key) {
        this.setDataValue(key, null)
        if (!this['$options'].includeNames) return;
        this['$options'].includeNames = this['$options'].includeNames.filter(n => n !== key);
    },
};
