import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
import { available_locales, available_locale_labels } from '@constants/locale_config';
import { getLocale, fallbackLocale } from '@locales';

module.exports = function(sequelize, DataTypes) {
    var DocumentGroup = sequelize.define(
        'DocumentGroup',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            groupname: {
                type: DataTypes.STRING(126),
            },
            ja_title: {
                type: DataTypes.TEXT('long'),
            },
            en_title: {
                type: DataTypes.TEXT('long'),
            },
            template: {
                type: DataTypes.STRING(255),
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'documentGroups',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(DocumentGroup),
                associate: function(models) {
                    DocumentGroup.hasMany(models.Document, {
                        as: 'Documents',
                        foreignKey: {
                            name: 'group_id',
                            allowNull: false,
                        },
                    });
                    DocumentGroup.hasMany(models.DocumentSection, {
                        as: 'Sections',
                        foreignKey: {
                            name: 'group_id',
                            allowNull: true,
                        },
                    });
                },
                show_include: (models) => [
                    {
                        model: models.Document,
                        as: 'Documents',
                        include: [
                            {
                                model: models.DocumentSection,
                                as: 'Section',
                            },
                        ],
                    },
                ],
                getTitle: (model, locale) => {
                    if (!model) return '';
                    switch (getLocale(locale)) {
                    case 'ja': return model.ja_title || model.dataValues.ja_title;
                    default:
                    case 'en': return model.en_title || model.dataValues.en_title;
                    }
                },
            },
            instanceMethods: {
                ...instanceMethods,
                sectionify: function(models) {
                    var EmptyDocuments = this.Documents ? this.Documents.filter(e => e.SectionId == null): []

                    if (EmptyDocuments.length == 0) {
                        return
                    }

                    const EmptySection = models.DocumentSection.build();
                    EmptySection.setValue('Documents', EmptyDocuments);

                    if (this.Sections) {
                        let sections = this.Sections.map(s => s);
                        sections.unshift(EmptySection);
                        this.setDataValue('Sections', sections);
                    } else {
                        this.setValue('Sections', [EmptySection]);
                    }
                    this.removeValue('Documents')
                },
                sortify: function(models) {
                    if (this.Sections.length == 0) return;
                    this.Sections.map(section => {
                        if (section.Documents.length == 0) return;
                        section.setValue('Documents', section.Documents.sort((a, b) => a.index - b.index));
                    });
                },
                getTitle: (locale) => {
                    switch (getLocale(locale)) {
                    case 'ja': return this.ja_title || this.dataValues.ja_title;
                    default:
                    case 'en': return this.en_title || this.dataValues.en_title;
                    }
                },
            },
        }
    );

    return DocumentGroup;
};
