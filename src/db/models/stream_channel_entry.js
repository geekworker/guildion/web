import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var StreamChannelEntry = sequelize.define(
        'StreamChannelEntry',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            MemberId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'members',
                    key: 'id',
                },
                field: 'member_id',
            },
            ChannelId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'streamChannels',
                    key: 'id',
                },
                field: 'channel_id',
                allowNull: false,
            },
            mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            messages_mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            mentions_mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            everyone_mentions_mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'streamChannelEntries',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(StreamChannelEntry),
                associate: function(models) {
                    StreamChannelEntry.belongsTo(models.Member, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'member_id',
                            allowNull: false,
                        },
                    });
                    StreamChannelEntry.belongsTo(models.StreamChannel, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'channel_id',
                            allowNull: false,
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return StreamChannelEntry;
};
