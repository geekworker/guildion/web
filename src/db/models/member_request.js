import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var MemberRequest = sequelize.define(
        'MemberRequest',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                field: 'guild_id',
            },
            VoterId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'voter_id',
            },
            is_accepted: {
                type: DataTypes.BOOLEAN,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'memberRequests',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(MemberRequest),
                associate: function(models) {
                    MemberRequest.belongsTo(models.User, {
                        as: 'Voter',
                        foreignKey: {
                            name: 'voter_id',
                            allowNull: false,
                        },
                    });
                    MemberRequest.belongsTo(models.Guild, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    MemberRequest.hasMany(models.Notification, {
                        foreignKey: 'notificatable_id',
                        constraints: false,
                        scope: {
                            messageable_type: 'MemberRequest',
                        },
                    });
                },
                guild_show_include: (models, { user_id }) => [
                    {
                        as: 'Voter',
                        model: models.User,
                    },
                    {
                        as: 'Guild',
                        model: models.Guild,
                    },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return MemberRequest;
};
