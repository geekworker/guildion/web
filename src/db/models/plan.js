import data_config from '@constants/data_config';
import autobind from 'class-autobind';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var Plan = sequelize.define(
        'Plan',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            amount: {
                type: DataTypes.INTEGER,
            },
            currency: {
                type: DataTypes.STRING(255),
            },
            interval: {
                type: DataTypes.STRING(255),
            },
            name: {
                type: DataTypes.STRING(255),
            },
            trial_days: {
                type: DataTypes.INTEGER,
            },
            billing_day: {
                type: DataTypes.INTEGER,
            },
            last4: {
                type: DataTypes.STRING(255),
            },
            locale: {
                type: DataTypes.STRING(255),
            },
            country_code: {
                type: DataTypes.STRING(255),
            },
            is_live: {
                type: DataTypes.BOOLEAN,
            },
            permission: {
                type: DataTypes.BOOLEAN,
            },
        },
        {
            tableName: 'plans',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(Plan),
                associate: function(models) {
                    Plan.hasMany(models.Subscription, {
                        foreignKey: {
                            name: 'plan_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Plan;
};
