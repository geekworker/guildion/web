import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Message = sequelize.define(
        'Message',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            MessageableId: {
                type: DataTypes.INTEGER,
                field: 'messageable_id',
                allowNull: false,
            },
            SenderId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'members',
                    key: 'id',
                },
                field: 'sender_id',
                allowNull: true,
            },
            messageable_type: {
                type: DataTypes.STRING(256),
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            text: {
                type: DataTypes.TEXT('long'),
            },
            is_static: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_log: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'messages',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Message),
                associate: function(models) {
                    Message.belongsTo(models.DMChannel, {
                        foreignKey: 'messageable_id',
                        constraints: false,
                    });
                    Message.belongsTo(models.StreamChannel, {
                        foreignKey: 'messageable_id',
                        constraints: false,
                    });
                    Message.belongsTo(models.TextChannel, {
                        foreignKey: 'messageable_id',
                        constraints: false,
                    });
                    Message.belongsTo(models.Member, {
                        as: 'Sender',
                        foreignKey: {
                            name: 'sender_id',
                            allowNull: true,
                        },
                    });
                    Message.hasMany(models.Attachment, {
                        foreignKey: {
                            name: 'message_id',
                            allowNull: false,
                        },
                    });
                    Message.belongsToMany(models.File, {
                        through: 'Attachment',
                        foreignKey: 'message_id',
                        otherKey: 'file_id',
                    });
                    Message.hasMany(models.Read, {
                        foreignKey: {
                            name: 'message_id',
                            allowNull: false,
                        },
                    });
                    Message.belongsToMany(models.Member, {
                        as: 'Readers',
                        through: 'Read',
                        foreignKey: 'message_id',
                        otherKey: 'member_id',
                    });
                    Message.hasMany(models.Reaction, {
                        foreignKey: {
                            name: 'message_id',
                            allowNull: false,
                        },
                    });
                    Message.belongsToMany(models.Member, {
                        as: 'ReactionMembers',
                        through: 'Reaction',
                        foreignKey: 'message_id',
                        otherKey: 'member_id',
                    });
                    Message.hasMany(models.Notification, {
                        foreignKey: 'notificatable_id',
                        constraints: false,
                        scope: {
                            messageable_type: 'Message',
                        },
                    });
                    Message.hasOne(models.Channelog, {
                        foreignKey: {
                            name: 'message_id',
                            allowNull: false,
                        },
                    });
                },
                index_include: models => [
                    {
                        as: 'Reads',
                        model: models.Read,
                        required: false,
                    },
                    {
                        model: models.Reaction,
                        required: false,
                    },
                    {
                        as: 'Sender',
                        model: models.Member,
                        required: false,
                        include: [
                            {
                                model: models.User,
                                required: false,
                            },
                        ],
                    },
                    {
                        as: 'Files',
                        model: models.File,
                        required: false,
                        through: { attributes: [] },
                    },
                    { model: models.TextChannel },
                    { model: models.DMChannel },
                    { model: models.StreamChannel },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
                setIncludeMapValue: () => {
                    if (!!this['$options'].includeMap) {
                        Object.keys(this['$options'].includeMap).map(name => {
                            this.setValue.bind(
                                name,
                                this['$options'].includeMap
                            );
                        });
                    }
                },
                getMentionName: () => {
                    var pattern = /\B@[a-z0-9_-]+/gi;
                    return this.dataValues.text
                        .match(pattern)
                        .map(str => str.replace('@', ''));
                },
                getHashtagName: () => {
                    var pattern = /\B#[a-z0-9_-]+/gi;
                    return this.dataValues.text
                        .match(pattern)
                        .map(str => str.replace('#', ''));
                },
            },
        }
    );

    Message.addHook('afterFind', findResult => {
        if (!findResult) return;
        if (!Array.isArray(findResult)) findResult = [findResult];
        for (const instance of findResult) {
            if (
                instance.messageable_type === 'StreamChannel' &&
                instance.StreamChannel !== undefined
            ) {
                instance.setValue('Messageable', instance.StreamChannel);
            } else if (
                instance.messageable_type === 'TextChannel' &&
                instance.TextChannel !== undefined
            ) {
                instance.setValue('Messageable', instance.TextChannel);
            } else if (
                instance.messageable_type === 'DMChannel' &&
                instance.DMChannel !== undefined
            ) {
                instance.setValue('Messageable', instance.DMChannel);
            }
            delete instance.StreamChannel;
            delete instance.dataValues.StreamChannel;
            delete instance.TextChannel;
            delete instance.dataValues.TextChannel;
            delete instance.DMChannel;
            delete instance.dataValues.DMChannel;
        }
    });

    return Message;
};
