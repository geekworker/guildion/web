import { Map } from 'immutable';
import data_config from '@constants/data_config';
import CONTENT_TYPE from '@constants/content_type';
import PROVIDER_TYPE from '@constants/provider_type';
import { ApiError } from '@extension/Error';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var File = sequelize.define(
        'File',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            MemberId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'members',
                    key: 'id',
                },
                field: 'member_id',
            },
            UploadId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'uploads',
                    key: 'id',
                },
                field: 'upload_id',
                allowNull: true,
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.TEXT('long'),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            thumbnail: {
                type: DataTypes.STRING(255),
            },
            url: {
                type: DataTypes.STRING(255),
            },
            format: {
                type: DataTypes.STRING(255),
            },
            size_byte: {
                type: DataTypes.INTEGER,
            },
            duration: {
                type: DataTypes.INTEGER,
            },
            pixel_width: {
                type: DataTypes.DECIMAL(
                    data_config.max_decimal_range,
                    data_config.min_decimal_range
                ),
            },
            pixel_height: {
                type: DataTypes.DECIMAL(
                    data_config.max_decimal_range,
                    data_config.min_decimal_range
                ),
            },
            provider: {
                type: DataTypes.STRING(255),
            },
            is_deleted: {
                type: DataTypes.BOOLEAN,
            },
            view_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            downloadable: {
                type: DataTypes.BOOLEAN,
                defaultValue: true,
            },
            is_uploaded: {
                type: DataTypes.BOOLEAN,
                defaultValue: true,
            },
            is_upload_failed: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'files',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(File),
                associate: function(models) {
                    File.belongsTo(models.Member, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'member_id',
                            allowNull: false,
                        },
                    });
                    File.belongsTo(models.Upload, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'upload_id',
                            allowNull: true,
                        },
                    });
                    File.hasMany(models.FileReference, {
                        foreignKey: {
                            name: 'file_id',
                            allowNull: false,
                        },
                    });
                    File.belongsToMany(models.Folder, {
                        through: 'FileReference',
                        foreignKey: 'file_id',
                        otherKey: 'folder_id',
                    });
                    File.hasMany(models.Stream, {
                        foreignKey: 'streamable_id',
                        constraints: false,
                        scope: {
                            messageable_type: 'File',
                        },
                    });
                    // File.belongsToMany(models.StreamChannel, {
                    //     as: 'StreamChannels',
                    //     through: 'Stream',
                    //     foreignKey: 'streamable_id',
                    //     otherKey: 'channel_id',
                    // });
                    File.hasMany(models.Attachment, {
                        foreignKey: {
                            name: 'file_id',
                            allowNull: false,
                        },
                    });
                    File.belongsToMany(models.Message, {
                        through: 'Attachment',
                        foreignKey: 'file_id',
                        otherKey: 'message_id',
                    });
                },
                file_formats: {
                    mp4: CONTENT_TYPE.mp4,
                    mov: CONTENT_TYPE.mov,
                    mp3: CONTENT_TYPE.mp3,
                    jpg: CONTENT_TYPE.jpg,
                    jpeg: CONTENT_TYPE.jpeg,
                    png: CONTENT_TYPE.png,
                    youtube: PROVIDER_TYPE.youtube,
                },
                movie_where: models => {
                    return {
                        $or: [
                            ...Object.keys(models.File.file_formats)
                                .filter(
                                    key =>
                                        models.File.file_formats[key].is_movie
                                )
                                .map(
                                    key => models.File.file_formats[key].value
                                ),
                        ],
                    };
                },
                image_where: models => {
                    return {
                        $or: [
                            ...Object.keys(models.File.file_formats)
                                .filter(
                                    key =>
                                        models.File.file_formats[key].is_image
                                )
                                .map(
                                    key => models.File.file_formats[key].value
                                ),
                        ],
                    };
                },
                sound_where: models => {
                    return {
                        $or: [
                            ...Object.keys(models.File.file_formats)
                                .filter(
                                    key =>
                                        models.File.file_formats[key].is_sound
                                )
                                .map(
                                    key => models.File.file_formats[key].value
                                ),
                        ],
                    };
                },
                index_include: (models, { guild_id }) => [
                    {
                        as: 'Member',
                        model: models.Member,
                        required: true,
                        where: {
                            guild_id: Number(guild_id) || 0,
                        },
                    },
                ],
                show_include: models => [
                    {
                        as: 'Member',
                        model: models.Member,
                        required: true,
                    },
                ],
                my_index_include: (models, { guild_id, user_id }) => [
                    {
                        as: 'Member',
                        model: models.Member,
                        required: true,
                        where: {
                            guild_id: Number(guild_id) || 0,
                            user_id: Number(user_id) || 0,
                        },
                    },
                ],
                folder_index_include: (models, { guild_id, folder_id }) => [
                    {
                        as: 'Member',
                        model: models.Member,
                        required: true,
                        where: {
                            guild_id: Number(guild_id) || 0,
                        },
                    },
                    {
                        as: 'FileReferences',
                        model: models.FileReference,
                        required: true,
                        where: {
                            folder_id: Number(folder_id) || 0,
                        },
                    },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    const fileFormatCreateHook = (file, options, callback) => {
        if (file.get('format') == '')
            return callback(
                new ApiError({
                    error: new Error('is_not_correct'),
                    tt_key: 'is_not_correct',
                    tt_params: {
                        data: 'file format',
                    },
                }),
                options
            );
        if (
            Object.keys(File.file_formats).filter(
                key => File.file_formats[key].value == file.get('format')
            ).length > 0
        ) {
            return callback(null, options);
        } else {
            return callback(
                new ApiError({
                    error: new Error('is_not_correct'),
                    tt_key: 'is_not_correct',
                    tt_params: {
                        data: 'file format',
                    },
                }),
                options
            );
        }
    };

    const fileFormatUpdateHook = (file, options, callback) => {
        if (file.get('format') == '')
            return callback(
                new ApiError({
                    error: new Error('is_not_correct'),
                    tt_key: 'is_not_correct',
                    tt_params: {
                        data: 'file format',
                    },
                }),
                options
            );
        if (
            Object.keys(File.file_formats).filter(
                key => File.file_formats[key].value == file.get('format')
            ).length > 0
        ) {
            return callback(null, options);
        } else {
            return callback(
                new ApiError({
                    error: new Error('is_not_correct'),
                    tt_key: 'is_not_correct',
                    tt_params: {
                        data: 'file format',
                    },
                }),
                options
            );
        }
    };

    const beforeCreateHook = (file, options, callback) => {
        fileFormatCreateHook(file, options, callback);
    };

    const beforeUpdateHook = (file, options, callback) => {
        fileFormatUpdateHook(file, options, callback);
    };

    File.hook('beforeCreate', beforeCreateHook);
    File.hook('beforeUpdate', beforeUpdateHook);

    return File;
};
