import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var AccessToken = sequelize.define(
        'AccessToken',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            IdentityId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'identites',
                    key: 'id',
                },
                field: 'identity_id',
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            DeviseId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'devises',
                    key: 'id',
                },
                field: 'devise_id',
                onUpdate: 'cascade',
                onDelete: 'cascade',
                allowNull: true,
            },
            token: {
                type: DataTypes.STRING(255),
            },
            expired_at: {
                type: DataTypes.DATE,
            },
            is_one_time: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'accessTokens',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(AccessToken),
                associate: function(models) {
                    AccessToken.belongsTo(models.Identity, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'identity_id',
                            allowNull: false,
                        },
                    });
                    AccessToken.belongsTo(models.Devise, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'devise_id',
                            allowNull: true,
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return AccessToken;
};
