import data_config from '@constants/data_config';
import autobind from 'class-autobind';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var TransferCharge = sequelize.define(
        'TransferCharge',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            ChargeId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'charges',
                    key: 'id',
                },
                field: 'charge_id',
            },
            TransferId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'transfers',
                    key: 'id',
                },
                field: 'transfer_id',
            },
        },
        {
            tableName: 'transferCharges',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(TransferCharge),
                associate: function(models) {
                    TransferCharge.belongsTo(models.Charge);
                    TransferCharge.belongsTo(models.Transfer);
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return TransferCharge;
};
