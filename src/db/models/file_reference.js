import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var FileReference = sequelize.define(
        'FileReference',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            FolderId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'folders',
                    key: 'id',
                },
                field: 'folder_id',
            },
            FileId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'files',
                    key: 'id',
                },
                field: 'file_id',
            },
            index: {
                type: DataTypes.INTEGER,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'fileReferences',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(FileReference),
                associate: function(models) {
                    FileReference.belongsTo(models.Folder, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'folder_id',
                            allowNull: false,
                        },
                    });
                    FileReference.belongsTo(models.File, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'file_id',
                            allowNull: false,
                        },
                    });
                    FileReference.hasMany(models.Stream, {
                        foreignKey: 'streamable_id',
                        constraints: false,
                        scope: {
                            messageable_type: 'FileReference',
                        },
                    });
                    // FileReference.belongsToMany(models.StreamChannel, {
                    //     as: 'StreamChannels',
                    //     through: 'Stream',
                    //     foreignKey: 'streamable_id',
                    //     otherKey: 'channel_id',
                    // });
                },
                index_include: models => [
                    {
                        as: 'File',
                        model: models.File,
                        required: true,
                        where: {
                            permission: true,
                        },
                        include: [
                            {
                                as: 'Member',
                                model: models.Member,
                                required: true,
                            },
                        ],
                    },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
                filing: function(models) {
                    return this.Files.map(file => {
                        file.setValue('index', this.index);
                        return file;
                    });
                },
            },
        }
    );

    return FileReference;
};
