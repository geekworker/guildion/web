const methods = (model) => {
    return {
        build_with: (value = {}, associates = []) => {
            if (value instanceof Array) {
                const instances = model.build(value, { include: associates });
                instances.forEach((instance, i) => {
                    associates.forEach(hash => {
                        const name = !!hash['as'] ? hash['as'] : hash['model'].name;
                        if (value[i][name] && !!value[i][name].dataValues) {
                            instance.setValue(name, hash['model'].build_with(value[i][name].get({ plain: true }), hash['include'] || []));
                        } else {
                            instance.setValue(name, hash['model'].build_with(value[i][name], hash['include'] || []));
                        }
                    })
                })
                return instances;
            } else {
                const instance = model.build(value, { include: associates });
                associates.forEach(hash => {
                    const name = !!hash['as'] ? hash['as'] : hash['model'].name;
                    if (value[name] && !!value[name].dataValues) {
                        instance.setValue(name, hash['model'].build_with(value[name].get({ plain: true }), hash['include'] || []));
                    } else {
                        instance.setValue(name, hash['model'].build_with(value[name], hash['include'] || []));
                    }
                })
                return instance;
            }
        },
    }
}

module.exports = methods;
