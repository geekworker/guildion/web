import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var Charge = sequelize.define(
        'Charge',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            CustomerId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'customers',
                    key: 'id',
                },
                field: 'customer_id',
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            CardId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'paymentCards',
                    key: 'id',
                },
                field: 'card_id',
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            SubscriptionId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'subscriptions',
                    key: 'id',
                },
                field: 'subscription_id',
                onUpdate: 'cascade',
                onDelete: 'cascade',
                allowNull: true,
            },
            ios_product_id: {
                type: DataTypes.STRING(255),
            },
            status: {
                type: DataTypes.STRING(255),
            },
            started_at: {
                type: DataTypes.DATE,
            },
            current_period_start_at: {
                type: DataTypes.DATE,
            },
            trial_start_at: {
                type: DataTypes.DATE,
            },
            trial_end_at: {
                type: DataTypes.DATE,
            },
            paused_at: {
                type: DataTypes.DATE,
            },
            canceled_at: {
                type: DataTypes.DATE,
            },
            resumed_at: {
                type: DataTypes.DATE,
            },
            expired_at: {
                type: DataTypes.DATE,
            },
            platform: {
                type: DataTypes.STRING(255),
            },
            is_in_retry_billing: {
                type: DataTypes.BOOLEAN,
            },
            is_autorenew_enabled: {
                type: DataTypes.BOOLEAN,
            },
            prorate: {
                type: DataTypes.BOOLEAN,
            },
            is_live: {
                type: DataTypes.BOOLEAN,
            },
            permission: {
                type: DataTypes.BOOLEAN,
            },
        },
        {
            tableName: 'charges',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(Charge),
                associate: function(models) {
                    Charge.belongsTo(models.Customer, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'customer_id',
                            allowNull: false,
                        },
                    });
                    Charge.belongsTo(models.PaymentCard, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'card_id',
                            allowNull: false,
                        },
                    });
                    Charge.belongsTo(models.Subscription, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'subscription_id',
                            allowNull: true,
                        },
                    });
                    Charge.hasMany(models.TransferCharge);
                    Charge.belongsToMany(models.Transfer, {
                        through: 'TransferCharge',
                        foreignKey: 'charge_id',
                        otherKey: 'transfer_id',
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Charge;
};
