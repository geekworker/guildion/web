import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var MemberRole = sequelize.define(
        'MemberRole',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            RoleId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'roles',
                    key: 'id',
                },
                field: 'role_id',
            },
            MemberId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'members',
                    key: 'id',
                },
                field: 'member_id',
            },
            position: {
                type: DataTypes.TEXT('long'),
            },
            priority: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'memberRoles',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(MemberRole),
                associate: function(models) {
                    MemberRole.belongsTo(models.Member, {
                        foreignKey: {
                            name: 'member_id',
                            allowNull: false,
                        },
                    });
                    MemberRole.belongsTo(models.Role, {
                        foreignKey: {
                            name: 'role_id',
                            allowNull: false,
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return MemberRole;
};
