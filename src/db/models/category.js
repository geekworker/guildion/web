import data_config from '@constants/data_config';
import autobind from 'class-autobind';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var Category = sequelize.define(
        'Category',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            ja_name: {
                type: DataTypes.TEXT('long'),
            },
            en_name: {
                type: DataTypes.TEXT('long'),
            },
            ja_groupname: {
                type: DataTypes.STRING(255),
            },
            en_groupname: {
                type: DataTypes.STRING(255),
            },
            guild_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'categories',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(Category),
                associate: function(models) {
                    Category.hasMany(models.Guild, {
                        foreignKey: {
                            name: 'category_id',
                            allowNull: true,
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Category;
};
