import data_config from '@constants/data_config';
import autobind from 'class-autobind';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var Customer = sequelize.define(
        'Customer',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            default_card: {
                type: DataTypes.STRING(255),
            },
            email: {
                type: DataTypes.STRING(255),
            },
            description: {
                type: DataTypes.STRING(255),
            },
            is_live: {
                type: DataTypes.BOOLEAN,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
            },
            permission: {
                type: DataTypes.BOOLEAN,
            },
        },
        {
            tableName: 'customers',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(Customer),
                associate: function(models) {
                    Customer.belongsTo(models.User);
                    Customer.hasMany(models.Charge, {
                        foreignKey: {
                            name: 'customer_id',
                            allowNull: true,
                        },
                    });
                    Customer.hasMany(models.PaymentCard, {
                        as: 'Cards',
                        foreignKey: {
                            name: 'customer_id',
                        },
                    });
                    Customer.hasMany(models.Subscription, {
                        as: 'Customer',
                        foreignKey: {
                            name: 'customer_id',
                        },
                    });
                },
                build_with: (value = {}, associate = {}) => {
                    const instance = Customer.build(value);
                    Object.keys(associate).forEach((key) => {
                        instance.setValue(key, associate[key]);
                    });
                    return instance;
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Customer;
};
