import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

const hashPasswordHook = (developer, options, callback) => {
    if (developer.get('password') == '') return callback(null, options);
    bcrypt.hash(developer.get('password'), 10, (err, hash) => {
        if (err) return callback(err);
        developer.set('password_hash', hash);
        developer.set('password', '');
        return callback(null, options);
    });
};

module.exports = function(sequelize, DataTypes) {
    var Developer = sequelize.define(
        'Developer',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            api_key: {
                type: DataTypes.STRING(255),
                allowNull: false,
                defaultValue: uuidv4(),
                unique: true,
            },
            email: {
                type: DataTypes.STRING(255),
                allowNull: false,
                unique: true,
                defaultValue: '',
            },
            token: {
                type: DataTypes.STRING(255),
                defaultValue: '',
            },
            email_is_verified: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            last_attempt_verify_email: {
                type: DataTypes.DATE,
            },
            verify_email_attempts: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            country_code: {
                type: DataTypes.STRING(255),
                defaultValue: '',
            },
            locale: {
                type: DataTypes.STRING(255),
                defaultValue: '',
            },
            username: {
                type: DataTypes.STRING(255),
                unique: true,
            },
            last_attempt_sign_in: {
                type: DataTypes.DATE,
            },
            sign_in_attempts: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            password_hash: {
                type: DataTypes.STRING(255),
            },
            password: {
                type: DataTypes.STRING(255),
                allowNull: true,
            },
            verified: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'developers',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            hooks: {
                beforeCreate: hashPasswordHook,
                beforeUpdate: hashPasswordHook,
            },
            classMethods: {
                // ...classMethods(Developer),
                associate: function(models) {},
            },
            instanceMethods: {
                ...instanceMethods,
                authenticate: async (password, password_hash) => {
                    const isValid = await bcrypt.compare(
                        password,
                        password_hash
                    );
                    return isValid;
                },
            },
        }
    );

    return Developer;
};
