import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Reaction = sequelize.define(
        'Reaction',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            MemberId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'members',
                    key: 'id',
                },
                field: 'member_id',
            },
            MessageId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'messages',
                    key: 'id',
                },
                field: 'message_id',
            },
            data: {
                type: DataTypes.STRING(255),
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'reactions',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Reaction),
                associate: function(models) {
                    Reaction.belongsTo(models.Member, {
                        as: 'Member',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'member_id',
                        },
                    });
                    Reaction.belongsTo(models.Message, {
                        as: 'Message',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'message_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Reaction;
};
