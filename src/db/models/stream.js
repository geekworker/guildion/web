import { Map } from 'immutable';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Stream = sequelize.define(
        'Stream',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            StreamableId: {
                type: DataTypes.INTEGER,
                field: 'streamable_id',
                allowNull: false,
            },
            ChannelId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'streamChannels',
                    key: 'id',
                },
                field: 'channel_id',
            },
            streamable_type: {
                type: DataTypes.STRING(256),
            },
            delivery_start_at: {
                type: DataTypes.DATE,
            },
            delivery_end_at: {
                type: DataTypes.DATE,
            },
            duration: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            is_live: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'streams',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Stream),
                associate: function(models) {
                    Stream.belongsTo(models.FileReference, {
                        foreignKey: 'streamable_id',
                        constraints: false,
                    });
                    Stream.belongsTo(models.File, {
                        foreignKey: 'streamable_id',
                        constraints: false,
                    });
                    Stream.belongsTo(models.StreamChannel, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'channel_id',
                            allowNull: false,
                        },
                    });
                    Stream.hasMany(models.Participant, {
                        foreignKey: {
                            name: 'stream_id',
                            allowNull: false,
                        },
                    });
                    Stream.belongsToMany(models.Member, {
                        as: 'JoinMembers',
                        through: 'Participant',
                        foreignKey: 'stream_id',
                        otherKey: 'member_id',
                    });
                    Stream.hasMany(models.Notification, {
                        foreignKey: 'notificatable_id',
                        constraints: false,
                        scope: {
                            messageable_type: 'Stream',
                        },
                    });
                    Stream.hasMany(models.Channelog, {
                        foreignKey: 'channelogable_id',
                        constraints: false,
                        scope: {
                            messageable_type: 'Stream',
                        },
                    });
                },

                index_include: models => [
                    {
                        model: models.Participant,
                        required: false,
                        where: {
                            is_live: true,
                        },
                        include: [
                            {
                                model: models.Member,
                                include: [
                                    {
                                        model: models.User,
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        model: models.File,
                        include: [
                            {
                                model: models.Member,
                                include: [
                                    {
                                        model: models.User,
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        model: models.FileReference,
                        include: [
                            {
                                model: models.Folder,
                            },
                            {
                                model: models.File,
                                include: [
                                    {
                                        model: models.Member,
                                        include: [
                                            {
                                                model: models.User,
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
                show_include: models => [
                    {
                        model: models.File,
                        include: [
                            {
                                model: models.Member,
                                include: [
                                    {
                                        model: models.User,
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        model: models.FileReference,
                        include: [
                            {
                                model: models.Folder,
                            },
                            {
                                model: models.File,
                                include: [
                                    {
                                        model: models.Member,
                                        include: [
                                            {
                                                model: models.User,
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
                setIncludeMapValue: () => {
                    if (!!this['$options'].includeMap) {
                        Object.keys(this['$options'].includeMap).map(name => {
                            this.setValue.bind(
                                name,
                                this['$options'].includeMap
                            );
                        });
                    }
                },
                setStreamable: () => {
                    if (
                        this.streamable_type === 'File' &&
                        this.File !== undefined
                    ) {
                        this.setValue('Streamable', this.File);
                    } else if (
                        this.streamable_type === 'FileReference' &&
                        this.FileReference !== undefined
                    ) {
                        this.setValue('Streamable', this.FileReference);
                    }
                    delete this.File;
                    delete this.dataValues.File;
                    delete this.FileReference;
                    delete this.dataValues.FileReference;
                },
            },
        }
    );

    Stream.addHook('afterFind', findResult => {
        if (!findResult) return;
        if (!Array.isArray(findResult)) findResult = [findResult];
        for (const instance of findResult) {
            // instance.setStreamable.bind()
            if (
                instance.streamable_type === 'File' &&
                instance.File !== undefined
            ) {
                instance.setValue('Streamable', instance.File);
            } else if (
                instance.streamable_type === 'FileReference' &&
                instance.FileReference !== undefined
            ) {
                instance.setValue('Streamable', instance.FileReference);
            }
            delete instance.File;
            delete instance.dataValues.File;
            delete instance.FileReference;
            delete instance.dataValues.FileReference;
        }
    });

    return Stream;
};
