import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var Tag = sequelize.define(
        'Tag',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.TEXT('long'),
            },
            locale: {
                type: DataTypes.STRING(255),
            },
            country_code: {
                type: DataTypes.STRING(255),
            },
            guild_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            is_nsfw: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'tags',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(Tag),
                associate: function(models) {
                    Tag.hasMany(models.GuildTag, {
                        foreignKey: {
                            name: 'tag_id',
                            allowNull: false,
                        },
                    });
                    Tag.belongsToMany(models.Guild, {
                        as: 'Guilds',
                        through: 'GuildTag',
                        foreignKey: 'tag_id',
                        otherKey: 'guild_id',
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Tag;
};
