import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var Transfer = sequelize.define(
        'Transfer',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            amount: {
                type: DataTypes.INTEGER,
            },
            currency: {
                type: DataTypes.STRING(255),
            },
            status: {
                type: DataTypes.STRING(255),
            },
            description: {
                type: DataTypes.STRING(255),
            },
            scheduled_at: {
                type: DataTypes.DATE,
            },
            term_start_at: {
                type: DataTypes.DATE,
            },
            term_end_at: {
                type: DataTypes.DATE,
            },
            transfer_amount: {
                type: DataTypes.INTEGER,
            },
            transfer_at: {
                type: DataTypes.DATE,
            },
            transfer_amount: {
                type: DataTypes.INTEGER,
            },
            carried_balance: {
                type: DataTypes.INTEGER,
            },
            charge_count: {
                type: DataTypes.INTEGER,
            },
            charge_fee: {
                type: DataTypes.INTEGER,
            },
            charge_gross: {
                type: DataTypes.INTEGER,
            },
            net: {
                type: DataTypes.INTEGER,
            },
            refund_amount: {
                type: DataTypes.INTEGER,
            },
            refund_count: {
                type: DataTypes.INTEGER,
            },
            is_live: {
                type: DataTypes.BOOLEAN,
            },
            permission: {
                type: DataTypes.BOOLEAN,
            },
        },
        {
            tableName: 'transfers',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(Transfer),
                associate: function(models) {
                    Transfer.hasMany(models.Charge);
                    Transfer.hasMany(models.TransferCharge);
                    Transfer.belongsToMany(models.Charge, {
                        through: 'TransferCharge',
                        foreignKey: 'transfer_id',
                        otherKey: 'charge_id',
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Transfer;
};
