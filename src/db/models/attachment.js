import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Attachment = sequelize.define(
        'Attachment',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            MessageId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'messages',
                    key: 'id',
                },
                field: 'message_id',
            },
            FileId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'files',
                    key: 'id',
                },
                field: 'file_id',
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'attachments',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Attachment),
                associate: function(models) {
                    Attachment.belongsTo(models.Message, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'message_id',
                        },
                    });
                    Attachment.belongsTo(models.File, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'file_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Attachment;
};
