import data_config from '@constants/data_config';
import autobind from 'class-autobind';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var PaymentCard = sequelize.define(
        'PaymentCard',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            CustomerId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'customers',
                    key: 'id',
                },
                field: 'customer_id',
            },
            name: {
                type: DataTypes.STRING(255),
            },
            last4: {
                type: DataTypes.STRING(255),
            },
            exp_month: {
                type: DataTypes.INTEGER,
            },
            exp_year: {
                type: DataTypes.INTEGER,
            },
            brand: {
                type: DataTypes.STRING(255),
            },
            cvc_check: {
                type: DataTypes.STRING(255),
            },
            fingerprint: {
                type: DataTypes.STRING(255),
            },
            address_state: {
                type: DataTypes.STRING(255),
            },
            address_city: {
                type: DataTypes.STRING(255),
            },
            address_line1: {
                type: DataTypes.STRING(255),
            },
            address_line2: {
                type: DataTypes.STRING(255),
            },
            address_zip: {
                type: DataTypes.STRING(255),
            },
            address_zip_check: {
                type: DataTypes.STRING(255),
            },
            country: {
                type: DataTypes.STRING(255),
            },
            is_private: {
                type: DataTypes.BOOLEAN,
            },
            permission: {
                type: DataTypes.BOOLEAN,
            },
        },
        {
            tableName: 'paymentCards',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(PaymentCard),
                associate: function(models) {
                    PaymentCard.belongsTo(models.Customer, {
                        as: 'Cards',
                        foreignKey: 'card_id',
                    });
                    PaymentCard.hasMany(models.Charge, {
                        foreignKey: {
                            name: 'card_id',
                        },
                    });
                    PaymentCard.hasMany(models.PaymentCardToken, {
                        foreignKey: {
                            name: 'card_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return PaymentCard;
};
