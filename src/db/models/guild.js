import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Guild = sequelize.define(
        'Guild',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            CategoryId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'categories',
                    key: 'id',
                },
                field: 'category_id',
                allowNull: true,
            },
            OwnerId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'owner_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.STRING(255),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            picture_small: {
                type: DataTypes.STRING(255),
            },
            picture_large: {
                type: DataTypes.STRING(255),
            },
            active_count: {
                type: DataTypes.INTEGER,
            },
            member_count: {
                type: DataTypes.INTEGER,
            },
            channel_count: {
                type: DataTypes.INTEGER,
            },
            file_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            size_byte: {
                type: DataTypes.INTEGER,
            },
            stream_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            movie_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            folder_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            playlist_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            role_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            locale: {
                type: DataTypes.STRING(255),
            },
            country_code: {
                type: DataTypes.STRING(255),
            },
            is_nsfw: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_public: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            bumped_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'guilds',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Guild),
                associate: function(models) {
                    Guild.belongsTo(models.User, {
                        onDelete: 'CASCADE',
                        as: 'Owner',
                        foreignKey: {
                            name: 'owner_id',
                            allowNull: false,
                        },
                    });
                    Guild.belongsTo(models.Category, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'category_id',
                            allowNull: true,
                        },
                    });
                    Guild.hasMany(models.GuildTag, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.belongsToMany(models.Tag, {
                        as: 'Tags',
                        through: 'GuildTag',
                        foreignKey: 'guild_id',
                        otherKey: 'tag_id',
                    });
                    Guild.hasMany(models.StreamChannel, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.hasMany(models.TextChannel, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.hasMany(models.Folder, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.hasMany(models.Member, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.hasMany(models.MemberRequest, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.belongsToMany(models.User, {
                        as: 'MemberRequesters',
                        through: 'MemberRequest',
                        foreignKey: 'guild_id',
                        otherKey: 'voter_id',
                    });
                    Guild.hasMany(models.SectionChannel, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.hasMany(models.DMChannel, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.hasMany(models.MemberRequest, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.belongsToMany(models.User, {
                        as: 'Users',
                        through: 'Member',
                        foreignKey: 'guild_id',
                        otherKey: 'user_id',
                    });
                    Guild.belongsToMany(models.User, {
                        as: 'MemberRequesters',
                        through: 'MemberRequest',
                        foreignKey: 'guild_id',
                        otherKey: 'voter_id',
                    });
                    Guild.hasMany(models.GuildBlock, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.belongsToMany(models.User, {
                        as: 'Blocks',
                        through: 'GuildBlock',
                        foreignKey: 'guild_id',
                        otherKey: 'user_id',
                    });
                    Guild.hasMany(models.GuildReport, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Guild.belongsToMany(models.User, {
                        as: 'Reports',
                        through: 'GuildReport',
                        foreignKey: 'guild_id',
                        otherKey: 'user_id',
                    });
                    Guild.hasMany(models.Role, {
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                },
                index_include: models => [
                    {
                        as: 'Tags',
                        model: models.Tag,
                        through: { attributes: [] },
                        required: false,
                    },
                    {
                        as: 'Category',
                        model: models.Category,
                        required: false,
                    },
                    {
                        as: 'Owner',
                        model: models.User,
                    },
                ],
                show_include: (models, { user_id }) => [
                    {
                        as: 'Owner',
                        model: models.User,
                    },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
                sectionify: function(models) {
                    var EmptyDMChannels = this.DMChannels
                        ? this.DMChannels.filter(e => e.SectionId == null)
                        : [];
                    var EmptyStreamChannels = this.StreamChannels
                        ? this.StreamChannels.filter(e => e.SectionId == null)
                        : [];
                    var EmptyTextChannels = this.TextChannels
                        ? this.TextChannels.filter(e => e.SectionId == null)
                        : [];

                    if (
                        EmptyDMChannels.length == 0 &&
                        EmptyStreamChannels.length == 0 &&
                        EmptyTextChannels.length == 0
                    ) {
                        return;
                    }

                    const EmptySection = models.SectionChannel.build();
                    EmptySection.setValue('DMChannels', EmptyDMChannels);
                    EmptySection.setValue(
                        'StreamChannels',
                        EmptyStreamChannels
                    );
                    EmptySection.setValue('TextChannels', EmptyTextChannels);
                    EmptySection.setDataValue('is_empty', true);

                    if (this.SectionChannels) {
                        let sections = this.SectionChannels.map(s => {
                            s.setDataValue('is_empty', false);
                            return s;
                        });
                        sections.unshift(EmptySection);
                        this.setDataValue('SectionChannels', sections);
                    } else {
                        this.setValue('SectionChannels', [EmptySection]);
                    }
                    this.removeValue('DMChannels');
                    this.removeValue('StreamChannels');
                    this.removeValue('TextChannels');
                },
            },
        }
    );

    return Guild;
};
