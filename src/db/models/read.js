import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Read = sequelize.define(
        'Read',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            MessageId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'messages',
                    key: 'id',
                },
                field: 'message_id',
            },
            MemberId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'members',
                    key: 'id',
                },
                field: 'member_id',
            },
            complete: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'reads',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Read),
                associate: function(models) {
                    Read.belongsTo(models.Member, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'member_id',
                            allowNull: false,
                        },
                    });
                    Read.belongsTo(models.Message, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'message_id',
                            allowNull: false,
                        },
                    });
                },
                guild_show_include: (models, { user_id, member_id }) => [
                    {
                        model: models.Message,
                    },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
                messagify: function(models) {
                    const message = this.Message || this.dataValues.Message;
                    if (!message) return;
                    message.setValue('Reads', [this]);
                    this.removeValue('Message');
                    return message;
                },
            },
        }
    );

    return Read;
};
