import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Member = sequelize.define(
        'Member',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                field: 'guild_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            nickname: {
                type: DataTypes.STRING(255),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            picture_small: {
                type: DataTypes.STRING(255),
            },
            picture_large: {
                type: DataTypes.STRING(255),
            },
            index: {
                type: DataTypes.INTEGER,
            },
            mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            messages_mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            mentions_mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            everyone_mentions_mute: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            message_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            stream_duration: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            file_movie_stream_duration: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            size_byte: {
                type: DataTypes.INTEGER,
            },
            file_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            movie_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            folder_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            playlist_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            role_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            temporary: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'members',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Member),
                associate: function(models) {
                    Member.belongsTo(models.User, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'user_id',
                            allowNull: false,
                        },
                    });
                    Member.belongsTo(models.Guild, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Member.hasMany(models.Message, {
                        foreignKey: {
                            name: 'sender_id',
                            allowNull: true,
                        },
                    });
                    Member.hasMany(models.File, {
                        foreignKey: {
                            name: 'member_id',
                            allowNull: false,
                        },
                    });
                    Member.hasMany(models.Read, {
                        foreignKey: {
                            name: 'member_id',
                        },
                    });
                    Member.belongsToMany(models.Message, {
                        as: 'Reads',
                        through: 'Read',
                        foreignKey: 'member_id',
                        otherKey: 'message_id',
                    });
                    Member.hasMany(models.Reaction, {
                        foreignKey: {
                            name: 'member_id',
                        },
                    });
                    Member.belongsToMany(models.Message, {
                        as: 'ReactionMessages',
                        through: 'Reaction',
                        foreignKey: 'member_id',
                        otherKey: 'message_id',
                    });
                    Member.hasMany(models.Participant, {
                        foreignKey: {
                            name: 'member_id',
                            allowNull: false,
                        },
                    });
                    Member.belongsToMany(models.Stream, {
                        as: 'JoinStreams',
                        through: 'Particiant',
                        foreignKey: 'member_id',
                        otherKey: 'stream_id',
                    });
                    Member.hasMany(models.StreamChannelEntry, {
                        foreignKey: {
                            name: 'member_id',
                        },
                    });
                    Member.belongsToMany(models.StreamChannel, {
                        as: 'StreamChannels',
                        through: 'StreamChannelEntry',
                        foreignKey: 'member_id',
                        otherKey: 'channel_id',
                    });
                    Member.hasMany(models.DMChannelEntry, {
                        foreignKey: {
                            name: 'member_id',
                        },
                    });
                    Member.belongsToMany(models.DMChannel, {
                        as: 'DMChannels',
                        through: 'DMChannelEntry',
                        foreignKey: 'member_id',
                        otherKey: 'channel_id',
                    });
                    Member.hasMany(models.TextChannelEntry, {
                        foreignKey: {
                            name: 'member_id',
                        },
                    });
                    Member.belongsToMany(models.TextChannel, {
                        as: 'TextChannels',
                        through: 'TextChannelEntry',
                        foreignKey: 'member_id',
                        otherKey: 'channel_id',
                    });
                    Member.hasMany(models.MemberRole, {
                        foreignKey: {
                            name: 'member_id',
                        },
                    });
                    Member.belongsToMany(models.Role, {
                        as: 'Roles',
                        through: 'MemberRole',
                        foreignKey: 'member_id',
                        otherKey: 'role_id',
                    });
                },
                index_include: models => [
                    { model: models.TextChannelEntry },
                    { model: models.DMChannelEntry },
                    { model: models.StreamChannelEntry },
                ],
                show_include: models => [
                    {
                        as: 'Guild',
                        model: models.Guild,
                        required: true,
                    },
                    {
                        as: 'User',
                        model: models.User,
                        required: true,
                        include: [
                            {
                                as: 'Guilds',
                                model: models.Guild,
                                required: false,
                            },
                        ],
                    },
                    { model: models.TextChannelEntry },
                    { model: models.DMChannelEntry },
                    { model: models.StreamChannelEntry },
                ],
                guild_show_include: (models, { user_id }) => [
                    {
                        model: models.User,
                    },
                    {
                        model: models.MemberRole,
                        include: [
                            {
                                model: models.Role,
                            },
                        ],
                    },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Member;
};
