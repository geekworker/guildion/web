import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Role = sequelize.define(
        'Role',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                field: 'guild_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            name: {
                type: DataTypes.STRING,
            },
            color: {
                type: DataTypes.STRING,
            },
            priority: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            is_default: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_everyone: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            mentionable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            admin: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            guild_managable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            roles_managable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            channels_managable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            channels_invitable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            kickable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            banable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            member_acceptable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            member_managable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            members_managable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            channels_viewable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            message_sendable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            messages_managable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            message_embeddable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            message_attachable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            messages_readable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            message_mentionable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            message_reactionable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_connectable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_speekable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_livestreamable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            movie_selectable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_mutable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_deafenable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            stream_movable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            folders_viewable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            folder_creatable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            folders_managable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            files_viewable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            file_creatable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            files_managable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            files_downloadable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'roles',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Role),
                associate: function(models) {
                    Role.belongsTo(models.Guild, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'guild_id',
                            allowNull: false,
                        },
                    });
                    Role.hasMany(models.MemberRole, {
                        foreignKey: {
                            name: 'role_id',
                        },
                    });
                    Role.belongsToMany(models.Member, {
                        as: 'Members',
                        through: 'MemberRole',
                        otherKey: 'member_id',
                        foreignKey: 'role_id',
                    });
                    Role.hasMany(models.StreamChannelRole, {
                        foreignKey: {
                            name: 'role_id',
                        },
                    });
                    Role.belongsToMany(models.StreamChannel, {
                        as: 'StreamChannels',
                        through: 'StreamChannelRole',
                        foreignKey: 'role_id',
                        otherKey: 'channel_id',
                    });
                    Role.hasMany(models.DMChannelRole, {
                        foreignKey: {
                            name: 'role_id',
                        },
                    });
                    Role.belongsToMany(models.DMChannel, {
                        as: 'DMChannels',
                        through: 'DMChannelRole',
                        foreignKey: 'role_id',
                        otherKey: 'channel_id',
                    });
                    Role.hasMany(models.TextChannelRole, {
                        foreignKey: {
                            name: 'role_id',
                        },
                    });
                    Role.belongsToMany(models.TextChannel, {
                        as: 'TextChannels',
                        through: 'TextChannelRole',
                        foreignKey: 'role_id',
                        otherKey: 'channel_id',
                    });
                },
                show_include: models => [
                    {
                        model: models.MemberRole,
                        include: [
                            {
                                model: models.Member,
                                include: [
                                    {
                                        model: models.User,
                                    },
                                ],
                            },
                        ],
                    },
                ],
                generate_uid: () => {
                    return (
                        String.prototype.string_to_utf8_hex_string('Ro') +
                        String.prototype.getUniqueString()
                    );
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Role;
};
