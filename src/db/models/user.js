import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const data_config = require('@constants/data_config');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define(
        'User',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            nickname: {
                type: DataTypes.TEXT('long'),
            },
            username: {
                type: DataTypes.STRING(126),
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
            picture_small: {
                type: DataTypes.STRING(255),
            },
            picture_large: {
                type: DataTypes.STRING(255),
            },
            locale: {
                type: DataTypes.STRING(255),
            },
            country_code: {
                type: DataTypes.STRING(255),
            },
            timezone: {
                type: DataTypes.STRING(255),
            },
            size_byte: {
                type: DataTypes.INTEGER,
            },
            admin: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            guild_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            message_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            file_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            movie_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            folder_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            playlist_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            role_count: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            stream_duration: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            file_movie_stream_duration: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            verified: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            active: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            actived_at: {
                type: DataTypes.DATE,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'users',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(User),
                associate: function(models) {
                    User.hasOne(models.Identity);
                    User.hasOne(models.Notification);
                    User.hasOne(models.Customer);
                    User.belongsToMany(models.Devise, {
                        as: 'Devises',
                        through: 'UserDevise',
                        foreignKey: 'user_id',
                        otherKey: 'devise_id',
                    });
                    User.hasMany(models.Guild, {
                        as: 'OwnerGuilds',
                        foreignKey: {
                            name: 'owner_id',
                            allowNull: false,
                        },
                    });
                    User.hasMany(models.Subscription, {
                        foreignKey: {
                            name: 'user_id',
                            allowNull: true,
                        },
                    });
                    User.hasMany(models.Member, {
                        foreignKey: {
                            name: 'user_id',
                            allowNull: false,
                        },
                    });
                    User.hasMany(models.MemberRequest, {
                        foreignKey: {
                            name: 'voter_id',
                            allowNull: false,
                        },
                    });
                    User.belongsToMany(models.Guild, {
                        as: 'MemberRequests',
                        through: 'MemberRequest',
                        foreignKey: 'voter_id',
                        otherKey: 'guild_id',
                    });
                    User.belongsToMany(models.Guild, {
                        as: 'Guilds',
                        through: 'Member',
                        foreignKey: 'user_id',
                        otherKey: 'guild_id',
                    });
                    User.hasMany(models.UserBlock, {
                        foreignKey: {
                            name: 'user_id',
                            allowNull: false,
                        },
                    });
                    User.hasMany(models.UserBlock, {
                        foreignKey: {
                            name: 'receiver_id',
                            allowNull: false,
                        },
                    });
                    User.hasMany(models.UserMute, {
                        foreignKey: {
                            name: 'user_id',
                            allowNull: false,
                        },
                    });
                    User.hasMany(models.UserMute, {
                        foreignKey: {
                            name: 'receiver_id',
                            allowNull: false,
                        },
                    });
                    User.hasMany(models.UserReport, {
                        foreignKey: {
                            name: 'user_id',
                            allowNull: false,
                        },
                    });
                    User.hasMany(models.UserReport, {
                        foreignKey: {
                            name: 'receiver_id',
                            allowNull: false,
                        },
                    });
                    User.belongsToMany(models.User, {
                        as: 'Blocks',
                        through: 'UserBlock',
                        foreignKey: 'user_id',
                        otherKey: 'receiver_id',
                    });
                    User.belongsToMany(models.User, {
                        as: 'Blockers',
                        through: 'UserBlock',
                        foreignKey: 'receiver_id',
                        otherKey: 'user_id',
                    });
                    User.belongsToMany(models.User, {
                        as: 'Reports',
                        through: 'UserReport',
                        foreignKey: 'user_id',
                        otherKey: 'receiver_id',
                    });
                    User.belongsToMany(models.User, {
                        as: 'Reporters',
                        through: 'UserReport',
                        foreignKey: 'receiver_id',
                        otherKey: 'user_id',
                    });
                    User.belongsToMany(models.User, {
                        as: 'Mutes',
                        through: 'UserMute',
                        foreignKey: 'user_id',
                        otherKey: 'receiver_id',
                    });
                    User.belongsToMany(models.User, {
                        as: 'Muters',
                        through: 'UserMute',
                        foreignKey: 'receiver_id',
                        otherKey: 'user_id',
                    });
                    User.belongsToMany(models.Guild, {
                        as: 'GuildBlockers',
                        through: 'GuildBlock',
                        foreignKey: 'user_id',
                        otherKey: 'guild_id',
                    });
                    User.hasMany(models.GuildBlock, {
                        foreignKey: {
                            name: 'user_id',
                            allowNull: false,
                        },
                    });
                    User.belongsToMany(models.Guild, {
                        as: 'GuildReporters',
                        through: 'GuildReport',
                        foreignKey: 'user_id',
                        otherKey: 'guild_id',
                    });
                    User.hasMany(models.GuildReport, {
                        foreignKey: {
                            name: 'user_id',
                            allowNull: false,
                        },
                    });
                },
                block_include: models => [
                    {
                        as: 'Reports',
                        model: models.User,
                        through: { attributes: [] },
                        required: false,
                    },
                    {
                        as: 'Reporters',
                        model: models.User,
                        through: { attributes: [] },
                        required: false,
                    },
                    {
                        as: 'Mutes',
                        model: models.User,
                        through: { attributes: [] },
                        required: false,
                    },
                    {
                        as: 'Muters',
                        model: models.User,
                        through: { attributes: [] },
                        required: false,
                    },
                    {
                        as: 'Blocks',
                        model: models.User,
                        through: { attributes: [] },
                        required: false,
                    },
                    {
                        as: 'Blockers',
                        model: models.User,
                        through: { attributes: [] },
                        required: false,
                    },
                    {
                        as: 'GuildBlockers',
                        model: models.Guild,
                        through: { attributes: [] },
                        required: false,
                    },
                ],
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return User;
};
