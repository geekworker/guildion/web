import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const data_config = require('@constants/data_config');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var GuildReport = sequelize.define(
        'GuildReport',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                field: 'guild_id',
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
        },
        {
            tableName: 'guildReports',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(GuildReport),
                associate: function(models) {
                    GuildReport.belongsTo(models.User, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'user_id',
                        },
                    });
                    GuildReport.belongsTo(models.Guild, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'guild_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return GuildReport;
};
