import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var Notification = sequelize.define(
        'Notification',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            NotificatableId: {
                type: DataTypes.INTEGER,
                field: 'notificatable_id',
                allowNull: false,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            notificatable_type: {
                type: DataTypes.STRING(256),
            },
            template: {
                type: DataTypes.STRING(255),
            },
            url: {
                type: DataTypes.STRING(255),
            },
            is_checked: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'notifications',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(Notification),
                associate: function(models) {
                    Notification.belongsTo(models.User, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            name: 'user_id',
                            allowNull: false,
                        },
                    });
                    Notification.belongsTo(models.Message, {
                        foreignKey: 'notificatable_id',
                        constraints: false,
                    });
                    Notification.belongsTo(models.Stream, {
                        foreignKey: 'notificatable_id',
                        constraints: false,
                    });
                    Notification.belongsTo(models.MemberRequest, {
                        foreignKey: 'notificatable_id',
                        constraints: false,
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    Notification.addHook("afterFind", findResult => {
        if (!findResult) return;
        if (!Array.isArray(findResult)) findResult = [findResult];
        for (const instance of findResult) {
            if (instance.notificatable_type === "Message" && instance.Message !== undefined) {
                instance.setValue('Notificatable', instance.Message)
            } else if (instance.notificatable_type === "Stream" && instance.Stream !== undefined) {
                instance.setValue('Notificatable', instance.Stream)
            } else if (instance.notificatable_type === "MemberRequest" && instance.MemberRequest !== undefined) {
                instance.setValue('Notificatable', instance.MemberRequest)
            }
            delete instance.Message;
            delete instance.dataValues.Message;
            delete instance.Stream;
            delete instance.dataValues.Stream;
            delete instance.MemberRequest;
            delete instance.dataValues.MemberRequest;
        }
    });

    return Notification;
};
