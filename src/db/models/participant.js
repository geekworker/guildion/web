import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';

module.exports = function(sequelize, DataTypes) {
    var Participant = sequelize.define(
        'Participant',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            MemberId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'members',
                    key: 'id',
                },
                field: 'member_id',
            },
            StreamId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'streams',
                    key: 'id',
                },
                field: 'stream_id',
            },
            delivery_start_at: {
                type: DataTypes.DATE,
            },
            delivery_end_at: {
                type: DataTypes.DATE,
            },
            camera_enable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            mic_enable: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_live: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            is_private: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'participants',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Participant),
                associate: function(models) {
                    Participant.belongsTo(models.Stream, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'stream_id',
                        },
                    });
                    Participant.belongsTo(models.Member, {
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'member_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return Participant;
};
