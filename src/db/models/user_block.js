import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const data_config = require('@constants/data_config');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var UserBlock = sequelize.define(
        'UserBlock',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            ReceiverId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'receiver_id',
            },
        },
        {
            tableName: 'userBlocks',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(UserBlock),
                associate: function(models) {
                    UserBlock.belongsTo(models.User, {
                        as: 'Blocks',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'user_id',
                        },
                    });
                    UserBlock.belongsTo(models.User, {
                        as: 'Blockers',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'receiver_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return UserBlock;
};
