import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const data_config = require('@constants/data_config');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var UserMute = sequelize.define(
        'UserMute',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            ReceiverId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'receiver_id',
            },
        },
        {
            tableName: 'userMutes',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(UserMute),
                associate: function(models) {
                    UserMute.belongsTo(models.User, {
                        as: 'Mutes',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'user_id',
                        },
                    });
                    UserMute.belongsTo(models.User, {
                        as: 'Muters',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'receiver_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return UserMute;
};
