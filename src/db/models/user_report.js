import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const data_config = require('@constants/data_config');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var UserReport = sequelize.define(
        'UserReport',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            ReceiverId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'receiver_id',
            },
            description: {
                type: DataTypes.TEXT('long'),
            },
        },
        {
            tableName: 'userReports',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(UserReport),
                associate: function(models) {
                    UserReport.belongsTo(models.User, {
                        as: 'Reports',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'user_id',
                        },
                    });
                    UserReport.belongsTo(models.User, {
                        as: 'Reporters',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'receiver_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return UserReport;
};
