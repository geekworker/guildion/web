import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
import { available_locales, available_locale_labels } from '@constants/locale_config';
import { getLocale, fallbackLocale } from '@locales';

module.exports = function(sequelize, DataTypes) {
    var DocumentSection = sequelize.define(
        'DocumentSection',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            SectionId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'documentSections',
                    key: 'id',
                },
                field: 'section_id',
                allowNull: true,
            },
            GroupId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'documentGroups',
                    key: 'id',
                },
                field: 'group_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            ja_title: {
                type: DataTypes.TEXT('long'),
            },
            en_title: {
                type: DataTypes.TEXT('long'),
            },
            template: {
                type: DataTypes.STRING(255),
            },
            index: {
                type: DataTypes.INTEGER,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'documentSections',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(DocumentSection),
                associate: function(models) {
                    DocumentSection.belongsTo(models.DocumentGroup, {
                        onDelete: 'CASCADE',
                        as: 'Group',
                        foreignKey: {
                            name: 'group_id',
                            allowNull: false,
                        },
                    });
                    DocumentSection.belongsTo(models.DocumentSection, {
                        onDelete: 'CASCADE',
                        as: 'Section',
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                    DocumentSection.hasMany(models.DocumentSection, {
                        onDelete: 'CASCADE',
                        as: 'Sections',
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                    DocumentSection.hasMany(models.Document, {
                        onDelete: 'CASCADE',
                        as: 'Documents',
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                },
                getTitle: (model, locale) => {
                    if (!model) return '';
                    switch (getLocale(locale)) {
                    case 'ja': return model.ja_title || model.dataValues.ja_title;
                    default:
                    case 'en': return model.en_title || model.dataValues.en_title;
                    }
                },
            },
            instanceMethods: {
                ...instanceMethods,
                getTitle: (locale) => {
                    switch (getLocale(locale)) {
                    case 'ja': return this.ja_title || this.dataValues.ja_title;
                    default:
                    case 'en': return this.en_title || this.dataValues.en_title;
                    }
                },
            },
        }
    );

    return DocumentSection;
};
