import { Map } from 'immutable';
import data_config from '@constants/data_config';
import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
import {
    available_locales,
    available_locale_labels,
} from '@constants/locale_config';
import { getLocale, fallbackLocale } from '@locales';

module.exports = function(sequelize, DataTypes) {
    var Document = sequelize.define(
        'Document',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            SectionId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'documentSections',
                    key: 'id',
                },
                field: 'section_id',
                allowNull: true,
            },
            GroupId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'documentGroups',
                    key: 'id',
                },
                field: 'group_id',
            },
            uid: {
                type: DataTypes.STRING(126),
            },
            ja_title: {
                type: DataTypes.TEXT('long'),
            },
            en_title: {
                type: DataTypes.TEXT('long'),
            },
            ja_html: {
                type: DataTypes.TEXT('long'),
            },
            en_html: {
                type: DataTypes.TEXT('long'),
            },
            ja_thumbnail: {
                type: DataTypes.STRING(255),
            },
            en_thumbnail: {
                type: DataTypes.STRING(255),
            },
            template: {
                type: DataTypes.STRING(255),
            },
            version: {
                type: DataTypes.STRING(255),
            },
            index: {
                type: DataTypes.INTEGER,
            },
            permission: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        {
            tableName: 'documents',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            classMethods: {
                // ...classMethods(Document),
                associate: function(models) {
                    Document.belongsTo(models.DocumentGroup, {
                        onDelete: 'CASCADE',
                        as: 'Group',
                        foreignKey: {
                            name: 'group_id',
                            allowNull: false,
                        },
                    });
                    Document.belongsTo(models.DocumentSection, {
                        onDelete: 'CASCADE',
                        as: 'Section',
                        foreignKey: {
                            name: 'section_id',
                            allowNull: true,
                        },
                    });
                },
                show_include: (models, { groupname }) => [
                    {
                        model: models.DocumentGroup,
                        as: 'Group',
                        where: {
                            groupname,
                        },
                    },
                    {
                        model: models.DocumentSection,
                        as: 'Section',
                    },
                ],
                getTitle: (model, locale) => {
                    if (!model) return '';
                    switch (getLocale(locale)) {
                        case 'ja':
                            return model.ja_title || model.dataValues.ja_title;
                        default:
                        case 'en':
                            return model.en_title || model.dataValues.en_title;
                    }
                },
                getHTML: (model, locale) => {
                    if (!model) return '';
                    switch (getLocale(locale)) {
                        case 'ja':
                            return model.ja_html || model.dataValues.ja_html;
                        default:
                        case 'en':
                            return model.en_html || model.dataValues.en_html;
                    }
                },
                getThumbnail: (model, locale) => {
                    if (!model) return '';
                    switch (getLocale(locale)) {
                        case 'ja':
                            return (
                                model.ja_thumbnail ||
                                model.dataValues.ja_thumbnail
                            );
                        default:
                        case 'en':
                            return (
                                model.en_thumbnail ||
                                model.dataValues.en_thumbnail
                            );
                    }
                },
            },
            instanceMethods: {
                ...instanceMethods,
                getTitle: locale => {
                    switch (getLocale(locale)) {
                        case 'ja':
                            return this.ja_title || this.dataValues.ja_title;
                        default:
                        case 'en':
                            return this.en_title || this.dataValues.en_title;
                    }
                },
                getHTML: locale => {
                    switch (getLocale(locale)) {
                        case 'ja':
                            return this.ja_html || this.dataValues.ja_html;
                        default:
                        case 'en':
                            return this.en_html || this.dataValues.en_html;
                    }
                },
                getThumbnail: locale => {
                    switch (getLocale(locale)) {
                        case 'ja':
                            return (
                                this.ja_thumbnail ||
                                this.dataValues.ja_thumbnail
                            );
                        default:
                        case 'en':
                            return (
                                this.en_thumbnail ||
                                this.dataValues.en_thumbnail
                            );
                    }
                },
            },
        }
    );

    return Document;
};
