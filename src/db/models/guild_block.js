import instanceMethods from '@models/instance_methods';
import classMethods from '@models/class_methods';
const data_config = require('@constants/data_config');
const uuidv4 = require('uuid/v4');

module.exports = function(sequelize, DataTypes) {
    var GuildBlock = sequelize.define(
        'GuildBlock',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            UserId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            GuildId: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'guilds',
                    key: 'id',
                },
                field: 'guild_id',
            },
        },
        {
            tableName: 'guildBlocks',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
            underscored: true,
            charset: 'utf8mb4',
            classMethods: {
                // ...classMethods(GuildBlock),
                associate: function(models) {
                    GuildBlock.belongsTo(models.User, {
                        as: 'GuildBlockers',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'user_id',
                        },
                    });
                    GuildBlock.belongsTo(models.Guild, {
                        as: 'Blocks',
                        onDelete: 'CASCADE',
                        foreignKey: {
                            allowNull: false,
                            name: 'guild_id',
                        },
                    });
                },
            },
            instanceMethods: {
                ...instanceMethods,
            },
        }
    );

    return GuildBlock;
};
