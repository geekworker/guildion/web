const safe2json = require('@extension/safe2json');
const tt = require('counterpart');
const data_config = require('@constants/data_config');
const image_config = require('@constants/image_config');
const Sequelize = require('sequelize');
const sequelize = new Sequelize();
var db = {};

const models = [
    require('@models/access_token')(sequelize, Sequelize),
    require('@models/attachment')(sequelize, Sequelize),
    require('@models/category')(sequelize, Sequelize),
    require('@models/channelog')(sequelize, Sequelize),
    require('@models/charge')(sequelize, Sequelize),
    require('@models/customer')(sequelize, Sequelize),
    require('@models/devise')(sequelize, Sequelize),
    require('@models/document')(sequelize, Sequelize),
    require('@models/document_group')(sequelize, Sequelize),
    require('@models/document_section')(sequelize, Sequelize),
    require('@models/file')(sequelize, Sequelize),
    require('@models/stream_channel')(sequelize, Sequelize),
    require('@models/text_channel')(sequelize, Sequelize),
    require('@models/guild')(sequelize, Sequelize),
    require('@models/guild_tag')(sequelize, Sequelize),
    require('@models/developer')(sequelize, Sequelize),
    require('@models/stream_channel_entry')(sequelize, Sequelize),
    require('@models/text_channel_entry')(sequelize, Sequelize),
    require('@models/dm_channel_entry')(sequelize, Sequelize),
    require('@models/stream_channel_role')(sequelize, Sequelize),
    require('@models/text_channel_role')(sequelize, Sequelize),
    require('@models/dm_channel_role')(sequelize, Sequelize),
    require('@models/identity')(sequelize, Sequelize),
    require('@models/member')(sequelize, Sequelize),
    require('@models/guild_block')(sequelize, Sequelize),
    require('@models/guild_report')(sequelize, Sequelize),
    require('@models/member_request')(sequelize, Sequelize),
    require('@models/member_role')(sequelize, Sequelize),
    require('@models/message')(sequelize, Sequelize),
    require('@models/notification')(sequelize, Sequelize),
    require('@models/participant')(sequelize, Sequelize),
    require('@models/payment_card')(sequelize, Sequelize),
    require('@models/payment_card_token')(sequelize, Sequelize),
    require('@models/plan')(sequelize, Sequelize),
    require('@models/folder')(sequelize, Sequelize),
    require('@models/file_reference')(sequelize, Sequelize),
    require('@models/reaction')(sequelize, Sequelize),
    require('@models/read')(sequelize, Sequelize),
    require('@models/role')(sequelize, Sequelize),
    require('@models/dm_channel')(sequelize, Sequelize),
    require('@models/section_channel')(sequelize, Sequelize),
    require('@models/stream')(sequelize, Sequelize),
    require('@models/subscription')(sequelize, Sequelize),
    require('@models/tag')(sequelize, Sequelize),
    require('@models/transfer')(sequelize, Sequelize),
    require('@models/transfer_charge')(sequelize, Sequelize),
    require('@models/upload')(sequelize, Sequelize),
    require('@models/user')(sequelize, Sequelize),
    require('@models/user_block')(sequelize, Sequelize),
    require('@models/user_devise')(sequelize, Sequelize),
    require('@models/user_mute')(sequelize, Sequelize),
    require('@models/user_report')(sequelize, Sequelize),
];

models.forEach(model => {
    db[model.name] = model;
});

models.forEach(model => {
    if (db[model.name].associate) {
        // console.log('entered', model.name);
        db[model.name].associate(db);
    }
});

const classMethods = require('@models/class_methods');
models.forEach(model => {
    Object.keys(classMethods(model)).forEach(key => {
        model[key] = classMethods(model)[key];
    });
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
