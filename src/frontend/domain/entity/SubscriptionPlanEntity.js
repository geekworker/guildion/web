import Entity from '@entity/Entity';
import { Enum, defineEnum } from '@extension/Enum';
import pathToRegexp from 'path-to-regexp';
import querystring from 'querystring';
import {
    SUBSCRIPTION_SPAN,
    SUBSCRIPTION_PLAN,
    PLAN_FUNCTION,
 } from '@constants/plan_config';

export class SubscriptionPlanEntity extends Entity {
    constructor({
        plan = SUBSCRIPTION_PLAN.VolumeMini,
        span = SUBSCRIPTION_SPAN.Month1,
        plan_function = SUBSCRIPTION_SPAN.Volume,
    }) {
        super();
        this.plan = plan || SUBSCRIPTION_PLAN.VolumeMini;
        this.span = span || SUBSCRIPTION_SPAN.Month1;
        this.plan_function = plan_function;
    }

    get priceString() {
        return this.span.priceString(this.plan)
    }
    get discountString() {
        return this.span.discountString(this.plan)
    }
    get monthPriceString() {
        return this.span.monthPriceString(this.plan)
    }
}
