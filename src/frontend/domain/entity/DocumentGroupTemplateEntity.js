import Entity from '@entity/Entity';
import { Enum, defineEnum } from '@extension/Enum';
import tt from 'counterpart';
import { getOS } from '@network/detection';

export const DOCUMENT_GROUP_TEMPLATE_TYPE = defineEnum({
    Links: {
        rawValue: 0,
        value: 'links',
    },
});
