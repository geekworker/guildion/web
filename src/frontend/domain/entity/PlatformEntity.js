import Entity from '@entity/Entity';
import { Enum, defineEnum } from '@extension/Enum';
import tt from 'counterpart';
import { getOS } from '@network/detection';
import config from '@constants/config';

export const PLATFORM_TYPE = defineEnum({
    Web: {
        rawValue: 0,
        value: 'web',
        translate: () => tt('platform.web'),
        available: false,
        thumbnail:
            config.IMG_MAIN_PROXY +
            '/public/images/brands/en/web_stream-channel.png',
        trim_thumbnail:
            config.IMG_MAIN_PROXY +
            '/public/images/brands/en/web_stream-channel.png',
    },
    iOS: {
        rawValue: 1,
        value: 'ios',
        translate: () => tt('platform.ios'),
        available: true,
        thumbnail:
            config.IMG_MAIN_PROXY +
            '/public/images/brands/en/iPhone-X_stream-channel.png',
        trim_thumbnail:
            config.IMG_MAIN_PROXY +
            '/public/images/brands/en/iPhone-X_stream-channel_trim.png',
    },
    Android: {
        rawValue: 2,
        value: 'android',
        translate: () => tt('platform.android'),
        available: false,
        thumbnail:
            config.IMG_MAIN_PROXY +
            '/public/images/brands/en/android_stream-channel.png',
        trim_thumbnail:
            config.IMG_MAIN_PROXY +
            '/public/images/brands/en/android_stream-channel.png',
    },
    MacBook: {
        rawValue: 3,
        value: 'macbook',
        translate: () => tt('platform.mac'),
        available: false,
        thumbnail:
            config.IMG_MAIN_PROXY +
            '/public/images/brands/en/macbook_stream-channel.png',
        trim_thumbnail:
            config.IMG_MAIN_PROXY +
            '/public/images/brands/en/macbook_stream-channel.png',
    },
});

export const current_platform = () => {
    switch (getOS()) {
        case 'Mac OS':
        case 'MacOS':
            return PLATFORM_TYPE.MacBook;
        case 'iOS':
            return PLATFORM_TYPE.iOS;
        case 'Android':
            return PLATFORM_TYPE.Android;
        default:
            return PLATFORM_TYPE.Web;
    }
};

export const current_platform_string = () => {
    return current_platform().value;
};
