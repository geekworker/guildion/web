import { ClientError, ApiError } from '@extension/Error';
import Entity from '@entity/Entity';
import { Enum, defineEnum } from '@extension/Enum';
import data_config from '@constants/data_config';

export class DocumentOutlineEntity extends Entity {
    static getExtension = str => {
        return /[.]/.exec(str) ? /[^.]+$/.exec(str) : '';
    };

    static outline_tag = 'h2';

    name = '';
    id = '#';

    constructor({
        name,
    }) {
        super();
        this.name = name;
    }
}

export class DocumentOutlineEntities extends Entity {
    constructor({ html }) {
        super();
        this.html = html;
        this.items = String.prototype.extractTagText(html, DocumentOutlineEntity.outline_tag).map(val => new DocumentOutlineEntity({ name: val })) || []
        this.setIDs();
    }

    setIDs() {
        if (!process.env.BROWSER || this.items.length == 0) return;
        const doc = new DOMParser().parseFromString(this.html, "text/html")
        const elements = doc.getElementsByTagName(DocumentOutlineEntity.outline_tag);
        for (var i = 0; i < elements.length; i++) {
            const element = elements[i];
            const item = this.searchByName(element.textContent)
            if (!!item && element.id) {
                item.id = `#${element.id}`
            }
        }
    }

    searchByName(name) {
        return this.items.filter(val => val.name == name).length > 0 && this.items.filter(val => val.name == name)[0]
    }
}
