import RepositoryImpl from '@repository/RepositoryImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@view_models';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class DocumentRepository extends RepositoryImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new DocumentRepository(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async get({ uid, groupname }) {
        const data = await super.apiCall('/api/v1/document', {
            uid,
            groupname,
        });
        return (
            data &&
            models.Document.build_with({ ...data.document }, [
                {
                    model: models.DocumentSection,
                    as: 'Section',
                },
                {
                    model: models.DocumentGroup,
                    as: 'Group',
                },
            ])
        );
    }

    async getGroup({ groupname }) {
        const data = await super.apiCall('/api/v1/document/group', {
            groupname,
        });

        return (
            data &&
            models.DocumentGroup.build_with({ ...data.group }, [
                {
                    model: models.DocumentSection,
                    as: 'Sections',
                    include: [
                        {
                            model: models.Document,
                            as: 'Documents',
                        },
                    ],
                },
            ])
        );
    }

    async update({ document }) {
        const data = await super.apiCall('/api/v1/document/update', {
            document,
        });

        return (
            data &&
            models.Document.build_with({ ...data.document }, [
                {
                    model: models.DocumentSection,
                    as: 'Section',
                },
                {
                    model: models.DocumentGroup,
                    as: 'Group',
                },
            ])
        );
    }
}
