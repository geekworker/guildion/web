import RepositoryImpl from '@repository/RepositoryImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@view_models';
import data_config from '@constants/data_config';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class GuildRepository extends RepositoryImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new GuildRepository(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async getWelcome({ uid }) {
        const data = await super.apiCall('/api/v1/guild/welcome', {
            uid,
        });
        return (
            data &&
            models.Guild.build_with({ ...data.guild }, [
                {
                    as: 'Tags',
                    model: models.Tag,
                },
                {
                    as: 'Category',
                    model: models.Category,
                },
                {
                    as: 'Owner',
                    model: models.User,
                },
                {
                    as: 'MemberRequests',
                    model: models.MemberRequest,
                },
                {
                    as: 'Members',
                    model: models.Member,
                },
            ])
        );
    }

    async getGuildsHero({ limit, offset }) {
        const data = await super.apiCall('/api/v1/guilds/hero', {
            limit: limit || data_config.fetch_data_limit('M'),
            offset: Number(offset || 0),
        });

        return data.guilds.map(guild =>
            models.Guild.build_with(
                {
                    ...guild,
                },
                []
            )
        );
    }

    async getGuildsWelcome({ limit, offset }) {
        const data = await super.apiCall('/api/v1/guilds/welcome', {
            limit: limit || data_config.fetch_data_limit('M'),
            offset: Number(offset || 0),
        });

        return data.guilds.map(guild =>
            models.Guild.build_with(
                {
                    ...guild,
                },
                [
                    {
                        as: 'Tags',
                        model: models.Tag,
                    },
                    {
                        as: 'Category',
                        model: models.Category,
                    },
                    {
                        as: 'Owner',
                        model: models.User,
                    },
                ]
            )
        );
    }
}
