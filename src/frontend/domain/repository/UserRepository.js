import RepositoryImpl from '@repository/RepositoryImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import data_config from '@constants/data_config';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class UserRepository extends RepositoryImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new UserRepository(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async syncNotificationId({ notification_id, current_user }) {
        const data = await super.apiCall('/api/v1/user/notification_id/sync', {
            notification_id,
            user: safe2json(current_user),
        });
        return data && data.user;
    }
}
