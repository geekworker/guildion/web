import { bindActionCreators } from 'redux';
import fetch from 'isomorphic-fetch';
import { ClientError } from '@extension/Error';
import { getEnvLocale } from '@network/detection';

export default class RepositoryImpl {
    constructor() {}

    async apiCall(path, payload, reqType = 'POST') {
        if (!!payload.api_key || !!payload.api_password) {
            throw new ClientError({
                error: new Error('payload is invalid'),
                tt_key: 'errors.invalid_response_from_server',
            });
        }
        payload._locale = getEnvLocale();
        const reqObjs = {
            POST: {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(payload),
            },
            PUT: {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        };
        const response = await fetch(path, reqObjs[reqType]).catch(e => {
            throw e;
        });
        const contentType = response.headers.get('content-type');
        if (!contentType || contentType.indexOf('application/json') === -1) {
            throw new ClientError({
                error: new Error('Invalid response from server'),
                tt_key: 'errors.invalid_response_from_server',
            });
        }
        let responseData = await response.json();
        return responseData;
    }

    async call(path, payload, reqType = 'GET') {
        const reqObjs = {
            GET: {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
            POST: {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(payload),
            },
            PUT: {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        };
        const response = await fetch(path, reqObjs[reqType]).catch(e => {
            throw e;
        });
        const contentType = response.headers.get('content-type');
        if (!contentType || contentType.indexOf('application/json') === -1) {
            throw new ClientError({
                error: new Error('Invalid response from server'),
                tt_key: 'errors.invalid_response_from_server',
            });
        }
        let responseData = await response.json();
        return responseData;
    }
}
