import UseCaseImpl from '@usecase/UseCaseImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import { GuildRepository } from '@repository';
import * as appActions from '@redux/App/AppReducer';
import * as guildActions from '@redux/Guild/GuildReducer';
import { browserHistory } from 'react-router';
import safe2plain from '@extension/safe2plain';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const guildRepository = GuildRepository.instance;

export default class GuildUseCase extends UseCaseImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new GuildUseCase(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    *fetchWelcome({ payload: { uid } }) {
        try {
            if (!uid) return;
            const _guild = yield select(state =>
                state.guild.getIn(['welcome_guilds', uid])
            );
            if (!!_guild) return;
            yield put(appActions.fetchDataBegin());
            const repository = yield guildRepository.getWelcome({ uid });
            if (!repository) return;
            yield put(
                guildActions.setWelcome({ guild: safe2plain(repository) })
            );
        } catch (e) {
            yield put(appActions.addError({ error: e }));
        }
        yield put(appActions.fetchDataEnd());
    }

    *fetchHeros({ payload: {} }) {
        try {
            const _guilds = yield select(state =>
                state.guild.get('hero_guilds_list')
            );
            if (!!_guilds && _guilds.size > 0) return;
            yield put(appActions.fetchDataBegin());
            const repositories = yield guildRepository.getGuildsHero({});
            if (!repositories) return;
            yield put(
                guildActions.setHeros({
                    guilds: repositories.map(repository =>
                        safe2plain(repository)
                    ),
                })
            );
        } catch (e) {
            yield put(appActions.addError({ error: e }));
        }
        yield put(appActions.fetchDataEnd());
    }

    *fetchWelcomes({ payload: {} }) {
        try {
            const _guilds = yield select(state =>
                state.guild.get('welcome_guilds_list')
            );
            if (!!_guilds && _guilds.size > 0) return;
            yield put(appActions.fetchDataBegin());
            const repositories = yield guildRepository.getGuildsWelcome({});
            if (!repositories) return;
            yield put(
                guildActions.setWelcomes({
                    guilds: repositories.map(repository =>
                        safe2plain(repository)
                    ),
                })
            );
        } catch (e) {
            yield put(appActions.addError({ error: e }));
        }
        yield put(appActions.fetchDataEnd());
    }

    *fetchMoreWelcomes({ payload: {} }) {
        try {
            const _guilds = yield select(state =>
                state.guild.get('welcome_guilds_list')
            );
            if (!!_guilds && _guilds.size == 0) return;
            yield put(appActions.fetchDataBegin());
            const repositories = yield guildRepository.getGuildsWelcome({
                offset: _guilds.size,
            });
            if (!repositories) return;
            yield put(
                guildActions.addWelcomes({
                    guilds: repositories.map(repository =>
                        safe2plain(repository)
                    ),
                })
            );
        } catch (e) {
            yield put(appActions.addError({ error: e }));
        }
        yield put(appActions.fetchDataEnd());
    }
}
