import UseCaseImpl from '@usecase/UseCaseImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import { DocumentRepository } from '@repository';
import {
    documentShowRoute,
    documentGroupShowRoute,
    documentEditRoute,
    notfoundRoute,
} from '@infrastructure/RouteInitialize';
import * as appActions from '@redux/App/AppReducer';
import * as documentActions from '@redux/Document/DocumentReducer';
import { browserHistory } from 'react-router';
import safe2plain from '@extension/safe2plain';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const documentRepository = DocumentRepository.instance;

export default class DocumentUseCase extends UseCaseImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new DocumentUseCase(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    *initShow({ payload: { pathname } }) {
        try {
            if (
                !documentShowRoute.isValidPath(pathname) &&
                !documentEditRoute.isValidPath(pathname)
            )
                return;
            const uid = documentShowRoute.isValidPath(pathname)
                ? documentShowRoute.params_value('uid', pathname)
                : documentEditRoute.params_value('uid', pathname);
            const groupname = documentShowRoute.isValidPath(pathname)
                ? documentShowRoute.params_value('groupname', pathname)
                : documentEditRoute.params_value('groupname', pathname);

            if (!uid || !groupname) return;
            const _doc = yield select(state =>
                state.document.getIn(['documents', uid])
            );
            if (!!_doc) return;
            yield put(appActions.fetchDataBegin());
            const repository = yield documentRepository.get({ uid, groupname });
            if (!repository) {
                browserHistory.replace(notfoundRoute.path);
            }
            yield put(
                documentActions.setShow({ document: safe2plain(repository) })
            );
        } catch (e) {
            browserHistory.replace(notfoundRoute.path);
            yield put(appActions.addError({ error: e }));
        }
        yield put(appActions.fetchDataEnd());
    }

    *initGroup({ payload: { pathname } }) {
        try {
            if (
                !documentShowRoute.isValidPath(pathname) &&
                !documentGroupShowRoute.isValidPath(pathname)
            )
                return;
            const groupname = documentShowRoute.isValidPath(pathname)
                ? documentShowRoute.params_value('groupname', pathname)
                : documentGroupShowRoute.params_value('groupname', pathname);
            if (!groupname) return;
            const _group = yield select(state =>
                state.document.getIn(['document_groups', groupname])
            );
            if (!!_group) return;
            yield put(appActions.fetchDataBegin());
            const repository = yield documentRepository.getGroup({ groupname });
            if (!repository) {
                browserHistory.replace(notfoundRoute.path);
            }
            yield put(
                documentActions.setGroup({ group: safe2plain(repository) })
            );
        } catch (e) {
            browserHistory.replace(notfoundRoute.path);
            yield put(appActions.addError({ error: e }));
        }
        yield put(appActions.fetchDataEnd());
    }

    *update({ payload: { document } }) {
        try {
            if (!document) return;
            yield put(appActions.screenLoadingBegin());
            const data = yield documentRepository.update({ document });
        } catch (e) {
            yield put(appActions.addError({ error: e }));
        }
        yield put(appActions.screenLoadingEnd());
    }
}
