import UseCaseImpl from '@usecase/UseCaseImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import UserRepository from '@repository/UserRepository';
import { homeIndexRoute } from '@infrastructure/RouteInitialize';
import { browserHistory } from 'react-router';
import data_config from '@constants/data_config';
import * as appActions from '@redux/App/AppReducer';
import tt from 'counterpart';
import Notification, { getOneSignalWindow } from '@network/notification';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const userRepository = UserRepository.instance;

export default class AppUseCase extends UseCaseImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new AppUseCase(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    *detectNotificationId({ payload: { pathname } }) {
        try {
            // let id,
            //     OneSignal = getOneSignalWindow();
            // if (process.env.BROWSER) {
            //     id = yield OneSignal.getUserId();
            // }
            // if (!id) return;
            // const state = yield select(state =>
            //     state.app.get('notification_id')
            // );
            // yield put(appActions.setNotificationId(id));
            // const current_user = yield select(state =>
            //     authActions.getCurrentUser(state)
            // );
            // if (!current_user) return;
            // if (current_user.notification_id == id) return;
            // yield userRepository.syncNotificationId({
            //     notification_id: id,
            //     current_user,
            // });
            /*.catch(async e => {
                await put(appActions.addError({ error: e }));
            });*/
        } catch (e) {}
    }
}
