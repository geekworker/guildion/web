import { RouteEntity, RouteEntities } from '@entity/RouteEntity';
import data_config from '@constants/data_config';
import config from '@constants/config';
import tt from 'counterpart';
import { available_locales } from '@constants/locale_config';

export const homeIndexRoute = new RouteEntity({
    path: '/',
    page: 'HomeIndex',
    component: require('@components/pages/HomeIndex'),
});

export const welcomeRoute = new RouteEntity({
    path: '/welcome',
    page: 'Welcome',
    component: require('@components/pages/Welcome'),
    localize: true,
});

export const guildsExploreRoute = new RouteEntity({
    path: '/guilds/explore',
    page: 'GuildsExplore',
    component: require('@components/pages/GuildsExplore'),
    localize: true,
});

export const guildWelcomeRoute = new RouteEntity({
    path: '/guild/welcome/:uid',
    page: 'GuildWelcome',
    component: require('@components/pages/GuildWelcome'),
    localize: true,
});

export const documentShowRoute = new RouteEntity({
    path: '/document/:groupname/:uid',
    page: 'DocumentShow',
    component: require('@components/pages/DocumentShow'),
    localize: true,
});

export const documentGroupShowRoute = new RouteEntity({
    path: '/document/:groupname',
    page: 'DocumentGroupShow',
    component: require('@components/pages/DocumentGroupShow'),
    localize: true,
});

export const documentEditRoute = new RouteEntity({
    path: '/document/:groupname/:uid/edit',
    page: 'DocumentEdit',
    component: require('@components/pages/DocumentEdit'),
    localize: true,
    development: true,
});

export const downloadsIndexRoute = new RouteEntity({
    path: '/downloads/:os?',
    page: 'DownloadsIndex',
    component: require('@components/pages/DownloadsIndex'),
    localize: true,
});

export const premiumIndexRoute = new RouteEntity({
    path: '/premium',
    page: 'PremiumIndex',
    component: require('@components/pages/PremiumIndex'),
    localize: true,
});

export const stopMailNotificationRoute = new RouteEntity({
    path: '/user/mail/notification',
    page: 'StopMailNotification',
    component: require('@components/pages/StopMailNotification'),
});

export const notfoundRoute = new RouteEntity({
    path: '/notfound',
    page: 'NotFound',
    component: require('@components/pages/NotFound'),
});

// export const xssRoute = new RouteEntity({
//     path: '/xss/test',
//     page: 'XSSTest',
//     component: require('@components/pages/XSSTest'),
// });

export const routeEntities = new RouteEntities({
    items: [
        notfoundRoute,
        homeIndexRoute,
        welcomeRoute,
        guildWelcomeRoute,
        documentShowRoute,
        documentGroupShowRoute,
        documentEditRoute,
        downloadsIndexRoute,
        premiumIndexRoute,
        stopMailNotificationRoute,
    ],
    notfoundRoute,
    routePath: homeIndexRoute.path,
});

export const getPageTitle = (pathname, state) => {
    available_locales.map(locale => {
        pathname = pathname.replace(`/${locale}`, '');
    });
    const page = routeEntities.resolveRoute(pathname).page;
    let title = tt(`pages.${page}`, { fallback: tt('pages.default') });

    if (page == guildWelcomeRoute.page) {
        const uid = guildWelcomeRoute.params_value('uid', pathname);
        if (
            uid &&
            !!state.guild.get('welcome_guilds') &&
            !!state.guild.getIn(['welcome_guilds', uid])
        ) {
            const _guild = state.guild.getIn(['welcome_guilds', uid]);
            const guild = _guild.toJS();
            title = tt(`pages.${guildWelcomeRoute.page}`, {
                data: guild.name,
            });
        }
    }
    return title + ' | ' + config.APP_NAME;
};

export const getPageDescription = (pathname, state) => {
    available_locales.map(locale => {
        pathname = pathname.replace(`/${locale}`, '');
    });
    const page = routeEntities.resolveRoute(pathname).page;
    let description = tt(`descriptions.${page}`, {
        fallback: tt('pages.default'),
    });
    if (page == guildWelcomeRoute.page) {
        const uid = guildWelcomeRoute.params_value('uid', pathname);
        if (
            uid &&
            !!state.guild.get('welcome_guilds') &&
            !!state.guild.getIn(['welcome_guilds', uid])
        ) {
            const _guild = state.guild.getIn(['welcome_guilds', uid]);
            const guild = _guild.toJS();
            description = tt(`descriptions.${guildWelcomeRoute.page}`, {
                data: guild.description,
            });
        }
    }
    return description;
};

export const getPageImage = pathname => {
    available_locales.map(locale => {
        pathname = pathname.replace(`/${locale}`, '');
    });
    const page = routeEntities.resolveRoute(pathname).page;
    let image = `${config.IMG_MAIN_PROXY}/public/images/brands/brand-logo.png`;
    return image;
};

export const getPageIndexable = (pathname, state) => {
    available_locales.map(locale => {
        pathname = pathname.replace(`/${locale}`, '');
    });
    const page = routeEntities.resolveRoute(pathname).page;
    let indexable = true;
    if (page == guildWelcomeRoute.page) {
        const uid = guildWelcomeRoute.params_value('uid', pathname);
        if (
            uid &&
            !!state.guild.get('welcome_guilds') &&
            !!state.guild.getIn(['welcome_guilds', uid])
        ) {
            const _guild = state.guild.getIn(['welcome_guilds', uid]);
            const guild = _guild.toJS();
            indexable = !guild.is_private;
        }
    }
    return indexable;
};

export const getPageOGPTag = (pathname, state) => {
    available_locales.map(locale => {
        pathname = pathname.replace(`/${locale}`, '');
    });
    const page = routeEntities.resolveRoute(pathname).page;
    let card = 'summary';
    let title = getPageTitle(pathname, state);
    let description = getPageDescription(pathname, state);
    let image = getPageImage(pathname);

    return {
        card,
        title,
        description,
        image,
    };
};

// export default function initialize() {}
