import 'babel-core/register';
import 'babel-polyfill';
import 'whatwg-fetch';
import store from 'store';
import { Set, Map, fromJS, List } from 'immutable';
import '@assets/stylesheets/app.scss';
import Iso from 'iso';
import clientRender from '@infrastructure/client_render';
import frontendLogger from '@utils/FrontendLogger';
import * as cu2 from '@network/current_user';
import extension from '@extension';

function runApp(initial_state) {
    const locale = store.get('language');
    if (locale) initial_state.user.locale = locale;

    // initial_state.auth.current_user = cu2.setInitialCurrentUser(store);

    const location = `${window.location.pathname}${window.location.search}${
        window.location.hash
    }`;

    try {
        clientRender(initial_state);
    } catch (error) {
        console.error(error);
    }
}

if (!window.Intl) {
    require.ensure(
        ['intl/dist/Intl'],
        require => {
            window.IntlPolyfill = window.Intl = require('intl/dist/Intl');
            require('intl/locale-data/jsonp/en-US.js');
            require('intl/locale-data/jsonp/es.js');
            require('intl/locale-data/jsonp/ru.js');
            require('intl/locale-data/jsonp/fr.js');
            require('intl/locale-data/jsonp/it.js');
            require('intl/locale-data/jsonp/ko.js');
            require('intl/locale-data/jsonp/ja.js');
            Iso.bootstrap(runApp);
        },
        'IntlBundle'
    );
} else {
    Iso.bootstrap(runApp);
}
