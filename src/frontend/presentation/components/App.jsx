import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AppPropTypes from '@extension/AppPropTypes';
import Header from '@modules/Header';
import * as appActions from '@redux/App/AppReducer';
import classNames from 'classnames';
import tt from 'counterpart';
import { Component } from 'react';
import resolveRoute from '@infrastructure/ResolveRoute';
import Modals from '@modules/Modals';
import Footer from '@modules/Footer';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import { OneSignalWindow } from '@network/notification';
import DocumentTitle from 'react-document-title';
import autobind from 'class-autobind';
import {
    routeEntities,
    getPageTitle,
    getPageDescription,
    getPageOGPTag,
    getPageIndexable,
} from '@infrastructure/RouteInitialize';
import config from '@constants/config';
import locale_config from '@constants/locale_config';
import { Link, browserHistory } from 'react-router';
import querystring from 'querystring';
import {
    getCurrentLocalePathname,
    getCurrentLocalePathnameByPathname,
} from '@locales';
import { available_locales } from '@constants/locale_config';
import { fallbackLocale } from '@locales';

class App extends Component {
    static redirect = url => {
        if (
            `${config.APP_PUBLIC_IP}/` == url ||
            `${config.APP_PUBLIC_IP}` == url ||
            `http://${config.APP_DOMAIN}/` == url ||
            `http://${config.APP_DOMAIN}` == url
        ) {
            window.location.replace(`https://${config.APP_DOMAIN}`);
        }
    };

    static pushLocaleState(locale) {
        const query = browserHistory.getCurrentLocation().query;
        const pathname = browserHistory.getCurrentLocation().pathname;

        if (
            window &&
            routeEntities.resolveRoute(pathname) &&
            routeEntities.resolveRoute(pathname).localize
        )
            window.history.replaceState(
                pathname,
                null,
                querystring.stringify(query)
                    ? `/${locale}${pathname}?${querystring.stringify(query)}`
                    : `/${locale}${pathname}`
            );
    }

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'App');
    }

    componentDidMount() {
        this.setMeta(this.props.ogp_tags);
        if (process.env.BROWSER) this.handleTheme();

        process.env.BROWSER && App.redirect(window.location.href);
        this.setLocale(this.props);

        if (window && document && process.env.NODE_ENV == 'production') {
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', process.env.GOOGLE_ANALYSTICS_CLIENT);
            // var s1 = document.createElement('script');
            // var code = `
            //     var OneSignal = window.OneSignal || [];
            //     OneSignal.push(function() {
            //         OneSignal.init({
            //             appId: "${process.env.ONESIGNAL_APP_ID}",
            //         });
            //     });
            // `;
            // try {
            //     s1.appendChild(document.createTextNode(code));
            //     document.head.appendChild(s1);
            // } catch (e) {
            //     s1.text = code;
            //     document.head.appendChild(s1);
            // }
        }
    }

    componentWillReceiveProps(np) {
        window.previousLocation = this.props.location;
        if (
            np.location.pathname &&
            getCurrentLocalePathnameByPathname(np.location.pathname) &&
            locale_config.available_locales.includes(
                getCurrentLocalePathnameByPathname(np.location.pathname)
            ) &&
            this.props.locale !=
                getCurrentLocalePathnameByPathname(np.location.pathname)
        ) {
            this.props.setLocale(
                getCurrentLocalePathnameByPathname(np.location.pathname)
            );
        } else {
            App.pushLocaleState(np.locale);
        }

        if (
            np.pathname != this.props.pathname &&
            process.env.NODE_ENV == 'production'
        ) {
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('config', process.env.GOOGLE_ANALYSTICS_CLIENT, {
                page_path: np.pathname,
            });
        }

        if (np.pathname != this.props.pathname) this.setHrefLang(np.location);

        if (this.props.description !== np.description)
            this.setDescription(np.description);

        if (this.props.indexable !== np.indexable)
            this.setIndexable(np.indexable);

        if (this.props.ogp_tags !== np.ogp_tags) this.setMeta(np.ogp_tags);
    }

    // getMetaContents(mn){
    //     var m = document.getElementsByTagName('meta');
    //     for(var i in m){
    //         if(m[i].name == mn){
    //             return m[i].content;
    //         }
    //     }
    // }

    setLocale(props) {
        if (
            props.location.pathname &&
            getCurrentLocalePathnameByPathname(props.location.pathname) &&
            locale_config.available_locales.includes(
                getCurrentLocalePathnameByPathname(props.location.pathname)
            )
        ) {
            this.props.setLocale(
                getCurrentLocalePathnameByPathname(props.location.pathname)
            );
        } else {
            App.pushLocaleState(props.locale);
        }

        this.setHrefLang(props.location);
    }

    setDescription(description) {
        if (!process.env.BROWSER) return;

        document
            .getElementsByName('description')[0]
            .setAttribute('content', description);
    }

    setHrefLang(location = browserHistory.getCurrentLocation()) {
        if (!process.env.BROWSER) return;
        const query = location.query;
        let pathname = location.pathname;
        available_locales.map(locale => {
            pathname = pathname.replace(`/${locale}`, '');
        });
        const page = routeEntities.resolveRoute(pathname);
        locale_config.available_locales.map(lang => {
            const id = locale_config.makeId(lang);
            if (!document.getElementById(id)) {
                var link = document.createElement('link');
                link.setAttribute('rel', 'alternate');
                link.setAttribute('hreflang', lang);
                link.hreflang = lang;

                if (page.localize) {
                    link.setAttribute(
                        'href',
                        `${config.CURRENT_APP_URL}${
                            querystring.stringify(query)
                                ? `/${lang}${pathname}?${querystring.stringify(
                                      query
                                  )}`
                                : `/${lang}${pathname}`
                        }`
                    );
                } else {
                    link.setAttribute(
                        'href',
                        `${config.CURRENT_APP_URL}${
                            querystring.stringify(query)
                                ? `${pathname}?${querystring.stringify(query)}`
                                : `${pathname}`
                        }`
                    );
                }
                document.head.appendChild(link);
            } else {
                const link = document.getElementById(id);
                if (page.localize) {
                    link.setAttribute(
                        'href',
                        `${config.CURRENT_APP_URL}${
                            querystring.stringify(query)
                                ? `/${lang}${pathname}?${querystring.stringify(
                                      query
                                  )}`
                                : `/${lang}${pathname}`
                        }`
                    );
                } else {
                    link.setAttribute(
                        'href',
                        `${config.CURRENT_APP_URL}${
                            querystring.stringify(query)
                                ? `${pathname}?${querystring.stringify(query)}`
                                : `${pathname}`
                        }`
                    );
                }
                link.setAttribute('hreflang', lang);
                link.hreflang = lang;
            }

            const defaultID = locale_config.makeId('x-default');
            if (!document.getElementById(defaultID)) {
                var link = document.createElement('link');
                link.setAttribute('rel', 'alternate');
                link.setAttribute('hreflang', 'x-default');
                link.hreflang = 'x-default';
                if (page.localize) {
                    link.setAttribute(
                        'href',
                        `${config.CURRENT_APP_URL}${
                            querystring.stringify(query)
                                ? `/${fallbackLocale}${
                                      pathname
                                  }?${querystring.stringify(query)}`
                                : `/${fallbackLocale}${pathname}`
                        }`
                    );
                } else {
                    link.setAttribute(
                        'href',
                        `${config.CURRENT_APP_URL}${
                            querystring.stringify(query)
                                ? `${pathname}?${querystring.stringify(query)}`
                                : `${pathname}`
                        }`
                    );
                }
                document.head.appendChild(link);
            } else {
                const link = document.getElementById(defaultID);
                if (page.localize) {
                    link.setAttribute(
                        'href',
                        `${config.CURRENT_APP_URL}${
                            querystring.stringify(query)
                                ? `/${fallbackLocale}${
                                      pathname
                                  }?${querystring.stringify(query)}`
                                : `/${fallbackLocale}${pathname}`
                        }`
                    );
                } else {
                    link.setAttribute(
                        'href',
                        `${config.CURRENT_APP_URL}${
                            querystring.stringify(query)
                                ? `${pathname}?${querystring.stringify(query)}`
                                : `${pathname}`
                        }`
                    );
                }
                link.setAttribute('hreflang', 'x-default');
                link.hreflang = 'x-default';
            }
        });
    }

    setMeta(obj) {
        if (!process.env.BROWSER || !obj) return;

        if (!document.getElementsByName('twitter:card')[0]) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'twitter:card');
            meta.setAttribute('content', obj.card);
            document.head.appendChild(meta);
        } else {
            document
                .getElementsByName('twitter:card')[0]
                .setAttribute('content', obj.card);
        }

        if (!document.getElementsByName('twitter:title')[0]) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'twitter:title');
            meta.setAttribute('content', obj.title);
            document.head.appendChild(meta);
        } else {
            document
                .getElementsByName('twitter:title')[0]
                .setAttribute('content', obj.title);
        }

        if (!document.getElementsByName('twitter:description')[0]) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'twitter:description');
            meta.setAttribute('content', obj.description);
            document.head.appendChild(meta);
        } else {
            document
                .getElementsByName('twitter:description')[0]
                .setAttribute('content', obj.description);
        }

        if (!document.getElementsByName('twitter:image')[0]) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'twitter:image');
            meta.setAttribute('content', obj.image);
            document.head.appendChild(meta);
        } else {
            document
                .getElementsByName('twitter:image')[0]
                .setAttribute('content', obj.image);
        }

        if (!document.getElementsByName('og:title')[0]) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'og:title');
            meta.setAttribute('content', obj.title);
            document.head.appendChild(meta);
        } else {
            document
                .getElementsByName('og:title')[0]
                .setAttribute('content', obj.title);
        }

        if (!document.getElementsByName('og:description')[0]) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'og:description');
            meta.setAttribute('content', obj.description);
            document.head.appendChild(meta);
        } else {
            document
                .getElementsByName('og:description')[0]
                .setAttribute('content', obj.description);
        }

        if (!document.getElementsByName('og:image')[0]) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'og:image');
            meta.setAttribute('content', obj.image);
            document.head.appendChild(meta);
        } else {
            document
                .getElementsByName('og:image')[0]
                .setAttribute('content', obj.image);
        }
    }

    setIndexable(indexable) {
        if (!document.getElementsByName('googlebot')[0] && !indexable) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'googlebot');
            meta.setAttribute('content', 'noindex, nofollow');
            document.head.appendChild(meta);
        } else if (document.getElementsByName('googlebot')[0] && indexable) {
            document.getElementsByName('googlebot')[0].remove();
        }

        if (!document.getElementsByName('bingbot')[0] && !indexable) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'bingbot');
            meta.setAttribute('content', 'noindex, nofollow');
            document.head.appendChild(meta);
        } else if (document.getElementsByName('bingbot')[0] && indexable) {
            document.getElementsByName('bingbot')[0].remove();
        }

        if (!document.getElementsByName('slurp')[0] && !indexable) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'slurp');
            meta.setAttribute('content', 'noindex, nofollow');
            document.head.appendChild(meta);
        } else if (document.getElementsByName('slurp')[0] && indexable) {
            document.getElementsByName('slurp')[0].remove();
        }

        if (!document.getElementsByName('duckduckbot')[0] && !indexable) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'duckduckbot');
            meta.setAttribute('content', 'noindex, nofollow');
            document.head.appendChild(meta);
        } else if (document.getElementsByName('duckduckbot')[0] && indexable) {
            document.getElementsByName('duckduckbot')[0].remove();
        }

        if (!document.getElementsByName('baiduspider')[0] && !indexable) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'baiduspider');
            meta.setAttribute('content', 'noindex, nofollow');
            document.head.appendChild(meta);
        } else if (document.getElementsByName('baiduspider')[0] && indexable) {
            document.getElementsByName('baiduspider')[0].remove();
        }

        if (!document.getElementsByName('yandexbot')[0] && !indexable) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'yandexbot');
            meta.setAttribute('content', 'noindex, nofollow');
            document.head.appendChild(meta);
        } else if (document.getElementsByName('yandexbot')[0] && indexable) {
            document.getElementsByName('yandexbot')[0].remove();
        }

        if (!document.getElementsByName('naver')[0] && !indexable) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'naver');
            meta.setAttribute('content', 'noindex, nofollow');
            document.head.appendChild(meta);
        } else if (document.getElementsByName('naver')[0] && indexable) {
            document.getElementsByName('naver')[0].remove();
        }

        if (
            !document.getElementsByName('facebookexternalhit')[0] &&
            !indexable
        ) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'facebookexternalhit');
            meta.setAttribute('content', 'index, follow');
            document.head.appendChild(meta);
        } else if (
            document.getElementsByName('facebookexternalhit')[0] &&
            indexable
        ) {
            document.getElementsByName('facebookexternalhit')[0].remove();
        }

        if (!document.getElementsByName('twitterbot')[0] && !indexable) {
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'twitterbot');
            meta.setAttribute('content', 'index, follow');
            document.head.appendChild(meta);
        } else if (document.getElementsByName('twitterbot')[0] && indexable) {
            document.getElementsByName('twitterbot')[0].remove();
        }
    }

    handleTheme() {
        const { nightmodeEnabled } = this.props;
        const themeClass = nightmodeEnabled ? 'theme-dark' : 'theme-original';
        const html = document.documentElement;
        html.classList.remove('theme-dark', 'theme-original');
        html.classList.add(themeClass);
    }

    render() {
        const {
            params,
            children,
            nightmodeEnabled,
            pathname,
            title,
            description,
        } = this.props;

        const params_keys = Object.keys(params);

        const themeClass = nightmodeEnabled ? ' theme-dark' : ' theme-original';

        return (
            <DocumentTitle title={title}>
                <div className={classNames('App', themeClass)} ref="App_root">
                    {children}
                    <Modals />
                </div>
            </DocumentTitle>
        );
    }
}

App.propTypes = {
    error: PropTypes.string,
    children: AppPropTypes.Children,
    pathname: PropTypes.string,
};

export default connect(
    (state, ownProps) => {
        return {
            pathname: ownProps.location.pathname,
            order: ownProps.params.order,
            category: ownProps.params.category,
            title: getPageTitle(ownProps.location.pathname, state),
            description: getPageDescription(ownProps.location.pathname, state),
            indexable: getPageIndexable(ownProps.location.pathname, state),
            ogp_tags: getPageOGPTag(ownProps.location.pathname, state),
            locale: state.app.get('locale'),
        };
    },
    dispatch => ({
        loginUser: () => {},
        syncCurrentUser: () => {},
        setLocale: locale => {
            dispatch(appActions.setLocale(locale));
        },
    })
)(App);
