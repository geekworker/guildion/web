import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import Img from 'react-image';
import SimpleButton from '@elements/SimpleButton';
import { PLATFORM_TYPE } from '@entity/PlatformEntity';
import { downloadsIndexRoute } from '@infrastructure/RouteInitialize';

class PlatformItem extends React.Component {
    static propTypes = {
        repository: PropTypes.oneOf(PLATFORM_TYPE.all),
    };

    static defaultProps = {
        repository: PLATFORM_TYPE.iOS,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'PlatformItem'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository } = this.props;

        let title = repository.translate();
        let desc = '';
        let button_text = '';
        switch (repository) {
            case PLATFORM_TYPE.Web:
                desc = tt('downloads.developing');
                button_text = tt('g.to_site');
                break;

            case PLATFORM_TYPE.iOS:
                desc = tt('downloads.developing');
                button_text = tt('g.download');
                break;

            case PLATFORM_TYPE.Android:
                desc = tt('downloads.developing');
                button_text = tt('downloads.to_site');
                break;

            case PLATFORM_TYPE.MacBook:
                desc = tt('downloads.developing');
                button_text = tt('downloads.to_site');
                break;

            default:
                break;
        }

        return (
            <div className="platform-item">
                <div className="platform-item__inner">
                    <div className="platform-item__container">
                        <div className="platform-item__container-title">
                            {title}
                        </div>
                        <div className="platform-item__container-desc">
                            {desc}
                        </div>
                        <div
                            className="platform-item__container-button"
                            style={{
                                display: repository.available
                                    ? 'block'
                                    : 'none',
                            }}
                        >
                            <SimpleButton
                                value={button_text}
                                url={downloadsIndexRoute.getPath({
                                    params: {
                                        os: repository.value,
                                    },
                                })}
                            />
                        </div>
                    </div>
                    <div className="platform-item__image">
                        <Img
                            className="platform-item__image__inner"
                            src={repository.thumbnail}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(PlatformItem);
