import React from 'react';
import PropTypes from 'prop-types';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import Icon from '@elements/Icon';
import resolveRoute from '@infrastructure/ResolveRoute';
import tt from 'counterpart';
import * as appActions from '@redux/App/AppReducer';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import * as routes from '@infrastructure/RouteInitialize';
import { getWindowSize } from '@network/window';
import Responsible from '@modules/Responsible';
import Img from 'react-image';
import autobind from 'class-autobind';
import data_config from '@constants/data_config';
import config from '@constants/config';
import classNames from 'classnames';
import SimpleButton from '@elements/SimpleButton';
import PictureItem from '@elements/PictureItem';
import DropDown from '@elements/DropDown';
import {
    available_locales,
    available_locale_labels,
} from '@constants/locale_config';
import { getLocale, fallbackLocale } from '@locales';
import { current_platform, PLATFORM_TYPE } from '@entity/PlatformEntity';

class Header extends React.Component {
    static propTypes = {
        pathname: PropTypes.string,
        onTicking: PropTypes.func,
        tickingEnable: PropTypes.bool,
        localeSelectable: PropTypes.bool,
    };

    static defaultProps = {
        tickingEnable: true,
        localeSelectable: true,
    };

    state = {
        search_mode: false,
        offset: 0,
        lastPosition: 0,
        ticking: false,
    };

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'Header');
    }

    handleResize() {
        var size = getWindowSize();
    }

    componentDidMount() {
        this.setState({ ticking: false });
        if (this.toggleHeader) this.toggleHeader(this.props);
        this.handleResize();
        if (process.env.BROWSER)
            window.addEventListener('resize', this.handleResize);
        if (process.env.BROWSER)
            window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        if (process.env.BROWSER)
            window.removeEventListener('resize', this.handleResize);
        if (process.env.BROWSER)
            window.removeEventListener('scroll', this.handleScroll);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.pathname != nextProps.pathname) {
            this.setState({ ticking: false });
        }
        if (this.toggleHeader) this.toggleHeader(nextProps);
    }

    onChangeLocale(e) {
        const { locale, setLocale } = this.props;
        let pathname = browserHistory.getCurrentLocation().pathname;
        available_locales.map(locale => {
            pathname = pathname.replace(`/${locale}`, '');
        });
        browserHistory.push(`/${locale == 'ja' ? 'en' : 'ja'}${pathname}`);
    }

    getLocaleOption() {
        return available_locales.map((locale, i) => {
            return {
                value: locale,
                label: available_locale_labels[i],
            };
        });
    }

    makeLocaleOption(locale) {
        if (!locale) return;
        return {
            value: getLocale(locale),
            label:
                available_locale_labels[
                    available_locales.indexOf(getLocale(locale))
                ],
        };
    }

    toggleHeader = props => {
        const {
            isHeaderVisible,
            route,
            showHeader,
            hideHeader,
            showNew,
        } = props;

        switch (route) {
            case routes.loginRoute:
            case routes.signupRoute:
                hideHeader();
                break;
            default:
                showHeader();
                break;
        }
    };

    handleScroll(e) {
        const {
            pathname,
            route,
            hideHeader,
            isHeaderVisible,
            onTicking,
            tickingEnable,
        } = this.props;

        if (!this.refs.headercmpRef) return;

        this.setState({ lastPosition: window.scrollY });

        window.requestAnimationFrame(() => {
            const { lastPosition, ticking, offset } = this.state;

            if (!tickingEnable) {
                this.setState({ ticking: false });
                return;
            }

            const height = 64;

            if (lastPosition > height) {
                if (lastPosition > offset) {
                    this.setState({ ticking: true });
                    if (onTicking) onTicking();
                } else {
                    this.setState({ ticking: false });
                }
                this.setState({ offset: lastPosition });
            } else {
                this.setState({ offset: lastPosition, ticking: false });
            }
        });
    }

    render() {
        const {
            pathname,
            route,
            hideHeader,
            isHeaderVisible,
            locale,
            localeSelectable,
        } = this.props;

        const { ticking } = this.state;

        // const { handleRequestSearch, toggleSideBar, toggleSearchMode } = this;

        const header_className = isHeaderVisible ? 'Header' : 'Header-hidden';

        const nomal_body = (
            <div className="Header__inner">
                <Link
                    to={routes.welcomeRoute.getPath()}
                    className="Header__logo__link"
                >
                    <Img
                        className="Header__logo"
                        src={`${
                            config.IMG_MAIN_PROXY
                        }/public/images/brands/white-logo.png`}
                        alt={tt('alts.default')}
                    />
                </Link>
                {localeSelectable && (
                    <div className="Header__locales">
                        <DropDown
                            placeholder={this.makeLocaleOption(locale).label}
                            onChange={this.onChangeLocale}
                            options={this.getLocaleOption()}
                            value={this.makeLocaleOption(locale)}
                        />
                    </div>
                )}
                <div className="Header__links">
                    <Link
                        to={routes.downloadsIndexRoute.getPath({
                            params: {
                                os: current_platform().value,
                            },
                        })}
                        className="Header__link"
                    >
                        {tt('g.download')}
                    </Link>
                    {/*<Link to={routes.downloadsIndexRoute.getPath({
                        params: {
                            os: PLATFORM_TYPE.Web.value,
                        }
                    })} className="Header__link">
                        {tt('platform.web')}
                    </Link>*/}
                    <Link
                        to={routes.premiumIndexRoute.path}
                        className="Header__link"
                    >
                        {tt('g.premium')}
                    </Link>
                </div>
            </div>
        );

        return (
            <div
                ref="headercmpRef"
                className={`${header_className} ${ticking &&
                    'Header-animation'}`}
            >
                {nomal_body}
            </div>
        );
    }
}

export { Header as _Header_ };

const mapStateToProps = (state, ownProps) => {
    const route = resolveRoute(ownProps.pathname);
    const isHeaderVisible = state.app.get('show_header');
    return {
        locale: state.app.get('locale'),
        route,
        isHeaderVisible,
        ...ownProps,
    };
};

const mapDispatchToProps = dispatch => ({
    showHeader: () => {
        dispatch(appActions.showHeader());
    },
    hideHeader: () => {
        dispatch(appActions.hideHeader());
    },
    setLocale: locale => {
        dispatch(appActions.setLocale(locale));
    },
});

const connectedHeader = connect(mapStateToProps, mapDispatchToProps)(Header);

export default connectedHeader;
