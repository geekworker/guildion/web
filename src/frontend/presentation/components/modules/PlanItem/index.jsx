import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import {
    SUBSCRIPTION_SPAN,
    SUBSCRIPTION_PLAN,
    PLAN_FUNCTION,
} from '@constants/plan_config';
import { SubscriptionPlanEntity } from '@entity/SubscriptionPlanEntity';
import classNames from 'classnames';
import { premiumIndexRoute } from '@infrastructure/RouteInitialize';

class PlanItem extends React.Component {
    static propTypes = {
        onClick: PropTypes.func,
        repository: PropTypes.object,
        span: PropTypes.oneOf(SUBSCRIPTION_SPAN.all),
    };
    static defaultProps = {
        repository: new SubscriptionPlanEntity({}),
        span: SUBSCRIPTION_SPAN.Month1,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'PlanItem');
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    onClick(e) {
        const { onClick } = this.props;
        if (e) e.preventDefault();
        if (onClick) onClick(e);
    }

    render() {
        const { onClick } = this;

        const { repository, span } = this.props;

        const active = repository.span == span;

        return (
            <div className={classNames('plan-item', { active })}>
                <Link
                    className="plan-item__content"
                    onClick={onClick}
                    to={premiumIndexRoute.path + '#plan-span'}
                >
                    <div className="plan-item__content-center">
                        <div className="plan-item__content-number">
                            {span.count}
                        </div>
                        <div className="plan-item__content-month">
                            {span.suffix()}
                        </div>
                        <div className="plan-item__content-price">
                            {span.priceString(repository.plan)}
                        </div>
                    </div>
                    <div className="plan-item__content-footer">
                        <div className="plan-item__content-footer__label">
                            {span.discountString(repository.plan)}
                        </div>
                    </div>
                </Link>
                <div className="plan-item__bottom">
                    <div className="plan-item__bottom-label">
                        {span.monthPriceString(repository.plan)}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(PlanItem);
