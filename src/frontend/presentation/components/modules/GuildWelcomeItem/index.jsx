import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import models from '@view_models';
import Img from 'react-image';
import { guildWelcomeRoute } from '@infrastructure/RouteInitialize';

class GuildWelcomeItem extends React.Component {
    static propTypes = {
        repository: PropTypes.instanceOf(models.Guild.Instance),
    };

    static defaultProps = {};

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'GuildWelcomeItem'
        );
    }

    componentWillMount() {}

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository } = this.props;

        if (!repository) return <div />;

        return (
            <Link
                className="guild-welcome-item"
                to={guildWelcomeRoute.getPath({
                    params: { uid: repository.uid },
                })}
            >
                <div className="guild-welcome-item__inner">
                    <div className="guild-welcome-item__background">
                        <div className="guild-welcome-item__background__image">
                            <img
                                className="guild-welcome-item__background__image-row"
                                src={repository.dataValues.picture_small}
                                alt={repository.dataValues.name}
                            />
                        </div>
                    </div>
                    <div className="guild-welcome-item__foreground">
                        <div className="guild-welcome-item__foreground__container">
                            <div className="guild-welcome-item__foreground__container-name">
                                {repository.dataValues.name}
                            </div>
                        </div>
                        <div className="guild-welcome-item__foreground__footer">
                            <div className="guild-welcome-item__foreground__footer-blur" />
                            <div className="guild-welcome-item__foreground__footer__container" />
                        </div>
                    </div>
                </div>
            </Link>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(GuildWelcomeItem);
