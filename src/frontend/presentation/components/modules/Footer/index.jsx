import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import config from '@constants/config';
import Icon from '@elements/Icon';
import Img from 'react-image';
import {
    downloadsIndexRoute,
    documentShowRoute,
    documentGroupShowRoute,
    premiumIndexRoute,
    welcomeRoute,
} from '@infrastructure/RouteInitialize';
import { current_platform, PLATFORM_TYPE } from '@entity/PlatformEntity';
import DropDown from '@elements/DropDown';
import {
    available_locales,
    available_locale_labels,
} from '@constants/locale_config';
import { getLocale, fallbackLocale } from '@locales';
import * as appActions from '@redux/App/AppReducer';

class Footer extends React.Component {
    static propTypes = {
        localeSelectable: PropTypes.bool,
    };

    static defaultProps = {
        localeSelectable: true,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'Footer');
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    onChangeLocale(e) {
        const { locale, setLocale } = this.props;
        let pathname = browserHistory.getCurrentLocation().pathname;
        available_locales.map(locale => {
            pathname = pathname.replace(`/${locale}`, '');
        });
        browserHistory.push(`/${locale == 'ja' ? 'en' : 'ja'}${pathname}`);
    }

    getLocaleOption() {
        return available_locales.map((locale, i) => {
            return {
                value: locale,
                label: available_locale_labels[i],
            };
        });
    }

    makeLocaleOption(locale) {
        if (!locale) return;
        return {
            value: getLocale(locale),
            label:
                available_locale_labels[
                    available_locales.indexOf(getLocale(locale))
                ],
        };
    }

    render() {
        const { locale, localeSelectable } = this.props;

        return (
            <div className="footer">
                <div className="footer__inner">
                    <div className="footer__social">
                        <div className="footer__social-headline">
                            {tt('footer.connect')}
                        </div>
                        <Link
                            className="footer__social-link"
                            to={'https://twitter.com/guildion'}
                            target="_blank"
                        >
                            <Icon
                                className="footer__social-link__icon"
                                src="twitter"
                                size="2x"
                            />
                        </Link>
                        <Link
                            className="footer__social-link"
                            to={'https://www.instagram.com/guildion.official'}
                            target={'_blank'}
                        >
                            <Icon
                                className="footer__social-link__icon"
                                src="instagram"
                                size="2x"
                            />
                        </Link>
                        {/* <Link className="footer__social-link" to={"/"}>
                            <Icon
                                className="footer__social-link__icon"
                                src="facebook"
                                size="2x"
                            />
                        </Link>*/}
                        <Link
                            className="footer__social-link"
                            to={'https://note.com/guildion'}
                            target={'_blank'}
                        >
                            <Icon
                                className="footer__social-link__icon"
                                src="brand-note"
                                size="2x"
                            />
                        </Link>
                        <Link
                            className="footer__social-link"
                            style={{ marginTop: '2px' }}
                            to={'https://medium.com/@guildion'}
                            target="_blank"
                        >
                            <Icon
                                className="footer__social-link__icon"
                                src="medium"
                                size="2x"
                            />
                        </Link>
                    </div>
                    <div className="footer__standard">
                        <div className="footer__standard-menu">
                            <div className="footer__standard-menu__item footer__item-top">
                                <div className="footer__standard-menu__item-title">
                                    {tt('footer.about')}
                                </div>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={welcomeRoute.path}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('footer.about')}
                                </Link>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={downloadsIndexRoute.getPath({
                                        params: {
                                            os: current_platform().value,
                                        },
                                    })}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('g.download')}
                                </Link>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={premiumIndexRoute.path}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('g.premium')}
                                </Link>
                            </div>
                        </div>
                        <div className="footer__standard-menu">
                            <div className="footer__standard-menu__item footer__item-top">
                                <div className="footer__standard-menu__item-title">
                                    {tt('g.platform')}
                                </div>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={downloadsIndexRoute.getPath({
                                        params: {
                                            os: PLATFORM_TYPE.Web.value,
                                        },
                                    })}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('platform.web')}
                                </Link>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={downloadsIndexRoute.getPath({
                                        params: {
                                            os: PLATFORM_TYPE.iOS.value,
                                        },
                                    })}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('platform.ios')}
                                </Link>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={downloadsIndexRoute.getPath({
                                        params: {
                                            os: PLATFORM_TYPE.Android.value,
                                        },
                                    })}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('platform.android')}
                                </Link>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={downloadsIndexRoute.getPath({
                                        params: {
                                            os: PLATFORM_TYPE.MacBook.value,
                                        },
                                    })}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('platform.mac')}
                                </Link>
                            </div>
                        </div>
                        <div className="footer__standard-menu">
                            <div className="footer__standard-menu__item footer__item-top">
                                <div className="footer__standard-menu__item-title">
                                    {tt('footer.for_users')}
                                </div>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={documentGroupShowRoute.getPath({
                                        params: { groupname: 'legals' },
                                    })}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('g.legals')}
                                </Link>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={documentGroupShowRoute.getPath({
                                        params: { groupname: 'supports' },
                                    })}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('g.support_center')}
                                </Link>
                            </div>
                            {/*<div className="footer__standard-menu__item">
                                <Link to={"/"} className="footer__standard-menu__item-link">
                                    {tt('g.blog')}
                                </Link>
                            </div>*/}
                        </div>
                        <div className="footer__standard-menu">
                            <div className="footer__standard-menu__item footer__item-top">
                                <div className="footer__standard-menu__item-title">
                                    {tt('footer.about_management')}
                                </div>
                            </div>
                            <div className="footer__standard-menu__item">
                                <Link
                                    to={documentShowRoute.getPath({
                                        params: {
                                            groupname: 'managements',
                                            uid: 'system',
                                        },
                                    })}
                                    className="footer__standard-menu__item-link"
                                >
                                    {tt('footer.management')}
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="footer__minimal">
                        <div className="footer__minimal-logo-links">
                            <Link
                                to={welcomeRoute.path}
                                className="footer__logo-link"
                            >
                                <Img
                                    className="footer__minimal__logo"
                                    src={`${
                                        config.IMG_MAIN_PROXY
                                    }/public/images/brands/white-logo.png`}
                                    alt={tt('alts.default')}
                                />
                            </Link>
                            <div className="footer__minimal__links">
                                <Link
                                    to={documentShowRoute.getPath({
                                        params: {
                                            uid: 'terms_and_conditions',
                                            groupname: 'legals',
                                        },
                                    })}
                                    className="footer__minimal__link"
                                >
                                    {tt('g.term')}
                                </Link>
                                <Link
                                    to={documentShowRoute.getPath({
                                        params: {
                                            uid: 'privacy_policies',
                                            groupname: 'legals',
                                        },
                                    })}
                                    className="footer__minimal__link"
                                >
                                    {tt('g.privacy')}
                                </Link>
                                <Link
                                    to={documentShowRoute.getPath({
                                        params: {
                                            uid: 'commercialtransaction',
                                            groupname: 'legals',
                                        },
                                    })}
                                    className="footer__minimal__link"
                                >
                                    {tt(
                                        'g.act_on_specified_commercial_transactions'
                                    )}
                                </Link>
                                {/*<Link to={'/'} className="footer__minimal__link">
                                    {tt('g.contact')}
                                </Link>*/}
                                {localeSelectable && (
                                    <div className="footer__minimal__dropdown">
                                        <DropDown
                                            placeholder={
                                                this.makeLocaleOption(locale)
                                                    .label
                                            }
                                            onChange={this.onChangeLocale}
                                            options={this.getLocaleOption()}
                                            value={this.makeLocaleOption(
                                                locale
                                            )}
                                        />
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({
        setLocale: locale => {
            dispatch(appActions.setLocale(locale));
        },
    })
)(Footer);
