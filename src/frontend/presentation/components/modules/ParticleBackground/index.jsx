import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import Particles from 'react-particles-js';
import classNames from 'classnames';

class ParticleBackground extends React.Component {
    static propTypes = {
        children: AppPropTypes.Children,
        className: PropTypes.string,
    };

    static defaultProps = {};

    state = {
        height: 0,
        width: 0,
    };

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'ParticleBackground'
        );
    }

    componentWillMount() {}

    componentDidMount() {
        this.handleResize();
        if (process.env.BROWSER)
            window.addEventListener('resize', this.handleResize);
    }

    componentWillReceiveProps(nextProps) {}

    componentWillUnmount() {
        if (process.env.BROWSER)
            window.removeEventListener('resize', this.handleResize);
    }

    getItemSize() {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('particle-background__foreground')[0],
            w = w.innerWidth || e.clientWidth || g.clientWidth,
            h = w.innerHeight || e.clientHeight || g.clientHeight;
        return {
            width: w,
            height: h,
        };
    }

    handleResize() {
        var size = this.getItemSize();
        this.setState({
            height: size.height,
            width: size.width,
        });
    }

    handleRect(divElement) {
        this.divElement = divElement;
        if (!!divElement) {
            this.setState({
                height: divElement.clientHeight,
                width: divElement.clientWidth,
            });
        }
    }

    render() {
        const { children, className } = this.props;

        const { height, width } = this.state;

        return (
            <div className="particle-background">
                <div className="particle-background__background">
                    <div
                        className="particle-background__background__particles"
                        style={{
                            width: `${width}px`,
                            height: `${height}px`,
                        }}
                    >
                        <Particles
                            params={{
                                particles: {
                                    number: {
                                        value: 100,
                                        density: {
                                            enable: false,
                                        },
                                    },
                                    size: {
                                        value: 4,
                                        random: true,
                                        anim: {
                                            speed: 4,
                                            size_min: 0.3,
                                        },
                                    },
                                    line_linked: {
                                        enable: false,
                                    },
                                    move: {
                                        random: true,
                                        speed: 1,
                                        direction: 'top',
                                        out_mode: 'out',
                                    },
                                },
                                interactivity: {
                                    events: {
                                        onhover: {
                                            enable: true,
                                            mode: 'bubble',
                                        },
                                        onclick: {
                                            enable: true,
                                            mode: 'repulse',
                                        },
                                    },
                                    modes: {
                                        bubble: {
                                            distance: 250,
                                            duration: 2,
                                            size: 1,
                                            opacity: 0.7,
                                        },
                                        repulse: {
                                            distance: 150,
                                            duration: 20,
                                        },
                                    },
                                },
                            }}
                        />
                    </div>
                </div>
                <div
                    className={classNames(
                        'particle-background__foreground',
                        className
                    )}
                >
                    {children}
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(ParticleBackground);
