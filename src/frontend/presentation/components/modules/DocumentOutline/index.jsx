import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import {
    DocumentOutlineEntity,
    DocumentOutlineEntities,
} from '@entity/DocumentOutlineEntity';
import DocumentOutlineItem from '@elements/DocumentOutlineItem';
import { sleep } from '@extension/sleep';

class DocumentOutline extends React.Component {
    static propTypes = {
        repositories: PropTypes.object,
    };

    static defaultProps = {
        repositories: null,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentOutline'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repositories } = this.props;

        const renderItem = items =>
            items.map((item, key) => (
                <div className="document-outline__item" key={key}>
                    <DocumentOutlineItem repository={item} />
                </div>
            ));

        return (
            <div className="document-outline">
                <div className="document-outline__inner">
                    <div className="document-outline__head">
                        {tt('g.outline')}
                    </div>
                    <div className="document-outline__items">
                        {repositories && renderItem(repositories.items)}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(DocumentOutline);
