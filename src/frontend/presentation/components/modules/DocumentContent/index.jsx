import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import models from '@view_models';
import DocumentHTMLViewer from '@elements/DocumentHTMLViewer';

class DocumentContent extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
    };

    static defaultProps = {
        repository: null,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentContent'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository, locale } = this.props;

        if (!repository) return <div />;

        return (
            <div className="document-content">
                <div className="document-content__breadcrumb" />
                <div className="document-content__header">
                    <h2 className="document-content__header-title">
                        {models.Document.getTitle(repository, locale)}
                    </h2>
                    <div className="document-content__header-border" />
                </div>
                <div className="document-content__body">
                    <DocumentHTMLViewer repository={repository} />
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentContent);
