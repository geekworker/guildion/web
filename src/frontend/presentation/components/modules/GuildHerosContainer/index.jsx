import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import models from '@view_models';
import GuildHeroItem from '@elements/GuildHeroItem';
import classNames from 'classnames';

class GuildHerosContainer extends React.Component {
    static propTypes = {
        direction: PropTypes.oneOf(['left', 'right']),
        repositories: PropTypes.arrayOf(
            PropTypes.instanceOf(models.Guild.Instance)
        ),
    };

    static defaultProps = {
        repositories: [],
        direction: 'right',
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'GuildHerosContainer'
        );
    }

    componentWillMount() {}

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repositories, direction } = this.props;

        const renderItems = items =>
            items.map((item, key) => (
                <div
                    className="guild-heros-container__item"
                    style={{
                        gridColomn: key + 1,
                    }}
                    key={key}
                >
                    <GuildHeroItem repository={item} />
                </div>
            ));

        return (
            <div className={classNames('guild-heros-container', direction)}>
                <div className="guild-heros-container__inner">
                    <div className="guild-heros-container__items">
                        {renderItems(repositories)}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(GuildHerosContainer);
