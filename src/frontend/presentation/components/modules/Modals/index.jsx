import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Reveal from '@elements/Reveal';
import { OrderedSet } from 'immutable';
import tt from 'counterpart';
import * as appActions from '@redux/App/AppReducer';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';

class Modals extends React.Component {
    static defaultProps = {
        className: '',
    };

    static propTypes = {
        className: PropTypes.string,
    };

    constructor() {
        super();
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'Modals');
    }

    render() {
        const {
            nightmodeEnabled,
            className,
        } = this.props;

        const themeClass = nightmodeEnabled ? ' theme-dark' : ' theme-original';

        return (
            <div className={className}>
            </div>
        );
    }
}

export default connect(
    state => {
        return {
        };
    },
    dispatch => ({
    })
)(Modals);
