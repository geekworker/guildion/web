import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import DotIndicator from '@elements/DotIndicator';

class TypingIndicator extends React.Component {
    static propTypes = {
        repositories: PropTypes.array,
    };

    static defaultProps = {
        repositories: [],
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'TypingIndicator'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repositories } = this.props;

        if (!repositories || repositories.length == 0) return <div />;

        return (
            <div className="typing-indicator fade-in--1">
                <div className="typing-indicator__inner">
                    <div className="typing-indicator__indicator">
                        <DotIndicator />
                    </div>
                    <div className="typing-indicator__users">
                        {tt('g.is_typing', {
                            data: repositories
                                .map(repository => repository.nickname)
                                .join(', '),
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(TypingIndicator);
