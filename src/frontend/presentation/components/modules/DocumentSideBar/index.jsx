import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import DocumentSideBarItem from '@elements/DocumentSideBarItem';
import DocumentSideBarSection from '@elements/DocumentSideBarSection';
import models from '@view_models';

class DocumentSideBar extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
    };

    static defaultProps = {
        repository: null,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentSideBar'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository, locale } = this.props;

        const renderItem = items =>
            items.map((item, key) => (
                <div className="document-side-bar__item" key={key}>
                    <DocumentSideBarItem repository={item} group={repository} />
                </div>
            ));

        const renderSection = items =>
            items.map((item, key) => (
                <div className="document-side-bar__section" key={key}>
                    <DocumentSideBarSection repository={item} />
                    <div className="document-side-bar__items">
                        {item && renderItem(item.Documents)}
                    </div>
                </div>
            ));
        return (
            <div className="document-side-bar">
                {repository && (
                    <div className="document-side-bar__inner">
                        <div className="document-side-bar__sections">
                            {renderSection(repository.Sections)}
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentSideBar);
