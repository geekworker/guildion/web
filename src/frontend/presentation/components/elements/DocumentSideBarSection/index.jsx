import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import classNames from 'classnames';
import models from '@view_models';

class DocumentSideBarSection extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
        active: PropTypes.bool,
    };

    static defaultProps = {
        repository: null,
        active: false,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentSideBarSection'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository, active, locale } = this.props;

        if (
            !repository ||
            !models.DocumentSection.getTitle(repository, locale) ||
            models.DocumentSection.getTitle(repository, locale).length == 0
        )
            return <div />;

        return (
            <div
                className={classNames('document-side-bar-section', { active })}
            >
                {models.DocumentSection.getTitle(repository, locale)}
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentSideBarSection);
