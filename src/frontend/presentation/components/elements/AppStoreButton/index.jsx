import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import Img from 'react-image';
import { IOS_DOWNLOAD_URL } from '@constants/config';
import {
    IOS_TESTFLIGHT_URL,
    IOS_TESTFLIGHT_MODE,
} from '@constants/testflight_config';

class AppStoreButton extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'AppStoreButton'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        return (
            <Link
                className="app-store-button"
                href={
                    IOS_TESTFLIGHT_MODE ? IOS_TESTFLIGHT_URL : IOS_DOWNLOAD_URL
                }
                target="_blank"
            />
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(AppStoreButton);
