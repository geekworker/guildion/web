import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';

class DotIndicator extends React.Component {
    static propTypes = {
        style: PropTypes.object,
    };

    static defaultProps = {
        style: {},
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DotIndicator'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { style } = this.props;
        return (
            <div className="dot-indicator" style={style}>
                <div className="bounce1" />
                <div className="bounce2" />
                <div className="bounce3" />
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(DotIndicator);
