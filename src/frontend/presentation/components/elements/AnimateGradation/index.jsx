import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';

class AnimateGradation extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {
        animationClass: 'animate-gradation',
    };

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'AnimateGradation'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    changeState() {
        if (this.state.animationClass === 'animate-gradation') {
            this.setState({
                animationClass: 'animate-gradation paused',
            });
        } else {
            this.setState({
                animationClass: 'animate-gradation',
            });
        }
    }

    render() {
        return <div className={this.state.animationClass} />;
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(AnimateGradation);
