import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import tt from 'counterpart';
import classNames from 'classnames';
import { browserHistory } from 'react-router';
import Icon from '@elements/Icon';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';

class SimpleButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value,
            active: this.props.active,
        };
        this.handleClick = this.handleClick.bind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'SimpleButton'
        );
    }

    componentWillUpdate(nextProps, nextState) {
        this.setState({
            value: nextProps.value,
            active: nextProps.active,
        });
    }

    handleClick = e => {
        const {
            onClick,
            url,
            submit,
            current_user,
            loginRequired,
        } = this.props;
        if (e && !submit) e.stopPropagation();
        if (!current_user && loginRequired) {
        }
        if (url) browserHistory.push(url);
        if (onClick) {
            onClick(this.state.value);
        }
    };

    componentWillReceiveProps(nextProps) {
        if (this.state.value != nextProps.value)
            this.setState({ value: nextProps.value });
        if (this.state.active != nextProps.active)
            this.setState({ active: nextProps.active });
    }

    render() {
        const { value, active } = this.state;
        const {
            className,
            classes,
            disabled,
            onClick,
            style,
            shadowBool,
            color,
            url,
            submit,
            src,
            size,
            target,
            fontWeight,
            fontSize,
            current_user,
            loginRequired,
            ...inputProps
        } = this.props;

        let button_class_name =
            color == '' ? 'simple-button' : `simple-button-${color}`;

        let simpleButton = submit ? (
            <button
                type="submit"
                className={classNames(`${button_class_name}__link`, {
                    active,
                })}
                id="simple-button_submit"
                onClick={e => this.handleClick(e)}
            >
                <div
                    className={classNames(button_class_name, className, {
                        active,
                    })}
                >
                    <div className={`${button_class_name}__items`}>
                        {src && (
                            <Icon
                                className={`${button_class_name}__items-image`}
                                src={src}
                                size={size || '2_2x'}
                            />
                        )}
                        <div
                            className={`${button_class_name}__items-text`}
                            style={{ fontWeight, fontSize }}
                        >
                            {this.state.value}
                        </div>
                    </div>
                </div>
            </button>
        ) : (
            <div
                className={classNames(`${button_class_name}__link`, className, {
                    active: active,
                })}
                onClick={e => this.handleClick(e)}
            >
                <div
                    className={classNames(button_class_name, {
                        active: active,
                    })}
                >
                    <div className={`${button_class_name}__items`}>
                        {src && (
                            <Icon
                                className={`${button_class_name}__items-image`}
                                src={src}
                                size={size || '2_2x'}
                            />
                        )}
                        <div
                            className={`${button_class_name}__items-text`}
                            style={{ fontWeight, fontSize }}
                        >
                            {this.state.value}
                        </div>
                    </div>
                </div>
            </div>
        );

        if (!!url && !!target) {
            simpleButton = (
                <Link
                    className={classNames(
                        `${button_class_name}__link`,
                        className,
                        {
                            active: active,
                        }
                    )}
                    to={loginRequired && !current_user ? '/' : url}
                    target="_blank"
                >
                    <div
                        className={classNames(button_class_name, {
                            active: active,
                        })}
                    >
                        <div className={`${button_class_name}__items`}>
                            {src && (
                                <Icon
                                    className={`${
                                        button_class_name
                                    }__items-image`}
                                    src={src}
                                    size={size || '2_2x'}
                                />
                            )}
                            <div
                                className={`${button_class_name}__items-text`}
                                style={{ fontWeight, fontSize }}
                            >
                                {this.state.value}
                            </div>
                        </div>
                    </div>
                </Link>
            );
        }

        return simpleButton;
    }
}

SimpleButton.defaultProps = {
    className: '',
    disabled: false,
    style: null,
    shadowBool: true,
    value: '',
    color: '',
    url: '',
    submit: false,
    active: false,
    fontWeight: 700,
    fontSize: 18,
    loginRequired: false,
};

SimpleButton.propTypes = {
    /** Override or extend the styles applied to the component. */
    classes: PropTypes.string,
    /** Custom top-level class */
    className: PropTypes.string,
    /** Disables text field. */
    disabled: PropTypes.bool,
    /** Fired when the button is tapped. */
    onClick: PropTypes.func,
    /** Override the inline-styles of the root element. */
    style: PropTypes.object,
    /** Disables shadow. */
    shadowBool: PropTypes.bool,
    /** The value of the text field. */
    value: PropTypes.string,
    /** The color of the button. */
    color: PropTypes.string,
    /** The url of the button. */
    url: PropTypes.string,
    /** The submit of the button. */
    submit: PropTypes.bool,
    /** The mode of the button layout. */
    active: PropTypes.bool,
    /** The src for icon. */
    src: PropTypes.string,
};

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(SimpleButton);
