import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import sanitizeHtml from 'sanitize-html';
import models from '@view_models';

class DocumentHTMLViewer extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
    };

    static defaultProps = {
        repository: null,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentHTMLViewer'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    htmlDecode(content) {
        let e = document.createElement('div');
        e.innerHTML = content;
        return e.childNodes.length === 0 ? '' : e.childNodes[0].nodeValue;
    }

    render() {
        const { repository, locale } = this.props;

        if (!repository) return <div />;

        return (
            <div className="document-html-viewer">
                <div
                    className="document-html-viewer__inner"
                    dangerouslySetInnerHTML={{
                        __html: models.Document.getHTML(
                            repository,
                            locale
                        ) /*sanitizeHtml(
                            models.Document.getHTML(repository, locale),
                            {
                                allowedTags: sanitizeHtml.defaults.allowedTags.concat(['img', 'iframe']),
                                allowedAttributes: false
                            }
                        )*/,
                    }}
                />
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentHTMLViewer);
