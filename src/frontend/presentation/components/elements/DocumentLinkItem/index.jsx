import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import models from '@view_models';
import { documentShowRoute } from '@infrastructure/RouteInitialize';

class DocumentLinkItem extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
    };

    static defaultProps = {
        repository: null,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentLinkItem'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository, group, locale } = this.props;

        if (!repository) return <div />;

        return (
            <Link
                className="document-link-item"
                to={documentShowRoute.getPath({
                    params: { uid: repository.uid, groupname: group.groupname },
                })}
            >
                <p className="document-link-item__text">
                    {models.Document.getTitle(repository, locale)}
                </p>
            </Link>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentLinkItem);
