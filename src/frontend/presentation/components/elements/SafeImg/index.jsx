import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import Img from 'react-image';

const limit = 10;

const generateFallbackImg = ({ count, ...restProps }) => {
    if (count == limit) {
        return <div />;
    }
    count += 1;
    return (
        <Img
            {...restProps}
            unloader={generateFallbackImg({ count, ...restProps })}
        />
    );
};

export const SafeImg = ({ ...restProps }) => {
    return (
        <Img
            {...restProps}
            unloader={generateFallbackImg({ count: 0, ...restProps })}
        />
    );
};

SafeImg.propTypes = {};

export default SafeImg;
