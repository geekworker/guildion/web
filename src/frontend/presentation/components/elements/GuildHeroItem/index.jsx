import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import models from '@view_models';
import Img from 'react-image';
import { guildWelcomeRoute } from '@infrastructure/RouteInitialize';

class GuildHeroItem extends React.Component {
    static propTypes = {
        repository: PropTypes.instanceOf(models.Guild.Instance),
    };

    static defaultProps = {};

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'GuildHeroItem'
        );
    }

    componentWillMount() {}

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository } = this.props;

        if (!repository) return <div />;

        return (
            <Link
                className="guild-hero-item"
                to={guildWelcomeRoute.getPath({
                    params: { uid: repository.uid },
                })}
            >
                <div className="guild-hero-item__inner">
                    <img
                        className="guild-hero-item__image"
                        src={repository.dataValues.picture_small}
                        alt={repository.dataValues.name}
                    />
                </div>
            </Link>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(GuildHeroItem);
