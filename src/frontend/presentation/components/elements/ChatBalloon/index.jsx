import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';

export const balloon_position = ['right', 'left', 'bottom'];

class ChatBalloon extends React.Component {
    static propTypes = {
        string: PropTypes.string,
        type: PropTypes.oneOf(balloon_position),
    };

    static defaultProps = {
        string: '',
        type: balloon_position[0],
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'ChatBalloon');
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { string, type } = this.props;

        return <div className={`chat-balloon-${type}`}>{string}</div>;
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(ChatBalloon);
