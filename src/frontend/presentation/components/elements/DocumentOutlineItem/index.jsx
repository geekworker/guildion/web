import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import classNames from 'classnames';
import tt from 'counterpart';
import {
    DocumentOutlineEntity,
    DocumentOutlineEntities,
} from '@entity/DocumentOutlineEntity';

class DocumentOutlineItem extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
        active: PropTypes.bool,
    };

    static defaultProps = {
        repository: null,
        active: false,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentOutlineItem'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository, locale } = this.props;

        if (!repository) return <div />;

        const active =
            repository.id == browserHistory.getCurrentLocation().hash;

        return (
            <a
                className={classNames('document-outline-item', { active })}
                href={repository.id}
            >
                {repository.name}
            </a>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentOutlineItem);
