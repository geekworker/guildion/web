import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import classNames from 'classnames';
import models from '@view_models';
import {
    documentShowRoute,
    documentGroupShowRoute,
    documentEditRoute,
} from '@infrastructure/RouteInitialize';

class DocumentSideBarItem extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
        group: PropTypes.object,
        active: PropTypes.bool,
    };

    static defaultProps = {
        repository: null,
        active: false,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentSideBarItem'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository, locale, group, pathname } = this.props;

        if (!repository || !group) return <div />;

        const active =
            !!pathname &&
            documentShowRoute.isValidPath(pathname) &&
            documentShowRoute.params_value('uid', pathname) == repository.uid;

        return (
            <Link
                className={classNames('document-side-bar-item', { active })}
                to={documentShowRoute.getPath({
                    params: { uid: repository.uid, groupname: group.groupname },
                })}
            >
                {models.Document.getTitle(repository, locale)}
            </Link>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            pathname: browserHistory.getCurrentLocation().pathname,
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentSideBarItem);
