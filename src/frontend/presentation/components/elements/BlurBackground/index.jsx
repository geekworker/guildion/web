import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import Img from 'react-image';
import classNames from 'classnames';

class BlurBackground extends React.Component {
    static propTypes = {
        children: AppPropTypes.Children,
        className: PropTypes.string,
        src: PropTypes.string,
        padding: PropTypes.string,
    };

    static defaultProps = {
        padding: '0 0 0 0',
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'BlurBackground'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { children, className, src, padding, gradation } = this.props;

        return (
            <div className={classNames('blur-background', className)}>
                <div
                    className="blur-background__back"
                    style={{
                        backgroundImage: `url('${src}')`,
                    }}
                >
                    {gradation ? (
                        <div className="blur-background__back-gradation" />
                    ) : (
                        <div className="blur-background__back-filter" />
                    )}
                </div>
                <div className="blur-background__children" style={{ padding }}>
                    {children}
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(BlurBackground);
