import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import classNames from 'classnames';
import Img from 'react-image';
import tt from 'counterpart';
import ChatBalloon from '@elements/ChatBalloon';
import Responsible from '@modules/Responsible';

class FunctionShowItem extends React.Component {
    static propTypes = {
        backTitle: PropTypes.string,
        title: PropTypes.string,
        text: PropTypes.string,
        balloonText: PropTypes.string,
        src: PropTypes.string,
        reverse: PropTypes.bool,
    };

    static defaultProps = {
        backTitle: '',
        title: '',
        text: '',
        balloonText: '',
        src: '',
        reverse: false,
    };

    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'FunctionShowItem'
        );
    }

    render() {
        const { backTitle, title, text, src, reverse, balloonText } = this.props;

        return (
            <div
                className={classNames('function-show-item', {
                    reverse,
                })}
            >
                <div className="function-show-item__back">{backTitle}</div>
                <div className="function-show-item__fore">
                    <div className="function-show-item__fore__right">
                        <Responsible
                            className="function-show-item__fore__right-balloon scrollin delay1"
                            defaultContent={
                                <ChatBalloon
                                    string={balloonText}
                                    type={reverse ? 'right' : 'left'}
                                />
                            }
                            breakFm={true}
                        />
                        <div className="function-show-item__fore__right-title scrollin delay2">
                            {title}
                        </div>
                        <div className="function-show-item__fore__right-desc scrollin delay3">
                            {text}
                        </div>
                        <Responsible
                            className="function-show-item__fore__right-balloon scrollin delay4"
                            breakingContent={
                                <ChatBalloon
                                    string={balloonText}
                                    type={'bottom'}
                                />
                            }
                            breakFm={true}
                        />
                    </div>
                    <div className="function-show-item__fore__left scrollin">
                        <Img
                            className="function-show-item__fore__left-image"
                            src={src}
                            alt={tt('alts.default')}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(FunctionShowItem);
