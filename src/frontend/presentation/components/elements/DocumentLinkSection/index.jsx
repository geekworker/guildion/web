import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import models from '@view_models';

class DocumentLinkSection extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
    };

    static defaultProps = {
        repository: null,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentLinkSection'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository, locale, group } = this.props;

        if (!repository) return <div />;

        return (
            <div className="document-link-section">
                <h2 className="document-link-section__text">
                    {models.DocumentSection.getTitle(repository, locale)}
                </h2>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentLinkSection);
