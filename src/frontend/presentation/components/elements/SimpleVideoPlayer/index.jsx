import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import videojs from 'video.js';

class SimpleVideoPlayer extends React.Component {
    static propTypes = {
        options: PropTypes.object,
    };

    static defaultProps = {};

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'SimpleVideoPlayer'
        );
    }

    componentDidMount() {
        this.player = videojs(this.videoNode, this.props.options, () => {});
    }

    componentWillReceiveProps(nextProps) {}

    componentWillUnmount() {
        if (this.player) {
            this.player.dispose();
        }
    }

    render() {
        const { src } = this.props;

        return (
            <div className="simple-video-player">
                <div data-vjs-player>
                    <video
                        ref={node => (this.videoNode = node)}
                        className="video-js"
                    />
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(SimpleVideoPlayer);
