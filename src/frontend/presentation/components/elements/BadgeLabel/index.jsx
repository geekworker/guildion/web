import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';

class BadgeLabel extends React.Component {
    static propTypes = {
        string: PropTypes.string,
        color: PropTypes.oneOf(['', 'main']),
    };

    static defaultProps = {
        string: '',
        color: '',
    };

    state = {
        height: 32,
    };

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'BadgeLabel');
    }

    componentDidMount() {
        this.handleHeight();
    }

    componentWillReceiveProps(nextProps) {
        const { string, color } = this.props;
        if (process.env.BROWSER && string != nextProps.string) {
            this.handleHeight();
        }
    }

    handleHeight() {
        if (process.env.BROWSER) {
            const height = document.getElementById('badge-label-container')
                .clientHeight;
            this.setState({ height });
        }
    }

    render() {
        const { string, color } = this.props;

        const { height } = this.state;

        const baseClassName =
            color && color.length > 0 ? `badge-label-${color}` : 'badge-label';

        return (
            <div
                className={baseClassName}
                style={{ borderRadius: `${height / 2}px` }}
                id="badge-label-container"
            >
                <div className={`${baseClassName}__inner`}>
                    <div className={`${baseClassName}__label`}>{string}</div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(BadgeLabel);
