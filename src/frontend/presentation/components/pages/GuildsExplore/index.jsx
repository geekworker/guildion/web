import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import GuildsExploreList from '@cards/GuildsExploreList';
import SiteComponent from '@pages/SiteComponent';
import * as guildActions from '@redux/Guild/GuildReducer';
import models from '@view_models';

class GuildsExplore extends React.Component {
    static propTypes = {
        repositories: PropTypes.arrayOf(
            PropTypes.instanceOf(models.Guild.Instance)
        ),
        heros: PropTypes.arrayOf(PropTypes.instanceOf(models.Guild.Instance)),
    };

    static defaultProps = {
        repositories: [],
        heros: [],
    };

    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'GuildsExplore'
        );
    }

    componentDidMount() {
        const { fetch } = this.props;
        if (fetch) fetch();
    }

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repositories, heros } = this.props;

        return (
            <SiteComponent>
                <GuildsExploreList repositories={repositories} heros={heros} />
            </SiteComponent>
        );
    }
}

module.exports = {
    path: '/guilds/explore',
    component: connect(
        (state, props) => {
            const repositories = guildActions.getWelcomeGuilds(state);

            const heros = guildActions.getHeroGuilds(state);

            return {
                repositories,
                heros,
                locale: state.app.get('locale'),
            };
        },
        dispatch => ({
            fetch: () => {
                dispatch(guildActions.fetchWelcomes({}));
                dispatch(guildActions.fetchHeros({}));
            },
        })
    )(GuildsExplore),
};
