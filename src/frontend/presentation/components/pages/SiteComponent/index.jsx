import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import classNames from 'classnames';
import * as appActions from '@redux/App/AppReducer';
import Header from '@modules/Header';
import Footer from '@modules/Footer';

class SiteComponent extends React.Component {
    static propTypes = {
        children: PropTypes.node,
        style: PropTypes.object,
    };

    static defaultProps = {
        style: {},
    };

    state = {
        offset: 0,
        lastPosition: 0,
    };

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'SiteComponent'
        );
    }

    getScrollIn(offset = window.scrollY) {
        if (!process.env.BROWSER) return;
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByClassName('scrollin'),
            elements = Array.from(g);

        elements.map(element => {
            var rect = element.getBoundingClientRect();
            if (rect.top <= w.innerHeight + 100) {
                element.classList.add('visible');
            }
        });
    }

    handleScroll(e) {
        if (!this.refs.scmpRef) return;
        this.setState({ lastPosition: window.scrollY });

        window.requestAnimationFrame(() => {
            const { lastPosition, offset } = this.state;

            this.getScrollIn(lastPosition);

            this.setState({ offset: lastPosition });
        });
    }

    componentDidMount() {
        if (process.env.BROWSER)
            window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnMount() {
        if (process.env.BROWSER)
            window.removeEventListener('scroll', this.handleScroll);
    }

    componentWillReceiveProps(nextProps) {}

    render() {
        const { style, children, pathname } = this.props;

        return (
            <div className="site-component" ref={'scmpRef'} style={style}>
                <Header pathname={pathname} />
                <div className="site-component__inner">{children}</div>
                <Footer />
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            pathname: state.app.getIn(['location', 'pathname']),
        };
    },

    dispatch => ({})
)(SiteComponent);
