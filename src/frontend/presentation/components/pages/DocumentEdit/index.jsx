import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import * as documentActions from '@redux/Document/DocumentReducer';
import DocumentContent from '@modules/DocumentContent';
import { Set, Map, fromJS, List } from 'immutable';
import models from '@view_models';
import safe2plain from '@extension/safe2plain';
import Header from '@modules/Header';

const AceEditor = process.env.BROWSER ? require('react-ace').default : <div />;

class DocumentEdit extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {
        repository: Map({}),
        html: '',
    };

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentEdit'
        );
    }

    componentDidMount() {
        try {
            require(`ace-builds/src-noconflict/mode-html`);
        } catch (e) {}
        try {
            require(`ace-builds/src-noconflict/snippets/html`);
        } catch (e) {}
        try {
            require(`ace-builds/src-noconflict/theme-monokai`);
        } catch (e) {}
    }

    componentWillReceiveProps(nextProps) {
        if (
            !this.props.repository &&
            !!nextProps.repository &&
            this.props.repository != nextProps.repository
        ) {
            this.setState({
                repository: Map(safe2plain(nextProps.repository)),
                html: `${models.Document.getTitle(
                    nextProps.repository,
                    nextProps.locale
                )}\n\n${models.Document.getHTML(
                    nextProps.repository,
                    nextProps.locale
                )}`,
            });
        }
    }

    onChange(newValue) {
        const { locale, update } = this.props;

        const { repository } = this.state;

        if (!repository) return;

        const _repository = repository.toJS();
        const title = newValue.split('\n')[0];
        switch (locale) {
            case 'ja':
                _repository.ja_html = newValue.replace(`${title}\n`, '') || ' ';
                _repository.ja_title = title || ' ';
                break;
            case 'en':
                _repository.en_html =
                    newValue.replace(`${title}\n`, ' ') || ' ';
                _repository.en_title = title || ' ';
                break;
                break;
        }

        update(_repository);

        this.setState({
            repository: Map(_repository),
            html: newValue,
        });
    }

    render() {
        const { locale } = this.props;

        const { repository, html } = this.state;

        if (!repository) return <div />;

        const _repository = repository.toJS();

        if (!_repository || Object.keys(_repository).length == 0)
            return <div />;

        const options = {
            selectOnLineNumbers: true,
        };

        return (
            <div className="document-edit">
                <Header ticking={false} />
                <div className="document-edit__inner clearfix">
                    <div className="document-edit__left">
                        <DocumentContent repository={_repository} />
                    </div>
                    <div className="document-edit__right">
                        <AceEditor
                            className="document-edit__right-editor"
                            mode="html"
                            theme="monokai"
                            onChange={this.onChange}
                            value={html}
                            name="UNIQUE_ID_OF_DIV"
                            editorProps={{ $blockScrolling: true }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = {
    path: '/document/:groupname/:uid/edit',
    component: connect(
        (state, props) => {
            const repository = documentActions.getDocument(
                state,
                props.routeParams.uid
            );
            return {
                repository,
                locale: state.app.get('locale'),
            };
        },
        dispatch => ({
            update: document => {
                dispatch(documentActions.update({ document }));
            },
        })
    )(DocumentEdit),
};
