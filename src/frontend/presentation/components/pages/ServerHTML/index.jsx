import React from 'react';
import config from '@constants/config';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import tt from 'counterpart';
import { getPageImage } from '@infrastructure/RouteInitialize';
import locale_config from '@constants/locale_config';
import { fallbackLocale } from '@locales';

export default class ServerHTML extends React.Component {
    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'ServerHTML');
    }

    render() {
        const { body, assets, locale, title, pathname } = this.props;

        //let page_title = title;

        return (
            <html lang="en">
                <head>
                    <meta charSet="utf-8" />
                    <meta
                        name="viewport"
                        content="width=device-width, initial-scale=1.0"
                    />
                    {locale_config.available_locales.map((lang, key) => (
                        <link
                            key={key}
                            rel="alternate"
                            hrefLang={lang}
                            href={`${config.CURRENT_APP_URL}/${lang}/`}
                            id={locale_config.makeId(lang)}
                        />
                    ))}
                    <link
                        rel="alternate"
                        hrefLang={'x-default'}
                        href={`${config.CURRENT_APP_URL}/${fallbackLocale}/`}
                        id={locale_config.makeId('x-default')}
                    />
                    <link rel="manifest" href="/manifest.json" />
                    <meta name="application-name" content={config.APP_NAME} />
                    <meta
                        httpEquiv="Content-Security-Policy"
                        content={`default-src *  blob: file: filesystem:;img-src * 'self' data: https: blob: file: filesystem:; script-src 'self' 'unsafe-inline' 'unsafe-eval' *;style-src 'self' 'unsafe-inline' *`}
                    />
                    <link
                        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600"
                        rel="stylesheet"
                        type="text/css"
                    />
                    <link
                        href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600"
                        rel="stylesheet"
                        type="text/css"
                    />
                    {assets.style.map((href, idx) => (
                        <link
                            href={href}
                            key={idx}
                            rel="stylesheet"
                            type="text/css"
                        />
                    ))}
                    <script
                        src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"
                        async
                    />
                    <script
                        async
                        src={`https://www.googletagmanager.com/gtag/js?id=${
                            process.env.GOOGLE_ANALYSTICS_CLIENT
                        }`}
                    />

                    <title>
                        {tt('pages.HomeIndex') + ' | ' + config.APP_NAME}
                    </title>
                    <meta
                        name="description"
                        content={tt('descriptions.HomeIndex')}
                    />

                    <meta name="og:title" content={tt('pages.HomeIndex')} />
                    <meta
                        name="og:description"
                        content={tt('descriptions.HomeIndex')}
                    />
                    <meta name="og:image" content={getPageImage(pathname)} />
                    <meta property="og:url" content={config.CURRENT_APP_URL} />

                    <meta
                        name="twitter:title"
                        content={tt('pages.HomeIndex')}
                    />
                    <meta
                        name="twitter:description"
                        content={tt('descriptions.HomeIndex')}
                    />
                    <meta
                        name="twitter:image"
                        content={getPageImage(pathname)}
                    />
                    <meta name="twitter:card" content={'summary'} />
                </head>
                <body>
                    <div
                        id="content"
                        dangerouslySetInnerHTML={{ __html: body }}
                    />
                    {assets.script.map((href, idx) => (
                        <script key={idx} src={href} />
                    ))}
                </body>
            </html>
        );
    }
}
