import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import tt from 'counterpart';
import { List } from 'immutable';
import constants from '@redux/constants';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import WelcomeList from '@cards/WelcomeList';
import SiteComponent from '@pages/SiteComponent';

class Welcome extends React.Component {
    constructor() {
        super();
        this.state = {};
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'Welcome');
    }

    render() {

        return (
            <SiteComponent>
                <WelcomeList />
            </SiteComponent>
        );
    }
}

module.exports = {
    path: '/welcome',
    component: connect(
        (state, ownProps) => {
            return {};
        },
        dispatch => {
            return {};
        }
    )(Welcome),
};
