import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import DownloadsIndexList from '@cards/DownloadsIndexList';
import SiteComponent from '@pages/SiteComponent';

class DownloadsIndex extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DownloadsIndex'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { os } = this.props.routeParams;
        return (
            <SiteComponent>
                <DownloadsIndexList os={os} />
            </SiteComponent>
        );
    }
}

module.exports = {
    path: '/downloads(/:os)',
    component: connect(
        (state, props) => {
            return {};
        },
        dispatch => ({})
    )(DownloadsIndex),
};
