import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import DocumentSideBar from '@modules/DocumentSideBar';
import DocumentOutline from '@modules/DocumentOutline';
import Header from '@modules/Header';
import Footer from '@modules/Footer';
import {
    DocumentOutlineEntity,
    DocumentOutlineEntities,
} from '@entity/DocumentOutlineEntity';
import models from '@view_models';

class DocumentComponent extends React.Component {
    static propTypes = {
        children: PropTypes.node,
        style: PropTypes.object,
        repository: PropTypes.object,
        group: PropTypes.object,
    };

    static defaultProps = {
        style: {},
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentComponent'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    makeOutlineEntities() {
        const { repository, locale } = this.props;
        if (!repository) return new DocumentOutlineEntities({ html: '' });
        const html = models.Document.getHTML(repository, locale);
        return new DocumentOutlineEntities({ html });
    }

    render() {
        const { style, children, repository, group } = this.props;
        return (
            <div className="document-component" style={style}>
                <Header tickingEnable={false} />
                <div className="document-component__inner" style={style}>
                    <div className="document-component__left">
                        <div className="document-component__left-in">
                            <DocumentSideBar repository={group} />
                        </div>
                    </div>
                    <div className="document-component__wrapper">
                        <div className="document-component__wrapper-in">
                            <div className="document-component__center">
                                <div className="document-component__center-in">
                                    {children}
                                </div>
                            </div>
                            <div className="document-component__right">
                                <div className="document-component__right-in">
                                    <DocumentOutline
                                        repositories={this.makeOutlineEntities()}
                                    />
                                </div>
                            </div>
                        </div>
                        <Footer />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentComponent);
