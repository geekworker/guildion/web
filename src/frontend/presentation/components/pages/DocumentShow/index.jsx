import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import DocumentComponent from '@pages/DocumentComponent';
import DocumentShowList from '@cards/DocumentShowList';
import * as documentActions from '@redux/Document/DocumentReducer';

class DocumentShow extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentShow'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { groupname, uid } = this.props.routeParams;
        const { repository, group } = this.props;

        return (
            <DocumentComponent repository={repository} group={group}>
                <DocumentShowList
                    groupname={groupname}
                    uid={uid}
                    repository={repository}
                    group={group}
                />
            </DocumentComponent>
        );
    }
}

module.exports = {
    path: '/document/:groupname/:uid',
    component: connect(
        (state, props) => {
            const repository = documentActions.getDocument(
                state,
                props.routeParams.uid
            );
            const group = documentActions.getDocumentGroup(
                state,
                props.routeParams.groupname
            );
            return {
                repository,
                group,
            };
        },
        dispatch => ({})
    )(DocumentShow),
};
