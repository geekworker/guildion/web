import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import PremiumIndexList from '@cards/PremiumIndexList';
import SiteComponent from '@pages/SiteComponent';

class PremiumIndex extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'PremiumIndex'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        return (
            <SiteComponent>
                <PremiumIndexList />
            </SiteComponent>
        );
    }
}

module.exports = {
    path: '/premium',
    component: connect(
        (state, props) => {
            return {};
        },
        dispatch => ({})
    )(PremiumIndex),
};
