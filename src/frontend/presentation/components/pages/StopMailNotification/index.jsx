import React from 'react';
import { Link } from 'react-router';
import Icon from '@elements/Icon';
import * as routes from '@infrastructure/RouteInitialize';
import Img from 'react-image';
import tt from 'counterpart';
import SiteComponent from '@pages/SiteComponent';
import config from '@constants/config';

class StopMailNotification extends React.Component {
    render() {
        return (
            <SiteComponent>
                <div className="not-found">
                    <div className="not-found__body">
                        <div className="not-found__body__logo">
                            <Img
                                className="not-found__body__logo-image"
                                src={`${
                                    config.IMG_MAIN_PROXY
                                }/public/images/brands/logo.png`}
                                alt={tt('alts.default')}
                            />
                        </div>
                        <div className="not-found__body__title">
                            {tt('g.stop_mail_notification.title')}
                        </div>
                        <div className="not-found__body__text">
                            {tt('g.stop_mail_notification.text')}
                        </div>
                        <Link className="not-found__body__to-home" to={'/'}>
                            {'Home →'}
                        </Link>
                    </div>
                </div>
            </SiteComponent>
        );
    }
}

module.exports = {
    path: '/user/mail/notification',
    component: StopMailNotification,
};
