import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import * as guildActions from '@redux/Guild/GuildReducer';
import { Set, Map, fromJS, List } from 'immutable';
import models from '@view_models';
import safe2plain from '@extension/safe2plain';
import Header from '@modules/Header';
import Footer from '@modules/Footer';
import GuildWelcomeList from '@cards/GuildWelcomeList';

class GuildWelcome extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {
        repository: Map({}),
    };

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'GuildWelcome'
        );
    }

    componentDidMount() {
        const { routeParams, fetch } = this.props;

        if (routeParams.uid && fetch) fetch(routeParams.uid);
    }

    componentWillReceiveProps(nextProps) {}

    render() {
        const { locale, repository } = this.props;

        if (!repository) return <div />;

        return (
            <div className="guild-welcome">
                <Header ticking={true} localeSelectable={false} />
                <div className="guild-welcome__inner clearfix">
                    <GuildWelcomeList repository={repository} />
                </div>
                <Footer localeSelectable={false} />
            </div>
        );
    }
}

module.exports = {
    path: '/guild/welcome/:uid',
    component: connect(
        (state, props) => {
            const repository = guildActions.getWelcomeGuild(
                state,
                props.routeParams.uid
            );

            return {
                repository,
                locale: state.app.get('locale'),
            };
        },
        dispatch => ({
            fetch: uid => {
                dispatch(guildActions.fetchWelcome({ uid }));
            },
        })
    )(GuildWelcome),
};
