import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import SiteComponent from '@pages/SiteComponent';
import * as documentActions from '@redux/Document/DocumentReducer';
import { DOCUMENT_GROUP_TEMPLATE_TYPE } from '@entity/DocumentGroupTemplateEntity';
import DocumentGroupLinksList from '@cards/DocumentGroupLinksList';

class DocumentGroupShow extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentGroupShow'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { groupname } = this.props.routeParams;
        const { repository } = this.props;

        if (!repository) return <SiteComponent />;

        switch (DOCUMENT_GROUP_TEMPLATE_TYPE.all.find(
            val => val.value == repository.template
        )) {
            default:
            case DOCUMENT_GROUP_TEMPLATE_TYPE.Links:
                return (
                    <SiteComponent>
                        <DocumentGroupLinksList repository={repository} />
                    </SiteComponent>
                );
        }
    }
}

module.exports = {
    path: '/document/:groupname',
    component: connect(
        (state, props) => {
            const repository = documentActions.getDocumentGroup(
                state,
                props.routeParams.groupname
            );
            return {
                repository,
            };
        },
        dispatch => ({})
    )(DocumentGroupShow),
};
