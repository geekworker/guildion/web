import React from 'react';
import { Link } from 'react-router';
import Img from 'react-image';
import tt from 'counterpart';
import SiteComponent from '@pages/SiteComponent';
import config from '@constants/config';

class NotFound extends React.Component {
    render() {
        return (
            //<SiteComponent>
            <div className="not-found">
                <div className="not-found__body">
                    <div className="not-found__body__logo">
                        <Img
                            className="not-found__body__logo-image"
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/logo.png`}
                            alt={tt('alts.default')}
                        />
                    </div>
                    <div className="not-found__body__title">
                        {'Sorry, that page doesn’t exist!'}
                    </div>
                    <Link className="not-found__body__to-home" to="/">
                        {'Home →'}
                    </Link>
                </div>
            </div>
            //</SiteComponent>
        );
    }
}

module.exports = {
    path: '*',
    component: NotFound,
};
