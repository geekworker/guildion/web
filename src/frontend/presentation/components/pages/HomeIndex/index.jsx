import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import * as appActions from '@redux/App/AppReducer';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import classNames from 'classnames';
import WelcomeList from '@cards/WelcomeList';
import SiteComponent from '@pages/SiteComponent';
import config from '@constants/config';

class HomeIndex extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'HomeIndex');
    }

    componentDidMount() {
        window.location.replace(`${config.CURRENT_APP_URL}/${this.props.locale}/welcome`);
    }

    render() {
        return (
            <SiteComponent>
                <WelcomeList/>
            </SiteComponent>
        );
    }
}

module.exports = {
    path: '/',
    component: connect(
        (state, props) => {
            return {
                locale: state.app.get('locale'),
            };
        },
        dispatch => ({})
    )(HomeIndex),
};
