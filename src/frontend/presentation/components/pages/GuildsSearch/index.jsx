import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import * as appActions from '@redux/App/AppReducer';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import classNames from 'classnames';
import GuildsSearchList from '@cards/GuildsSearchList';
import SiteComponent from '@pages/SiteComponent';

class GuildsSearch extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'GuildsSearch'
        );
    }

    render() {
        var { q } = this.props.routeParams;

        if (!q) section = '';

        return (
            <SiteComponent>
                <GuildsSearchList q={q} />
            </SiteComponent>
        );
    }
}

module.exports = {
    path: '/guilds/search/:q',
    component: connect(
        (state, props) => {
            return {};
        },
        dispatch => ({})
    )(GuildsSearch),
};
