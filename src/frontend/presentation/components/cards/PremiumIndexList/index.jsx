import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import classNames from 'classnames';
import tt from 'counterpart';
import Img from 'react-image';
import {
    SUBSCRIPTION_SPAN,
    SUBSCRIPTION_PLAN,
    PLAN_FUNCTION,
} from '@constants/plan_config';
import { SubscriptionPlanEntity } from '@entity/SubscriptionPlanEntity';
import DropDown from '@elements/DropDown';
import PlanItem from '@modules/PlanItem';
import BadgeLabel from '@elements/BadgeLabel';
import SimpleButton from '@elements/SimpleButton';
import { Map, List } from 'immutable';
import {
    premiumIndexRoute,
    downloadsIndexRoute,
} from '@infrastructure/RouteInitialize';
import { current_platform } from '@entity/PlatformEntity';
import config from '@constants/config';

class PremiumIndexList extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {
        offset: 0,
        lastPosition: 0,
        repository: Map(new SubscriptionPlanEntity({})),
    };

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'PremiumIndexList'
        );
    }

    getScrollIn(offset = window.scrollY) {
        if (!process.env.BROWSER) return;
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByClassName('scrollin'),
            elements = Array.from(g);

        elements.map(element => {
            var rect = element.getBoundingClientRect();
            if (rect.top <= w.innerHeight / 5 * 4) {
                element.classList.add('visible');
            }
        });
    }

    handleScroll(e) {
        this.setState({ lastPosition: window.scrollY });

        window.requestAnimationFrame(() => {
            const { lastPosition, offset } = this.state;

            this.getScrollIn(lastPosition);

            this.setState({ offset: lastPosition });
        });
    }

    componentDidMount() {
        if (process.env.BROWSER)
            window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        if (process.env.BROWSER)
            window.removeEventListener('scroll', this.handleScroll);
    }

    onChangePlan(e) {
        let { repository } = this.state;
        repository = repository.toJS();
        repository.plan = e.value;
        this.setState({ repository: Map(repository) });
    }

    getPlanOption() {
        return SUBSCRIPTION_PLAN.all.map(plan => {
            return {
                value: plan,
                label: plan.name(),
            };
        });
    }

    makePlanOption(plan) {
        if (!plan) return;
        return {
            value: plan,
            label: plan.name(),
        };
    }

    render() {
        let { repository } = this.state;

        repository = repository.toJS();

        const renderingPlanSpans = items =>
            items.map((item, key) => (
                <div className="premium-index-list__plans-span" key={key}>
                    <div className="premium-index-list__plans-span__inner">
                        <PlanItem
                            repository={repository}
                            span={item}
                            key={`p${key}`}
                            onClick={e => {
                                let { repository } = this.state;
                                repository = repository.toJS();
                                repository.span = item;
                                this.setState({ repository: Map(repository) });
                            }}
                        />
                        {repository.span == item && (
                            <div className="premium-index-list__plans-span__badge">
                                <BadgeLabel
                                    string={tt('premium.trial')}
                                    color="main"
                                />
                            </div>
                        )}
                    </div>
                </div>
            ));

        const header = (
            <div className="premium-index-list__header">
                <div className="premium-index-list__header__inner">
                    <div className="premium-index-list__header-logo clearfix">
                        <Img
                            className="premium-index-list__header-logo__image"
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/brand-logo.png`}
                        />
                        <div className="premium-index-list__header-logo__label">
                            Premium
                        </div>
                    </div>
                    <div className="premium-index-list__header-title">
                        {tt('premium.title')}
                    </div>
                    <div className="premium-index-list__header-desc">
                        {tt('premium.desc')}
                    </div>
                </div>
            </div>
        );

        const plans = (
            <div className="premium-index-list__plans">
                <div className="premium-index-list__plans__inner">
                    <div className="premium-index-list__plans-select">
                        <label className="premium-index-list__plans-select-label">
                            {tt('premium.select_plan')}
                        </label>
                        <DropDown
                            placeholder={tt('premium.select_plan')}
                            onChange={this.onChangePlan}
                            options={this.getPlanOption()}
                            value={
                                repository.plan &&
                                this.makePlanOption(repository.plan)
                            }
                        />
                    </div>
                    <div className="premium-index-list__plans-spans">
                        {renderingPlanSpans(SUBSCRIPTION_SPAN.all)}
                    </div>
                    <div className="premium-index-list__plans-desc">
                        {tt('premium.trial_after')}
                    </div>
                    <div className="premium-index-list__plans-button">
                        <SimpleButton value={tt('premium.start')} />
                    </div>
                </div>
            </div>
        );

        const table_header = items =>
            items.map((item, key) => (
                <div
                    className={classNames(
                        key == 0
                            ? 'premium-index-list__table__header-colomn-first'
                            : 'premium-index-list__table__header-colomn',
                        { active: repository.plan == item }
                    )}
                    key={key}
                >
                    <div
                        className={classNames(
                            'premium-index-list__table__header-colomn__title',
                            { active: repository.plan == item }
                        )}
                    >
                        {item.name()}
                    </div>
                    <div
                        className={classNames(
                            'premium-index-list__table__header-colomn__body',
                            { active: repository.plan == item }
                        )}
                    >
                        {`${SUBSCRIPTION_SPAN.Month1.priceString(item)}/${tt(
                            'g.month'
                        )}`}
                    </div>
                </div>
            ));

        const table_row_cell = (items, plan_function) =>
            items.map((item, key) => (
                <div
                    className={classNames(
                        'premium-index-list__table__container-row__cell',
                        { active: repository.plan == item }
                    )}
                    key={key}
                >
                    {item.func_value(plan_function)}
                </div>
            ));

        const table_row = items =>
            items.map((item, key) => (
                <div
                    className="premium-index-list__table__container-row"
                    key={key}
                >
                    <div className="premium-index-list__table__container-row__header">
                        {item.string()}
                    </div>
                    {table_row_cell(SUBSCRIPTION_PLAN.all, item)}
                </div>
            ));

        const table = (
            <div className="premium-index-list__table">
                <div className="premium-index-list__table__inner">
                    <div className="premium-index-list__table__header">
                        {table_header(SUBSCRIPTION_PLAN.all)}
                    </div>
                    <div className="premium-index-list__table__container">
                        {table_row(PLAN_FUNCTION.all)}
                    </div>
                </div>
            </div>
        );

        const finally_body = (
            <div className="welcome-list__finally">
                <div className="welcome-list__finally__inner clearfix">
                    <div className="welcome-list__finally-left scrollin delay1">
                        <Img
                            alt={tt('alts.default')}
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/en/ogp.png`}
                            className="welcome-list__finally-left__image"
                        />
                    </div>
                    <div className="welcome-list__finally-right">
                        <Img
                            alt={tt('alts.default')}
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/white-logo.png`}
                            className="welcome-list__finally-right__logo scrollin delay2"
                        />
                        <div className="welcome-list__finally-right__title scrollin delay3">
                            {tt('welcome.let_start')}
                        </div>
                        <div className="welcome-list__finally-right__start-button scrollin delay4">
                            <SimpleButton
                                value={tt('welcome.start_title')}
                                color={'white'}
                            />
                        </div>
                        <Link
                            to={downloadsIndexRoute.getPath({
                                params: {
                                    os: current_platform().value,
                                },
                            })}
                            className="welcome-list__finally-right__download-button scrollin delay4"
                        >
                            {tt('welcome.download_title')}
                        </Link>
                    </div>
                </div>
            </div>
        );

        return (
            <div className="premium-index-list">
                {header}
                {plans}
                {table}
                {finally_body}
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(PremiumIndexList);
