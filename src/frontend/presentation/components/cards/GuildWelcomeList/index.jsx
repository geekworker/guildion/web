import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import Img from 'react-image';
import classNames from 'classnames';
import models from '@view_models';
import BlurBackground from '@elements/BlurBackground';
import Responsible from '@modules/Responsible';
import SimpleButton from '@elements/SimpleButton';
import AppStoreButton from '@elements/AppStoreButton';
import Icon from '@elements/Icon';
import config from '@constants/config';
import { guildWelcomeRoute } from '@infrastructure/RouteInitialize';
import {
    IOS_TESTFLIGHT_URL,
    IOS_TESTFLIGHT_MODE,
} from '@constants/testflight_config';

class GuildWelcomeList extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
    };

    static defaultProps = {
        repository: null,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'GuildWelcomeList'
        );
    }

    componentWillMount() {}

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    onClickJoin(e) {
        const { repository } = this.props;

        window.location = `${config.IOS_SCHEME}${guildWelcomeRoute.getPath({
            params: { uid: repository.uid },
        })}`;
        setTimeout(function() {
            window.location = IOS_TESTFLIGHT_MODE
                ? IOS_TESTFLIGHT_URL
                : config.IOS_DOWNLOAD_URL;
        }, 1000);
    }

    render() {
        const { repository } = this.props;

        if (!repository || !repository.dataValues) return <div />;

        const header = (
            <section className="guild-welcome-list__header">
                <BlurBackground
                    src={repository.dataValues.picture_small}
                    padding={'0 0 0 0'}
                >
                    <div className="guild-welcome-list__header__profile">
                        <Img
                            className="guild-welcome-list__header__profile-image"
                            src={repository.dataValues.picture_small}
                            alt={tt('alts.default')}
                        />
                    </div>
                </BlurBackground>
            </section>
        );

        const content = (
            <div className="guild-welcome-list__content">
                <div className="guild-welcome-list__content__inner">
                    <h1 className="guild-welcome-list__content-name">
                        {repository.dataValues.name}
                    </h1>
                    <h2 className="guild-welcome-list__content-description">
                        {repository.dataValues.description == ''
                            ? tt('guild.no_description')
                            : repository.dataValues.description}
                    </h2>
                </div>
            </div>
        );

        const footer = (
            <div className="guild-welcome-list__footer">
                <div className="guild-welcome-list__footer__inner">
                    <div className="guild-welcome-list__footer-profile">
                        <Img
                            className="guild-welcome-list__footer-profile-image"
                            src={repository.dataValues.picture_small}
                            alt={tt('alts.default')}
                        />
                    </div>
                    <div className="guild-welcome-list__footer-name">
                        {repository.dataValues.name}
                    </div>
                    <div className="guild-welcome-list__footer-button">
                        <SimpleButton
                            value={tt('guild.join')}
                            onClick={this.onClickJoin}
                        />
                    </div>
                </div>
            </div>
        );

        return (
            <div className="guild-welcome-list">
                {header}
                {content}
                {footer}
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(GuildWelcomeList);
