import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import Img from 'react-image';
import AppStoreButton from '@elements/AppStoreButton';
import SimpleButton from '@elements/SimpleButton';
import { PLATFORM_TYPE, current_platform } from '@entity/PlatformEntity';
import PlatformItem from '@modules/PlatformItem';
import classNames from 'classnames';
import {
    IOS_TESTFLIGHT_URL,
    IOS_TESTFLIGHT_MODE,
} from '@constants/testflight_config';

class DownloadsIndexList extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {
        offset: 0,
        lastPosition: 0,
    };

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DownloadsIndexList'
        );
    }

    getScrollIn(offset = window.scrollY) {
        if (!process.env.BROWSER) return;
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByClassName('scrollin'),
            elements = Array.from(g);

        elements.map(element => {
            var rect = element.getBoundingClientRect();
            if (rect.top <= w.innerHeight / 5 * 4) {
                element.classList.add('visible');
            }
        });
    }

    handleScroll(e) {
        this.setState({ lastPosition: window.scrollY });

        window.requestAnimationFrame(() => {
            const { lastPosition, offset } = this.state;

            this.getScrollIn(lastPosition);

            this.setState({ offset: lastPosition });
        });
    }

    componentDidMount() {
        if (process.env.BROWSER)
            window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        if (process.env.BROWSER)
            window.removeEventListener('scroll', this.handleScroll);
    }

    componentWillReceiveProps(nextProps) {}

    render() {
        const { os, repository } = this.props;

        let title = '';
        let desc = '';
        let button_text = '';
        switch (repository) {
            case PLATFORM_TYPE.Web:
                title = tt('downloads.title');
                desc = tt('downloads.developing_title', {
                    data: PLATFORM_TYPE.Web.translate(),
                });
                // desc = tt('downloads.available', { data: PLATFORM_TYPE.Web.translate() });
                button_text = tt('g.to_site');
                break;
            case PLATFORM_TYPE.iOS:
                title = tt('downloads.title');
                desc = tt('downloads.testflighting_title', {
                    data: PLATFORM_TYPE.iOS.translate(),
                });
                // desc = tt('downloads.downloadable', { data: PLATFORM_TYPE.iOS.translate() });
                break;
            case PLATFORM_TYPE.Android:
                title = tt('downloads.title');
                desc = tt('downloads.developing_title', {
                    data: PLATFORM_TYPE.Android.translate(),
                });
                break;
            case PLATFORM_TYPE.MacBook:
                title = tt('downloads.title');
                desc = tt('downloads.developing_title', {
                    data: PLATFORM_TYPE.MacBook.translate(),
                });
                break;
        }

        const renderItems = (
            items = PLATFORM_TYPE.all.filter(val => val != repository)
        ) =>
            items.map((item, key) => (
                <div className="downloads-index-list__item" key={key}>
                    <PlatformItem repository={item} />
                </div>
            ));

        const header = (
            <div className="downloads-index-list__header">
                {!!repository && (
                    <div className="downloads-index-list__header-foreground">
                        <Img
                            src={repository.trim_thumbnail}
                            className={classNames(
                                'downloads-index-list__header-foreground__image',
                                {
                                    'web-image':
                                        repository == PLATFORM_TYPE.Web,
                                    'macbook-image':
                                        repository == PLATFORM_TYPE.MacBook,
                                }
                            )}
                        />
                        <div className="downloads-index-list__header-foreground__container">
                            <div className="downloads-index-list__header-foreground__container-text">
                                {title}
                            </div>
                            <div className="downloads-index-list__header-foreground__container-desc">
                                {desc}
                            </div>
                            {repository == PLATFORM_TYPE.iOS &&
                                repository.available && (
                                    <div
                                        className="downloads-index-list__header-foreground__container-app-button"
                                        style={{
                                            display: repository.available
                                                ? 'block'
                                                : 'none',
                                        }}
                                    >
                                        <AppStoreButton />
                                    </div>
                                )}
                            {repository == PLATFORM_TYPE.Web &&
                                repository.available && (
                                    <div className="downloads-index-list__header-foreground__container-button">
                                        <SimpleButton
                                            color="white"
                                            url={'/'}
                                            value={button_text}
                                        />
                                    </div>
                                )}
                        </div>
                    </div>
                )}
            </div>
        );

        return (
            <div className="downloads-index-list">
                {header}
                <div className="downloads-index-list__items">
                    {renderItems()}
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            repository: PLATFORM_TYPE.all.find(val => val.value == props.os),
        };
    },

    dispatch => ({})
)(DownloadsIndexList);
