import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import DocumentLinkSection from '@elements/DocumentLinkSection';
import DocumentLinkItem from '@elements/DocumentLinkItem';
import models from '@view_models';

class DocumentGroupLinksList extends React.Component {
    static propTypes = {
        repository: PropTypes.object,
    };

    static defaultProps = {
        repository: null,
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'DocumentGroupLinksList'
        );
    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    render() {
        const { repository, locale } = this.props;

        if (!repository) return <div />;

        const renderItem = items =>
            items.map((item, key) => (
                <div className="document-group-links-list__item" key={key}>
                    <DocumentLinkItem repository={item} group={repository} />
                </div>
            ));

        const renderSection = items =>
            items.map((item, key) => (
                <div className="document-group-links-list__section" key={key}>
                    <div className="document-group-links-list__section-item">
                        <DocumentLinkSection
                            repository={item}
                            group={repository}
                        />
                    </div>
                    <div className="document-group-links-list__items">
                        {renderItem(item.Documents)}
                    </div>
                </div>
            ));

        return (
            <div className="document-group-links-list">
                <div className="document-group-links-list__header">
                    <div className="document-group-links-list__header-title">
                        {models.DocumentGroup.getTitle(repository, locale)}
                    </div>
                </div>
                <div className="document-group-links-list__inner">
                    <div className="document-group-links-list__sections">
                        {renderSection(repository.Sections)}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {
            locale: state.app.get('locale'),
        };
    },

    dispatch => ({})
)(DocumentGroupLinksList);
