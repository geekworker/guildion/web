import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import SearchInput from '@elements/SearchInput';
import { welcomeRoute } from '@infrastructure/RouteInitialize';
import Img from 'react-image';
import config from '@constants';

class GuildsSearchList extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'GuildsSearchList'
        );
    }

    componentWillMount() {}

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    handleRequestSearch = e => {
        if (!e || e == '') return;
        // browserHistory.push(
        //     discordServerSearchRoute.getPath({
        //         params: {
        //             q: e,
        //         },
        //     })
        // );
    };

    render() {
        const header = (
            <div className="guilds-search-list__header">
                <Link
                    to={welcomeRoute.getPath()}
                    className="guilds-search-list__header-link"
                >
                    <Img
                        src={`${
                            config.IMG_MAIN_PROXY
                        }/public/images/brands/white-logo.png`}
                        className="guilds-search-list__header-image"
                        alt={tt('alts.default')}
                    />
                </Link>
                <div className="guilds-search-list__header-search">
                    <SearchInput onRequestSearch={this.handleRequestSearch} />
                </div>
            </div>
        );

        return <div className="guilds-search-list" />;
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(GuildsSearchList);
