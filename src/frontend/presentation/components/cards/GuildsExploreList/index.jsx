import React from 'react';
import PropTypes from 'prop-types';
import AppPropTypes from '@extension/AppPropTypes';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import autobind from 'class-autobind';
import tt from 'counterpart';
import ParticleBackground from '@modules/ParticleBackground';
import Img from 'react-image';
import { welcomeRoute } from '@infrastructure/RouteInitialize';
import config from '@constants/config';
import SearchInput from '@elements/SearchInput';
import GuildHerosContainer from '@modules/GuildHerosContainer';
import BlurBackground from '@elements/BlurBackground';
import models from '@view_models';
import * as guildActions from '@redux/Guild/GuildReducer';
import GuildWelcomeItem from '@modules/GuildWelcomeItem';

class GuildsExploreList extends React.Component {
    static propTypes = {
        repositories: PropTypes.arrayOf(
            PropTypes.instanceOf(models.Guild.Instance)
        ),
        heros: PropTypes.arrayOf(PropTypes.instanceOf(models.Guild.Instance)),
    };

    static defaultProps = {
        repositories: [],
        heros: [],
    };

    state = {};

    constructor(props) {
        super(props);
        autobind(this);
        this.shouldComponentUpdate = shouldComponentUpdate(
            this,
            'GuildsExploreList'
        );
    }

    componentWillMount() {}

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    handleRequestSearch = e => {
        if (!e || e == '') return;
        // browserHistory.push(
        //     discordServerSearchRoute.getPath({
        //         params: {
        //             q: e,
        //         },
        //     })
        // );
    };

    render() {
        const { repositories, heros } = this.props;

        const header = (
            <div className="guilds-explore-list__header">
                <BlurBackground
                    src={`${
                        config.IMG_MAIN_PROXY
                    }/public/images/brands/brand-dark-white-transparent-mini-logo%40landscape.png`}
                    padding={'0 0 0 0'}
                >
                    <div className="guilds-explore-list__header-heros">
                        <div className="guilds-explore-list__header-hero">
                            <GuildHerosContainer
                                repositories={heros}
                                direction={'left'}
                            />
                        </div>
                        <div className="guilds-explore-list__header-hero">
                            <GuildHerosContainer
                                repositories={heros}
                                direction={'right'}
                            />
                        </div>
                    </div>
                    <div className="guilds-explore-list__header-title">
                        {tt('explore.title')}
                    </div>
                    <div className="guilds-explore-list__header-desc">
                        {tt('explore.desc')}
                    </div>
                    <div className="guilds-explore-list__header-search">
                        <SearchInput
                            onRequestSearch={this.handleRequestSearch}
                        />
                    </div>
                </BlurBackground>
            </div>
        );

        const renderItems = items =>
            items.map((item, key) => (
                <div
                    className="guilds-explore-list__body-item"
                    style={{
                        gridColomn: (key + 1) / 4,
                        gridRow: (key + 1) % 5,
                    }}
                    key={key}
                >
                    <GuildWelcomeItem repository={item} />
                </div>
            ));

        const body = (
            <div className="guilds-explore-list__body">
                <div className="guilds-explore-list__body-items">
                    {renderItems(repositories)}
                </div>
            </div>
        );

        return (
            <div className="guilds-explore-list">
                {header}
                {body}
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },

    dispatch => ({})
)(GuildsExploreList);
