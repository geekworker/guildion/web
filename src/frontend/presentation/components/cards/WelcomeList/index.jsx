import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shouldComponentUpdate from '@extension/shouldComponentUpdate';
import Icon from '@elements/Icon';
import tt from 'counterpart';
import Responsible from '@modules/Responsible';
import Img from 'react-image';
import SimpleButton from '@elements/SimpleButton';
import * as appActions from '@redux/App/AppReducer';
import { sleep } from '@extension/sleep';
import AppStoreButton from '@elements/AppStoreButton';
import ScrollNavigator from '@elements/ScrollNavigator';
import image_config from '@constants/image_config';
import AnimateGradation from '@elements/AnimateGradation';
import autobind from 'class-autobind';
import FunctionShowItem from '@elements/FunctionShowItem';
import {
    premiumIndexRoute,
    downloadsIndexRoute,
} from '@infrastructure/RouteInitialize';
import { current_platform } from '@entity/PlatformEntity';
import SimpleVideoPlayer from '@elements/SimpleVideoPlayer';
import models from '@view_models';
import { Set, Map, fromJS, List } from 'immutable';
import { scrollTo } from '@extension/scroll';
import TypingIndicator from '@modules/TypingIndicator';
import config from '@constants/config';
import {
    IOS_TESTFLIGHT_URL,
    IOS_TESTFLIGHT_MODE,
} from '@constants/testflight_config';

class WelcomeList extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    static chats_scrollable_id = 'welcome-list-card__scrollable';

    state = {
        offset: 0,
        lastPosition: 0,
        chats: List([]), // { text: '', User: models }
        users: List([]),
    };

    constructor(props) {
        super(props);
        this.shouldComponentUpdate = shouldComponentUpdate(this, 'WelcomeList');
        autobind(this);
    }

    componentDidMount() {
        this.startChatAnimation();
    }

    componentWillUnmount() {
        if (this.timer) clearInterval(this.timer);
    }

    componentWillReceiveProps(nextProps) {}

    startChatAnimation() {
        const seed = tt('welcome.seed')[
            Math.floor(Math.random() * Math.floor(tt('welcome.seed').length))
        ];
        const master_chats = seed['chats'];
        const master_users = [
            models.User.build({
                nickname: 'noah',
                picture_small: `${
                    config.IMG_MAIN_PROXY
                }/public/images/profiles/samples/man1.jpg`,
            }),
            models.User.build({
                nickname: 'ann',
                picture_small: `${
                    config.IMG_MAIN_PROXY
                }/public/images/profiles/samples/woman1.jpg`,
            }),
        ];
        const master_chats_length = seed['chats'].length;
        const master_users_length = master_users.length;
        this.setState({
            chats: List([
                {
                    User: master_users[0 % master_users_length],
                    text: master_chats[0],
                },
            ]),
            users: List(master_users.map(val => val.get({ plain: true }))),
        });

        this.timer = setInterval(() => {
            if (!this.refs.cardRef) return;
            const chats_list = this.state.chats;
            const chats_arr = chats_list.toJS();
            if (chats_arr.length >= master_chats_length) {
                clearInterval(this.timer);
                return;
            }
            chats_arr.push({
                User: master_users[chats_arr.length % master_users_length],
                text: master_chats[chats_arr.length],
            });
            this.setState({ chats: List(chats_arr) });
            const element = document.getElementById(
                WelcomeList.chats_scrollable_id
            );
            scrollTo(element, element.scrollHeight, 1000);
        }, 3000);
    }

    render() {
        const { chats, users } = this.state;
        const chats_arr = chats.toJS();
        const users_arr = users.toJS();

        const videoJsOptions = {
            autoplay: true,
            controls: false,
            playsinline: true,
            roop: true,
            muted: true,
            preload: 'auto',
            sources: [
                {
                    src: `${
                        config.IMG_MAIN_PROXY
                    }/public/movies/watch-party-sample1.mp4`,
                    type: 'video/mp4',
                },
            ],
        };

        const dummyChats = items =>
            items.map((item, key) => (
                <div
                    className="welcome-list__card-item clearfix fade-in--1"
                    key={key}
                >
                    <div className="welcome-list__card-item__left">
                        <Img
                            src={item.User.picture_small}
                            className="welcome-list__card-item__left-image"
                        />
                    </div>
                    <div className="welcome-list__card-item__right">
                        <div className="welcome-list__card-item__right-name">
                            {item.User.nickname}
                        </div>
                        <div className="welcome-list__card-item__right-chat">
                            {item.text}
                        </div>
                    </div>
                </div>
            ));

        const card = (
            <div className="welcome-list__card" ref="cardRef">
                <div className="welcome-list__card-name">
                    {tt('welcome.seed')[0].channelname}
                </div>
                <div className="welcome-list__card-video">
                    <SimpleVideoPlayer options={videoJsOptions} />
                </div>
                <div className="welcome-list__card-items">
                    <div
                        className="welcome-list__card-items__inner"
                        id={WelcomeList.chats_scrollable_id}
                    >
                        {chats_arr &&
                            chats_arr.length > 0 &&
                            dummyChats(chats_arr)}
                    </div>
                </div>
                <div className="welcome-list__card-indicator">
                    <TypingIndicator repositories={users_arr || []} />
                </div>
            </div>
        );

        const header = (
            <div className="welcome-list__header">
                <div className="welcome-list__header__background" />
                <div className="welcome-list__header__foreground clearfix">
                    <div className="welcome-list__header__foreground-left">
                        <div className="welcome-list__header__foreground-left__title">
                            {tt('welcome.title')}
                        </div>
                        <div className="welcome-list__header__foreground-left__desc">
                            {tt('welcome.desc')}
                        </div>
                        {IOS_TESTFLIGHT_MODE ? (
                            <div className="welcome-list__header__foreground-left__ios-button">
                                <AppStoreButton />
                            </div>
                        ) : (
                            <div>
                                <div className="welcome-list__header__foreground-left__start-button">
                                    <SimpleButton
                                        value={tt('welcome.start_title')}
                                        color={'white'}
                                    />
                                </div>
                                <Link
                                    to={downloadsIndexRoute.getPath({
                                        params: {
                                            os: current_platform().value,
                                        },
                                    })}
                                    className="welcome-list__header__foreground-left__download-button"
                                >
                                    {tt('welcome.download_title')}
                                </Link>
                            </div>
                        )}
                    </div>
                    <div className="welcome-list__header__foreground-right">
                        <div className="welcome-list__header__foreground-right__cards">
                            <div className="welcome-list__header__foreground-right__card2" />
                            <div className="welcome-list__header__foreground-right__card1" />
                            <div className="welcome-list__header__foreground-right__card">
                                {card}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        const functoins_body = (
            <div className="welcome-list__function">
                <div className="welcome-list__function-items">
                    <div className="welcome-list__function-item scrollin">
                        <FunctionShowItem
                            title={tt('welcome.functions.watch_party.title')}
                            text={tt('welcome.functions.watch_party.desc')}
                            balloonText={tt(
                                'welcome.functions.watch_party.tip'
                            )}
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/en/iPhone-X_stream-channel.png`}
                            backTitle={''}
                            reverse={true}
                        />
                    </div>
                    <div className="welcome-list__function-item scrollin">
                        <FunctionShowItem
                            title={tt('welcome.functions.guild.title')}
                            text={tt('welcome.functions.guild.desc')}
                            balloonText={tt('welcome.functions.guild.tip')}
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/en/iPhone-X_guild.png`}
                            backTitle={''}
                            reverse={false}
                        />
                    </div>
                    <div className="welcome-list__function-item scrollin">
                        <FunctionShowItem
                            title={tt('welcome.functions.background.title')}
                            text={tt('welcome.functions.background.desc')}
                            balloonText={tt('welcome.functions.background.tip')}
                            help={tt('welcome.functions.background.help')}
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/en/iPhone-X_background.png`}
                            backTitle={''}
                            reverse={true}
                        />
                    </div>
                    <div className="welcome-list__function-item scrollin">
                        <FunctionShowItem
                            title={tt('welcome.functions.youtube.title')}
                            text={tt('welcome.functions.youtube.desc')}
                            balloonText={tt('welcome.functions.youtube.tip')}
                            help={tt('welcome.functions.youtube.help')}
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/en/iPhone-X_file-new.png`}
                            backTitle={''}
                            reverse={false}
                        />
                    </div>
                </div>
            </div>
        );

        const finally_body = (
            <div className="welcome-list__finally">
                <div className="welcome-list__finally__inner clearfix">
                    <div className="welcome-list__finally-left scrollin delay1">
                        <Img
                            alt={tt('alts.default')}
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/en/ogp.png`}
                            className="welcome-list__finally-left__image"
                        />
                    </div>
                    <div className="welcome-list__finally-right">
                        <Img
                            alt={tt('alts.default')}
                            src={`${
                                config.IMG_MAIN_PROXY
                            }/public/images/brands/white-logo.png`}
                            className="welcome-list__finally-right__logo scrollin delay2"
                        />
                        <div className="welcome-list__finally-right__title scrollin delay3">
                            {tt('welcome.let_start')}
                        </div>
                        {IOS_TESTFLIGHT_MODE ? (
                            <div className="welcome-list__finally-right__ios-button scrollin delay4">
                                <AppStoreButton />
                            </div>
                        ) : (
                            <div>
                                <div className="welcome-list__finally-right__start-button scrollin delay4">
                                    <SimpleButton
                                        value={tt('welcome.start_title')}
                                        color={'white'}
                                    />
                                </div>
                                <Link
                                    to={downloadsIndexRoute.getPath({
                                        params: {
                                            os: current_platform().value,
                                        },
                                    })}
                                    className="welcome-list__finally-right__download-button scrollin delay4"
                                >
                                    {tt('welcome.download_title')}
                                </Link>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );

        return (
            <div className="welcome-list">
                {header}
                {functoins_body}
                {finally_body}
            </div>
        );
    }
}

export default connect(
    (state, props) => {
        return {};
    },
    dispatch => ({})
)(WelcomeList);
