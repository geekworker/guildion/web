import { fromJS, Map, List } from 'immutable';
import { DEFAULT_LANGUAGE } from '@infrastructure/client_config';
import { browserHistory } from 'react-router';
import safe2json from '@extension/safe2json';
import models from '@view_models';

export const FETCH_WELCOME = 'guild/FETCH_WELCOME';
export const RESET_WELCOME = 'guild/RESET_WELCOME';
export const SET_WELCOME = 'guild/SET_WELCOME';

export const FETCH_WELCOMES = 'guild/FETCH_WELCOMES';
export const RESET_WELCOMES = 'guild/RESET_WELCOMES';
export const SET_WELCOMES = 'guild/SET_WELCOMES';
export const ADD_WELCOMES = 'guild/ADD_WELCOMES';
export const GET_MORE_WELCOMES = 'guild/GET_MORE_WELCOMES';

export const FETCH_HEROS = 'guild/FETCH_HEROS';
export const RESET_HEROS = 'guild/RESET_HEROS';
export const SET_HEROS = 'guild/SET_HEROS';

export const SET_CACHES = 'guild/SET_CACHES';
export const RESET_CACHES = 'guild/SET_CACHES';
export const SET_DELETES = 'guild/SET_DELETES';
export const RESET_DELETES = 'guild/SET_DELETES';

const defaultState = fromJS({
    welcome_guilds: Map({}),
    welcome_guilds_list: List([]),
    hero_guilds_list: List([]),
    caches: List([]),
    deletes: List([]),
});

export default function reducer(state = defaultState, action) {
    const payload = action.payload;

    switch (action.type) {
        case '@@router/LOCATION_CHANGE':
            return state.merge({
                caches: List([]),
                deletes: List([]),
            });

        case SET_CACHES: {
            const guilds = action.payload.guilds;
            if (!(guilds instanceof Array)) return state;
            if (guilds.length == 0) return state;
            let before = state.get('caches') || List([]);
            before = before.filter(val => !guilds.find(x => x.id == val.id));
            return state.set('caches', List(guilds.map(val => Map(val))));
        }

        case RESET_CACHES: {
            return state.set('caches', List([]));
        }

        case SET_DELETES: {
            const guilds = action.payload.guilds;
            if (!(guilds instanceof Array)) return state;
            if (guilds.length == 0) return state;
            let before = state.get('deletes') || List([]);
            before = before.filter(val => !guilds.find(x => x.id == val.id));
            return state.set('deletes', List(guilds.map(val => Map(val))));
        }

        case FETCH_WELCOME:
            return state;

        case SET_WELCOME: {
            if (!payload.guild || !payload.guild.uid) return state;
            return state.setIn(
                ['welcome_guilds', payload.guild.uid],
                Map(payload.guild)
            );
        }

        case RESET_WELCOME: {
            return state.merge({
                welcome_guilds: Map({}),
            });
        }

        case FETCH_WELCOMES:
            return state;

        case SET_WELCOMES: {
            if (!payload.guilds) return state;
            return state.set(
                'welcome_guilds_list',
                List(
                    Array.prototype
                        .unique_by_id(action.payload.guilds)
                        .map(val => Map(val))
                )
            );
        }

        case RESET_WELCOMES: {
            return state.set('welcome_guilds_list', List([]));
        }

        case ADD_WELCOMES: {
            if (!payload.guilds) return state;
            let before = state.get('welcome_guilds_list');
            before = before.toJS();
            return state.set(
                'welcome_guilds_list',
                List(
                    Array.prototype
                        .unique_by_id(before.concat(action.payload.guilds))
                        .map(val => Map(val))
                )
            );
        }

        case GET_MORE_WELCOMES:
            return state;

        case FETCH_HEROS:
            return state;

        case SET_HEROS: {
            if (!payload.guilds) return state;
            return state.set(
                'hero_guilds_list',
                List(
                    Array.prototype
                        .unique_by_id(action.payload.guilds)
                        .map(val => Map(val))
                )
            );
        }

        case RESET_HEROS: {
            return state.set('hero_guilds_list', List([]));
        }

        default:
            return state;
    }
}

export const fetchWelcome = payload => ({
    type: FETCH_WELCOME,
    payload,
});

export const setWelcome = payload => ({
    type: SET_WELCOME,
    payload,
});

export const resetWelcome = payload => ({
    type: RESET_WELCOME,
    payload,
});

export const fetchWelcomes = payload => ({
    type: FETCH_WELCOMES,
    payload,
});

export const resetWelcomes = payload => ({
    type: RESET_WELCOMES,
    payload,
});

export const setWelcomes = payload => ({
    type: SET_WELCOMES,
    payload,
});

export const addWelcomes = payload => ({
    type: ADD_WELCOMES,
    payload,
});

export const getMoreWelcomes = payload => ({
    type: GET_MORE_WELCOMES,
    payload,
});

export const fetchHeros = payload => ({
    type: FETCH_HEROS,
    payload,
});

export const resetHeros = payload => ({
    type: RESET_HEROS,
    payload,
});

export const setHeros = payload => ({
    type: SET_HEROS,
    payload,
});

export const setCaches = payload => ({
    type: SET_CACHES,
    payload,
});

export const resetCaches = payload => ({
    type: RESET_DELETES,
    payload,
});

export const setDeletes = payload => ({
    type: SET_CACHES,
    payload,
});

export const resetDeletes = payload => ({
    type: RESET_DELETES,
    payload,
});

export const getWelcomeGuild = (state, uid) => {
    const val = state.guild.getIn(['welcome_guilds', uid]);
    if (!val) return;
    const content = val.toJS();
    return models.Guild.build_with(content, [
        {
            as: 'Tags',
            model: models.Tag,
        },
        {
            as: 'Category',
            model: models.Category,
        },
        {
            as: 'Owner',
            model: models.User,
        },
        {
            as: 'MemberRequests',
            model: models.MemberRequest,
        },
        {
            as: 'Members',
            model: models.Member,
        },
    ]);
};

export const getHeroGuilds = state => {
    const val = state.guild.get('hero_guilds_list');
    if (!val) return;
    const contents = val.toJS();
    return contents.map(content => models.Guild.build_with(content, []));
};

export const getWelcomeGuilds = state => {
    const val = state.guild.get('welcome_guilds_list');
    if (!val) return;
    const contents = val.toJS();
    return contents.map(content =>
        models.Guild.build_with(content, [
            {
                as: 'Tags',
                model: models.Tag,
            },
            {
                as: 'Category',
                model: models.Category,
            },
        ])
    );
};
