import { fromJS, Set, List } from 'immutable';
import {
    call,
    put,
    select,
    fork,
    takeLatest,
    takeEvery,
} from 'redux-saga/effects';

import { LOCATION_CHANGE } from 'react-router-redux';
import { browserHistory } from 'react-router';
import { translate } from '@infrastructure/Translator';
import { GuildUseCase } from '@usecase';
import * as guildActions from './GuildReducer';

const guildUseCase = GuildUseCase.instance;

export const guildWatches = [
    takeEvery(guildActions.FETCH_WELCOME, guildUseCase.fetchWelcome),
    takeEvery(guildActions.FETCH_WELCOMES, guildUseCase.fetchWelcomes),
    takeEvery(guildActions.FETCH_HEROS, guildUseCase.fetchHeros),
];
