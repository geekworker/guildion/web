import { Map, OrderedMap, List, fromJS } from 'immutable';
import tt from 'counterpart';
import { browserHistory } from 'react-router';
import safe2json from '@extension/safe2json';
import { ClientError } from '@extension/Error';
import * as detection from '@network/detection';
import { DEFAULT_LANGUAGE } from '@infrastructure/client_config';

export const FETCH_DATA_BEGIN = 'app/FETCH_DATA_BEGIN';
export const FETCH_DATA_END = 'app/FETCH_DATA_END';
export const FETCH_MORE_DATA_BEGIN = 'app/FETCH_MORE_DATA_BEGIN';
export const FETCH_MORE_DATA_END = 'app/FETCH_MORE_DATA_END';
export const DISPATCH_DATA_BEGIN = 'app/DISPATCH_DATA_BEGIN';
export const DISPATCH_DATA_END = 'app/DISPATCH_DATA_END';
export const SCREEN_LOADING_BEGIN = 'app/SCREEN_LOADING_BEGIN';
export const SCREEN_LOADING_END = 'app/SCREEN_LOADING_END';
export const SET_COUNTRY_CODE = 'app/SET_COUNTRY_CODE';
export const SET_LOCALE = 'app/SET_LOCALE';
export const SET_TIMEZOON = 'app/SET_TIMEZOON';
export const SET_NOTIFICATION_ID = 'app/SET_NOTIFICATION_ID';

export const ADD_ERROR = 'app/ADD_ERROR';
export const REMOVE_ERROR = 'app/REMOVE_ERROR';
export const CLEAR_ERROR = 'app/CLEAR_ERROR';
export const ADD_SUCCESS = 'app/ADD_SUCCESS';
export const REMOVE_SUCCESS = 'app/REMOVE_SUCCESS';
export const CLEAR_SUCCESS = 'app/CLEAR_SUCCESS';

export const SHOW_HEADER = 'app/SHOW_HEADER';
export const HIDE_HEADER = 'app/HIDE_HEADER';

export const SET_ENV = 'app/SET_ENV';

export const defaultState = Map({
    loading: false,
    sending: false,
    more_loading: false,
    screen_loading: false,
    show_header: true,
    errors: List([]),
    successes: List([]),
    countryCode: 'JP',
    locale: DEFAULT_LANGUAGE,
    timezoon: 'Asia/Tokyo',
    location: {},
    notifications: null,
    notification_id: null,
    env: Map({}),
});

export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case '@@router/LOCATION_CHANGE':
            return state.set('location', {
                pathname: action.payload.pathname,
                loading: false,
                sending: false,
                more_loading: false,
                screen_loading: false,
            });

        case SET_COUNTRY_CODE:
            return state.set('countryCode', action.payload.countryCode);
        case SET_LOCALE:
            tt.setLocale(action.payload.locale);
            return state.set('locale', action.payload.locale);
        case SET_TIMEZOON:
            return state.set('timezoon', action.payload.timezoon);
        case SET_NOTIFICATION_ID:
            return state.set('notification_id', action.payload.notification_id);

        case ADD_ERROR: {
            if (action.payload.error) {
                let { error } = action.payload;
                // console.log(error);
                error = safe2json(error);
                let errors = state.get('errors');
                if (!errors) {
                    errors = List([]);
                }
                return state.merge({
                    errors: List(errors.push(error)),
                });
            }
            return state;
        }

        case REMOVE_ERROR: {
            if (action.payload.error) {
                const vals = state.get('errors');
                const pre_errors = vals.toJS();
                const errors = pre_errors.filter(
                    e =>
                        e.key != action.payload.error.key &&
                        e.tt_key != action.payload.error.tt_key &&
                        e.message != action.payload.error.message
                );
                return state.set('errors', List(errors));
            }
            return state;
        }

        case CLEAR_ERROR: {
            return state.set('errors', List([]));
        }

        case ADD_SUCCESS: {
            if (action.payload.success) {
                let { success } = action.payload;
                let successes = state.get('successes');
                if (!successes) {
                    successes = List([]);
                }
                return state.merge({
                    successes: List(successes.push(success)),
                });
            }
            return state;
        }

        case REMOVE_SUCCESS: {
            if (action.payload.success) {
                const { success } = action.payload;
                const vals = state.get('successes');
                const successes = vals.toJS();
                return state.set(
                    'successes',
                    List(successes.filter(val => val != success))
                );
            }
            return state;
        }

        case CLEAR_SUCCESS: {
            return state.set('successes', List([]));
        }

        case SHOW_HEADER: {
            return state.merge({
                show_header: true,
            });
        }

        case HIDE_HEADER:
            return state.merge({
                show_header: false,
            });

        case FETCH_DATA_BEGIN:
            return state.set('loading', true);
        case FETCH_DATA_END:
            return state.set('loading', false);
        case FETCH_MORE_DATA_BEGIN:
            return state.set('more_loading', true);
        case FETCH_MORE_DATA_END:
            return state.set('more_loading', false);
        case DISPATCH_DATA_BEGIN:
            return state.set('sending', true);
        case DISPATCH_DATA_END:
            return state.set('sending', false);
        case SCREEN_LOADING_BEGIN:
            return state.set('screen_loading', true);
        case SCREEN_LOADING_END:
            return state.set('screen_loading', false);

        default:
            return state;
    }
}

export const showHeader = payload => ({
    type: SHOW_HEADER,
    payload,
});

export const hideHeader = payload => ({
    type: HIDE_HEADER,
    payload,
});

export const addError = payload => ({
    type: ADD_ERROR,
    payload,
});

export const removeError = payload => ({
    type: REMOVE_ERROR,
    payload,
});

export const clearError = payload => ({
    type: CLEAR_ERROR,
    payload,
});

export const addSuccess = payload => ({
    type: ADD_SUCCESS,
    payload,
});

export const removeSuccess = payload => ({
    type: REMOVE_SUCCESS,
    payload,
});

export const clearSuccess = payload => ({
    type: CLEAR_SUCCESS,
    payload,
});

export const screenLoadingBegin = () => ({
    type: SCREEN_LOADING_BEGIN,
});

export const screenLoadingEnd = () => ({
    type: SCREEN_LOADING_END,
});

export const fetchDataBegin = () => ({
    type: FETCH_DATA_BEGIN,
});

export const fetchDataEnd = () => ({
    type: FETCH_DATA_END,
});

export const fetchMoreDataBegin = () => ({
    type: FETCH_MORE_DATA_BEGIN,
});

export const fetchMoreDataEnd = () => ({
    type: FETCH_MORE_DATA_END,
});

export const dispatchDataBegin = () => ({
    type: DISPATCH_DATA_BEGIN,
});

export const dispatchDataEnd = () => ({
    type: DISPATCH_DATA_END,
});

export const setCountryCode = countryCode => ({
    type: SET_COUNTRY_CODE,
    payload: { countryCode },
});

export const setLocale = locale => ({
    type: SET_LOCALE,
    payload: { locale },
});

export const setTimezoon = timezoon => ({
    type: SET_TIMEZOON,
    payload: { timezoon },
});

export const setNotificationId = timezoon => ({
    type: SET_NOTIFICATION_ID,
    payload: { notification_id },
});

export const getErrorsFromKey = (state, key) => {
    const vals = state.app.get('errors');
    if (!vals) return [];
    const errors = vals.toJS();
    return errors.filter(e => e.key == key).map(e => {
        e.error = new Error(e.message);
        return new ClientError(e);
    });
};

export const getSuccess = state => {
    const vals = state.app.get('successes');
    if (!vals) return [];
    const successes = vals.toJS();
    return successes || [];
};
