import { all } from 'redux-saga/effects';
import { appWatches } from '@redux/App';
import { documentWatches } from '@redux/Document';
import { guildWatches } from '@redux/Guild';

export default function* rootSaga() {
    yield all([...appWatches, ...documentWatches, ...guildWatches]);
}
