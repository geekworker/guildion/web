import { fromJS, Map, List } from 'immutable';
import { DEFAULT_LANGUAGE } from '@infrastructure/client_config';
import { browserHistory } from 'react-router';
import safe2json from '@extension/safe2json';
import models from '@view_models';

export const RESET_SHOW = 'document/RESET_SHOW';
export const SET_SHOW = 'document/SET_SHOW';

export const SET_GROUP = 'document/SET_GROUP';
export const RESET_GROUP = 'document/RESET_GROUP';

export const UPDATE = 'document/UPDATE';

export const SET_CACHES = 'document/SET_CACHES';
export const RESET_CACHES = 'document/SET_CACHES';
export const SET_DELETES = 'document/SET_DELETES';
export const RESET_DELETES = 'document/SET_DELETES';

const defaultState = fromJS({
    document_groups: Map({}),
    documents: Map({}),
    caches: List([]),
    deletes: List([]),
});

export default function reducer(state = defaultState, action) {
    const payload = action.payload;

    switch (action.type) {
        case '@@router/LOCATION_CHANGE':
            return state.merge({
                caches: List([]),
                deletes: List([]),
            });

        case SET_CACHES: {
            const documents = action.payload.documents;
            if (!(documents instanceof Array)) return state;
            if (documents.length == 0) return state;
            let before = state.get('caches') || List([]);
            before = before.filter(val => !documents.find(x => x.id == val.id));
            return state.set('caches', List(documents.map(val => Map(val))));
        }

        case RESET_CACHES: {
            return state.set('caches', List([]));
        }

        case SET_DELETES: {
            const documents = action.payload.documents;
            if (!(documents instanceof Array)) return state;
            if (documents.length == 0) return state;
            let before = state.get('deletes') || List([]);
            before = before.filter(val => !documents.find(x => x.id == val.id));
            return state.set('deletes', List(documents.map(val => Map(val))));
        }

        case SET_SHOW: {
            if (!payload.document || !payload.document.uid) return state;
            return state.setIn(
                ['documents', payload.document.uid],
                Map(payload.document),
            );
        }

        case RESET_SHOW: {
            return state.merge({
                documents: Map({}),
            });
        }

        case SET_GROUP: {
            if (!payload.group || !payload.group.groupname) return state;
            return state.setIn(
                ['document_groups', payload.group.groupname],
                Map(payload.group),
            );
        }

        case RESET_GROUP: {
            return state.merge({
                document_groups: Map({}),
            });
        }
        default:
            return state;
    }
}

export const setShow = payload => ({
    type: SET_SHOW,
    payload,
});

export const resetShow = payload => ({
    type: RESET_SHOW,
    payload,
});

export const setGroup = payload => ({
    type: SET_GROUP,
    payload,
});

export const resetGroup = payload => ({
    type: RESET_GROUP,
    payload,
});

export const update = payload => ({
    type: UPDATE,
    payload,
});

export const setCaches = payload => ({
    type: SET_CACHES,
    payload,
});

export const resetCaches = payload => ({
    type: RESET_DELETES,
    payload,
});

export const setDeletes = payload => ({
    type: SET_CACHES,
    payload,
});

export const resetDeletes = payload => ({
    type: RESET_DELETES,
    payload,
});

export const getDocument = (state, uid) => {
    const val = state.document.getIn(['documents', uid]);
    if (!val) return;
    const content = val.toJS();
    return models.Document.build_with(content, [
        {
            model: models.DocumentSection,
            as: 'Section',
        },
        {
            model: models.DocumentGroup,
            as: 'Group',
        },
    ]);
};

export const getDocumentGroup = (state, groupname) => {
    const val = state.document.getIn(['document_groups', groupname]);
    if (!val) return;
    const content = val.toJS();
    return models.DocumentGroup.build_with(content, [
        {
            model: models.DocumentSection,
            as: 'Sections',
            include: [
                {
                    model: models.Document,
                    as: 'Documents',
                },
            ],
        },
    ]);
};
