import documentReducer from './DocumentReducer';
import { documentWatches } from './DocumentSaga';

export { documentReducer, documentWatches };
