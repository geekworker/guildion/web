import { fromJS, Set, List } from 'immutable';
import {
    call,
    put,
    select,
    fork,
    takeLatest,
    takeEvery,
} from 'redux-saga/effects';

import { LOCATION_CHANGE } from 'react-router-redux';
import { browserHistory } from 'react-router';
import { translate } from '@infrastructure/Translator';
import { DocumentUseCase } from '@usecase';
import * as documentActions from './DocumentReducer';

const documentUseCase = DocumentUseCase.instance;

export const documentWatches = [
    takeEvery(LOCATION_CHANGE, documentUseCase.initShow),
    takeEvery(LOCATION_CHANGE, documentUseCase.initGroup),
    takeEvery(documentActions.UPDATE, documentUseCase.update),
];
