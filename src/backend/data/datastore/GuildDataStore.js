import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import {
    apiGuildShowFetchValidates,
    apiGuildCreateValidates,
    apiGuildUpdateValidates,
    apiGuildDeleteValidates,
    apiGuildJoinValidates,
    apiGuildLeaveValidates,
} from '@validations/guild';
import safe2json from '@extension/safe2json';
import AWSHandler from '@network/aws';
import { missingArgument } from '@extension/log';
import { getLocale, fallbackLocale } from '@locales';
import tt from 'counterpart';
import Sequelize from 'sequelize';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const awsHandler = AWSHandler.instance;

export default class GuildDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new GuildDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async getGuild({ guild, user }) {
        if (!guild || !user || !user.id) return;

        await apiGuildShowFetchValidates.isValid({
            user,
            guild,
        });

        const result = await models.Guild.findOne({
            where: {
                $or: [
                    {
                        id: Number(guild.id) || 0,
                    },
                    {
                        uid: guild.uid,
                    },
                ],
                permission: true,
            },
            include: models.Guild.show_include(models, {
                user_id: Number(user.id) || 0,
            }),
        });

        const member = await models.Member.findOne({
            where: {
                user_id: Number(user.id) || 0,
                guild_id: Number(result.id) || 0,
                permission: true,
            },
        });

        const [
            _text_channels,
            __stream_channels,
            dm_channels,
            members,
            section_channels,
            member_requests,
            roles,
            reads,
            member_roles,
            text_channel_roles,
            stream_channel_roles,
        ] = await Promise.all([
            models.TextChannel.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                    permission: true,
                },
                include: models.TextChannel.guild_show_include(models, {
                    user_id: Number(user.id) || 0,
                    member_id: Number(member.id) || 0,
                }),
            }),
            models.StreamChannel.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                    permission: true,
                },
                include: models.StreamChannel.guild_show_include(models, {
                    user_id: Number(user.id) || 0,
                    member_id: Number(member.id) || 0,
                }),
            }),
            models.DMChannel.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                    permission: true,
                    $or: [
                        {
                            is_static: true,
                        },
                        {
                            is_im: true,
                        },
                        {
                            is_system: false,
                        },
                        {
                            message_count: { $gt: 0 },
                        },
                    ],
                },
                include: models.DMChannel.guild_show_include(models, {
                    user_id: Number(user.id) || 0,
                    member_id: Number(member.id) || 0,
                }),
            }),
            models.Member.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                    permission: true,
                },
                include: models.Member.guild_show_include(models, {
                    user_id: Number(user.id) || 0,
                }),
            }),
            models.SectionChannel.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                    permission: true,
                },
            }),
            models.MemberRequest.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                    is_accepted: null,
                    permission: true,
                },
            }),
            models.Role.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                    permission: true,
                },
            }),
            models.Read.findAll({
                where: {
                    member_id: Number(member.id) || 0,
                    complete: false,
                },
                include: models.Read.guild_show_include(models, {
                    user_id: Number(user.id) || 0,
                    member_id: Number(member.id) || 0,
                }),
            }),
            models.Role.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                    permission: true,
                },
                include: [
                    {
                        model: models.MemberRole,
                        where: { member_id: member.id },
                    },
                ],
                order: [['priority', 'DESC']],
            }),
            models.TextChannelRole.findAll({
                where: {
                    permission: true,
                },
                include: [
                    {
                        model: models.TextChannel,
                        where: {
                            guild_id: Number(result.id) || 0,
                        },
                    },
                    {
                        model: models.Role,
                    },
                ],
            }),
            models.StreamChannelRole.findAll({
                where: {
                    permission: true,
                },
                include: [
                    {
                        model: models.StreamChannel,
                        where: {
                            guild_id: Number(result.id) || 0,
                        },
                    },
                    {
                        model: models.Role,
                    },
                ],
            }),
        ]);

        result.setValue('member_request_count', member_requests.length);

        const _stream_channels = __stream_channels.filter(
            val =>
                val.is_static ||
                val.is_im ||
                !val.is_system ||
                (!val.is_dm && !val.temporary) ||
                val.message_count > 0 ||
                ((val.is_dm || val.temporary) &&
                    val.Streams.filter(
                        stream =>
                            stream.is_live && stream.Participants.length > 0
                    ).length > 0)
        );

        let text_channels = [];
        let stream_channels = [];

        if (
            member_roles.length > 0 &&
            Number(user.id) != Number(guild.OwnerId) &&
            !member_roles[0].admin &&
            !member_roles[0].guild_managable &&
            !member_roles[0].channels_managable
        ) {
            text_channels = _text_channels.filter(channel => {
                const channel_roles = text_channel_roles.filter(
                    val => val.ChannelId == channel.id
                );
                if (channel_roles.length == 0) return channel;
                const includes_roles = channel_roles.filter(
                    val =>
                        member_roles.filter(mr => mr.id == val.RoleId).length >
                        0
                );
                if (includes_roles.length == 0) return channel;
                const prioritest_include_role = includes_roles.sort(
                    (a, b) => b.Role.priority - a.Role.priority
                )[0];
                return prioritest_include_role.channel_viewable
                    ? channel
                    : null;
            });
            stream_channels = _stream_channels.filter(channel => {
                if (channel.is_static || channel.is_im || channel.temporary)
                    return channel;
                const channel_roles = stream_channel_roles.filter(
                    val => val.ChannelId == channel.id
                );
                if (channel_roles.length == 0) return channel;
                const includes_roles = channel_roles.filter(
                    val =>
                        member_roles.filter(mr => mr.id == val.RoleId).length >
                        0
                );
                if (includes_roles.length == 0) return channel;
                const prioritest_include_role = includes_roles.sort(
                    (a, b) => b.Role.priority - a.Role.priority
                )[0];
                return prioritest_include_role.channel_viewable
                    ? channel
                    : null;
            });
        } else {
            text_channels = _text_channels;
            stream_channels = _stream_channels;
        }

        const unread_messages = Array.prototype.unique_by_id(
            reads.map(val => val.messagify(models)).filter(val => !!val)
        );
        const before_sections = [
            ...text_channels
                .map(val => {
                    val.setValue(
                        'Messages',
                        unread_messages.filter(
                            message =>
                                message.MessageableId == val.id &&
                                message.messageable_type == 'TextChannel'
                        )
                    );
                    val.setValue('unread_count', val.Messages.length);
                    return val;
                })
                .map(val => val.sectionify(models))
                .filter(val => !!val),
            ...stream_channels
                .map(val => {
                    val.setValue(
                        'Messages',
                        unread_messages.filter(
                            message =>
                                message.MessageableId == val.id &&
                                message.messageable_type == 'StreamChannel'
                        )
                    );
                    val.setValue('unread_count', val.Messages.length);
                    return val;
                })
                .map(val => val.sectionify(models))
                .filter(val => !!val),
            ...dm_channels
                .map(val => {
                    val.setValue(
                        'Messages',
                        unread_messages.filter(
                            message =>
                                message.MessageableId == val.id &&
                                message.messageable_type == 'DMChannel'
                        )
                    );
                    val.setValue('unread_count', val.Messages.length);
                    return val;
                })
                .map(val => val.sectionify(models))
                .filter(val => !!val),
            ...section_channels,
        ];

        result.setValue(
            'SectionChannels',
            models.SectionChannel.sectionifies(before_sections)
        );
        result.setValue(
            'TextChannels',
            text_channels.filter(val => !val.SectionId)
        );
        result.setValue(
            'DMChannels',
            dm_channels.filter(val => !val.SectionId)
        );
        result.setValue(
            'StreamChannels',
            stream_channels.filter(val => !val.SectionId)
        );
        result.setValue('Members', members);
        result.setValue('MemberRequests', member_requests);
        result.setValue('Roles', roles);

        result.sectionify(models);
        this.updateActiveCount(result);

        return result;
    }

    async getGuildsForHero({ limit }) {
        const results = await models.Guild.findAll({
            where: {
                is_private: false,
                permission: true,
            },
            limit: Number(limit || data_config.fetch_data_limit('S')),
            order: [Sequelize.fn('RAND')],
        });
        return results;
    }

    async getGuildsForWelcome({ limit, offset }) {
        const results = await models.Guild.findAll({
            where: {
                is_private: false,
                permission: true,
            },
            order: [['bumped_at', 'DESC']],
            offset: Number(offset || 0),
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return results;
    }

    async getGuildInfo({ guild }) {
        if (!guild) return;

        const result = await models.Guild.findOne({
            where: {
                $or: [
                    {
                        id: Number(guild.id) || 0,
                    },
                    {
                        uid: guild.uid,
                    },
                ],
                permission: true,
            },
            include: [...models.Guild.index_include(models)],
        });

        await this.setSearchIndexInclude(result);

        return result;
    }

    async getGuildChannels({ guild, user }) {
        if (!guild || !user || !user.id) return;

        await apiGuildShowFetchValidates.isValid({
            user,
            guild,
        });

        const result = await models.Guild.findOne({
            where: {
                $or: [
                    {
                        id: Number(guild.id) || 0,
                    },
                    {
                        uid: guild.uid,
                    },
                ],
                permission: true,
            },
            include: models.Guild.show_include(models, {
                user_id: Number(user.id) || 0,
            }),
        });

        const member = await models.Member.findOne({
            where: {
                user_id: Number(user.id) || 0,
                guild_id: Number(result.id) || 0,
                permission: true,
            },
        });

        const [
            text_channels,
            stream_channels,
            dm_channels,
            section_channels,
        ] = await Promise.all([
            models.TextChannel.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                },
                include: [
                    {
                        model: models.SectionChannel,
                        required: false,
                    },
                ],
            }),
            models.StreamChannel.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                    temporary: false,
                    is_dm: false,
                },
                include: [
                    {
                        model: models.SectionChannel,
                        required: false,
                    },
                ],
            }),
            models.DMChannel.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                },
                include: [
                    {
                        model: models.SectionChannel,
                        required: false,
                    },
                ],
            }),
            models.SectionChannel.findAll({
                where: {
                    guild_id: Number(result.id) || 0,
                },
            }),
        ]);

        const before_sections = [
            ...text_channels
                .map(val => val.sectionify(models))
                .filter(val => !!val),
            ...stream_channels
                .map(val => val.sectionify(models))
                .filter(val => !!val),
            ...dm_channels
                .map(val => val.sectionify(models))
                .filter(val => !!val),
            ...section_channels,
        ];
        result.setValue(
            'SectionChannels',
            models.SectionChannel.sectionifies(before_sections)
        );
        result.setValue(
            'TextChannels',
            text_channels.filter(val => !val.SectionId)
        );
        result.setValue(
            'DMChannels',
            stream_channels.filter(val => !val.SectionId)
        );
        result.setValue(
            'StreamChannels',
            dm_channels.filter(val => !val.SectionId)
        );

        result.sectionify(models);

        return result;
    }

    async getGuildRoles({ guild, user }) {
        if (!guild || !user || !user.id) return;

        await apiGuildShowFetchValidates.isValid({
            user,
            guild,
        });

        const result = await models.Guild.findOne({
            where: {
                $or: [
                    {
                        id: Number(guild.id) || 0,
                    },
                    {
                        uid: guild.uid,
                    },
                ],
                permission: true,
            },
        });

        const results = await models.Role.findAll({
            where: {
                guild_id: Number(result.id),
                permission: true,
            },
        });

        return results;
    }

    async getUserGuilds({ user }) {
        if (!user || !user.id) return;

        user = await models.User.findOne({
            where: {
                id: Number(user.id) || 0,
                permission: true,
            },
        });

        const members = await models.Member.findAll({
            where: {
                user_id: Number(user.id) || 0,
                permission: true,
            },
            order: [['index', 'ASC']],
        });

        const guilds = await Promise.all(
            members.map(async member => {
                const guild = await models.Guild.findOne({
                    where: {
                        id: member.GuildId,
                    },
                });
                guild.setValue('Members', [member]);
                guild.setValue('index', member.index);
                return guild;
            })
        );

        return guilds;
    }

    async create({ user, guild }) {
        await apiGuildCreateValidates.isValid({
            user,
            guild,
        });

        const created = await models.Guild.create({
            category_id: Number(guild.CategoryId) || null,
            CategoryId: Number(guild.CategoryId) || null,
            OwnerId: Number(user.id) || 0,
            owner_id: Number(user.id) || 0,
            uid: String.prototype.getUniqueString(),
            name: guild.name,
            description: guild.description,
            picture_small:
                guild.picture_small || data_config.Image.default_guild_image(),
            picture_large: guild.picture_large,
            locale: getLocale(guild.locale),
            country_code: guild.country_code || 'JP',
            is_nsfw: guild.is_nsfw,
            is_private: guild.is_private,
            is_public: guild.is_public,
            permission: true,
        });

        // const member = await models.Member.create({
        //     guild_id: Number(created.id) || 0,
        //     GuildId: Number(created.id) || 0,
        //     UserId: Number(user.id) || 0,
        //     user_id: Number(user.id) || 0,
        //     nickname: user.nickname,
        //     description: user.description,
        //     picture_small: user.picture_small,
        //     picture_large: user.picture_large,
        //     permission: true,
        // });

        guild.id = created.id;

        await this.createTags(guild);

        return created;
    }

    async update({ user, guild }) {
        await apiGuildUpdateValidates.isValid({
            user,
            guild,
        });

        const data = await models.Guild.findOne({
            where: {
                id: Number(guild.id),
            },
            raw: false,
        });

        const uid_guild = await models.Guild.findOne({
            where: {
                uid: guild.uid,
            },
            raw: false,
        });

        if (!!uid_guild && Number(data.id) != Number(uid_guild.id)) {
            throw new ApiError({
                error: new Error('errors.invalid_response_from_server'),
                tt_key: 'errors.is_already_exists',
                tt_params: { data: 'g.uid' },
            });
        }

        const before_picture_small = data.picture_small;
        const before_picture_large = data.picture_large;

        const result = await data.update({
            category_id: Number(guild.CategoryId) || null,
            CategoryId: Number(guild.CategoryId) || null,
            // OwnerId: Number(user.id) || 0,
            // owner_id: Number(user.id) || 0,
            name: guild.name,
            description: guild.description,
            picture_small: guild.picture_small,
            picture_large: guild.picture_large,
            locale: getLocale(guild.locale),
            country_code: guild.country_code || 'JP',
            is_nsfw: guild.is_nsfw,
            is_private: guild.is_private,
            is_public: guild.is_public,
        });

        if (
            before_picture_small != guild.picture_small &&
            AWSHandler.deletable(before_picture_small)
        ) {
            // await awsHandler.deleteFile(before_picture_small);
        }

        if (
            before_picture_large != guild.picture_large &&
            AWSHandler.deletable(before_picture_large)
        ) {
            // await awsHandler.deleteFile(before_picture_large);
        }

        await this.updateTags(guild);
        await this.update_locale({
            guild: result,
            before_locale: data.locale,
            current_locale: result.locale,
        });
        this.updateCount(result);

        return result;
    }

    async index_update({ guild, current_user }) {
        const before = await models.Member.findOne({
            where: {
                user_id: Number(current_user.id) || 0,
                guild_id: Number(guild.id) || 0,
            },
        });

        const result = await before.update({
            index: Number(guild.index) || 0,
        });

        return result;
    }

    async update_locale({ guild, before_locale, current_locale }) {
        const everyone_role = await models.Role.findOne({
            where: {
                guild_id: Number(guild.id),
                is_everyone: true,
            },
        });

        await everyone_role.update({
            name: tt('guild.everyone', { locale: current_locale }),
        });

        const dm_section = await models.SectionChannel.findOne({
            where: {
                guild_id: Number(guild.id),
                is_dm: true,
            },
        });

        await dm_section.update({
            name: tt('guild.dm_section', { locale: current_locale }),
        });
    }

    async delete({ guild, user }) {
        await apiGuildDeleteValidates.isValid({
            user,
            guild,
        });

        const data = await models.Guild.findOne({
            where: {
                id: Number(guild.id),
            },
            raw: false,
        });

        const before_picture_small = data.picture_small;
        const before_picture_large = data.picture_large;

        await models.Guild.destroy({
            where: {
                id: Number(guild.id),
            },
        });

        if (AWSHandler.deletable(before_picture_small)) {
            await awsHandler.deleteFile(before_picture_small);
        }

        if (AWSHandler.deletable(before_picture_large)) {
            await awsHandler.deleteFile(before_picture_large);
        }

        return;
    }

    async createTags(guild) {
        let createds = [];

        const tags = await Promise.all(
            guild.Tags.filter(item => !!item)
                .filter(item => !!item.name && item.name != '')
                .map(async tag => {
                    return await models.Tag.findOrCreate({
                        where: {
                            name: tag.name,
                            is_private: false,
                            permission: true,
                            locale: guild.locale,
                        },
                    }).spread(async (tag, created) => {
                        await Promise.all([
                            models.GuildTag.findOrCreate({
                                where: {
                                    guild_id: Number(guild.id),
                                    tag_id: Number(tag.id),
                                    is_private: false,
                                    permission: true,
                                },
                            }),
                            created && createds.push(tag),
                        ]);
                        return tag;
                    });
                })
        ).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_guild',
            });
        });

        return;
    }

    async updateTags(guild) {
        const befores = await models.GuildTag.findAll({
            where: {
                guild_id: Number(guild.id),
            },
            raw: false,
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_guild',
            });
        });

        await Promise.all(
            befores.map(val =>
                models.GuildTag.destroy({
                    where: {
                        id: Number(val.id),
                    },
                })
            )
        ).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_guild',
            });
        });

        await this.createTags(guild);
        return;
    }

    async search({ keyword, limit, offset }) {
        let results = [];

        const uid_guild = await models.Guild.findOne({
            where: {
                uid: keyword.replace('@', ''),
                permission: true,
            },
            include: [...models.Guild.index_include(models)],
        });

        if (uid_guild) return [uid_guild];

        const category = await models.Category.findOne({
            where: {
                $or: [
                    {
                        ja_name: {
                            $like: keyword,
                        },
                    },
                    {
                        ja_groupname: {
                            $like: keyword,
                        },
                    },
                    {
                        en_name: {
                            $like: keyword,
                        },
                    },
                    {
                        en_groupname: {
                            $like: keyword,
                        },
                    },
                ],
            },
            attributes: ['id'],
        });

        if (category) {
            results = await models.Guild.findAll({
                where: {
                    CategoryId: category.id,
                    OwnerId: {
                        $ne: null,
                    },
                    is_private: false,
                    permission: true,
                },
                include: [...models.Guild.index_include(models)],
                order: [['bumped_at', 'DESC']],
                offset: Number(offset || 0),
                limit: Number(limit || data_config.fetch_data_limit('S')),
            });
        }

        const tag = await models.Tag.findOne({
            where: {
                name: keyword,
            },
            attributes: ['id'],
        });

        if (tag) {
            results = await models.Guild.findAll({
                where: {
                    OwnerId: {
                        $ne: null,
                    },
                    is_private: false,
                    permission: true,
                },
                order: [['bumped_at', 'DESC']],
                include: [
                    {
                        model: models.GuildTag,
                        where: {
                            TagId: tag.id,
                        },
                        attributes: ['id'],
                    },
                    ...models.Guild.index_include(models),
                ],
                offset: Number(offset || 0),
                limit: Number(limit || data_config.fetch_data_limit('S')),
            });
        }

        if (
            results.length >= Number(limit || data_config.fetch_data_limit('S'))
        ) {
            return Array.prototype.unique_by_id(results);
        }

        let like_results = await Promise.all(
            generateLikeQuery(keyword).map(val => {
                return models.Guild.findAll({
                    where: {
                        OwnerId: {
                            $ne: null,
                        },
                        is_private: false,
                        permission: true,
                        $or: [
                            {
                                name: {
                                    $like: val,
                                },
                            },
                            {
                                description: {
                                    $like: val,
                                },
                            },
                        ],
                    },
                    include: [...models.Guild.index_include(models)],
                    order: [['bumped_at', 'DESC']],
                    offset: Number(offset || 0) + results.length,
                    limit: Number(limit || data_config.fetch_data_limit('S')),
                });
            })
        ).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_guild',
            });
        });

        like_results = Array.prototype.concat.apply([], like_results);
        results = results.concat(like_results);

        return Array.prototype.unique_by_id(results);
    }

    async setSearchIndexInclude(guild) {
        const [member_requests, members] = await Promise.all([
            models.MemberRequest.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    permission: true,
                },
            }),
            models.Member.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                },
            }),
        ]);

        guild.setValue('MemberRequests', member_requests);
        guild.setValue('Members', members);
    }

    async setSearchIndexIncludes(guilds) {
        await Promise.all(
            guilds.map(guild => this.setSearchIndexInclude(guild))
        );
    }

    async bump(guild_id) {
        if (!guild_id) return;

        let guild = await models.Guild.findOne({
            where: {
                guild_id,
            },
        });

        if (!guild) return;

        const base = Date.prototype.getHoursPast(
            data_config.Guild.bump_hour_limit
        );

        if (base > guild.bumped_at) {
            return await guild.update({
                bumped_at: new Date(),
            });
        }
    }

    async bumpLeftMinutes(guild_id) {
        if (!guild_id) return;

        let guild = await models.Guild.findOne({
            where: {
                guild_id,
            },
        });

        if (!guild) return;

        const base = Date.prototype.getHoursPast(
            data_config.Guild.bump_hour_limit
        );

        if (base > guild.bumped_at) return;
        return (
            (guild.bumped_at.getHoursAgoFrom.bind(
                data_config.Guild.bump_hour_limit
            ) -
                new Date()) /
            6000
        );
    }

    async join({ guild, user, temporary = false }) {
        await apiGuildJoinValidates.isValid({
            user,
            guild,
        });

        const member = await models.Member.create({
            guild_id: guild.id,
            GuildId: guild.id,
            UserId: Number(user.id) || 0,
            user_id: Number(user.id) || 0,
            uid: String.prototype.getUniqueString(),
            nickname: user.nickname,
            description: user.description,
            picture_small: user.picture_small,
            picture_large: user.picture_large,
            temporary,
            permission: true,
        });

        this.updateCount(guild);

        return member;
    }

    async leave({ guild, user }) {
        await apiGuildLeaveValidates.isValid({
            user,
            guild,
        });

        const member = await models.Member.findOne({
            where: {
                guild_id: Number(guild.id) || 0,
                user_id: Number(user.id) || 0,
                permission: true,
            },
        });

        if (member) {
            await models.Member.destroy({
                where: {
                    uid: member.uid,
                },
            });

            const members = await models.Member.findAll({
                where: {
                    user_id: Number(member.UserId) || 0,
                    index: {
                        $gt: Number(member.index) || 0,
                    },
                    permission: true,
                },
            });

            Promise.all(
                members.map(member =>
                    member.update({
                        index: member.index - 1,
                    })
                )
            );
        }

        this.updateCount(guild);

        return;
    }

    async updateCount(value) {
        if (!value) return;
        const guild = await models.Guild.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!guild) return;

        const [
            text_channels,
            stream_channels,
            dm_channels,
            members,
            section_channels,
            folders,
            roles,
        ] = await Promise.all([
            models.TextChannel.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    permission: true,
                },
            }),
            models.StreamChannel.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    permission: true,
                },
            }),
            models.DMChannel.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    permission: true,
                },
            }),
            models.Member.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    permission: true,
                },
                include: [
                    {
                        model: models.User,
                        as: 'User',
                    },
                ],
            }),
            models.SectionChannel.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    permission: true,
                },
            }),
            models.Folder.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    permission: true,
                },
            }),
            models.Role.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    permission: true,
                },
            }),
        ]);

        const result = await guild.update({
            member_count: members.length,
            active_count: members.filter(member =>
                Number.prototype.castBool(member.User.active)
            ).length,
            channel_count:
                text_channels.length +
                dm_channels.length +
                stream_channels.length,
            file_count: members.reduce((a, b) => a + (b.file_count || 0), 0),
            size_byte: members.reduce(
                (a, b) => a + (b.dataValues.size_byte || 0),
                0
            ),
            movie_count: members.reduce((a, b) => a + (b.movie_count || 0), 0),
            folder_count: folders.length,
            playlist_count: folders.filter(folder =>
                Number.prototype.castBool(folder.is_playlist)
            ).length,
            role_count: roles.length,
        });

        return result;
    }

    async updateActiveCount(value) {
        if (!value) return;
        const guild = await models.Guild.findOne({
            where: {
                id: Number(value.id) || 0,
            },
        });
        if (!guild) return;

        const members = await models.Member.findAll({
            where: {
                guild_id: Number(guild.id) || 0,
            },
            include: [
                {
                    model: models.User,
                    as: 'User',
                },
            ],
        });

        const result = await guild.update({
            member_count: members.length,
            active_count: members.filter(member =>
                Number.prototype.castBool(member.User.active)
            ).length,
        });

        return result;
    }

    async updateCountFromUser(value) {
        if (!value) return;
        const user = await models.User.findOne({
            where: {
                id: Number(value.id) || 0,
            },
        });
        if (!user) return;

        const guilds = await models.Guild.findAll({
            where: {
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                    where: { user_id: user.id },
                },
            ],
        });

        return await Promise.all(guilds.map(guild => this.updateCount(guild)));
    }

    async create_section({ user, channel, guild }) {
        // await apiTextChannelCreateValidates.isValid({
        //     user,
        //     section,
        //     channel,
        // });

        const created = await models.SectionChannel.create({
            guild_id: Number(channel.GuildId) || 0,
            GuildId: Number(channel.GuildId) || 0,
            uid: models.SectionChannel.generate_uid(),
            name: channel.name,
            description: channel.description,
            index: Number(channel.index) || 0,
            permission: true,
        });

        created.index = await this.setLastIndex(created);

        return created;
    }

    async update_section({ user, channel, guild }) {
        // await apiTextChannelUpdateValidates.isValid({
        //     user,
        //     channel,
        // });

        const data = await models.SectionChannel.findOne({
            where: {
                id: Number(channel.id),
            },
        });

        const result = await data.update({
            name: channel.name,
            description: channel.description,
        });

        return result;
    }

    async section_index_update({ user, channel }) {
        // await apiTextChannelUpdateValidates.isValid({
        //     user,
        //     channel,
        // });

        const data = await models.SectionChannel.findOne({
            where: {
                id: Number(channel.id),
            },
        });

        const result = await data.update({
            index: Number(channel.index) || 0,
        });

        return result;
    }

    async delete_section({ channel, user }) {
        // await apiTextChannelDeleteValidates.isValid({
        //     user,
        //     channel,
        // });

        const result = await models.SectionChannel.destroy({
            where: {
                id: Number(channel.id),
            },
        });

        const [
            section_channels,
            text_channels,
            stream_channels,
            dm_channels,
        ] = await Promise.all([
            models.SectionChannel.findAll({
                where: {
                    guild_id: Number(result.GuildId) || 0,
                    index: {
                        $gt: Number(result.index) || 0,
                    },
                    is_dm: false,
                    permission: true,
                },
            }),
            models.TextChannel.findAll({
                where: {
                    guild_id: Number(result.GuildId) || 0,
                    index: {
                        $gt: Number(result.index) || 0,
                    },
                    section_id: null,
                    permission: true,
                },
            }),
            models.StreamChannel.findAll({
                where: {
                    guild_id: Number(result.GuildId) || 0,
                    index: {
                        $gt: Number(result.index) || 0,
                    },
                    section_id: null,
                    permission: true,
                    temporary: false,
                },
            }),
            models.DMChannel.findAll({
                where: {
                    guild_id: Number(result.GuildId) || 0,
                    index: {
                        $gt: Number(result.index) || 0,
                    },
                    section_id: null,
                    permission: true,
                },
            }),
        ]);

        Promise.all(
            section_channels.map(section_channel =>
                section_channel.update({
                    index: section_channel.index - 1,
                })
            )
        );

        Promise.all(
            text_channels.map(text_channel =>
                text_channel.update({
                    index: text_channel.index - 1,
                })
            )
        );

        Promise.all(
            stream_channels.map(stream_channel =>
                stream_channel.update({
                    index: stream_channel.index - 1,
                })
            )
        );

        Promise.all(
            dm_channels.map(dm_channel =>
                dm_channel.update({
                    index: dm_channel.index - 1,
                })
            )
        );

        return result;
    }

    async block({ target, user, guild }) {
        if (!target || !user || !guild) return;

        await models.GuildBlock.findOrCreate({
            where: {
                user_id: Number(target.id) || 0,
                guild_id: Number(guild.id) || 0,
            },
        });

        await models.MemberRequest.destroy({
            where: {
                voter_id: Number(target.id) || 0,
                guild_id: Number(guild.id) || 0,
            },
        });
    }

    async unblock({ target, user, guild }) {
        if (!target || !user || !guild) return;

        await models.GuildBlock.destroy({
            where: {
                user_id: Number(target.id) || 0,
                guild_id: Number(guild.id) || 0,
            },
        });
    }

    async report({ user, guild, description }) {
        if (!user || !guild || !description) return;

        await models.GuildReport.findOrCreate({
            where: {
                guild_id: Number(guild.id) || 0,
                user_id: Number(user.id) || 0,
                description,
            },
        });
    }

    async getCurrentMemberRequests({ guild, user, limit }) {
        if (!guild || !user || !user.id) return [];

        guild = await models.Guild.findOne({
            where: {
                $or: [
                    {
                        id: Number(guild.id) || 0,
                    },
                    {
                        uid: guild.uid,
                    },
                ],
            },
        });

        const requests = await models.MemberRequest.findAll({
            where: {
                guild_id: Number(guild.id) || 0,
                is_accepted: null,
                permission: true,
            },
            include: models.MemberRequest.guild_show_include(models, {
                user_id: Number(user.id) || 0,
            }),
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });

        return requests;
    }

    async getIncrementMemberRequests({ guild, user, limit, last_id }) {
        if (!guild || !user || !user.id) return [];

        guild = await models.Guild.findOne({
            where: {
                $or: [
                    {
                        id: Number(guild.id) || 0,
                    },
                    {
                        uid: guild.uid,
                    },
                ],
            },
        });

        const requests = await models.MemberRequest.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                guild_id: Number(guild.id) || 0,
                is_accepted: null,
                permission: true,
            },
            include: models.MemberRequest.guild_show_include(models, {
                user_id: Number(user.id) || 0,
            }),
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });

        return requests.reverse();
    }

    async getDecrementMemberRequests({ guild, user, limit, first_id }) {
        if (!guild || !user || !user.id) return [];

        guild = await models.Guild.findOne({
            where: {
                $or: [
                    {
                        id: Number(guild.id) || 0,
                    },
                    {
                        uid: guild.uid,
                    },
                ],
            },
        });

        const requests = await models.MemberRequest.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                guild_id: Number(guild.id) || 0,
                is_accepted: null,
                permission: true,
            },
            include: models.MemberRequest.guild_show_include(models, {
                user_id: Number(user.id) || 0,
            }),
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });

        return requests;
    }

    async makeMemberRequest({ user, guild }) {
        if (!user || !user.id || !guild || !guild.id) return;

        if (Number.prototype.castBool(guild.is_public)) return;

        const results = await Promise.all([
            models.MemberRequest.findOrCreate({
                where: {
                    voter_id: Number(user.id),
                    guild_id: Number(guild.id),
                    permission: true,
                },
            }),
            models.User.findOne({
                where: {
                    id: Number(user.id) || 0,
                },
            }),
            models.Guild.findOne({
                where: {
                    id: Number(guild.id) || 0,
                },
            }),
        ]).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        const [memberRequest, created] = results[0];

        memberRequest.setValue('Voter', results[1]);
        memberRequest.setValue('Guild', results[2]);

        this.updateCount(results[2]);

        return memberRequest;
    }

    async acceptFriendRequest({ user, guild }) {
        if (!user || !user.id || !guild || !guild.id) return;
        let request = await models.MemberRequest.findOne({
            where: {
                voter_id: Number(user.id),
                guild_id: Number(guild.id),
                permission: true,
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        if (!request) return;

        const result = await request
            .update({
                is_accepted: true,
                permission: false,
            })
            .catch(e => {
                throw new ApiError({
                    error: e,
                    tt_key: 'errors.invalid_response_from_server',
                });
            });

        const member = await this.join({ user, guild }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        this.updateCount(guild);

        return member;
    }

    async denyFriendRequest({ user, guild }) {
        if (!user || !user.id || !guild || !guild.id) return;
        let request = await models.MemberRequest.findOne({
            where: {
                voter_id: Number(user.id),
                guild_id: Number(guild.id),
                permission: true,
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        if (!request) return;

        const result = await request
            .update({
                is_accepted: false,
                permission: false,
            })
            .catch(e => {
                throw new ApiError({
                    error: e,
                    tt_key: 'errors.invalid_response_from_server',
                });
            });

        this.updateCount(guild);

        return result;
    }

    async setLastIndex(value) {
        if (!value) return;
        const channel = await models.SectionChannel.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!channel) return;

        const [
            section_channels,
            text_channels,
            stream_channels,
            dm_channels,
        ] = await Promise.all([
            models.SectionChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    is_dm: false,
                    permission: true,
                },
            }),
            models.TextChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: null,
                    permission: true,
                },
            }),
            models.StreamChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: null,
                    temporary: false,
                    permission: true,
                },
            }),
            models.DMChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: null,
                    permission: true,
                },
            }),
        ]);

        const result = await channel.update({
            index:
                section_channels.length +
                text_channels.length +
                stream_channels.length +
                dm_channels.length,
        });

        return result.index;
    }
}
