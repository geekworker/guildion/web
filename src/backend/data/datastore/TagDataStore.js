import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { missingArgument } from '@extension/log';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class TagDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new TagDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async updateCount(value) {
        if (!value) return;
        const tag = await models.Tag.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!tag) return;

        const guilds = await models.Guild.findAll({
            include: [
                {
                    model: models.GuildTag,
                    where: {
                        TagId: tag.id,
                    },
                    attributes: ['id'],
                },
            ],
            attributes: ['id'],
        });

        const result = await tag.update({
            guild_count: guilds.length,
        });

        return result;
    }

    async updateCountFromDiscordServer(guild) {
        if (!guild || !guild.id) return;
        const tags = await models.Tag.findAll({
            include: [
                {
                    model: models.GuildTag,
                    where: {
                        GuildId: Number(guild.id) || 0,
                    },
                    attributes: ['id'],
                },
            ],
            attributes: ['id'],
        });
        const results = await Promise.all(
            tags.map(tag => this.updateCount(tag))
        );
        return results;
    }
}
