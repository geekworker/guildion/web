import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import safe2json from '@extension/safe2json';
import safe2array from '@extension/safe2array';
import AWSHandler from '@network/aws';
import { missingArgument } from '@extension/log';
import tt from 'counterpart';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const awsHandler = AWSHandler.instance;

export default class RoleDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new RoleDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async create({ role, _locale }) {
        if (!role.name || role.name == '') {
            role.name = tt('role.default_name', { locale: _locale });
        }
        const created = await models.Role.create({
            ...role,
            ...role.dataValues,
            guild_id: Number(role.GuildId) || 0,
            GuildId: Number(role.GuildId) || 0,
            uid: models.Role.generate_uid(),
            permission: true,
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return created;
    }

    async update({ role }) {
        const data = await models.Role.findOne({
            where: {
                id: Number(role.id),
            },
        });

        const result = await data.update({
            ...role,
            guild_id: Number(data.GuildId) || 0,
            GuildId: Number(data.GuildId) || 0,
        });

        return result;
    }

    async delete({ role }) {
        if (Number.prototype.castBool(role.is_default)) return;
        const result = await models.Role.destroy({
            where: {
                id: Number(role.id),
            },
        });

        return result;
    }

    async create_member_role({ member_role }) {
        const created = await models.MemberRole.create({
            ...member_role,
            position: member_role.position,
            priority: Number(member_role.priority) || 0,
            member_id: Number(member_role.MemberId) || 0,
            MemberId: Number(member_role.MemberId) || 0,
            role_id: Number(member_role.RoleId) || 0,
            RoleId: Number(member_role.RoleId) || 0,
            permission: true,
        });

        return created;
    }

    async update_member_role({ member_role }) {
        const data = await models.MemberRole.findOne({
            where: {
                id: Number(member_role.id),
            },
        });

        const result = await data.update({
            ...member_role,
            position: member_role.position,
            priority: Number(member_role.priority) || 0,
            member_id: Number(member_role.MemberId) || 0,
            MemberId: Number(member_role.MemberId) || 0,
            role_id: Number(member_role.RoleId) || 0,
            RoleId: Number(member_role.RoleId) || 0,
        });

        return result;
    }

    async delete_member_role({ member_role }) {
        const result = await models.MemberRole.destroy({
            where: {
                id: Number(member_role.id),
            },
        });

        return result;
    }

    async set_default_roles({ member }) {
        const roles = await models.Role.findAll({
            where: {
                guild_id: Number(member.GuildId) || 0,
                is_default: true,
                permission: true,
            },
        });

        return roles.map(role =>
            models.MemberRole.create({
                member_id: Number(member.id) || 0,
                MemberId: Number(member.id) || 0,
                role_id: Number(role.id) || 0,
                RoleId: Number(role.id) || 0,
                permission: true,
            })
        );
    }
}
