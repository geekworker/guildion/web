import DataStoreImpl from '@datastore/DataStoreImpl';
import Cookie from 'js-cookie';
import { Set, Map, fromJS, List } from 'immutable';
import models from '@models';
import jwt from 'jsonwebtoken';
import validator from 'validator';
import { ApiError } from '@extension/Error';
import config from '@constants/config';
import session_config from '@constants/session_config';
import auth_config from '@constants/auth_config';
import data_config from '@constants/data_config';
import notification_config from '@constants/notification_config';
import {
    apiSessionRegisterValidates,
    apiSessionSeedRegisterValidates,
    apiReSendConfirmEmailValidates,
    apiResetPasswordEmailValidates,
} from '@validations/session';
import uuidv4 from 'uuid/v4';
import tt from 'counterpart';
import safe2json from '@extension/safe2json';
import { getLocale, fallbackLocale } from '@locales';
import { missingArgument } from '@extension/log';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class SessionDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new SessionDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    generate6Code() {
        return `${Math.floor(Math.random() * 10)}${Math.floor(
            Math.random() * 10
        )}${Math.floor(Math.random() * 10)}${Math.floor(
            Math.random() * 10
        )}${Math.floor(Math.random() * 10)}${Math.floor(Math.random() * 10)}`;
    }

    async register({
        nickname,
        email,
        password,
        locale = 'ja',
        country_code = 'JP',
        timezone = 'Asia/Tokyo',
        enable_mail_notification = false,
    }) {
        await apiSessionRegisterValidates.isValid({
            user: {
                nickname,
                email,
                password,
            },
            nickname,
            email,
            password,
        });

        const mailToken = this.generate6Code();

        const mail_notification_token = await jwt.sign(
            {
                type: 'mail_notification',
                email: email,
            },
            process.env.JWT_SECRET
        );

        locale = getLocale(locale);

        const user = await models.User.create({
            username: String.prototype.getUniqueString(),
            uid: String.prototype.getUniqueString(),
            nickname,
            picture_small: data_config.Image.default_user_image(),
            picture_large: '', // data_config.Image.default_background_image,
            locale,
            country_code,
            timezone,
            verified: false,
            is_private: false,
            permission: true,
            admin: false,
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        const identity = await models.Identity.create({
            UserId: user.id,
            username: user.username,
            token: mailToken,
            email,
            email_is_verified: false,
            mail_notification_token,
            enable_mail_notification,
            country_code: user.country_code,
            password,
            delete_password_token: user.delete_password_token,
            verified: user.verified,
            is_deleted: user.is_deleted,
            permission: user.permission,
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        locale = getLocale(locale);

        // if (session_config.EMAIL_CONFIRM_MODE) {
        //     await mail.send(email, 'confirm_code', {
        //         code: mailToken,
        //         title: tt('emails.confirm_code.title', {
        //             locale,
        //             fallbackLocale,
        //         }),
        //         text: tt('emails.confirm_code.body', {
        //             locale,
        //             fallbackLocale,
        //         }),
        //         foot_text: '',
        //         end_text: '',
        //         inc_name: config.INC_FULL_NAME,
        //         unsubscribe_text: tt('emails.unsubscribe_text', {
        //             locale,
        //             fallbackLocale,
        //         }),
        //         unsubscribe_url:
        //             notification_config.notification_unsubscribe_url,
        //         unsubscribe: tt('emails.unsubscribe', {
        //             locale,
        //             fallbackLocale,
        //         }),
        //         contact: tt('emails.contact', { locale, fallbackLocale }),
        //     });
        // }

        return {
            user,
            identity,
        };
    }

    async confirmEmail({ email, code }) {
        if (!email || email == '' || !code || code == '')
            throw new ApiError({
                error: e,
                tt_key: 'errors.is_not_correct_auth',
            });

        const identity = await models.Identity.findOne({
            where: {
                email,
            },
        });

        const result = await identity.authenticateToken.bind(identity, code);

        if (!result)
            throw new ApiError({
                error: new Error(`is_not_correct_auth`),
                tt_key: 'errors.is_not_correct_auth',
            });

        if (!identity)
            throw new ApiError({
                error: new Error(`not_exists`),
                tt_key: 'errors.not_exists',
                tt_params: { content: 'User' },
            });
        if (!identity.permission)
            throw new ApiError({
                error: new Error(`is_not_permitted`),
                tt_key: 'errors.is_not_permitted',
                tt_params: { data: identity.username },
            });
        if (Number.prototype.castBool(identity.verified))
            throw new ApiError({
                error: new Error(`invalid_access_token`),
                tt_key: 'errors.invalid_access_token',
            });

        const user = await models.User.findOne({
            where: {
                username: identity.username,
            },
        });

        let val = await identity.update({
            email_is_verified: true, //token == identity.token,
            verified: true,
            last_attempt_verify_email: new Date(),
            verify_email_attempts: (identity.verify_email_attempts || 0) + 1,
        });

        const val_user = await user.update({
            verified: true,
        });

        val.setValue('User', val_user);

        return val;
    }

    async resendConfirmEmail({ email, locale = 'ja' }) {
        await apiReSendConfirmEmailValidates.isValid({
            identity: { email },
            email,
        });

        const mailToken = this.generate6Code();

        let identity = await models.Identity.findOne({
            where: {
                email,
            },
        });

        if (!identity)
            throw new ApiError({
                error: new Error('user is not exists'),
                tt_key: 'errors.not_exists',
                tt_params: { content: 'user' },
            });

        identity = await identity
            .update({
                email,
                token: mailToken,
            })
            .catch(e => {
                throw new ApiError({
                    error: e,
                    tt_key: 'errors.is_already_exists',
                    tt_params: { data: 'email' },
                });
            });

        locale = getLocale(locale);

        // await this.sendEmail(email, 'confirm_code', {
        //     code: mailToken,
        //     title: tt('emails.confirm_code.title', { locale, fallbackLocale }),
        //     text: tt('emails.confirm_code.body', { locale, fallbackLocale }),
        //     foot_text: '',
        //     end_text: '',
        //     inc_name: config.INC_FULL_NAME,
        //     unsubscribe_text: tt('emails.unsubscribe_text', {
        //         locale,
        //         fallbackLocale,
        //     }),
        //     unsubscribe_url: notification_config.notification_unsubscribe_url,
        //     unsubscribe: tt('emails.unsubscribe', { locale, fallbackLocale }),
        //     contact: tt('emails.contact', { locale, fallbackLocale }),
        // });

        return identity;
    }

    async setAccessToken({ devise, identity, is_one_time = false }) {
        if (!identity) return;
        const session = await models.Identity.findOne({
            where: {
                username: identity.username,
                id: Number(identity.id),
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });
        if (!session) return;

        const access_token = auth_config.expired_mode
            ? await jwt.sign(
                  {
                      type: 'access_token',
                      username: session.username,
                  },
                  process.env.JWT_SECRET,
                  {
                      expiresIn: auth_config.expires_in,
                  }
              )
            : await jwt.sign(
                  {
                      type: 'access_token',
                      username: session.username,
                  },
                  process.env.JWT_SECRET
              );

        const _devise = await models.Devise.findOne({
            where: {
                uid: devise.uid,
            },
        });

        const result = await models.AccessToken.create({
            identity_id: session.id,
            token: access_token,
            devise_id: Number(_devise.id) || null,
            DeviseId: Number(_devise.id) || null,
            expired_at: Date.prototype.getHoursAgo(auth_config.expires_in / 60),
            is_one_time,
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        if (!result) return;

        return result.token;
    }

    async checkAccessToken({ access_token, devise, deleting = true }) {
        const decoded = await this.verifyToken(
            access_token,
            'access_token'
        ).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_access_token',
            });
        });

        if (decoded && decoded.type !== 'access_token') {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_access_token',
            });
        }

        const identity = await models.Identity.findOne({
            where: {
                username: decoded.username,
            },
            include: [
                {
                    model: models.User,
                    as: 'User',
                },
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        const _devise = await models.Devise.findOne({
            where: {
                uid: !!devise.dataValues ? devise.dataValues.uid : devise.uid,
            },
        });

        const model = await models.AccessToken.findOne({
            where: {
                token: access_token,
                $or: [
                    {
                        devise_id: Number(_devise.id) || 0,
                    },
                    {
                        is_one_time: true,
                    },
                ],
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        if (!model) return;

        if (Number(model.is_one_time) && deleting) {
            await models.AccessToken.destroy({
                where: {
                    id: Number(model.id),
                },
            }).catch(e => {
                throw new ApiError({
                    error: e,
                    tt_key: 'errors.invalid_response_from_server',
                });
            });
        }

        return identity;
    }

    async verifyToken(token, type) {
        if (!token) {
            throw new ApiError({
                error: new Error('Token is required'),
                tt_key: 'errors.is_required',
                tt_params: { data: 'Token' },
            });
        }

        const decoded = await jwt.verify(token, process.env.JWT_SECRET);

        if (type && decoded.type !== type) {
            throw new ApiError({
                error: new Error('Token is invalid'),
                tt_key: 'errors.is_not_correct',
                tt_params: { data: token },
            });
        }
        return decoded;
    }
}
