import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import {
    apiMessageCreateValidates,
    apiMessageUpdateValidates,
    apiMessageDeleteValidates,
} from '@validations/message';
import safe2json from '@extension/safe2json';
import safe2array from '@extension/safe2array';
import AWSHandler from '@network/aws';
import { missingArgument } from '@extension/log';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const awsHandler = AWSHandler.instance;

export default class MessageDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new MessageDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async getCurrentMessages({
        messageable_type,
        messageable_id,
        current_user,
        limit,
        is_static,
    }) {
        if (!messageable_type || !messageable_id) return;

        let where = {};
        if (is_static) {
            where.is_static = is_static;
        }

        const messages = await models.Message.findAll({
            where: {
                ...where,
                messageable_id: Number(messageable_id) || 0,
                messageable_type,
                permission: true,
            },
            include: [...models.Message.index_include(models)],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        await Promise.all(
            messages.filter(message => message.is_log).map(async message => {
                message.setValue(
                    'Channelog',
                    await models.Channelog.findOne({
                        where: {
                            message_id: Number(message.id) || 0,
                        },
                        include: [...models.Channelog.show_include(models)],
                    })
                );
            })
        );

        return messages.reverse();
    }

    async getIncrementMessages({
        messageable_type,
        messageable_id,
        current_user,
        last_id,
        limit,
        is_static,
    }) {
        if (!messageable_type || !messageable_id) return;

        let where = {};
        if (is_static) {
            where.is_static = is_static;
        }

        const results = await models.Message.findAll({
            where: {
                ...where,
                id: {
                    $gt: Number(last_id) || 0,
                },
                messageable_id: Number(messageable_id) || 0,
                messageable_type,
                permission: true,
            },
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [...models.Message.index_include(models)],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        await Promise.all(
            results.filter(message => message.is_log).map(async message => {
                message.setValue(
                    'Channelog',
                    await models.Channelog.findOne({
                        where: {
                            message_id: Number(message.id) || 0,
                        },
                        include: [...models.Channelog.show_include(models)],
                    })
                );
            })
        );

        return results;
    }

    async getDecrementMessages({
        messageable_type,
        messageable_id,
        current_user,
        first_id,
        limit,
        is_static,
    }) {
        if (!messageable_type || !messageable_id) return;

        let where = {};
        if (is_static) {
            where.is_static = is_static;
        }

        const results = await models.Message.findAll({
            where: {
                ...where,
                id: {
                    $lt: Number(first_id) || 0,
                },
                messageable_id: Number(messageable_id) || 0,
                messageable_type,
                permission: true,
            },
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [...models.Message.index_include(models)],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        await Promise.all(
            results.filter(message => message.is_log).map(async message => {
                message.setValue(
                    'Channelog',
                    await models.Channelog.findOne({
                        where: {
                            message_id: Number(message.id) || 0,
                        },
                        include: [...models.Channelog.show_include(models)],
                    })
                );
            })
        );

        return results.reverse();
    }

    async create({ message, current_user }) {
        await apiMessageCreateValidates.isValid({
            message,
            user: current_user,
        });

        const result = await models.Message.create({
            messageable_id: Number(message.MessageableId) || 0,
            MessageableId: Number(message.MessageableId) || 0,
            messageable_type: message.messageable_type,
            uid: String.prototype.getUniqueString(),
            sender_id: Number(message.SenderId) || 0,
            SenderId: Number(message.SenderId) || 0,
            text: message.text,
            is_log: false,
            permission: true,
        });

        this.unread_all_member({ message: result });

        return result;
    }

    async create_channelog({ channelog, message, current_user }) {
        const result = await models.Message.create({
            messageable_id: Number(message.MessageableId) || 0,
            MessageableId: Number(message.MessageableId) || 0,
            messageable_type: message.messageable_type,
            uid: String.prototype.getUniqueString(),
            is_log: true,
            permission: true,
        });

        this.unread_all_member({ message: result });

        const _result = await models.Channelog.create({
            channelogable_id: Number(channelog.ChannelogableId) || 0,
            ChannelogableId: Number(channelog.ChannelogableId) || 0,
            channelogable_type: channelog.channelogable_type,
            uid: String.prototype.getUniqueString(),
            message_id: Number(result.id) || 0,
            MessageId: Number(result.id) || 0,
            thumbnail: channelog.thumbnail,
            action: channelog.action,
            template: channelog.template,
            permission: true,
        });

        return result;
    }

    async update({ message, current_user }) {
        await apiMessageUpdateValidates.isValid({
            message,
            user: current_user,
        });

        const sender = await models.Member.findOne({
            where: {
                user_id: Number(current_user.id) || 0,
                guild_id: Number(message.GuildId) || 0,
            },
        });

        const before = await models.Message.findOne({
            where: {
                uid: message.uid,
            },
        });

        const result = await before.update({
            text: message.text,
        });

        return result;
    }

    async updateStatic({ message, current_user, is_static }) {
        await apiMessageUpdateValidates.isValid({
            message,
            user: current_user,
        });

        const sender = await models.Member.findOne({
            where: {
                user_id: Number(current_user.id) || 0,
                guild_id: Number(message.GuildId) || 0,
            },
        });

        const before = await models.Message.findOne({
            where: {
                uid: message.uid,
            },
        });

        const result = await before.update({
            is_static: Number.prototype.castBool(is_static),
        });

        return result;
    }

    async delete({ message, current_user }) {
        await apiMessageDeleteValidates.isValid({
            message,
            user: current_user,
        });

        const sender = await models.Member.findOne({
            where: {
                user_id: Number(current_user.id) || 0,
                guild_id: Number(message.GuildId) || 0,
            },
        });

        await models.Message.destroy({
            where: {
                uid: message.uid,
            },
        });

        return;
    }

    async search({
        messageable_id,
        messageable_type,
        keyword,
        limit,
        offset,
        is_static,
    }) {
        let where = {};
        if (messageable_id) {
            where.messageable_id = Number(messageable_id) || 0;
        }

        if (messageable_type) {
            where.messageable_type = messageable_type;
        }

        if (is_static) {
            where.is_static = is_static;
        }

        let like_results = await Promise.all(
            generateLikeQuery(keyword).map(val => {
                return models.Message.findAll({
                    where: {
                        ...where,
                        $or: [
                            {
                                text: {
                                    $like: val,
                                },
                            },
                        ],
                    },
                    offset: Number(offset || 0),
                    limit: Number(limit || data_config.fetch_data_limit('M')),
                    order: [['id', 'DESC']],
                    include: [
                        {
                            as: 'Sender',
                            model: models.Member,
                            required: true,
                            include: [
                                {
                                    model: models.User,
                                    required: true,
                                },
                            ],
                        },
                        {
                            model: models.File,
                            through: { attributes: [] },
                            required: false,
                            where: {
                                $or: [
                                    {
                                        name: {
                                            $like: val,
                                        },
                                    },
                                    {
                                        description: {
                                            $like: val,
                                        },
                                    },
                                ],
                            },
                        },
                    ],
                });
            })
        ).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        like_results = Array.prototype.concat.apply([], like_results);

        return Array.prototype.unique_by_id(like_results);
    }

    async unread_all_member({ message }) {
        let members = [];
        switch (message.messageable_type) {
            case 'TextChannel':
                members = members.concat(
                    await models.Member.findAll({
                        include: [
                            {
                                model: models.TextChannelEntry,
                                where: {
                                    channel_id:
                                        Number(message.MessageableId) || 0,
                                },
                            },
                        ],
                    })
                );
                break;

            case 'DMChannel':
                members = members.concat(
                    await models.Member.findAll({
                        include: [
                            {
                                model: models.DMChannelEntry,
                                where: {
                                    channel_id:
                                        Number(message.MessageableId) || 0,
                                },
                            },
                        ],
                    })
                );
                break;

            case 'StreamChannel':
                members = members.concat(
                    await models.Member.findAll({
                        include: [
                            {
                                model: models.StreamChannelEntry,
                                where: {
                                    channel_id:
                                        Number(message.MessageableId) || 0,
                                },
                            },
                        ],
                    })
                );
                break;
        }

        return await Promise.all(
            members
                .filter(member => Number(member.id) != Number(message.SenderId))
                .map(member => this.unread({ member, message }))
        );
    }

    async unread({ message, member }) {
        const [read, created] = await models.Read.findOrCreate({
            where: {
                message_id: Number(message.id) || 0,
                member_id: Number(member.id) || 0,
            },
        });

        return await read.update({ complete: false });
    }

    async read({ message, member }) {
        const results = await models.Read.update(
            {
                complete: true,
            },
            {
                where: {
                    message_id: Number(message.id) || 0,
                    member_id: Number(member.id) || 0,
                },
            }
        );

        return results[0];
    }

    async filterBlockMessages({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserBlock.findAll({
            where: {
                user_id: Number(user.id) || 0,
            },
            raw: true,
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (
                    blocks.filter(
                        block => block.ReceiverId == val.Sender.UserId
                    ).length == 0
                )
                    return val;
            })
            .filter(val => !!val);
    }

    async filterBlockerMessages({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserBlock.findAll({
            where: {
                receiver_id: Number(user.id) || 0,
            },
            raw: true,
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (
                    blocks.filter(block => block.UserId == val.Sender.UserId)
                        .length == 0
                )
                    return val;
            })
            .filter(val => !!val);
    }

    async filterValidMessages({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;

        return await this.filterBlockerMessages({
            user,
            targets: await this.filterBlockMessages({
                user,
                targets,
            }),
        });
    }
}
