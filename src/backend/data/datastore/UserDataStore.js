import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { missingArgument } from '@extension/log';
import safe2json from '@extension/safe2json';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import {
    apiUserCreateValidates,
    apiUserUpdateValidates,
} from '@validations/user';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import { getLocale, fallbackLocale } from '@locales';
import AWSHandler from '@network/aws';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const awsHandler = AWSHandler.instance;

export default class UserDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new UserDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async makeActive({ user }) {
        if (!user || !user.id) return;

        user = await models.User.findOne({
            where: {
                id: Number(user.id) || 0,
            },
        });

        const result = await user.update({
            active: true,
            actived_at: new Date(),
        });

        return result;
    }

    async makeInActive({ user }) {
        if (!user || !user.id) return;

        user = await models.User.findOne({
            where: {
                id: Number(user.id) || 0,
            },
        });

        const result = await user.update({
            active: false,
            actived_at: new Date(),
        });

        return result;
    }

    async updateUser({ user }) {
        if (!user) return;

        await apiUserUpdateValidates.isValid({
            user,
            nickname: user.nickname,
        });

        let before = await models.User.findOne({
            where: {
                id: Number(user.id) || 0,
            },
        });

        const identity = await models.Identity.findOne({
            where: {
                user_id: Number(user.id) || 0,
            },
        });

        if (before.username != user.username) {
            const uid_user = await models.User.findOne({
                where: {
                    username: user.username,
                },
            });

            if (!!uid_user && Number(before.id) != Number(uid_user.id)) {
                throw new ApiError({
                    error: new Error('errors.invalid_response_from_server'),
                    tt_key: 'errors.is_already_exists',
                    tt_params: { data: 'g.username' },
                });
            }

            await identity
                .update({
                    username: user.username,
                })
                .catch(e => {
                    throw new ApiError({
                        error: e,
                        tt_key: 'errors.is_already_exists',
                        tt_params: { data: 'g.username' },
                    });
                });

            before = await before.update({
                username: user.username,
            });
        }

        const before_picture_small = before.picture_small;
        const before_picture_large = before.picture_large;

        const ps_members = await models.Member.findAll({
            where: { picture_small: before_picture_small },
        });

        const pl_members = await models.Member.findAll({
            where: { picture_small: before_picture_small },
        });

        if (
            before_picture_small != user.picture_small &&
            AWSHandler.deletable(before_picture_small) &&
            ps_members.length == 0
        ) {
            await awsHandler.deleteFile(before_picture_small);
        }

        if (
            before_picture_large != user.picture_large &&
            AWSHandler.deletable(before_picture_large) &&
            pl_members.length == 0
        ) {
            await awsHandler.deleteFile(before_picture_large);
        }

        return await before.update({
            nickname: user.nickname,
            description: user.description,
            picture_small: user.picture_small,
            picture_large: user.picture_large,
            locale: user.locale,
            country_code: user.country_code,
        });
    }

    async updateUserLang({ user }) {
        if (!user) return;

        const before = await models.User.findOne({
            where: {
                id: Number(user.id) || 0,
            },
        });

        return await before.update({
            locale: user.locale,
            country_code: user.country_code,
        });
    }

    async deleteUser({ user }) {
        if (!user) return;

        // const withdrawal = await models.Withdrawal.create({
        //     twitter_username: user.twitter_username,
        //     twitter_id: user.twitter_id,
        //     valid: true,
        // }).catch(e => {
        //     throw new ApiError({
        //         error: e,
        //         tt_key: 'errors.invalid_response_from_server',
        //     });
        // });

        const result = await models.User.destroy({
            where: {
                username: user.username,
                id: Number(user.id) || 0,
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        await awsHandler.deleteDirectory(user.uid);
    }

    async getMembers({ current_user }) {
        const members = await models.Member.findAll({
            where: {
                user_id: Number(current_user.id) || 0,
                permission: true,
            },
            order: [['id', 'DESC']],
            include: [
                {
                    model: models.Guild,
                },
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return members;
    }

    async getCurrentMembers({ current_user, limit }) {
        const members = await models.Member.findAll({
            where: {
                user_id: Number(current_user.id) || 0,
                permission: true,
            },
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                {
                    model: models.Guild,
                },
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return members;
    }

    async getIncrementMembers({ current_user, limit, last_id }) {
        const members = await models.Member.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                user_id: Number(current_user.id) || 0,
                permission: true,
            },
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                {
                    model: models.Guild,
                },
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return members.reverse();
    }

    async getDecrementMembers({ current_user, limit, first_id }) {
        const members = await models.Member.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                user_id: Number(current_user.id) || 0,
                permission: true,
            },
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                {
                    model: models.Guild,
                },
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return members;
    }

    async block({ user, target }) {
        if (!user || !target) return;

        await models.UserBlock.findOrCreate({
            where: {
                receiver_id: Number(target.id) || 0,
                user_id: Number(user.id) || 0,
            },
        });
    }

    async unblock({ user, target }) {
        if (!user || !target) return;

        await models.UserBlock.destroy({
            where: {
                receiver_id: Number(target.id) || 0,
                user_id: Number(user.id) || 0,
            },
        });
    }

    async mute({ user, target }) {
        if (!user || !target) return;

        await models.UserMute.findOrCreate({
            where: {
                receiver_id: Number(target.id) || 0,
                user_id: Number(user.id) || 0,
            },
        });
    }

    async unmute({ user, target }) {
        if (!user || !target) return;

        await models.UserMute.destroy({
            where: {
                receiver_id: Number(target.id) || 0,
                user_id: Number(user.id) || 0,
            },
        });
    }

    async report({ user, target, description }) {
        if (!user || !target || !description) return;

        await models.UserReport.findOrCreate({
            where: {
                receiver_id: Number(target.id) || 0,
                user_id: Number(user.id) || 0,
                description,
            },
        });
    }

    async filterBlockUser({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserBlock.findAll({
            where: {
                user_id: Number(user.id) || 0,
            },
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (
                    blocks.filter(block => block.ReceiverId == val.id).length ==
                    0
                )
                    return val;
            })
            .filter(val => !!val);
    }

    async filterMuteUser({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserMute.findAll({
            where: {
                user_id: Number(user.id) || 0,
            },
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (
                    blocks.filter(block => block.ReceiverId == val.id).length ==
                    0
                )
                    return val;
            })
            .filter(val => !!val);
    }

    async filterBlockerUser({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserBlock.findAll({
            where: {
                receiver_id: Number(user.id) || 0,
            },
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (blocks.filter(block => block.UserId == val.id).length == 0)
                    return val;
            })
            .filter(val => !!val);
    }

    async filterMuterUser({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserMute.findAll({
            where: {
                receiver_id: Number(user.id) || 0,
            },
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (blocks.filter(block => block.UserId == val.id).length == 0)
                    return val;
            })
            .filter(val => !!val);
    }

    async filterValidUser({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;

        return await this.filterBlockerUser({
            user,
            targets: await this.filterMuterUser({
                user,
                targets: await this.filterBlockUser({
                    user,
                    targets: await this.filterMuteUser({ user, targets }),
                }),
            }),
        });
    }

    async updateCount(value) {
        if (!value) return;
        const user = await models.User.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!user) return;

        const [members, uploads] = await Promise.all([
            models.Member.findAll({
                where: {
                    permission: true,
                    user_id: user.id,
                },
            }),
            models.Upload.findAll({
                where: {
                    permission: true,
                    user_id: user.id,
                },
            }),
        ]);

        const result = await user.update({
            guild_count: members.length,
            size_byte: uploads.reduce(
                (a, b) => a + (b.dataValues.size_byte || 0),
                0
            ),
            file_count: members.reduce(
                (a, b) => a + (b.dataValues.file_count || 0),
                0
            ),
            movie_count: members.reduce(
                (a, b) => a + (b.dataValues.movie_count || 0),
                0
            ),
            message_count: members.reduce(
                (a, b) => a + (b.dataValues.message_count || 0),
                0
            ),
            folder_count: members.reduce(
                (a, b) => a + (b.dataValues.folder_count || 0),
                0
            ),
            playlist_count: members.reduce(
                (a, b) => a + (b.dataValues.playlist_count || 0),
                0
            ),
            role_count: members.reduce(
                (a, b) => a + (b.dataValues.role_count || 0),
                0
            ),
            stream_duration: members.reduce(
                (a, b) => a + (b.dataValues.stream_duration || 0),
                0
            ),
            file_movie_stream_duration: members.reduce(
                (a, b) => a + (b.dataValues.file_movie_stream_duration || 0),
                0
            ),
        });

        return result;
    }
}
