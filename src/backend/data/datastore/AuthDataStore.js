import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { apiAuthenticateIdentityValidates } from '@validations/auth';
import data_config from '@constants/data_config';
import uuidv4 from 'uuid/v4';
import jwt from 'jsonwebtoken';
import tt from 'counterpart';
import { ApiError } from '@extension/Error';
import { missingArgument } from '@extension/log';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class AuthDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new AuthDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async authenticate({ email, password }) {
        await apiAuthenticateIdentityValidates.isValid({
            email,
            password,
        });

        const identity = await models.Identity.findOne({
            where: {
                email,
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.is_not_correct_auth',
            });
        });

        const result = await identity.authenticate.bind(password);

        if (!result)
            throw new ApiError({
                error: new Error(`is_not_correct_auth`),
                tt_key: 'errors.is_not_correct_auth',
            });

        const user = await models.User.findOne({
            where: {
                id: Number(identity.UserId) || 0,
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.is_not_correct_auth',
            });
        });

        identity.User = user;

        return identity;
    }
}
