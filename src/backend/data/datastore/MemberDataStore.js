import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import {
    apiMemberTransferOwnerValidates,
    apiMemberSwitchAdminValidates,
    apiMemberCreateValidates,
    apiMemberUpdateValidates,
} from '@validations/member';
import safe2json from '@extension/safe2json';
import AWSHandler from '@network/aws';
import { missingArgument } from '@extension/log';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const awsHandler = AWSHandler.instance;

export default class MemberDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new MemberDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async update({ member }) {
        await apiMemberUpdateValidates.isValid({
            user,
            nickname: member.nickname,
        });

        const data = await models.Member.findOne({
            where: {
                id: Number(member.id),
            },
        });

        const user = await models.User.findOne({
            where: {
                id: Number(member.UserId),
            },
        });

        const before_picture_small = data.picture_small;
        const before_picture_large = data.picture_large;

        const ps_members = await models.Member.findAll({
            where: {
                id: { $ne: data.id },
                picture_small: before_picture_small,
            },
        });

        const pl_members = await models.Member.findAll({
            where: {
                id: { $ne: data.id },
                picture_small: before_picture_small,
            },
        });

        if (
            before_picture_small != user.picture_small &&
            before_picture_small != member.picture_small &&
            AWSHandler.deletable(before_picture_small) &&
            ps_members.length == 0
        ) {
            await awsHandler.deleteFile(before_picture_small);
        }

        if (
            before_picture_large != user.picture_large &&
            before_picture_large != member.picture_large &&
            AWSHandler.deletable(before_picture_large) &&
            pl_members.length == 0
        ) {
            await awsHandler.deleteFile(before_picture_large);
        }

        const result = await data.update({
            nickname: member.nickname,
            description: member.description,
            picture_small: member.picture_small,
            picture_large: member.picture_large,
            mute: member.mute,
            messages_mute: member.messages_mute,
            mentions_mute: member.mentions_mute,
            everyone_mentions_mute: member.everyone_mentions_mute,
        });

        return result;
    }

    async all_update({ current_user }) {
        const members = await models.Member.findAll({
            where: {
                user_id: Number(current_user.id),
            },
        });

        const picture_smalls = Array.from(
            new Set(members.map(member => member.picture_small))
        );
        const picture_larges = Array.from(
            new Set(members.map(member => member.picture_large))
        );

        const results = await Promise.all(
            members.map(member =>
                member.update({
                    nickname: current_user.nickname,
                    description: current_user.description,
                    picture_small: current_user.picture_small,
                    picture_large: current_user.picture_large,
                })
            )
        );

        const ps_results = await Promise.all(
            picture_smalls.map(async picture_small => {
                if (
                    picture_small != current_user.picture_small &&
                    AWSHandler.deletable(picture_small)
                ) {
                    await awsHandler.deleteFile(picture_small);
                }
            })
        );

        const pl_results = await Promise.all(
            picture_larges.map(async picture_large => {
                if (
                    picture_large != current_user.picture_large &&
                    AWSHandler.deletable(picture_large)
                ) {
                    await awsHandler.deleteFile(picture_large);
                }
            })
        );

        return results;
    }

    async updateNotification({ member }) {
        const data = await models.Member.findOne({
            where: {
                id: Number(member.id),
            },
            raw: false,
        });

        const result = await data.update({
            mute: member.mute,
            messages_mute: member.messages_mute,
            mentions_mute: member.mentions_mute,
            everyone_mentions_mute: member.everyone_mentions_mute,
        });

        return result;
    }

    async getMember({ member }) {
        if (!member || !member.id) return;

        const result = await models.Member.findOne({
            where: {
                id: Number(member.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.MemberRole,
                    include: [
                        {
                            model: models.Role,
                        },
                    ],
                },
                {
                    model: models.User,
                },
            ],
        });

        return result;
    }

    async getAllGuildMembers({ guild, current_user }) {
        if (!guild || !guild.id) return;

        const members = await models.Member.findAll({
            where: {
                guild_id: Number(guild.id) || 0,
                permission: true,
            },
            order: [['id', 'DESC']],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return members;
    }

    async getCurrentGuildMembers({ guild, current_user, limit }) {
        if (!guild || !guild.id) return;

        const members = await models.Member.findAll({
            where: {
                guild_id: Number(guild.id) || 0,
                permission: true,
            },
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return members;
    }

    async getIncrementGuildMembers({ guild, current_user, limit, last_id }) {
        if (!guild || !guild.id) return;

        const members = await models.Member.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                guild_id: Number(guild.id) || 0,
                permission: true,
            },
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return members.reverse();
    }

    async getDecrementGuildMembers({ guild, current_user, limit, first_id }) {
        if (!guild || !guild.id) return;

        const members = await models.Member.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                guild_id: Number(guild.id) || 0,
                permission: true,
            },
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return members;
    }

    async updateTextChannelMembersPermission({
        members,
        channel,
        current_user,
    }) {
        if (!members || members.length == 0) return [];
        if (!channel || !channel.id || !channel.GuildId) return [];

        const [guild, channel_roles] = await Promise.all([
            models.Guild.findOne({
                where: {
                    id: Number(channel.GuildId) || 0,
                    permission: true,
                },
            }),
            models.TextChannelRole.findAll({
                where: {
                    channel_id: Number(channel.id) || 0,
                    permission: true,
                },
                include: [
                    {
                        model: models.Role,
                    },
                ],
            }),
        ]);

        const results = await Promise.all(
            members.map(async member => {
                const entry = await models.TextChannelEntry.findOne({
                    where: {
                        channel_id: Number(channel.id) || 0,
                        member_id: Number(member.id) || 0,
                    },
                });
                if (entry) return null;
                member.setValue('TextChannelEntry', entry);
                const member_roles = await models.Role.findAll({
                    include: [
                        {
                            model: models.MemberRole,
                            where: { member_id: member.id },
                        },
                    ],
                    order: [['priority', 'DESC']],
                });
                member.setValue('Roles', member_roles);

                if (
                    channel_roles.length > 0 &&
                    member_roles.length > 0 &&
                    Number(member.UserId) != Number(guild.OwnerId) &&
                    !member_roles[0].admin &&
                    !member_roles[0].guild_managable &&
                    !member_roles[0].channels_managable
                ) {
                    const includes_roles = channel_roles.filter(
                        val =>
                            member_roles.filter(mr => mr.id == val.RoleId)
                                .length > 0
                    );
                    if (includes_roles.length == 0) return channel;
                    const prioritest_include_role = includes_roles.sort(
                        (a, b) => b.Role.priority - a.Role.priority
                    )[0];

                    await entry.update({
                        permission:
                            prioritest_include_role.Role.channels_viewable &&
                            prioritest_include_role.channel_viewable,
                    });

                    return member;
                } else {
                    await entry.update({
                        permission: true,
                    });
                    return member;
                }
            })
        );

        return results.filter(val => !!val);
    }

    async updateStreamChannelMembersPermission({
        members,
        channel,
        current_user,
    }) {
        if (!members || members.length == 0) return [];
        if (!channel || !channel.id || !channel.GuildId) return [];

        const [guild, channel_roles] = await Promise.all([
            models.Guild.findOne({
                where: {
                    id: Number(channel.GuildId) || 0,
                    permission: true,
                },
            }),
            models.StreamChannelRole.findAll({
                where: {
                    channel_id: Number(channel.id) || 0,
                    permission: true,
                },
                include: [
                    {
                        model: models.Role,
                    },
                ],
            }),
        ]);

        const results = await Promise.all(
            members.map(async member => {
                const entry = await models.StreamChannelEntry.findOne({
                    where: {
                        channel_id: Number(channel.id) || 0,
                        member_id: Number(member.id) || 0,
                    },
                });
                if (entry) return null;
                member.setValue('StreamChannelEntry', entry);
                const member_roles = await models.Role.findAll({
                    include: [
                        {
                            model: models.MemberRole,
                            where: { member_id: member.id },
                        },
                    ],
                    order: [['priority', 'DESC']],
                });
                member.setValue('Roles', member_roles);

                if (
                    channel_roles.length > 0 &&
                    member_roles.length > 0 &&
                    Number(member.UserId) != Number(guild.OwnerId) &&
                    !member_roles[0].admin &&
                    !member_roles[0].guild_managable &&
                    !member_roles[0].channels_managable
                ) {
                    const includes_roles = channel_roles.filter(
                        val =>
                            member_roles.filter(mr => mr.id == val.RoleId)
                                .length > 0
                    );
                    if (includes_roles.length == 0) return channel;
                    const prioritest_include_role = includes_roles.sort(
                        (a, b) => b.Role.priority - a.Role.priority
                    )[0];

                    await entry.update({
                        permission:
                            prioritest_include_role.Role.channels_viewable &&
                            prioritest_include_role.channel_viewable,
                    });

                    return member;
                } else {
                    await entry.update({
                        permission: true,
                    });
                    return member;
                }
            })
        );
        return results.filter(val => !!val);
    }

    async updateDMChannelMembersPermission({ members, channel, current_user }) {
        if (!members || members.length == 0) return [];
        if (!channel || !channel.id || !channel.GuildId) return [];

        const [guild] = await Promise.all([
            models.Guild.findOne({
                where: {
                    id: Number(channel.GuildId) || 0,
                    permission: true,
                },
            }),
        ]);

        const results = await Promise.all(
            members.map(async member => {
                const entry = await models.DMChannelEntry.findOne({
                    where: {
                        channel_id: Number(channel.id) || 0,
                        member_id: Number(member.id) || 0,
                    },
                });
                if (entry) return null;
                member.setValue('DMChannelEntry', entry);
                await entry.update({
                    permission: true,
                });
                return member;
            })
        );

        return results.filter(val => !!val);
    }

    async getCurrentTextChannelMembers({ channel, current_user, limit }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const entries = await models.TextChannelEntry.findAll({
            where: {
                channel_id: Number(channel.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return entries.map(entry => {
            const member = entry.Member;
            delete entry.Member;
            delete entry.dataValues.Member;
            member.setValue('TextChannelEntry', entry);
            return member;
        });
    }

    async getIncrementTextChannelMembers({
        channel,
        current_user,
        last_id,
        limit,
    }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const entries = await models.TextChannelEntry.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                channel_id: Number(channel.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return entries
            .map(entry => {
                const member = entry.Member;
                delete entry.Member;
                delete entry.dataValues.Member;
                member.setValue('TextChannelEntry', entry);
                return member;
            })
            .reverse();
    }

    async getDecrementTextChannelMembers({
        channel,
        current_user,
        first_id,
        limit,
    }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const entries = await models.TextChannelEntry.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                channel_id: Number(channel.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return entries.map(entry => {
            const member = entry.Member;
            delete entry.Member;
            delete entry.dataValues.Member;
            member.setValue('TextChannelEntry', entry);
            return member;
        });
    }

    async getCurrentStreamChannelMembers({ channel, current_user, limit }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const entries = await models.StreamChannelEntry.findAll({
            where: {
                channel_id: Number(channel.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return entries.map(entry => {
            const member = entry.Member;
            delete entry.Member;
            delete entry.dataValues.Member;
            member.setValue('StreamChannelEntry', entry);
            return member;
        });
    }

    async getIncrementStreamChannelMembers({
        channel,
        current_user,
        last_id,
        limit,
    }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const entries = await models.StreamChannelEntry.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                channel_id: Number(channel.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return entries
            .map(entry => {
                const member = entry.Member;
                delete entry.Member;
                delete entry.dataValues.Member;
                member.setValue('StreamChannelEntry', entry);
                return member;
            })
            .reverse();
    }

    async getDecrementStreamChannelMembers({
        channel,
        current_user,
        first_id,
        limit,
    }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const entries = await models.StreamChannelEntry.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                channel_id: Number(channel.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return entries.map(entry => {
            const member = entry.Member;
            delete entry.Member;
            delete entry.dataValues.Member;
            member.setValue('StreamChannelEntry', entry);
            return member;
        });
    }

    async getCurrentDMChannelMembers({ channel, current_user, limit }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const entries = await models.DMChannelEntry.findAll({
            where: {
                channel_id: Number(channel.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return entries.map(entry => {
            const member = entry.Member;
            delete entry.Member;
            delete entry.dataValues.Member;
            member.setValue('DMChannelEntry', entry);
            return member;
        });
    }

    async getIncrementDMChannelMembers({
        channel,
        current_user,
        last_id,
        limit,
    }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const entries = await models.DMChannelEntry.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                channel_id: Number(channel.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return entries
            .map(entry => {
                const member = entry.Member;
                delete entry.Member;
                delete entry.dataValues.Member;
                member.setValue('DMChannelEntry', entry);
                return member;
            })
            .reverse();
    }

    async getDecrementDMChannelMembers({
        channel,
        current_user,
        first_id,
        limit,
    }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const entries = await models.DMChannelEntry.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                channel_id: Number(channel.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return entries.map(entry => {
            const member = entry.Member;
            delete entry.Member;
            delete entry.dataValues.Member;
            member.setValue('DMChannelEntry', entry);
            return member;
        });
    }

    async getAllTextChannelMembers({
        channel,
        current_user,
        role_ignore = true,
    }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const where = {
            channel_id: Number(channel.id) || 0,
        };
        if (!role_ignore) {
            where.permission = true;
        }
        const entries = await models.TextChannelEntry.findAll({
            where: {
                ...where,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
        });
        return entries.map(entry => entry.Member);
    }

    async getAllStreamChannelMembers({
        channel,
        current_user,
        role_ignore = true,
    }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const where = {
            channel_id: Number(channel.id) || 0,
        };
        if (!role_ignore) {
            where.permission = true;
        }
        const entries = await models.StreamChannelEntry.findAll({
            where: {
                ...where,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
        });
        return entries.map(entry => entry.Member);
    }

    async getAllDMChannelMembers({
        channel,
        current_user,
        role_ignore = true,
    }) {
        if (!channel || !channel.id || !channel.GuildId) return [];
        const where = {
            channel_id: Number(channel.id) || 0,
        };
        if (!role_ignore) {
            where.permission = true;
        }
        const entries = await models.DMChannelEntry.findAll({
            where: {
                ...where,
            },
            include: [
                {
                    model: models.Member,
                },
            ],
        });
        return entries.map(entry => entry.Member);
    }

    async getTextChannelInvitableMembers({ channel, current_user }) {
        if (!channel || !channel.id) return [];

        const members = await this.getAllTextChannelMembers({
            channel,
            role_ignore: true,
            current_user,
        });

        const current_member = await models.Member.findOne({
            where: {
                guild_id: Number(channel.GuildId) || 0,
                user_id: Number(current_user.id) || 0,
            },
        });

        if (!current_member) return [];

        const all_members = await this.getAllGuildMembers({
            guild: {
                id: Number(channel.GuildId) || 0,
            },
            current_user,
        });

        const ids = [current_member.id, ...members.map(val => !!val && val.id)];

        return all_members.filter(val => val && !ids.includes(val.id));
    }

    async getStreamChannelInvitableMembers({ channel, current_user }) {
        if (!channel || !channel.id || !channel.GuildId) return [];

        const members = await this.getAllStreamChannelMembers({
            channel,
            role_ignore: true,
            current_user,
        });

        const current_member = await models.Member.findOne({
            where: {
                guild_id: Number(channel.GuildId) || 0,
                user_id: Number(current_user.id) || 0,
            },
        });

        if (!current_member) return [];

        const all_members = await this.getAllGuildMembers({
            guild: {
                id: Number(channel.GuildId) || 0,
            },
            current_user,
        });

        const ids = [current_member.id, ...members.map(val => !!val && val.id)];

        return all_members.filter(val => val && !ids.includes(val.id));
    }

    async getDMChannelInvitableMembers({ channel, current_user }) {
        if (!channel || !channel.id || !channel.GuildId) return [];

        const members = await this.getAllDMChannelMembers({
            channel,
            current_member,
            role_ignore: true,
        });

        const current_member = await models.Member.findOne({
            where: {
                guild_id: Number(channel.GuildId) || 0,
                user_id: Number(current_user.id) || 0,
            },
        });

        if (!current_member) return [];

        const all_members = await this.getAllGuildMembers({
            guild: {
                id: Number(channel.GuildId) || 0,
            },
            current_user,
        });

        const ids = [current_member.id, ...members.map(val => !!val && val.id)];

        return all_members.filter(val => val && !ids.includes(val.id));
    }

    async getCurrentRoleMembers({ role, current_user, limit }) {
        if (!role || !role.id) return [];
        const results = await models.Member.findAll({
            where: {
                permission: true,
            },
            include: [
                {
                    model: models.MemberRole,
                    where: {
                        role_id: Number(role.id) || 0,
                    },
                    include: [
                        {
                            model: models.Role,
                        },
                    ],
                },
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return results;
    }

    async getIncrementRoleMembers({ role, current_user, limit, last_id }) {
        if (!role || !role.id) return [];
        const results = await models.Member.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                permission: true,
            },
            include: [
                {
                    model: models.MemberRole,
                    where: {
                        role_id: Number(role.id) || 0,
                    },
                    include: [
                        {
                            model: models.Role,
                        },
                    ],
                },
            ],
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return results.reverse();
    }

    async getDecrementRoleMembers({ role, current_user, limit, first_id }) {
        if (!role || !role.id) return [];
        const results = await models.Member.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                permission: true,
            },
            include: [
                {
                    model: models.MemberRole,
                    where: {
                        role_id: Number(role.id) || 0,
                    },
                    include: [
                        {
                            model: models.Role,
                        },
                    ],
                },
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        });
        return results;
    }

    async transferOwner({ guild, target, owner }) {
        await apiMemberTransferOwnerValidates.isValid({
            target,
            guild,
        });

        const data = await models.Guild.findOne({
            where: {
                id: Number(guild.id),
            },
        });

        const o_member = await models.Member.findOne({
            where: {
                guild_id: Number(guild.id) || 0,
                GuildId: Number(guild.id) || 0,
                UserId: Number(guild.OwnerId) || 0,
                user_id: Number(guild.OwnerId) || 0,
                permission: true,
            },
        });

        const t_member = await models.Member.findOne({
            where: {
                guild_id: Number(guild.id) || 0,
                GuildId: Number(guild.id) || 0,
                UserId: Number(target.id) || 0,
                user_id: Number(target.id) || 0,
                permission: true,
            },
        });

        const result = await data.update({
            OwnerId: Number(target.id) || 0,
            owner_id: Number(target.id) || 0,
        });

        return result;
    }

    async updateCount(value) {
        if (!value) return;
        const member = await models.Member.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!member) return;

        const [
            files,
            movies,
            messages,
            folders,
            playlists,
            roles,
            participants,
            file_movie_participants,
        ] = await Promise.all([
            models.File.findAll({
                where: {
                    permission: true,
                    member_id: member.id,
                },
            }),
            models.File.findAll({
                where: {
                    format: models.File.movie_where(models),
                    member_id: member.id,
                    permission: true,
                },
            }),
            models.Message.findAll({
                where: {
                    permission: true,
                    sender_id: member.id,
                },
            }),
            models.Folder.findAll({
                where: {
                    permission: true,
                    member_id: member.id,
                },
            }),
            models.Folder.findAll({
                where: {
                    permission: true,
                    member_id: member.id,
                    is_playlist: true,
                },
            }),
            models.MemberRole.findAll({
                where: {
                    permission: true,
                    member_id: member.id,
                },
            }),
            models.Participant.findAll({
                where: {
                    permission: true,
                    member_id: member.id,
                    is_live: false,
                },
            }),
            models.Participant.findAll({
                where: {
                    permission: true,
                    member_id: Number(member.id),
                    is_live: false,
                },
                include: [
                    {
                        model: models.Stream,
                        include: [
                            {
                                where: {
                                    format: models.File.movie_where(models),
                                },
                                model: models.File,
                            },
                        ],
                    },
                ],
            }),
        ]);

        const result = await member.update({
            size_byte: files.reduce(
                (a, b) => a + (b.dataValues.size_byte || 0),
                0
            ),
            file_count: files.length,
            movie_count: movies.length,
            message_count: messages.length,
            folder_count: folders.length,
            playlist_count: playlists.length,
            role_count: roles.length,
            stream_duration: participants.reduce(
                (a, b) =>
                    a +
                    (b.dataValues.delivery_end_at -
                        b.dataValues.delivery_start_at),
                0
            ),
            file_movie_stream_duration: file_movie_participants.reduce(
                (a, b) =>
                    a +
                    (b.dataValues.delivery_end_at -
                        b.dataValues.delivery_start_at),
                0
            ),
        });

        return result;
    }

    async index_update({ member, current_user }) {
        const before = await models.Member.findOne({
            where: {
                id: Number(member.id) || 0,
            },
        });

        const result = await before.update({
            index: Number(member.index) || 0,
        });

        return result;
    }

    async setLastIndex(value) {
        if (!value) return;
        const member = await models.Member.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!member) return;

        const members = await models.Member.findAll({
            where: {
                user_id: Number(member.UserId) || 0,
                permission: true,
            },
        });

        const result = await member.update({
            index: 1,
        });

        const results = await Promise.all(
            members.map(val => val.update({ index: val.index + 1 }))
        );

        return result.index;
    }
}
