import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class ReactionDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new ReactionDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async create({ reaction }) {
        const result = await models.Reaction.create({
            MessageId: Number(reaction.MessageId) || 0,
            message_id: Number(reaction.MessageId) || 0,
            MemberId: Number(reaction.MemberId) || 0,
            member_id: Number(reaction.MemberId) || 0,
            data: reaction.data,
            permission: true,
        });
        return result;
    }

    async delete({ reaction }) {
        if (!reaction) return;
        if (!reaction.id) return;

        const result = await models.Reaction.destroy({
            where: {
                id: Number(reaction.id) || 0,
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });
    }
}
