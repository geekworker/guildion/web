import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import {
    apiTextChannelCreateValidates,
    apiTextChannelUpdateValidates,
    apiTextChannelDeleteValidates,
} from '@validations/stream_channel';
import safe2json from '@extension/safe2json';
import AWSHandler from '@network/aws';
import { missingArgument } from '@extension/log';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const awsHandler = AWSHandler.instance;

export default class TextChannelDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new TextChannelDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async getChannel({ channel, current_user }) {
        if (!channel || !channel.uid) return;

        const result = await models.TextChannel.findOne({
            where: {
                uid: channel.uid,
                permission: true,
            },
            // include: models.TextChannel.show_include(models),
        });

        const [entries, textChannelRoles] = await Promise.all([
            await models.TextChannelEntry.findAll({
                where: {
                    channel_id: result.id,
                },
                include: [
                    {
                        model: models.Member,
                    },
                ],
            }),
            await models.TextChannelRole.findAll({
                where: {
                    channel_id: result.id,
                    permission: true,
                },
                include: [
                    {
                        model: models.Role,
                    },
                ],
            }),
        ]);

        const members = entries.map(entry => {
            const member = entry.Member || entry.dataValues.Member;
            delete entry.Member;
            delete entry.dataValues.Member;
            member.setValue('TextChannelEntry', entry);
            return member;
        });

        result.setValue('Members', members);
        result.setValue('TextChannelRoles', textChannelRoles);

        return result;
    }

    async create({ user, channel, section, members }) {
        // await apiTextChannelCreateValidates.isValid({
        //     user,
        //     section,
        //     channel,
        //     members
        // });

        const created = await models.TextChannel.create({
            guild_id: Number(channel.GuildId) || 0,
            GuildId: Number(channel.GuildId) || 0,
            SectionId: section ? Number(section.id) || null : null,
            section_id: section ? Number(section.id) || null : null,
            uid: models.TextChannel.generate_uid(),
            name: channel.name,
            description: channel.description,
            read_only: channel.read_only,
            index: Number(channel.index) || 0,
            is_nsfw: channel.is_nsfw,
            is_private: channel.is_private,
            read_only: channel.read_only,
            is_default: true,
            permission: true,
        });

        await this.findOrCreateEntries({ channel: created, members });
        created.index = await this.setLastIndex(created);

        return created;
    }

    async update({ user, channel, section, members }) {
        // await apiTextChannelUpdateValidates.isValid({
        //     user,
        //     channel,
        // });

        const data = await models.TextChannel.findOne({
            where: {
                id: Number(channel.id),
            },
            raw: false,
        });

        const result = await data.update({
            name: channel.name,
            description: channel.description,
            read_only: channel.read_only,
            is_nsfw: channel.is_nsfw,
            is_default: channel.is_default,
            is_private: channel.is_private,
            read_only: channel.read_only,
            permission: channel.permission,
        });

        // this.findOrCreateEntries({ channel: result, members });

        return result;
    }

    async section_index_update({ user, channel }) {
        // await apiTextChannelUpdateValidates.isValid({
        //     user,
        //     channel,
        //     section,
        // });

        const data = await models.TextChannel.findOne({
            where: {
                id: Number(channel.id),
            },
            raw: false,
        });

        const result = await data.update({
            SectionId: Number(channel.SectionId) || null,
            section_id: Number(channel.SectionId) || null,
            index: Number(channel.index) || 0,
        });

        if (!data.SectionId && !!result.SectionId) {
            const [
                section_channels,
                text_channels,
                stream_channels,
                dm_channels,
            ] = await Promise.all([
                await models.SectionChannel.findAll({
                    where: {
                        guild_id: Number(channel.GuildId) || 0,
                        is_dm: false,
                        index: {
                            $gt: Number(channel.index) || 0,
                        },
                        permission: true,
                    },
                }),
                await models.TextChannel.findAll({
                    where: {
                        guild_id: Number(channel.GuildId) || 0,
                        section_id: null,
                        index: {
                            $gt: Number(channel.index) || 0,
                        },
                        permission: true,
                    },
                }),
                await models.StreamChannel.findAll({
                    where: {
                        guild_id: Number(channel.GuildId) || 0,
                        section_id: null,
                        index: {
                            $gt: Number(channel.index) || 0,
                        },
                        temporary: false,
                        permission: true,
                    },
                }),
                await models.DMChannel.findAll({
                    where: {
                        guild_id: Number(channel.GuildId) || 0,
                        section_id: null,
                        index: {
                            $gt: Number(channel.index) || 0,
                        },
                        permission: true,
                    },
                }),
            ]);

            Promise.all(
                text_channels.map(text_channel =>
                    text_channel.update({
                        index: text_channel.index - 1,
                    })
                )
            );

            Promise.all(
                stream_channels.map(stream_channel =>
                    stream_channel.update({
                        index: stream_channel.index - 1,
                    })
                )
            );

            Promise.all(
                dm_channels.map(dm_channel =>
                    dm_channel.update({
                        index: dm_channel.index - 1,
                    })
                )
            );

            Promise.all(
                section_channels.map(section_channel =>
                    section_channel.update({
                        index: section_channel.index - 1,
                    })
                )
            );
        }

        return result;
    }

    async updateEntry({ entry }) {
        const before = await models.TextChannelEntry.findOne({
            where: {
                id: Number(entry.id),
            },
        });

        const result = await before.update({
            mute: entry.mute,
            messages_mute: entry.messages_mute,
            mentions_mute: entry.mentions_mute,
            everyone_mentions_mute: entry.everyone_mentions_mute,
        });

        return result;
    }

    async delete({ channel, user }) {
        // await apiTextChannelDeleteValidates.isValid({
        //     user,
        //     channel,
        // });

        const result = await models.TextChannel.destroy({
            where: {
                id: Number(channel.id),
            },
        });

        const [
            text_channels,
            stream_channels,
            dm_channels,
            section_channels,
        ] = await Promise.all([
            await models.TextChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                },
            }),
            await models.StreamChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                    temporary: false,
                },
            }),
            await models.DMChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                },
            }),
            await models.SectionChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    is_dm: false,
                    permission: true,
                },
            }),
        ]);

        Promise.all(
            text_channels.map(text_channel =>
                text_channel.update({
                    index: text_channel.index - 1,
                })
            )
        );

        Promise.all(
            stream_channels.map(stream_channel =>
                stream_channel.update({
                    index: stream_channel.index - 1,
                })
            )
        );

        Promise.all(
            dm_channels.map(dm_channel =>
                dm_channel.update({
                    index: dm_channel.index - 1,
                })
            )
        );

        if (!channel.SectionId) {
            Promise.all(
                section_channels.map(section_channel =>
                    section_channel.update({ index: section_channel.index - 1 })
                )
            );
        }

        return result;
    }

    async findOrCreateEntries({ channel, members }) {
        let entries = [];
        if (
            Number.prototype.castBool(channel.is_private) &&
            !!members &&
            members.length > 0
        ) {
            const deleted = await models.TextChannelEntry.destroy({
                where: {
                    channel_id: Number(channel.id) || 0,
                    member_id: {
                        $notIn: members.map(val => Number(val.UserId) || 0),
                    },
                },
            });

            entries = await Promise.all(
                members.map(async member => {
                    const [
                        entry,
                        created,
                    ] = await models.TextChannelEntry.findOrCreate({
                        where: {
                            ChannelId: Number(channel.id) || 0,
                            channel_id: Number(channel.id) || 0,
                            MemberId: Number(member.id) || 0,
                            member_id: Number(member.id) || 0,
                        },
                    });
                    if (created) {
                        return await entry.update({
                            permission: true,
                        });
                    }
                    return entry;
                })
            );
        }

        if (!Number.prototype.castBool(channel.is_private)) {
            const all_members = await models.Member.findAll({
                where: {
                    permission: true,
                },
                include: [
                    {
                        model: models.Guild,
                        where: {
                            id: Number(channel.GuildId) || 0,
                        },
                    },
                ],
            });

            const deleted = await models.TextChannelEntry.destroy({
                where: {
                    channel_id: Number(channel.id) || 0,
                    member_id: {
                        $notIn: all_members.map(val => Number(val.UserId) || 0),
                    },
                },
            });

            entries = await Promise.all(
                all_members.map(async member => {
                    const [
                        entry,
                        created,
                    ] = await models.TextChannelEntry.findOrCreate({
                        where: {
                            ChannelId: Number(channel.id) || 0,
                            channel_id: Number(channel.id) || 0,
                            MemberId: Number(member.id) || 0,
                            member_id: Number(member.id) || 0,
                        },
                    });
                    if (created) {
                        return await entry.update({
                            permission: true,
                        });
                    }
                    return entry;
                })
            );
        }

        this.updateCount(channel);

        return entries;
    }

    async join({ channel, member }) {
        // await apiTextChannelJoinValidates.isValid({
        //     member,
        //     channel,
        // });

        const [entry, created] = await models.TextChannelEntry.findOrCreate({
            where: {
                ChannelId: Number(channel.id) || 0,
                MemberId: Number(member.id) || 0,
            },
        });

        if (created) {
            entry.update({
                permission: true,
            });
        }

        this.updateCount(channel);

        return entry;
    }

    async leave({ channel, member }) {
        // await apiTextChannelLeaveValidates.isValid({
        //     member,
        //     channel,
        // });

        await models.TextChannelEntry.destroy({
            where: {
                channel_id: Number(channel.id) || 0,
                member_id: Number(member.id) || 0,
            },
        });

        this.updateCount(channel);

        return;
    }

    async add_role({ channel, role }) {
        let [entry, created] = await models.TextChannelRole.findOrCreate({
            where: {
                ChannelId: Number(channel.id) || 0,
                RoleId: Number(role.id) || 0,
                permission: true,
            },
        });

        if (created) {
            entry = await entry.update({
                roles_managable: Number.prototype.castBool(
                    role.roles_managable
                ),
                channel_managable: Number.prototype.castBool(
                    role.channels_managable
                ),
                channel_members_managable: Number.prototype.castBool(
                    role.channels_managable
                ),
                kickable: Number.prototype.castBool(role.kickable),
                invitable: Number.prototype.castBool(role.channels_invitable),
                channel_viewable: Number.prototype.castBool(
                    role.channels_viewable
                ),
                message_sendable: Number.prototype.castBool(
                    role.message_sendable
                ),
                messages_managable: Number.prototype.castBool(
                    role.messages_managable
                ),
                message_embeddable: Number.prototype.castBool(
                    role.message_embeddable
                ),
                message_attachable: Number.prototype.castBool(
                    role.message_attachable
                ),
                messages_readable: Number.prototype.castBool(
                    role.messages_readable
                ),
                message_mentionable: Number.prototype.castBool(
                    role.message_mentionable
                ),
                message_reactionable: Number.prototype.castBool(
                    role.message_mentionable
                ),
            });
        }

        return entry;
    }

    async edit_role({ channelRole }) {
        const data = await models.TextChannelRole.findOne({
            where: {
                id: Number(channelRole.id) || 0,
            },
        });

        const result = await data.update({
            ...channelRole,
        });

        return result;
    }

    async remove_role({ channel, role }) {
        await models.TextChannelRole.destroy({
            where: {
                channel_id: Number(channel.id) || 0,
                role_id: Number(role.id) || 0,
            },
        });

        return;
    }

    async switchPermission({ user, channel, permission }) {
        const member = await models.Member.findOne({
            where: {
                guild_id: Number(channel.GuildId) || 0,
                user_id: Number(user.id) || 0,
            },
        });

        const guild = await models.Guild.findOne({
            where: {
                id: Number(channel.GuildId) || 0,
            },
        });

        const data = await models.TextChannel.findOne({
            where: {
                id: Number(channel.id),
            },
        });

        const result = await data.update({
            permission: Number.prototype.castBool(permission),
        });

        return result;
    }

    async updateCount(value) {
        if (!value) return;
        const channel = await models.TextChannel.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!channel) return;

        const entries = await models.TextChannelEntry.findAll({
            where: {
                channel_id: Number(channel.id),
            },
        });

        const messages = await models.Message.findAll({
            where: {
                messageable_id: Number(channel.id),
                messageable_type: 'TextChannel',
            },
        });

        const result = await channel.update({
            member_count: entries.length,
            message_count: messages.length,
        });

        return result;
    }

    async updateCountFromMessage(message) {
        if (
            !message ||
            message.messageable_type != 'TextChannel' ||
            !Number(message.messageable_id)
        )
            return;
        const channel = { id: Number(message.messageable_id) || 0 };
        return this.updateCount(channel);
    }

    async setLastIndex(value) {
        if (!value) return;
        const channel = await models.TextChannel.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!channel) return;

        const [
            text_channels,
            stream_channels,
            dm_channels,
            section_channels,
        ] = await Promise.all([
            await models.TextChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                },
            }),
            await models.StreamChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                    temporary: false,
                },
            }),
            await models.DMChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                },
            }),
            await models.SectionChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    is_dm: false,
                    permission: true,
                },
            }),
        ]);

        const result = await channel.update({
            index:
                text_channels.length +
                stream_channels.length +
                dm_channels.length,
        });

        if (!channel.SectionId) {
            Promise.all(
                section_channels.map(channel =>
                    channel.update({ index: channel.index + 1 })
                )
            );
        }

        return result.index;
    }

    async reads({ channel, member }) {
        const reads = await models.Read.findAll({
            where: {
                member_id: Number(member.id) || 0,
                complete: false,
            },
            include: [
                {
                    model: models.Message,
                    where: {
                        messageable_id: Number(channel.id) || 0,
                        messageable_type: 'TextChannel',
                    },
                },
            ],
        });

        const results = await Promise.map(
            reads,
            read => read.update({ complete: true }),
            { concurrency: 10 }
        );

        return results;
    }
}
