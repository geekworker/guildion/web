import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class DocumentDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new DocumentDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async update({ document }) {
        const data = await models.Document.findOne({
            where: {
                id: Number(document.id),
            },
        });

        const result = await data.update({
            ja_html: document.ja_html,
            en_html: document.en_html,
            ja_title: document.ja_title,
            en_title: document.en_title,
            ja_thumbnail: document.ja_thumbnail,
            en_thumbnail: document.en_thumbnail,
        });

        return result;
    }

    async findOrCreate({ document }) {
        if (!!document.section_uid && document.section_uid != '') {
            const section = await models.DocumentSection.findOne({
                where: {
                    uid: document.section_uid,
                },
            });
            document.SectionId = section.id;
        }

        if (!!document.group_uid && document.group_uid != '') {
            const group = await models.DocumentGroup.findOne({
                where: {
                    uid: document.group_uid,
                },
            });
            document.GroupId = group.id;
        }

        let data = await models.Document.findOne({
            where: {
                uid: document.uid,
            },
        });

        if (!data) {
            data = await models.Document.create({
                uid: document.uid,
                section_id: Number(document.SectionId) || null,
                group_id: Number(document.GroupId) || 0,
            });
        }

        const result = await data.update({
            section_id: Number(document.SectionId) || null,
            SectionId: Number(document.SectionId) || null,
            group_id: Number(document.GroupId) || 0,
            GroupId: Number(document.GroupId) || 0,
            ja_html: document.ja_html,
            en_html: document.en_html,
            ja_title: document.ja_title,
            en_title: document.en_title,
            ja_thumbnail: document.ja_thumbnail,
            en_thumbnail: document.en_thumbnail,
            template: document.template,
            version: document.version,
            index: Number(document.index) || 0,
            permission: document.permission,
        });

        return result;
    }

    async findOrCreateSection({ documentSection }) {
        if (
            !!documentSection.section_uid &&
            documentSection.section_uid != ''
        ) {
            const section = await models.DocumentSection.findOne({
                where: {
                    uid: documentSection.section_uid,
                },
            });
            documentSection.SectionId = section.id;
        }

        if (!!documentSection.group_uid && documentSection.group_uid != '') {
            const group = await models.DocumentGroup.findOne({
                where: {
                    uid: documentSection.group_uid,
                },
            });
            documentSection.GroupId = group.id;
        }

        let data = await models.DocumentSection.findOne({
            where: {
                uid: documentSection.uid,
            },
        });

        if (!data) {
            data = await models.DocumentSection.create({
                uid: documentSection.uid,
                section_id: Number(documentSection.SectionId) || null,
                group_id: Number(documentSection.GroupId) || 0,
            });
        }

        const result = await data.update({
            section_id: Number(documentSection.SectionId) || null,
            SectionId: Number(documentSection.SectionId) || null,
            group_id: Number(documentSection.GroupId) || 0,
            GroupId: Number(documentSection.GroupId) || 0,
            ja_title: documentSection.ja_title,
            en_title: documentSection.en_title,
            template: documentSection.template,
            index: Number(documentSection.index) || 0,
            permission: documentSection.permission,
        });

        return result;
    }

    async findOrCreateGroup({ documentGroup }) {
        const [data, created] = await models.DocumentGroup.findOrCreate({
            where: {
                uid: documentGroup.uid,
            },
        });

        const result = await data.update({
            groupname: documentGroup.groupname,
            ja_title: documentGroup.ja_title,
            en_title: documentGroup.en_title,
            template: documentGroup.template,
            permission: documentGroup.permission,
        });

        return result;
    }

    async freezeNotSyncs({ document_uids }) {
        if (!document_uids || document_uids.length == 0) return [];
        const results = await models.Document.update(
            {
                permission: false,
            },
            {
                where: {
                    uid: {
                        $notIn: document_uids,
                    },
                },
            }
        );
    }

    async freezeNotSyncSections({ documentSection_uids }) {
        if (!documentSection_uids || documentSection_uids.length == 0)
            return [];
        const results = await models.DocumentSection.update(
            {
                permission: false,
            },
            {
                where: {
                    uid: {
                        $notIn: documentSection_uids,
                    },
                },
            }
        );
    }

    async freezeNotSyncGroups({ documentGroup_uids }) {
        if (!documentGroup_uids || documentGroup_uids.length == 0) return [];
        const results = await models.DocumentGroup.update(
            {
                permission: false,
            },
            {
                where: {
                    uid: {
                        $notIn: documentGroup_uids,
                    },
                },
            }
        );
    }
}
