import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import AWSHandler from '@network/aws';
import safe2json from '@extension/safe2json';
import safe2array from '@extension/safe2array';
import {
    apiStreamCreateValidates,
    apiStreamSwitchValidates,
    apiStreamJoinValidates,
    apiStreamLeaveValidates,
} from '@validations/stream';
import path from 'path';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const awsHandler = AWSHandler.instance;

export default class StreamDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new StreamDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async startLive({ stream }) {
        // await apiStreamCreateValidates.isValid({
        //     stream,
        // });

        const result = await models.Stream.create({
            streamable_id: Number(stream.streamable_id) || 0,
            StreamableId: Number(stream.streamable_id) || 0,
            streamable_type: stream.streamable_type,
            ChannelId: Number(stream.ChannelId) || 0,
            channel_id: Number(stream.ChannelId) || 0,
            delivery_start_at: new Date(),
            is_live: true,
            permission: true,
        });

        const created = await models.Stream.findOne({
            where: {
                id: result.id,
            },
            include: [...models.Stream.index_include(models)],
        });

        return created;
    }

    async switchLive({ stream, before_stream }) {
        // await apiStreamSwitchValidates.isValid({
        //     stream,
        //     before_stream,
        // });

        const before_participants = await models.Participant.findAll({
            where: {
                is_live: true,
                stream_id: Number(before_stream.id) || 0,
            },
            include: [
                {
                    as: 'Member',
                    model: models.Member,
                },
            ],
        });

        const result = await models.Stream.create({
            streamable_id: Number(stream.streamable_id) || 0,
            StreamableId: Number(stream.streamable_id) || 0,
            streamable_type: stream.streamable_type,
            ChannelId: Number(stream.ChannelId) || 0,
            channel_id: Number(stream.ChannelId) || 0,
            delivery_start_at: new Date(),
            is_live: true,
            permission: true,
        });

        const results = await Promise.all(
            before_participants.map(participant =>
                this.join({ stream: result, member: participant.Member })
            )
        );

        await this.endLive({ stream: before_stream });

        const created = await models.Stream.findOne({
            where: {
                id: result.id,
            },
            include: [...models.Stream.index_include(models)],
        });

        return created;
    }

    async endLive({ stream }) {
        if (!stream || !stream.id) return;

        stream = await models.Stream.findOne({
            where: {
                id: Number(stream.id) || 0,
            },
        });

        if (!stream) return;

        stream = await stream.update({
            delivery_end_at: new Date(),
            is_live: false,
        });

        const participants = await models.Participant.findAll({
            where: {
                is_live: true,
                stream_id: Number(stream.id) || 0,
            },
        });

        const p_results = await Promise.all(
            participants.map(participant =>
                participant.update({
                    is_live: false,
                    delivery_end_at: new Date(),
                })
            )
        );

        const channel = await models.StreamChannel.findOne({
            where: {
                id: Number(stream.ChannelId) || 0,
                temporary: true,
            },
        });

        if (!!channel) {
            channel.update({ permission: false });
        }

        return stream;
    }

    async saveDurationOffset({ stream, duration }) {
        if (!stream || !stream.id) return;

        stream = await models.Stream.findOne({
            where: {
                id: Number(stream.id) || 0,
            },
        });

        if (!stream) return;

        const participants = await models.Participant.findAll({
            where: {
                is_live: true,
                stream_id: Number(stream.id) || 0,
            },
        });

        const channel = await models.StreamChannel.findOne({
            where: {
                id: Number(stream.ChannelId) || 0,
                temporary: true,
            },
        });

        if ((!!channel && participants.length == 0) || (participants.length == 0 && stream.StreamableId == null) || (participants.length == 0 && stream.StreamableId == 0)) {
            return await this.endLive({ stream });
        }

        stream = await stream.update({
            duration: Number(duration) || 0,
        });

        return stream;
    }

    async delete({ stream }) {
        if (!stream || !stream.id) return;

        const result = await models.Stream.destroy({
            where: {
                id: Number(stream.id) || 0,
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return result;
    }

    async endLiveAll({ member }) {
        if (!member || !member.id) return;

        const participants = await models.Participant.findAll({
            where: {
                member_id: Number(member.id) || 0,
                is_live: true,
            },
            include: [
                {
                    model: models.Stream,
                    required: true,
                },
            ],
        });

        if (participants.length == 0) return;

        const results = await Promise.all(
            participants.map(participant =>
                this.leave({ stream: participant.Stream, member })
            )
        );

        return results;
    }

    async endLiveAllFromUser({ user }) {
        if (!user || !user.id) return;

        const members = await models.Member.findAll({
            where: {
                user_id: Number(user.id) || 0,
            },
        });

        if (members.length == 0) return;

        return await Promise.all(
            members.map(member => this.endLiveAll({ member }))
        );
    }

    async join({ member, stream }) {
        if (!member || !member.id || !stream || !stream.id) return;

        // await apiStreamJoinValidates.isValid({
        //     member,
        //     stream,
        // });

        const participants = await models.Participant.findAll({
            where: {
                stream_id: Number(stream.id) || 0,
                member_id: Number(member.id) || 0,
                is_live: true,
            },
            order: [['id', 'DESC']],
        });

        if (participants.length > 0) return participants[0];

        const participant = await models.Participant.create({
            stream_id: Number(stream.id) || 0,
            member_id: Number(member.id) || 0,
            delivery_start_at: new Date(),
            is_live: true,
            permission: true,
        });

        return participant;
    }

    async leave({ member, stream }) {
        if (!member || !member.id || !stream || !stream.id) return;

        // await apiStreamLeaveValidates.isValid({
        //     participant: {
        //         member,
        //         stream,
        //     },
        // });

        const participants = await models.Participant.findAll({
            where: {
                member_id: Number(member.id) || 0,
                stream_id: Number(stream.id) || 0,
                is_live: true,
            },
            order: [['id', 'DESC']],
        });

        if (participants.length == 0) return;

        const results = await Promise.all(
            participants.map(val =>
                val.update({
                    delivery_end_at: val.delivery_end_at || new Date(),
                    is_live: false,
                })
            )
        );

        const all_participants = await models.Participant.findAll({
            where: {
                member_id: Number(member.id) || 0,
                is_live: true,
            },
            order: [['id', 'DESC']],
        });

        const all_results = await Promise.all(
            all_participants.map(val =>
                val.update({
                    delivery_end_at: val.delivery_end_at || new Date(),
                    is_live: false,
                })
            )
        );

        return results[0];
    }
}
