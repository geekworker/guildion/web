import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { missingArgument } from '@extension/log';
import Promise from 'bluebird';
import safe2json from '@extension/safe2json';
import data_config from '@constants/data_config';
import { ApiError } from '@extension/Error';
import validator from 'validator';
import config from '@constants/config';
import notification_config from '@constants/notification_config';
import Notification from '@network/notification';
import querystring from 'querystring';

const notification = Notification.instance;

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class NotificationDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new NotificationDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async getNotificationIds(user) {
        if (!user) return [];
        const user_devises = models.UserDevise.findAll({
            where: {
                user_id: Number(user.id) || 0,
            },
        });
        const devises = await Promise.all(
            user_devises.map(d =>
                models.Devise.findOne({
                    where: {
                        id: d.DeviseId,
                    },
                })
            )
        );
        return devises
            .filter(
                devise =>
                    devise.notification_id != null &&
                    devise.notification_id != ''
            )
            .map(devise => devise.notification_id);
    }

    async onCreateMessage({ current_user, message }) {
        if (!message || !message.id || !current_user || !current_user.id)
            return;

        message = await models.Message.findOne({
            where: {
                id: Number(message.id) || 0,
            },
        });

        const sender = await models.Member.findOne({
            where: {
                id: Number(message.SenderId) || 0,
            },
        });

        let mention_names = (message.text.match(/\B@[a-z0-9_-]+/gi) || []).map(
            str => str.replace('@', '')
        ); // message.getMentionName.bind();
        const isEveryone =
            mention_names.includes('everyone') ||
            mention_names.includes('here');
        mention_names = mention_names.filter(
            n => n !== 'everyone' || n !== 'here'
        );

        let members = [];
        let channel;
        switch (message.messageable_type) {
            case 'TextChannel':
                channel = await models.TextChannel.findOne({
                    where: { id: message.MessageableId },
                });
                members = members.concat(
                    await this.getTextChannelEntryMembers(
                        current_user,
                        channel,
                        {
                            mute: false,
                            mentions_mute: false,
                            mentions: mention_names,
                        }
                    )
                );
                members = members.concat(
                    await this.getTextChannelEntryMembers(
                        current_user,
                        channel,
                        isEveryone
                            ? {
                                  mute: false,
                                  everyone_mentions_mute: false,
                              }
                            : {
                                  mute: false,
                                  messages_mute: false,
                              }
                    )
                );
                break;
            case 'DMChannel':
                channel = await models.DMChannel.findOne({
                    where: { id: message.MessageableId },
                });
                members = members.concat(
                    await this.getDMChannelEntryMembers(current_user, channel, {
                        mute: false,
                        mentions_mute: false,
                        mentions: mention_names,
                    })
                );
                members = members.concat(
                    await this.getDMChannelEntryMembers(
                        current_user,
                        channel,
                        isEveryone
                            ? {
                                  mute: false,
                                  everyone_mentions_mute: false,
                              }
                            : {
                                  mute: false,
                                  messages_mute: false,
                              }
                    )
                );
                break;
            case 'StreamChannel':
                channel = await models.StreamChannel.findOne({
                    where: { id: message.MessageableId },
                });
                members = members.concat(
                    await this.getStreamChannelEntryMembers(
                        current_user,
                        channel,
                        {
                            mute: false,
                            mentions_mute: false,
                            mentions: mention_names,
                        }
                    )
                );
                members = members.concat(
                    await this.getStreamChannelEntryMembers(
                        current_user,
                        channel,
                        isEveryone
                            ? {
                                  mute: false,
                                  everyone_mentions_mute: false,
                              }
                            : {
                                  mute: false,
                                  messages_mute: false,
                              }
                    )
                );
                break;
        }

        members = Array.prototype.unique_by_id(members);

        const guild = await models.Guild.findOne({
            where: {
                id: Number(channel.GuildId) || 0,
            },
        });

        if (members.length > 0) {
            await Promise.all(
                members.filter(val => val.id != sender.id).map(async member => {
                    await models.Notification.create({
                        user_id: member.UserId,
                        notificatable_type: 'Message',
                        notificatable_id: message.id,
                        NotificatableId: message.id,
                        template: 'create_message',
                        url:
                            config.CURRENT_APP_URL +
                            `/guild/${guild.uid}/${channel.uid}`,
                    });
                    await notification.push({
                        tt_key: 'create_message',
                        url:
                            config.IOS_SCHEME +
                            `/guild/${guild.uid}/${channel.uid}`,
                        ids: await this.getNotificationIds({
                            id: member.UserId,
                        }),
                        title: `${guild.name}#${channel.name}`,
                        body: `${sender.nickname}:${message.text}`,
                    });
                })
            );
        }
    }

    async getStreamChannelEntryMembers(
        user,
        channel,
        {
            mute = true,
            messages_mute = true,
            mentions_mute = true,
            everyone_mentions_mute = true,
            mentions = [],
        }
    ) {
        if (!channel || !channel.id) return;

        let where = {
            permission: true,
        };
        if (!mute) {
            where.mute = { $ne: true };
        }
        if (!messages_mute) {
            where.messages_mute = { $ne: true };
        }
        if (!mentions_mute) {
            where.mentions_mute = { $ne: true };
        }
        if (!everyone_mentions_mute) {
            where.everyone_mentions_mute = { $ne: true };
        }

        const entries = await models.StreamChannelEntry.findAll({
            where: {
                channel_id: Number(channel.id) || 0,
                ...where,
            },
            include: [
                {
                    model: models.Member,
                    where,
                },
            ],
        });

        const members = entries
            .map(entry => entry.Member)
            .filter(
                member =>
                    !mentions ||
                    mentions.length == 0 ||
                    mentions.includes(member.nickname)
            );

        return await this.filterValidMember({
            user,
            targets: members,
        });
    }

    async getDMChannelEntryMembers(
        user,
        channel,
        {
            mute = true,
            messages_mute = true,
            mentions_mute = true,
            everyone_mentions_mute = true,
            mentions = [],
        }
    ) {
        if (!channel || !channel.id) return;

        let where = {
            permission: true,
        };
        if (!mute) {
            where.mute = { $ne: true };
        }
        if (!messages_mute) {
            where.messages_mute = { $ne: true };
        }
        if (!mentions_mute) {
            where.mentions_mute = { $ne: true };
        }
        if (!everyone_mentions_mute) {
            where.everyone_mentions_mute = { $ne: true };
        }

        const entries = await models.DMChannelEntry.findAll({
            where: {
                channel_id: Number(channel.id) || 0,
                ...where,
            },
            include: [
                {
                    model: models.Member,
                    where,
                },
            ],
        });

        const members = entries
            .map(entry => entry.Member)
            .filter(
                member =>
                    !mentions ||
                    mentions.length == 0 ||
                    mentions.includes(member.nickname)
            );

        return await this.filterValidMember({
            user,
            targets: members,
        });
    }

    async getTextChannelEntryMembers(
        user,
        channel,
        {
            mute = true,
            messages_mute = true,
            mentions_mute = true,
            everyone_mentions_mute = true,
            mentions = [],
        }
    ) {
        if (!channel || !channel.id) return;

        let where = {
            permission: true,
        };
        if (!mute) {
            where.mute = { $ne: true };
        }
        if (!messages_mute) {
            where.messages_mute = { $ne: true };
        }
        if (!mentions_mute) {
            where.mentions_mute = { $ne: true };
        }
        if (!everyone_mentions_mute) {
            where.everyone_mentions_mute = { $ne: true };
        }

        const entries = await models.TextChannelEntry.findAll({
            where: {
                channel_id: Number(channel.id) || 0,
                ...where,
            },
            include: [
                {
                    model: models.Member,
                    where,
                },
            ],
        });

        const members = entries
            .map(entry => entry.Member)
            .filter(
                member =>
                    !mentions ||
                    mentions.length == 0 ||
                    mentions.includes(member.nickname)
            );

        return await this.filterValidMember({
            user,
            targets: members,
        });
    }

    async filterBlockMember({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserBlock.findAll({
            where: {
                user_id: Number(user.id) || 0,
            },
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (
                    blocks.filter(block => block.ReceiverId == val.UserId)
                        .length == 0
                )
                    return val;
            })
            .filter(val => !!val);
    }

    async filterMuteMember({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserMute.findAll({
            where: {
                user_id: Number(user.id) || 0,
            },
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (
                    blocks.filter(block => block.ReceiverId == val.UserId)
                        .length == 0
                )
                    return val;
            })
            .filter(val => !!val);
    }

    async filterBlockerMember({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserBlock.findAll({
            where: {
                receiver_id: Number(user.id) || 0,
            },
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (
                    blocks.filter(block => block.UserId == val.UserId).length ==
                    0
                )
                    return val;
            })
            .filter(val => !!val);
    }

    async filterMuterMember({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;
        const blocks = await models.UserMute.findAll({
            where: {
                receiver_id: Number(user.id) || 0,
            },
        });

        return targets
            .filter(val => !!val)
            .map(val => {
                if (
                    blocks.filter(block => block.UserId == val.UserId).length ==
                    0
                )
                    return val;
            })
            .filter(val => !!val);
    }

    async filterValidMember({ user, targets }) {
        if (!user || !targets || targets.length == 0) return targets;

        return await this.filterBlockerMember({
            user,
            targets: await this.filterMuterMember({
                user,
                targets: await this.filterBlockMember({
                    user,
                    targets: targets,
                }),
            }),
        });
    }
}
