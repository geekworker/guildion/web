import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import safe2json from '@extension/safe2json';
import AWSHandler from '@network/aws';
import { missingArgument } from '@extension/log';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const awsHandler = AWSHandler.instance;

export default class FolderDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new FolderDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async create_temporary({ current_member, files }) {
        const result = await models.Folder.create({
            MemberId: Number(current_member.id) || 0,
            member_id: Number(current_member.id) || 0,
            GuildId: Number(current_member.GuildId) || 0,
            guild_id: Number(current_member.GuildId) || 0,
            uid: models.Folder.generate_uid(),
            name: '',
            description: '',
            is_playlist: true,
            temporary: true,
            permission: true,
        });

        const results = await Promise.all(
            files.map((file, i) =>
                models.FileReference.create({
                    folder_id: Number(result.id) || 0,
                    FolderId: Number(result.id) || 0,
                    file_id: Number(file.id) || 0,
                    FileId: Number(file.id) || 0,
                    index: i,
                    permission: true,
                })
            )
        );

        return results;
    }

    async create({ folder, current_user }) {
        // await apiFileCreateValidates.isValid({
        //     file,
        // });

        const result = await models.Folder.create({
            MemberId: Number(folder.MemberId) || 0,
            member_id: Number(folder.MemberId) || 0,
            GuildId: Number(folder.GuildId) || 0,
            guild_id: Number(folder.GuildId) || 0,
            uid: models.Folder.generate_uid(),
            name: folder.name,
            description: folder.description,
            thumbnail: folder.thumbnail,
            index: folder.index,
            read_only: Number.prototype.castBool(folder.read_only),
            is_playlist: Number.prototype.castBool(folder.is_playlist),
            is_nsfw: Number.prototype.castBool(folder.is_nsfw),
            is_private: Number.prototype.castBool(folder.is_private),
            permission: true,
        });

        folder.id = result.id;

        if (folder.FileReferences) {
            folder.FileReferences.map(reference => {
                reference.FolderId = result.id;
            });
            result.setValue('FileReferences', folder.FileReferences);
        }

        result.index = await this.setLastIndex(result);

        return result;
    }

    async update({ folder, current_user }) {
        // await apiFileUpdateValidates.isValid({
        //     file,
        // });

        const before = await models.Folder.findOne({
            where: {
                uid: folder.uid,
            },
        });

        const result = await before.update({
            // MemberId: Number(folder.MemberId) || 0,
            // member_id: Number(folder.MemberId) || 0,
            // GuildId: Number(folder.GuildId) || 0,
            // guild_id: Number(folder.GuildId) || 0,
            // uid: models.Folder.generate_uid(),
            name: folder.name,
            description: folder.description,
            thumbnail: folder.thumbnail,
            index: folder.index,
            read_only: Number.prototype.castBool(folder.read_only),
            is_playlist: Number.prototype.castBool(folder.is_playlist),
            is_nsfw: Number.prototype.castBool(folder.is_nsfw),
            is_private: Number.prototype.castBool(folder.is_private),
        });

        return result;
    }

    async delete({ folder, current_user }) {
        // await apiFileDeleteValidates.isValid({
        //     file,
        // });

        await models.Folder.destroy({
            where: {
                uid: folder.uid,
            },
        });

        const folders = await models.Folder.findAll({
            where: {
                guild_id: Number(folder.GuildId) || 0,
                index: {
                    $gt: Number(folder.index) || 0,
                },
            },
        });

        Promise.all(
            folders.map(folder =>
                folder.update({
                    index: folder.index - 1,
                })
            )
        );

        return;
    }

    async getAllFolderFiles({
        guild_id,
        folder_id,
        current_user,
        limit,
        offset,
    }) {
        if (!guild_id || !folder_id) return;

        const folder = await models.Folder.findOne({
            where: {
                id: Number(folder_id) || 0,
            },
        });

        if (!folder) return;

        const files = await models.File.findAll({
            where: {
                is_private: false,
                permission: true,
                format: folder.is_playlist
                    ? models.File.movie_where(models)
                    : { $ne: null },
            },
            include: [
                ...models.File.folder_index_include(models, {
                    guild_id: Number(guild_id) || 0,
                    folder_id: Number(folder_id) || 0,
                }),
            ],
            order: [['index', 'ASC']],
            offset: Number(offset || 0),
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return files;
    }

    async getCurrentFolderFiles({ folder_id, current_user, limit }) {
        if (!folder_id) return;

        const folder = await models.Folder.findOne({
            where: {
                id: Number(folder_id) || 0,
            },
        });

        if (!folder) return;

        const references = await models.FileReference.findAll({
            where: {
                folder_id: Number(folder.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.File,
                    where: {
                        is_private: false,
                        permission: true,
                    },
                    include: [
                        {
                            model: models.Member,
                        },
                    ],
                },
            ],
            order: [['index', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return references;
    }

    async getIncrementFolderFiles({ folder_id, current_user, last_id, limit }) {
        if (!folder_id) return;

        const folder = await models.Folder.findOne({
            where: {
                id: Number(folder_id) || 0,
            },
        });

        if (!folder) return;

        const reference = await models.FileReference.findOne({
            where: {
                id: Number(last_id) || 0,
            },
        });

        if (!reference) return;

        const references = await models.FileReference.findAll({
            where: {
                index: {
                    $gt: Number(reference.index) || 0,
                },
                folder_id: Number(folder.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.File,
                    where: {
                        is_private: false,
                        permission: true,
                    },
                    include: [
                        {
                            model: models.Member,
                        },
                    ],
                },
            ],
            order: [['index', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return references;
    }

    async getDecrementFolderFiles({
        folder_id,
        current_user,
        first_id,
        limit,
    }) {
        if (!folder_id) return;

        const folder = await models.Folder.findOne({
            where: {
                id: Number(folder_id) || 0,
            },
        });

        if (!folder) return;

        const reference = await models.FileReference.findOne({
            where: {
                id: Number(first_id) || 0,
            },
        });

        if (!reference) return;

        const references = await models.FileReference.findAll({
            where: {
                index: {
                    $lt: Number(reference.index) || 0,
                },
                folder_id: Number(folder.id) || 0,
                permission: true,
            },
            include: [
                {
                    model: models.File,
                    where: {
                        is_private: false,
                        permission: true,
                    },
                    include: [
                        {
                            model: models.Member,
                        },
                    ],
                },
            ],
            order: [['index', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return references.reverse();
    }

    async getCurrentGuildFiles({ guild_id, current_user, limit, is_playlist }) {
        if (!guild_id) return [];

        const member = await models.Member.findOne({
            where: {
                guild_id: Number(guild_id) || 0,
                user_id: Number(current_user.id) || 0,
            },
        });

        if (!member) return [];

        const files = await models.File.findAll({
            where: {
                format: Number.prototype.castBool(is_playlist)
                    ? models.File.movie_where(models)
                    : { $ne: null },
                permission: true,
                $or: [
                    {
                        member_id: member.id,
                    },
                    {
                        is_private: false,
                    },
                ],
            },
            include: [
                ...models.File.index_include(models, {
                    guild_id: Number(guild_id) || 0,
                }),
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return files.reverse();
    }

    async getIncrementGuildFiles({
        guild_id,
        current_user,
        last_id,
        limit,
        is_playlist,
    }) {
        if (!guild_id) return [];

        const member = await models.Member.findOne({
            where: {
                guild_id: Number(guild_id) || 0,
                user_id: Number(current_user.id) || 0,
            },
        });

        if (!member) return [];

        const results = await models.File.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                format: Number.prototype.castBool(is_playlist)
                    ? models.File.movie_where(models)
                    : { $ne: null },
                permission: true,
                $or: [
                    {
                        member_id: member.id,
                    },
                    {
                        is_private: false,
                    },
                ],
            },
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                ...models.File.index_include(models, {
                    guild_id: Number(guild_id) || 0,
                }),
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return results;
    }

    async getDecrementGuildFiles({
        guild_id,
        current_user,
        first_id,
        limit,
        is_playlist,
    }) {
        if (!guild_id) return [];

        const member = await models.Member.findOne({
            where: {
                guild_id: Number(guild_id) || 0,
                user_id: Number(current_user.id) || 0,
            },
        });

        if (!member) return [];

        const results = await models.File.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                format: Number.prototype.castBool(is_playlist)
                    ? models.File.movie_where(models)
                    : { $ne: null },
                permission: true,
                $or: [
                    {
                        member_id: member.id,
                    },
                    {
                        is_private: false,
                    },
                ],
            },
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                ...models.File.index_include(models, {
                    guild_id: Number(guild_id) || 0,
                }),
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return results.reverse();
    }

    async getCurrentGuildMyFiles({ guild_id, current_user, limit }) {
        if (!guild_id) return;

        const files = await models.File.findAll({
            where: {
                permission: true,
            },
            include: [
                ...models.File.my_index_include(models, {
                    guild_id: Number(guild_id) || 0,
                    user_id: Number(current_user.id) || 0,
                }),
            ],
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return files.reverse();
    }

    async getIncrementGuildMyFiles({ guild_id, current_user, last_id, limit }) {
        if (!guild_id) return;

        const results = await models.File.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                permission: true,
            },
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                ...models.File.my_index_include(models, {
                    guild_id: Number(guild_id) || 0,
                    user_id: Number(current_user.id) || 0,
                }),
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return results;
    }

    async getDecrementGuildMyFiles({
        guild_id,
        current_user,
        first_id,
        limit,
    }) {
        if (!guild_id) return;

        const results = await models.File.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                permission: true,
            },
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                ...models.File.my_index_include(models, {
                    guild_id: Number(guild_id) || 0,
                    user_id: Number(current_user.id) || 0,
                }),
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return results.reverse();
    }

    async updateCount(value) {
        if (!value) return;
        const folder = await models.Folder.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!folder) return;

        const [files, movies] = await Promise.all([
            models.FileReference.findAll({
                where: {
                    folder_id: folder.id,
                },
                include: [
                    {
                        model: models.File,
                        where: {
                            is_private: false,
                            permission: true,
                        },
                    },
                ],
            }),
            models.FileReference.findAll({
                where: {
                    folder_id: folder.id,
                },
                include: [
                    {
                        model: models.File,
                        where: {
                            format: models.File.movie_where(models),
                            permission: true,
                        },
                    },
                ],
            }),
        ]);

        const result = await folder.update({
            file_count: files.length,
            movie_count: movies.length,
        });

        return result;
    }

    async index_update({ folder, current_user }) {
        const before = await models.Folder.findOne({
            where: {
                id: Number(folder.id) || 0,
            },
        });

        const result = await before.update({
            index: Number(folder.index) || 0,
        });

        return result;
    }

    async setLastIndex(value) {
        if (!value) return;
        const folder = await models.Folder.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!folder) return;

        const folders = await models.Folder.findAll({
            where: {
                id: {
                    $ne: folder.id,
                },
                guild_id: Number(folder.GuildId) || 0,
                permission: true,
            },
        });

        const result = await folder.update({
            index: 1,
        });

        const results = await Promise.all(
            folders.map(folder => folder.update({ index: folder.index + 1 }))
        );

        return result.index;
    }
}
