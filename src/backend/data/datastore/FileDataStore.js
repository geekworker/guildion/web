import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import {
    apiFileCreateValidates,
    apiFileUpdateValidates,
    apiFileDeleteValidates,
    apiAttachmentCreateValidates,
    apiAttachmentUpdateValidates,
    apiAttachmentDeleteValidates,
    apiFileReferenceCreateValidates,
    apiFileReferenceUpdateValidates,
    apiFileReferenceDeleteValidates,
} from '@validations/file';
import safe2json from '@extension/safe2json';
import AWSHandler from '@network/aws';
import { missingArgument } from '@extension/log';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const awsHandler = AWSHandler.instance;

export default class FileDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new FileDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async getCurrentUserFiles({ current_user, limit }) {
        if (!current_user || !current_user.id) return;

        const files = await models.File.findAll({
            where: {
                format: { $ne: null },
                permission: true,
                provider: {
                    $or: ['', null],
                },
            },
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                {
                    model: models.Member,
                    where: {
                        user_id: Number(current_user.id),
                    },
                },
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return files;
    }

    async getIncrementUserFiles({ current_user, last_id, limit }) {
        if (!current_user || !current_user.id) return;

        const results = await models.File.findAll({
            where: {
                id: {
                    $gt: Number(last_id) || 0,
                },
                format: { $ne: null },
                provider: {
                    $or: ['', null],
                },
                permission: true,
                is_private: false,
            },
            order: [['id', 'ASC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                {
                    model: models.Member,
                    where: {
                        user_id: Number(current_user.id),
                    },
                },
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return results.reverse();
    }

    async getDecrementUserFiles({ current_user, first_id, limit }) {
        if (!current_user || !current_user.id) return;

        const results = await models.File.findAll({
            where: {
                id: {
                    $lt: Number(first_id) || 0,
                },
                format: { $ne: null },
                provider: {
                    $or: ['', null],
                },
                permission: true,
                is_private: false,
            },
            order: [['id', 'DESC']],
            limit: Number(limit || data_config.fetch_data_limit('S')),
            include: [
                {
                    model: models.Member,
                    where: {
                        user_id: Number(current_user.id),
                    },
                },
            ],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        return results;
    }

    async create({ file, current_user }) {
        await apiFileCreateValidates.isValid({
            file,
        });

        const result = await models.File.create({
            MemberId: Number(file.MemberId) || 0,
            member_id: Number(file.MemberId) || 0,
            uid: String.prototype.getUniqueString(),
            name: file.name,
            description: file.description,
            thumbnail: file.thumbnail,
            url: file.url,
            format: file.format,
            duration: file.duration,
            pixel_width: file.pixel_width,
            pixel_height: file.pixel_height,
            size_byte: file.size_byte,
            provider: file.provider,
            is_private: file.is_private,
            permission: true,
            is_uploaded: true,
            is_upload_failed: false,
        });

        file.id = result.id;

        if (file.FileReference) {
            const result = await models.FileReference.create({
                folder_id: Number(file.FileReference.FolderId) || 0,
                FolderId: Number(file.FileReference.FolderId) || 0,
                file_id: Number(file.id) || 0,
                FileId: Number(file.id) || 0,
                index: Number(file.FileReference.index) || 0,
                permission: true,
            });

            models.Folder.update(
                {
                    updatedAt: new Date(),
                },
                {
                    where: {
                        id: Number(file.FileReference.FolderId) || 0,
                    },
                }
            );
        }

        if (!file.UploadId || (Number(file.UploadId) || 0) == 0) {
            if (Number(file.size_byte) > 0) {
                const upload = await models.Upload.create({
                    user_id: Number(current_user.id) || 0,
                    UserId: Number(current_user.id) || 0,
                    uid: String.prototype.getUniqueString(),
                    name: file.name,
                    description: file.description,
                    thumbnail: file.thumbnail,
                    url: file.url,
                    format: file.format,
                    duration: file.duration,
                    pixel_width: file.pixel_width,
                    pixel_height: file.pixel_height,
                    size_byte: file.size_byte,
                    provider: file.provider,
                    is_private: file.is_private,
                    permission: true,
                    is_uploaded: true,
                    is_upload_failed: false,
                });

                await result.update({
                    upload_id: Number(upload.id) || 0,
                    UploadId: Number(upload.id) || 0,
                });

                result.UploadId = upload.id;
            }
        }

        return result;
    }

    async update({ file, current_user }) {
        await apiFileUpdateValidates.isValid({
            file,
        });

        const before = await models.File.findOne({
            where: {
                uid: file.uid,
            },
        });

        const result = await before.update({
            name: file.name,
            is_private: file.is_private,
        });

        return result;
    }

    async delete({ file, current_user }) {
        await apiFileDeleteValidates.isValid({
            file,
        });

        if (!!file.thumbnail && AWSHandler.deletable(file.thumbnail)) {
            await awsHandler.deleteFile(file.thumbnail);
        }

        if (!!file.url && AWSHandler.deletable(file.url)) {
            await awsHandler.deleteFile(file.url);
        }

        await this.delete_attachment(file);
        this.delete_channelog(file);

        await models.File.destroy({
            where: {
                uid: file.uid,
            },
        });

        return;
    }

    async delete_attachment(file) {
        const messages = await models.Message.findAll({
            where: {
                text: {
                    $or: [null, ''],
                },
            },
            include: [
                {
                    model: models.Attachment,
                    where: {
                        file_id: Number(file.id) || 0,
                    },
                },
            ],
        });

        await Promise.all(
            messages.map(async val => {
                val.setValue(
                    'Attachments',
                    await models.Attachment.findAll({
                        where: { message_id: val.id },
                    })
                );
            })
        );

        Promise.all([
            models.Message.destroy({
                where: {
                    id: {
                        $in: messages
                            .filter(val => val.Attachments.length == 1)
                            .map(val => val.id),
                    },
                },
            }),
        ]);
    }

    async delete_channelog(file) {
        const references = await models.FileReference.findAll({
            where: {
                file_id: Number(file.id) || 0,
            },
        });

        const [file_streams, reference_streams] = await Promise.all([
            models.Stream.findAll({
                where: {
                    streamable_id: Number(file.id) || 0,
                    streamable_type: 'File',
                },
            }),
            models.Stream.findAll({
                where: {
                    streamable_id: {
                        $in: references.map(val => val.id),
                    },
                    streamable_type: 'FileReference',
                },
            }),
        ]);

        const channelogs = await models.Channelog.findAll({
            where: {
                channelogable_id: {
                    $in: reference_streams
                        .map(val => val.id)
                        .concat(file_streams.map(val => val.id)),
                },
                channelogable_type: 'Stream',
            },
        });

        Promise.all([
            models.Message.destroy({
                where: {
                    id: {
                        $in: channelogs.map(val => val.MessageId),
                    },
                },
            }),
        ]);
    }

    async attachment_create({ attachment, current_user }) {
        await apiAttachmentCreateValidates.isValid({
            attachment,
        });

        let file;
        if (
            attachment.File &&
            (!attachment.File.id ||
                attachment.File.id == 0 ||
                attachment.File.id == '0')
        ) {
            file = await this.create({ file: attachment.File, current_user });
        } else {
            file = attachment.File;
        }

        if (!file) return;

        const result = await models.Attachment.create({
            message_id: Number(attachment.MessageId) || 0,
            MessageId: Number(attachment.MessageId) || 0,
            file_id: Number(file.id) || 0,
            FileId: Number(file.id) || 0,
            permission: true,
        });

        result.setValue('File', file);

        return result;
    }

    async attachment_update({ attachment, current_user }) {
        await apiAttachmentUpdateValidates.isValid({
            attachment,
        });

        const result = await this.update({
            file: attachment.File,
            current_user,
        });

        return result;
    }

    async attachment_delete({ attachment, current_user }) {
        await apiAttachmentDeleteValidates.isValid({
            attachment,
        });

        await this.delete({ file: attachment.File, current_user });

        return;
    }

    async create_files_from_message({ message, current_user }) {
        if (
            !message ||
            !message.Files ||
            !(message.Files instanceof Array) ||
            message.Files.length == 0
        )
            return;
        const attachments = message.Files.map(file => {
            const attachment = models.Attachment.build({
                message_id: Number(message.id) || 0,
                MessageId: Number(message.id) || 0,
                // file_id: Number(file.id) || 0,
                // FileId: Number(file.id) || 0,
                permission: true,
            });
            attachment.setValue('File', file);
            return attachment;
        });

        const results = await Promise.all(
            attachments.map(attachment =>
                this.attachment_create({ attachment, current_user })
            )
        );

        return results;
    }

    async update_files_from_message({ message, current_user }) {
        if (
            !message ||
            !message.Files ||
            !(message.Files instanceof Array) ||
            message.Files.length == 0
        )
            return;
        const attachments = message.Files.map(file => {
            const attachment = models.Attachment.build({
                message_id: Number(message.id) || 0,
                MessageId: Number(message.id) || 0,
                // file_id: Number(file.id) || 0,
                // FileId: Number(file.id) || 0,
                permission: true,
            });
            attachment.setValue('File', file);
            return attachment;
        });

        const befores = await models.File.findAll({
            include: [
                {
                    model: models.Attachment,
                    where: {
                        message_id: Number(message.id) || 0,
                    },
                },
            ],
        });

        await Promise.all(
            befores.map(file => this.delete({ file, current_user }))
        );

        const results = await Promise.all(
            attachments.map(attachment =>
                this.attachment_create({ attachment, current_user })
            )
        );

        return results;
    }

    async file_reference_create({ file_reference, current_user }) {
        await apiFileReferenceCreateValidates.isValid({
            file_reference,
        });

        let file;

        if (file_reference.File && !file_reference.FileId) {
            file = await this.create({
                file: file_reference.File,
                current_user,
            });
        } else {
            file = file_reference.File;
            file.id = file_reference.FileId;
        }

        if (!file) return;

        let result = await models.FileReference.create({
            folder_id: Number(file_reference.FolderId) || 0,
            FolderId: Number(file_reference.FolderId) || 0,
            file_id: Number(file.id) || 0,
            FileId: Number(file.id) || 0,
            index: Number(file_reference.index) || 0,
            permission: true,
        });

        result.index = await this.setLastIndex(result);

        models.Folder.update(
            {
                updatedAt: new Date(),
            },
            {
                where: {
                    id: Number(file_reference.FolderId) || 0,
                },
            }
        );

        const updated = await models.FileReference.findOne({
            where: {
                id: Number(result.id) || 0,
            },
            include: [...models.FileReference.index_include(models)],
        });

        return updated;
    }

    async file_reference_update({ file_reference, current_user }) {
        await apiFileReferenceUpdateValidates.isValid({
            file_reference,
        });

        const result = await this.update({ file: file_reference.File });

        models.Folder.update(
            {
                updatedAt: new Date(),
            },
            {
                where: {
                    id: Number(file_reference.FolderId) || 0,
                },
            }
        );

        const updated = await models.FileReference.findOne({
            where: {
                id: Number(file_reference.id) || 0,
            },
            include: [...models.FileReference.index_include(models)],
        });

        return updated;
    }

    async index_update({ file_reference, current_user }) {
        await apiFileReferenceUpdateValidates.isValid({
            file_reference,
        });

        const before = await models.FileReference.findOne({
            where: {
                id: Number(file_reference.id) || 0,
            },
        });

        const result = await before.update({
            // folder_id: Number(file_reference.FolderId) || 0,
            // FolderId: Number(file_reference.FolderId) || 0,
            index: Number(file_reference.index) || 0,
            permission: true,
        });

        return result;
    }

    async file_reference_delete({ file_reference, current_user }) {
        await apiFileReferenceDeleteValidates.isValid({
            file_reference,
        });

        await models.FileReference.destroy({
            where: {
                id: Number(file_reference.id) || 0,
            },
        });

        models.Folder.update(
            {
                updatedAt: new Date(),
            },
            {
                where: {
                    id: Number(file_reference.FolderId) || 0,
                },
            }
        );

        const references = await models.FileReference.findAll({
            where: {
                index: {
                    $gt: Number(file_reference.index) || 0,
                },
            },
        });

        Promise.all(
            references.map(reference =>
                reference.update({
                    index: reference.index - 1,
                })
            )
        );

        return;
    }

    async setLastIndex(value) {
        if (!value) return;
        const reference = await models.FileReference.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!reference) return;

        const references = await models.FileReference.findAll({
            where: {
                folder_id: Number(reference.FolderId) || 0,
                permission: true,
            },
        });

        const result = await reference.update({
            index: references.length,
        });

        return result.index;
    }
}
