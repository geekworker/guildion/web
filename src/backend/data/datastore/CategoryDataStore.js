import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { missingArgument } from '@extension/log';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class CategoryDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new CategoryDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async updateCount(value) {
        if (!value) return;
        const category = await models.Category.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!category) return;

        const guilds = await models.Guild.findAll({
            where: {
                category_id: Number(category.id),
            },
            attributes: ['id'],
        });

        const result = await category.update({
            guild_count: guilds.length,
        });

        return result;
    }

    async updateCountFromGuild(guild) {
        if (!guild || !guild.id) return;
        const category = await models.Category.findOne({
            where: {
                id: Number(guild.CategoryId),
            },
            attributes: ['id'],
        });
        const result = await this.updateCount(category);
        return result;
    }

    async init_categories() {
        await Promise.all([
            // MEMO: ja
            models.Category.findOrCreate({
                where: {
                    ja_name: 'コミュニティー',
                    ja_groupname: 'コミュニティー',
                    en_name: 'Community',
                    en_groupname: 'Community',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'チーム',
                    ja_groupname: 'チーム',
                    en_name: 'Team',
                    en_groupname: 'Team',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'クリエイター',
                    ja_groupname: 'クリエイター',
                    en_name: 'Creativity',
                    en_groupname: 'Creativity',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'ファンコミュニティー',
                    ja_groupname: 'ファンコミュニティー',
                    en_name: 'Fan Community',
                    en_groupname: 'Fan Community',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'YouTube',
                    ja_groupname: 'YouTube',
                    en_name: 'YouTube',
                    en_groupname: 'YouTube',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: '勉強',
                    ja_groupname: '勉強',
                    en_name: 'Study & Learning',
                    en_groupname: 'Study & Learning',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'ファミリー',
                    ja_groupname: 'ファミリー',
                    en_name: 'Family',
                    en_groupname: 'Family',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: '教育',
                    ja_groupname: '教育',
                    en_name: 'Education',
                    en_groupname: 'Education',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: '音楽',
                    ja_groupname: '音楽',
                    en_name: 'Music',
                    en_groupname: 'Music',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: '映画',
                    ja_groupname: '映画',
                    en_name: 'film',
                    en_groupname: 'film',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'アニメ・漫画',
                    ja_groupname: 'アニメ・漫画',
                    en_name: 'Anime',
                    en_groupname: 'Anime',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'ドラマ',
                    ja_groupname: 'ドラマ',
                    en_name: 'TV drama',
                    en_groupname: 'TV drama',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'テレビ',
                    ja_groupname: 'テレビ',
                    en_name: 'TV',
                    en_groupname: 'TV',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: '語学',
                    ja_groupname: '語学',
                    en_name: 'Langedge',
                    en_groupname: 'Langedge',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'グローバル',
                    ja_groupname: 'グローバル',
                    en_name: 'Global',
                    en_groupname: 'Global',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'ゲーム',
                    ja_groupname: 'ゲーム',
                    en_name: 'Gaming',
                    en_groupname: 'Gaming',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'お笑い',
                    ja_groupname: 'お笑い',
                    en_name: 'Comedy',
                    en_groupname: 'Comedy',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: '政治・ニュース',
                    ja_groupname: '政治・ニュース',
                    en_name: 'Politics & News',
                    en_groupname: 'Politics & News',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'テクノロジー',
                    ja_groupname: 'テクノロジー',
                    en_name: 'Technology',
                    en_groupname: 'Technology',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'クラシック',
                    ja_groupname: 'クラシック',
                    en_name: 'Classics',
                    en_groupname: 'Classics',
                    permission: true,
                    is_private: false,
                },
            }),
            models.Category.findOrCreate({
                where: {
                    ja_name: 'その他',
                    ja_groupname: 'その他',
                    en_name: 'Others',
                    en_groupname: 'Others',
                    permission: true,
                    is_private: false,
                },
            }),
        ]);
    }
}
