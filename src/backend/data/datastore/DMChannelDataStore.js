import DataStoreImpl from '@datastore/DataStoreImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import {
    generateLikeQuery,
    generateOrQuery,
    generateOrQueries,
} from '@extension/query';
import expo from '@extension/object2json';
import Promise from 'bluebird';
import Cookies from 'js-cookie';
import {
    apiDMChannelCreateValidates,
    apiDMChannelUpdateValidates,
    apiDMChannelDeleteValidates,
} from '@validations/dm_channel';
import safe2json from '@extension/safe2json';
import AWSHandler from '@network/aws';
import { missingArgument } from '@extension/log';

const awsHandler = AWSHandler.instance;

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class DMChannelDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new DMChannelDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async getChannel({ channel, current_user }) {
        if (!channel || !channel.uid) return;

        const result = await models.DMChannel.findOne({
            where: {
                uid: channel.uid,
                permission: true,
            },
            // include: models.DMChannel.show_include(models),
            order: [['created_at', 'DESC']],
        });

        const [entries, dmChannelRoles] = await Promise.all([
            models.DMChannelEntry.findAll({
                where: {
                    channel_id: result.id,
                },
                include: [
                    {
                        model: models.Member,
                    },
                ],
            }),
            models.DMChannelRole.findAll({
                where: {
                    channel_id: result.id,
                    permission: true,
                },
                include: [
                    {
                        model: models.Role,
                    },
                ],
            }),
        ]);

        const members = entries.map(entry => {
            const member = entry.Member || entry.dataValues.Member;
            delete entry.Member;
            delete entry.dataValues.Member;
            member.setValue('DMChannelEntry', entry);
            return member;
        });

        result.setValue('Members', members);
        result.setValue('DMChannelRoles', dmChannelRoles);

        return result;
    }

    async create({ user, channel, section, members }) {
        // await apiDMChannelCreateValidates.isValid({
        //     user,
        //     section,
        //     channel,
        //     members
        // });

        const created = await models.DMChannel.create({
            guild_id: Number(channel.GuildId) || 0,
            GuildId: Number(channel.GuildId) || 0,
            SectionId: section ? Number(section.id) || null : null,
            section_id: section ? Number(section.id) || null : null,
            uid: models.DMChannel.generate_uid(),
            name: channel.name,
            is_private: channel.is_private,
            is_system: channel.is_system,
            is_static: false,
            is_im: false,
            permission: true,
        });

        await this.findOrCreateEntries({ channel: created, members });
        created.index = await this.setLastIndex(created);

        return created;
    }

    async update({ user, channel, members }) {
        // await apiDMChannelUpdateValidates.isValid({
        //     user,
        //     channel,
        // });

        const data = await models.DMChannel.findOne({
            where: {
                id: Number(channel.id),
            },
            raw: false,
        });

        const result = await data.update({
            name: channel.name,
            description: channel.description,
            is_private: channel.is_private,
            permission: channel.permission,
        });

        // this.findOrCreateEntries({ channel: result, members });

        return result;
    }

    async updateEntry({ entry }) {
        const before = await models.DMChannelEntry.findOne({
            where: {
                id: Number(entry.id),
            },
        });

        const result = await before.update({
            mute: entry.mute,
            messages_mute: entry.messages_mute,
            mentions_mute: entry.mentions_mute,
            everyone_mentions_mute: entry.everyone_mentions_mute,
        });

        return result;
    }

    async section_index_update({ user, channel }) {
        // await apiDMChannelUpdateValidates.isValid({
        //     user,
        //     channel,
        //     section,
        // });

        const data = await models.DMChannel.findOne({
            where: {
                id: Number(channel.id),
            },
            raw: false,
        });

        const result = await data.update({
            //SectionId: Number(channel.SectionId) || null,
            //section_id: Number(channel.SectionId) || null,
            index: Number(channel.index) || 0,
        });

        return result;
    }

    async delete({ channel, user }) {
        // await apiDMChannelDeleteValidates.isValid({
        //     user,
        //     channel,
        // });

        const result = await models.DMChannel.destroy({
            where: {
                id: Number(channel.id),
            },
        });

        const [
            text_channels,
            stream_channels,
            dm_channels,
            section_channels,
        ] = await Promise.all([
            models.TextChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                },
            }),
            models.StreamChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                    temporary: false,
                },
            }),
            models.DMChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                },
            }),
            // await models.SectionChannel.findAll({
            //     where: {
            //         guild_id: Number(channel.GuildId) || 0,
            //         is_dm: false,
            //         permission: true,
            //     },
            // }),
        ]);

        Promise.all(
            text_channels.map(text_channel =>
                text_channel.update({
                    index: text_channel.index - 1,
                })
            )
        );

        Promise.all(
            stream_channels.map(stream_channel =>
                stream_channel.update({
                    index: stream_channel.index - 1,
                })
            )
        );

        Promise.all(
            dm_channels.map(dm_channel =>
                dm_channel.update({
                    index: dm_channel.index - 1,
                })
            )
        );

        return result;
    }

    async findOrCreateDMChannel({ guild, user, target }) {
        const member = await models.Member.findOne({
            where: {
                guild_id: Number(guild.id) || 0,
                user_id: Number(user.id) || 0,
            },
        });

        if (Number(member.id) == Number(target.id)) {
            return await models.DMChannel.findOne({
                where: {
                    is_im: true,
                    member_count: 1,
                },
                include: [
                    {
                        model: models.DMChannelEntry,
                        where: {
                            member_id: Number(member.id),
                        },
                    },
                ],
            });
        }

        const channel = await models.DMChannel.findOne({
            where: {
                member_count: 2,
                permission: true,
                is_system: true,
            },
            include: [
                {
                    as: 'Members',
                    model: models.Member,
                    through: { attributes: [] },
                    required: true,
                    where: {
                        id: {
                            $in: [
                                Number(member.id) || 0,
                                Number(target.id) || 0,
                            ],
                        },
                    },
                },
            ],
        });

        if (!!channel && channel.Members.length == 2) {
            return await this.getChannel({ channel });
        } else {
            const section = await models.SectionChannel.findOne({
                where: {
                    guild_id: Number(guild.id) || 0,
                    is_dm: true,
                },
            });
            const created = await this.create({
                user,
                channel: models.DMChannel.build({
                    guild_id: Number(guild.id) || 0,
                    GuildId: Number(guild.id) || 0,
                    name: `${member.nickname}, ${target.nickname}`,
                    is_system: true,
                    is_private: true,
                }),
                section,
                members: [member, target],
            });
            return await this.getChannel({ channel: created });
        }
    }

    async findOrCreateEntries({ channel, members }) {
        let entries = [];
        if (
            Number.prototype.castBool(channel.is_private) &&
            !!members &&
            members.length > 0
        ) {
            const deleted = await models.DMChannelEntry.destroy({
                where: {
                    channel_id: Number(channel.id) || 0,
                    member_id: {
                        $notIn: members.map(val => Number(val.UserId) || 0),
                    },
                },
            });

            entries = await Promise.all(
                members.map(async member => {
                    const [
                        entry,
                        created,
                    ] = await models.DMChannelEntry.findOrCreate({
                        where: {
                            ChannelId: Number(channel.id) || 0,
                            channel_id: Number(channel.id) || 0,
                            MemberId: Number(member.id) || 0,
                            member_id: Number(member.id) || 0,
                        },
                    });
                    if (created) {
                        return await entry.update({
                            permission: true,
                        });
                    }
                    return entry;
                })
            );
        }

        if (!Number.prototype.castBool(channel.is_private)) {
            const all_members = await models.Member.findAll({
                where: {
                    permission: true,
                },
                include: [
                    {
                        model: models.DM,
                        where: {
                            id: Number(channel.DMId) || 0,
                        },
                    },
                ],
            });

            const deleted = await models.DMChannelEntry.destroy({
                where: {
                    channel_id: Number(channel.id) || 0,
                    member_id: {
                        $notIn: all_members.map(val => Number(val.UserId) || 0),
                    },
                },
            });

            entries = await Promise.all(
                all_members.map(async member => {
                    const [
                        entry,
                        created,
                    ] = await models.DMChannelEntry.findOrCreate({
                        where: {
                            ChannelId: Number(channel.id) || 0,
                            channel_id: Number(channel.id) || 0,
                            MemberId: Number(member.id) || 0,
                            member_id: Number(member.id) || 0,
                        },
                    });
                    if (created) {
                        return await entry.update({
                            permission: true,
                        });
                    }
                    return entry;
                })
            );
        }

        this.updateCount(channel);

        return entries;
    }

    async join({ channel, member }) {
        // await apiDMChannelJoinValidates.isValid({
        //     member,
        //     channel,
        // });

        const [entry, created] = await models.DMChannelEntry.findOrCreate({
            where: {
                ChannelId: Number(channel.id) || 0,
                MemberId: Number(member.id) || 0,
            },
        });

        if (created) {
            entry.update({
                permission: true,
            });
        }

        this.updateCount(channel);

        return entry;
    }

    async leave({ channel, member }) {
        // await apiDMChannelLeaveValidates.isValid({
        //     member,
        //     channel,
        // });

        await models.DMChannelEntry.destroy({
            where: {
                channel_id: Number(channel.id) || 0,
                member_id: Number(member.id) || 0,
            },
        });

        this.updateCount(channel);

        return;
    }

    async add_role({ channel, role }) {
        let [entry, created] = await models.DMChannelRole.findOrCreate({
            where: {
                ChannelId: Number(channel.id) || 0,
                RoleId: Number(role.id) || 0,
                permission: true,
            },
        });

        if (created) {
            entry = await entry.update({
                roles_managable: Number.prototype.castBool(
                    role.roles_managable
                ),
                channel_managable: Number.prototype.castBool(
                    role.channels_managable
                ),
                channel_members_managable: Number.prototype.castBool(
                    role.channels_managable
                ),
                kickable: Number.prototype.castBool(role.kickable),
                invitable: Number.prototype.castBool(role.channels_invitable),
                channel_viewable: Number.prototype.castBool(
                    role.channels_viewable
                ),
                message_sendable: Number.prototype.castBool(
                    role.message_sendable
                ),
                messages_managable: Number.prototype.castBool(
                    role.messages_managable
                ),
                message_embeddable: Number.prototype.castBool(
                    role.message_embeddable
                ),
                message_attachable: Number.prototype.castBool(
                    role.message_attachable
                ),
                messages_readable: Number.prototype.castBool(
                    role.messages_readable
                ),
                message_mentionable: Number.prototype.castBool(
                    role.message_mentionable
                ),
                message_reactionable: Number.prototype.castBool(
                    role.message_mentionable
                ),
            });
        }

        return entry;
    }

    async edit_role({ channelRole }) {
        const data = await models.DMChannelRole.findOne({
            where: {
                id: Number(channelRole.id) || 0,
            },
        });

        const result = await data.update({
            ...channelRole,
        });

        return result;
    }

    async remove_role({ channel, role }) {
        await models.DMChannelRole.destroy({
            where: {
                channel_id: Number(channel.id) || 0,
                role_id: Number(role.id) || 0,
            },
        });

        return;
    }

    async switchPermission({ user, channel, permission }) {
        const member = await models.Member.findOne({
            where: {
                guild_id: Number(channel.GuildId) || 0,
                user_id: Number(user.id) || 0,
            },
        });

        const guild = await models.Guild.findOne({
            where: {
                id: Number(channel.GuildId) || 0,
            },
        });

        const data = await models.DMChannel.findOne({
            where: {
                id: Number(channel.id),
            },
        });

        const result = await data.update({
            permission: Number.prototype.castBool(permission),
        });

        return result;
    }

    async updateCount(value) {
        if (!value) return;
        const channel = await models.DMChannel.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!channel) return;

        const entries = await models.DMChannelEntry.findAll({
            where: {
                channel_id: Number(channel.id),
            },
        });

        const messages = await models.Message.findAll({
            where: {
                messageable_id: Number(channel.id),
                messageable_type: 'DMChannel',
            },
        });

        const result = await channel.update({
            member_count: entries.length,
            message_count: messages.length,
        });

        return result;
    }

    async updateCountFromMessage(message) {
        if (
            !message ||
            message.messageable_type != 'DMChannel' ||
            !Number(message.messageable_id)
        )
            return;
        const channel = { id: Number(message.messageable_id) || 0 };
        return this.updateCount(channel);
    }

    async setLastIndex(value) {
        if (!value) return;
        const channel = await models.DMChannel.findOne({
            where: {
                id: Number(value.id),
            },
        });
        if (!channel) return;

        const [
            text_channels,
            stream_channels,
            dm_channels,
            section_channels,
        ] = await Promise.all([
            models.TextChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                },
            }),
            models.StreamChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                    temporary: false,
                },
            }),
            models.DMChannel.findAll({
                where: {
                    guild_id: Number(channel.GuildId) || 0,
                    section_id: Number(channel.SectionId) || null,
                    permission: true,
                },
            }),
        ]);

        const result = await channel.update({
            index:
                text_channels.length +
                stream_channels.length +
                dm_channels.length,
        });

        return result;
    }

    async reads({ channel, member }) {
        const reads = await models.Read.findAll({
            where: {
                member_id: Number(member.id) || 0,
                complete: false,
            },
            include: [
                {
                    model: models.Message,
                    where: {
                        messageable_id: Number(channel.id) || 0,
                        messageable_type: 'DMChannel',
                    },
                },
            ],
        });

        const results = await Promise.map(
            reads,
            read => read.update({ complete: true }),
            { concurrency: 10 }
        );

        return results;
    }
}
