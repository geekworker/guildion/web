import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import {
    StreamDataStore,
    FileDataStore,
    FolderDataStore,
    MessageDataStore,
} from '@datastore';
import safe2json from '@extension/safe2json';
import safe2array from '@extension/safe2array';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const streamDataStore = StreamDataStore.instance;
const fileDataStore = FileDataStore.instance;
const folderDataStore = FolderDataStore.instance;
const messageDataStore = MessageDataStore.instance;

export default class StreamHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new StreamHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetStreamRequest(router, ctx, next) {
        const { id, identity } = router.request.body;

        const stream = await models.Stream.findOne({
            where: {
                id: Number(id) || 0,
                permission: true,
            },
            include: [models.Stream.index_include(models)],
        });

        router.body = {
            success: true,
            stream: stream.get({ plain: true }),
        };
    }

    async handleStartLiveRequest(router, ctx, next) {
        let { stream, identity, loggable } = router.request.body;

        const channel = await models.StreamChannel.findOne({
            where: {
                id: Number(stream.ChannelId) || 0,
            },
        });

        const current_member = await models.Member.findOne({
            where: {
                user_id: identity.UserId,
                UserId: identity.UserId,
                guild_id: channel.GuildId,
                GuildId: channel.GuildId,
            },
        });

        if (
            stream.streamable_type == 'File' &&
            stream.Streamable &&
            (stream.streamable_id == null || stream.streamable_id == 0)
        ) {
            const file = await fileDataStore.create({
                file: stream.Streamable,
                current_user: identity.User,
            });
            stream.streamable_id = file.id;
            stream.Streamable = file;
        } else if (
            stream.Files &&
            stream.Files.length > 0 &&
            (stream.streamable_id == null || stream.streamable_id == 0)
        ) {
            stream.Files = safe2array(stream.Files);
            stream.Files = await Promise.all(
                stream.Files.map(file =>
                    fileDataStore.create({ file, current_user: identity.User })
                )
            );

            const references = await folderDataStore.create_temporary({
                current_member,
                files: stream.Files,
            });

            stream.streamable_id = references[0].id;
            stream.streamable_type == 'FileReference';
        }

        let before_streams = await models.Stream.findAll({
            where: {
                channel_id: Number(stream.ChannelId) || 0,
                is_live: true,
            },
            order: [['id', 'DESC']],
            limit: 1,
        });

        let result;
        if (before_streams.length == 0) {
            result = await streamDataStore.startLive({
                stream,
            });
        } else {
            result = await streamDataStore.switchLive({
                stream,
                before_stream: before_streams[0],
            });
        }

        const participant = await streamDataStore.join({
            member: current_member,
            stream: result,
        });

        if (result.streamable_id && result.streamable_type && Number.prototype.castBool(loggable)) {
            await messageDataStore.create_channelog({
                message: {
                    MessageableId: channel.id,
                    messageable_type: 'StreamChannel',
                },
                channelog: {
                    ChannelogableId: Number(result.id) || 0,
                    channelogable_type: 'Stream',
                    action: 'start_stream',
                },
                current_user: identity.User,
            });
        }

        router.body = {
            success: true,
            stream: result.get({ plain: true }),
            participant: participant.get({ plain: true }),
        };
    }

    async handleSwitchLiveRequest(router, ctx, next) {
        const { stream, before_stream, identity } = router.request.body;

        if (
            stream.streamable_type == 'File' &&
            (stream.streamable_id == null || stream.streamable_id == 0)
        ) {
            const file = await fileDataStore.create({
                file: stream.Streamable,
                current_user: identity.User,
            });
            stream.streamable_id = file.id;
            stream.Streamable = File;
        }

        const result = await streamDataStore.switchLive({
            stream,
            before_stream,
        });

        if (result.streamable_id && result.streamable_type) {
            await messageDataStore.create_channelog({
                message: {
                    MessageableId: channel.id,
                    messageable_type: 'StreamChannel',
                },
                channelog: {
                    ChannelogableId: Number(result.id) || 0,
                    channelogable_type: 'Stream',
                    action: 'start_stream',
                },
                current_user: identity.User,
            });
        }

        router.body = {
            success: true,
            stream: result.get({ plain: true }),
        };
    }

    async handleEndLiveRequest(router, ctx, next) {
        const { stream, identity } = router.request.body;

        const result = await streamDataStore.endLive({
            stream,
        });

        router.body = {
            success: true,
            stream: result.get({ plain: true }),
        };
    }

    async handleJoinRequest(router, ctx, next) {
        let { channel, identity } = router.request.body;

        channel = await models.StreamChannel.findOne({
            where: { id: Number(channel.id) || 0 },
        });

        const member = await models.Member.findOne({
            where: {
                guild_id: channel.GuildId,
                user_id: identity.User.id,
            },
        });

        const streams = await models.Stream.findAll({
            where: {
                is_live: true,
                channel_id: channel.id,
            },
            order: [['id', 'DESC']],
            limit: 1,
            include: [...models.Stream.show_include(models)],
        });

        if (streams.length > 0) {
            const stream = streams[0];
            const participant = await streamDataStore.join({
                member,
                stream,
            });
            router.body = {
                success: true,
                stream: stream.get({ plain: true }),
                participant: participant.get({ plain: true }),
            };
            return;
        } else {
            const stream = models.Stream.build({
                is_live: true,
                channel_id: channel.id,
                ChannelId: channel.id,
                delivery_start_at: new Date(),
                permission: true,
            });

            const result = await streamDataStore.startLive({
                stream,
            });

            const participant = await streamDataStore.join({
                member,
                stream: result,
            });

            router.body = {
                success: true,
                stream: result.get({ plain: true }),
                participant: participant.get({ plain: true }),
            };
        }
    }

    async handleLeaveRequest(router, ctx, next) {
        let { identity, channel, duration } = router.request.body;

        channel = await models.StreamChannel.findOne({
            where: { id: Number(channel.id) || 0 },
        });

        const member = await models.Member.findOne({
            where: {
                guild_id: channel.GuildId,
                user_id: identity.User.id,
            },
        });

        const streams = await models.Stream.findAll({
            where: {
                is_live: true,
                channel_id: channel.id,
            },
            order: [['id', 'DESC']],
            limit: 1,
            include: [...models.Stream.show_include(models)],
        });

        let stream = streams[0];

        const participant = await streamDataStore.leave({
            member,
            stream,
        });

        const participants = await models.Participant.findAll({
            where: {
                is_live: true,
                stream_id: stream.id,
            },
        });

        stream = await streamDataStore.saveDurationOffset({ stream, duration });

        router.body = {
            success: true,
            stream: stream.get({ plain: true }),
            participant: participant.get({ plain: true }),
        };
    }
}
