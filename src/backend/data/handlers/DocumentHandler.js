import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { DocumentDataStore } from '@datastore';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const documentDataStore = DocumentDataStore.instance;

export default class DocumentHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new DocumentHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetRequest(router, ctx, next) {
        const { id, uid, groupname } = router.request.body;

        const doc = await models.Document.findOne({
            where: {
                $or: [
                    {
                        id: Number(id) || 0,
                    },
                    {
                        uid: uid,
                    },
                ],
                permission: true,
            },
            include: [...models.Document.show_include(models, { groupname })],
        });

        router.body = {
            success: true,
            document: doc.get({ plain: true }),
        };
    }

    async handleGetGroupRequest(router, ctx, next) {
        const { id, uid, groupname } = router.request.body;

        const group = await models.DocumentGroup.findOne({
            where: {
                $or: [
                    {
                        id: Number(id) || 0,
                    },
                    {
                        uid: uid,
                    },
                    {
                        groupname: groupname,
                    },
                ],
                permission: true,
            },
        });

        const [documents, sections] = await Promise.all([
            models.Document.findAll({
                where: {
                    section_id: null,
                    group_id: group.id,
                    permission: true,
                },
            }),
            models.DocumentSection.findAll({
                where: {
                    group_id: group.id,
                    permission: true,
                },
                include: [
                    {
                        as: 'Documents',
                        model: models.Document,
                        where: {
                            permission: true,
                        },
                    },
                ],
                order: [['index', 'ASC']],
            }),
        ]);

        group.setValue('Sections', sections);
        group.setValue('Documents', documents);
        group.sectionify(models);
        group.sortify(models);

        router.body = {
            success: true,
            group: group.get({ plain: true }),
        };
    }

    async handleGetGroupFromDocumentRequest(router, ctx, next) {
        const { id, uid } = router.request.body;

        const doc = await models.Document.findOne({
            where: {
                $or: [
                    {
                        id: Number(id) || 0,
                    },
                    {
                        uid: uid,
                    },
                ],
                permission: true,
            },
        });

        const group = await models.DocumentGroup.findOne({
            where: {
                $or: [
                    {
                        id: doc.GroupId,
                    },
                ],
                permission: true,
            },
            include: [...models.DocumentGroup.show_include(models)],
        });

        router.body = {
            success: true,
            group: group.get({ plain: true }),
        };
    }

    async handleUpdateRequest(router, ctx, next) {
        const { identity, document } = router.request.body;

        const result = await documentDataStore.update({
            document,
        });

        router.body = {
            success: true,
            document: result.get({ plain: true }),
        };
    }

    async handleFindOrCreateRequest(router, ctx, next) {
        const { document } = router.request.body;

        const result = await documentDataStore.findOrCreate({
            document,
        });

        router.body = {
            success: true,
            document: result.get({ plain: true }),
        };
    }

    async handleFindOrCreateSectionRequest(router, ctx, next) {
        const { documentSection } = router.request.body;

        const result = await documentDataStore.findOrCreateSection({
            documentSection,
        });

        router.body = {
            success: true,
            documentSection: result.get({ plain: true }),
        };
    }

    async handleFindOrCreateGroupRequest(router, ctx, next) {
        const { documentGroup } = router.request.body;

        const result = await documentDataStore.findOrCreateGroup({
            documentGroup,
        });

        router.body = {
            success: true,
            documentGroup: result.get({ plain: true }),
        };
    }

    async handleFreezeNotSyncsRequest(router, ctx, next) {
        const { document_uids } = router.request.body;

        const results = await documentDataStore.freezeNotSyncs({
            document_uids,
        });

        router.body = {
            success: true,
        };
    }

    async handleFreezeNotSyncSectionsRequest(router, ctx, next) {
        const { documentSection_uids } = router.request.body;

        const results = await documentDataStore.freezeNotSyncSections({
            documentSection_uids,
        });

        router.body = {
            success: true,
        };
    }

    async handleFreezeNotSyncGroupsRequest(router, ctx, next) {
        const { documentGroup_uids } = router.request.body;

        const results = await documentDataStore.freezeNotSyncGroups({
            documentGroup_uids,
        });

        router.body = {
            success: true,
        };
    }
}
