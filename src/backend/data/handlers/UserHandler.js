import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import safe2json from '@extension/safe2json';
import {
    SessionDataStore,
    UserDataStore,
    StreamDataStore,
    GuildDataStore,
} from '@datastore';
import uuidv4 from 'uuid/v4';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const sessionDataStore = SessionDataStore.instance;
const userDataStore = UserDataStore.instance;
const streamDataStore = StreamDataStore.instance;
const guildDataStore = GuildDataStore.instance;

export default class UserHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new UserHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleSyncUserRequest(router, ctx, next) {
        const { accessToken, devise } = router.request.body;

        if (!accessToken || !devise) {
            router.body = {
                success: true,
                exist: false,
            };
            return;
        }

        const identity = await sessionDataStore
            .checkAccessToken({
                access_token: accessToken,
                devise,
                deleting: false,
            })
            .catch(e => {
                router.body = {
                    success: true,
                    exist: false,
                };
                return;
            });

        if (!identity) {
            router.body = {
                success: true,
                exist: false,
            };
            return;
        }

        let where = {};
        if (process.env.NODE_ENV == 'development') {
            where.id = Number(identity.UserId) || 0;
        } else {
            where.id = Number(identity.UserId) || 0;
            where.username = identity.username;
        }

        const synced_user = await models.User.findOne({
            where,
        }).catch(e => {
            router.body = {
                success: true,
                exist: false,
            };
            return;
        });

        if (!synced_user) {
            router.body = {
                success: true,
                exist: false,
            };
            return;
        }

        router.body = {
            success: true,
            user: synced_user.get({ plain: true }),
            exist: !!synced_user,
        };
    }

    async handleRegisterUserDeviseRequest(router, ctx, next) {
        const { devise } = router.request.body;

        if (!devise) {
            router.body = {
                success: true,
                exist: false,
            };
            return;
        }

        const _devise = await models.Devise.create({
            uid: uuidv4(),
            udid: devise.udid,
            os: devise.os,
            model: devise.model,
            locale: devise.locale,
            country_code: devise.country_code,
            notification_id: devise.notification_id,
            app_version: devise.app_version,
            permission: true,
        });

        router.body = {
            success: true,
            devise: _devise.get({ plain: true }),
        };
    }

    async handleUpdateUserDeviseRequest(router, ctx, next) {
        const { identity, devise } = router.request.body;

        if (!devise || !identity) {
            router.body = {
                success: true,
                exist: false,
            };
            return;
        }

        let before = await models.Devise.findOne({
            where: {
                uid: devise.uid,
            },
        });

        if (!before) {
            before = await models.Devise.create({
                uid: devise.uid || uuidv4(),
                udid: devise.udid,
                os: devise.os,
                model: devise.model,
                locale: devise.locale,
                country_code: devise.country_code,
                notification_id: devise.notification_id,
                app_version: devise.app_version,
                permission: true,
            });
        }

        const result = await before.update({
            os: devise.os,
            model: devise.model,
            locale: devise.locale,
            country_code: devise.country_code,
            notification_id: devise.notification_id,
            app_version: devise.app_version,
            permission: true,
        });

        const [middle_result, created] = await models.UserDevise.findOrCreate({
            where: {
                user_id: identity.UserId,
                devise_id: before.id,
            },
        });

        await middle_result.update({
            is_sign_in: true,
            permission: true,
        });

        router.body = {
            success: true,
            devise: result.get({ plain: true }),
        };
    }

    async handleUserDeviseLogoutRequest(router, ctx, next) {
        const { identity, devise } = router.request.body;

        if (!devise || !identity) {
            router.body = {
                success: true,
                exist: false,
            };
            return;
        }

        const before = await models.Devise.findOne({
            where: {
                uid: devise.uid,
            },
        });

        const result = await models.UserDevise.findOne({
            where: {
                user_id: identity.UserId,
                devise_id: before.id,
            },
        });

        if (result) {
            await result.update({
                is_sign_in: false,
            });
        }

        router.body = {
            success: true,
            devise: result.get({ plain: true }),
        };
    }

    async handleGetUserMembersRequest(router, ctx, next) {
        const { identity } = router.request.body;

        const results = await userDataStore.getMembers({
            current_user: identity.User,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetCurrentUserMembersRequest(router, ctx, next) {
        const { identity, limit } = router.request.body;

        const results = await userDataStore.getCurrentMembers({
            current_user: identity.User,
            limit,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetIncrementUserMembersRequest(router, ctx, next) {
        const { identity, limit, last_id } = router.request.body;

        const results = await userDataStore.getIncrementMembers({
            current_user: identity.User,
            limit,
            last_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetDecrementUserMembersRequest(router, ctx, next) {
        const { identity, limit, first_id } = router.request.body;

        const results = await userDataStore.getDecrementMembers({
            current_user: identity.User,
            limit,
            first_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleActiveRequest(router, ctx, next) {
        const { identity } = router.request.body;

        await userDataStore.makeInActive({ user: identity.User });
        await streamDataStore.endLiveAllFromUser({ user: identity.User });

        const result = await userDataStore.makeActive({ user: identity.User });

        router.body = {
            success: true,
            user: result.get({ plain: true }),
        };
    }

    async handleInActiveRequest(router, ctx, next) {
        const { identity } = router.request.body;

        const result = await userDataStore.makeInActive({
            user: identity.User,
        });

        await streamDataStore.endLiveAllFromUser({ user: identity.User });

        guildDataStore.updateCountFromUser(identity.User);

        router.body = {
            success: true,
            user: result.get({ plain: true }),
        };
    }

    async handleUpdateUserRequest(router, ctx, next) {
        const { identity, user } = router.request.body;

        const result = await userDataStore.updateUser({ user: user });

        router.body = {
            success: true,
            user: result.get({ plain: true }),
        };
    }

    async handleDeleteUserRequest(router, ctx, next) {
        const { identity } = router.request.body;

        const result = await userDataStore.deleteUser({ user: identity.User });

        router.body = {
            success: true,
        };
    }

    async handleBlockRequest(router, ctx, next) {
        const { identity, target } = router.request.body;

        const result = await userDataStore.block({
            user: identity.User,
            target,
        });

        router.body = {
            success: true,
        };
    }

    async handleUnBlockRequest(router, ctx, next) {
        const { identity, target } = router.request.body;

        const result = await userDataStore.unblock({
            user: identity.User,
            target,
        });

        router.body = {
            success: true,
        };
    }

    async handleMuteRequest(router, ctx, next) {
        const { identity, target } = router.request.body;

        const result = await userDataStore.mute({
            user: identity.User,
            target,
        });

        router.body = {
            success: true,
        };
    }

    async handleUnMuteRequest(router, ctx, next) {
        const { identity, target } = router.request.body;

        const result = await userDataStore.unmute({
            user: identity.User,
            target,
        });

        router.body = {
            success: true,
        };
    }

    async handleReportRequest(router, ctx, next) {
        const { identity, target, description } = router.request.body;

        const result = await userDataStore.report({
            user: identity.User,
            target,
            description,
        });

        router.body = {
            success: true,
        };
    }

    async handleGetBlocksRequest(router, ctx, next) {
        const { identity, limit, offset } = router.request.body;

        const blocks = await models.User.findAll({
            where: {
                permission: true,
            },
            include: [
                {
                    model: models.UserBlock,
                    where: {
                        user_id: Number(identity.UserId) || 0,
                    },
                },
            ],
        });

        router.body = {
            success: true,
            users: blocks.map(val => val.get({ plain: true })),
        };
    }

    async handleGetMutesRequest(router, ctx, next) {
        const { identity, id, limit, offset } = router.request.body;

        const mutes = await models.User.findAll({
            where: {
                permission: true,
            },
            include: [
                {
                    model: models.UserMute,
                    where: {
                        user_id: Number(identity.UserId) || 0,
                    },
                },
            ],
        });

        router.body = {
            success: true,
            users: mutes.map(val => val.get({ plain: true })),
        };
    }

    async handleGetUserBlockStatusRequest(router, ctx, next) {
        const { identity } = router.request.body;

        const user = await models.User.findOne({
            where: {
                id: Number(identity.UserId) || 0,
                permission: true,
            },
        });

        const [
            Reports,
            Reporters,
            Mutes,
            Muters,
            Blocks,
            Blockers,
            GuildBlockers,
        ] = await Promise.all([
            models.User.findAll({
                include: [
                    {
                        model: models.UserReport,
                        where: {
                            user_id: identity.UserId,
                        },
                    },
                ],
            }),
            models.User.findAll({
                include: [
                    {
                        model: models.UserReport,
                        where: {
                            receiver_id: identity.UserId,
                        },
                    },
                ],
            }),
            models.User.findAll({
                include: [
                    {
                        model: models.UserMute,
                        where: {
                            user_id: identity.UserId,
                        },
                    },
                ],
            }),
            models.User.findAll({
                include: [
                    {
                        model: models.UserMute,
                        where: {
                            receiver_id: identity.UserId,
                        },
                    },
                ],
            }),
            models.User.findAll({
                include: [
                    {
                        model: models.UserBlock,
                        where: {
                            user_id: identity.UserId,
                        },
                    },
                ],
            }),
            models.User.findAll({
                include: [
                    {
                        model: models.UserBlock,
                        where: {
                            receiver_id: identity.UserId,
                        },
                    },
                ],
            }),
            models.Guild.findAll({
                include: [
                    {
                        model: models.GuildBlock,
                        where: {
                            user_id: identity.UserId,
                        },
                    },
                ],
            }),
        ]);

        user.setValue('Reports', Reports);
        user.setValue('Reporters', Reporters);
        user.setValue('Mutes', Mutes);
        user.setValue('Muters', Muters);
        user.setValue('Blocks', Blocks);
        user.setValue('Blockers', Blockers);
        user.setValue('GuildBlockers', GuildBlockers);

        router.body = {
            success: true,
            user: user.get({ plain: true }),
        };
    }
}
