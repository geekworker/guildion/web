import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import ReactionDataStore from '@datastore/ReactionDataStore';
import MessageDataStore from '@datastore/MessageDataStore';
import expo from '@extension/object2json';
import safe2json from '@extension/safe2json';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const reactionDataStore = ReactionDataStore.instance;
const messageDataStore = MessageDataStore.instance;

export default class ReactionHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new ReactionHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleCreateReactionRequest(router, ctx, next) {
        const { identity, reaction } = router.request.body;

        const result = await reactionDataStore.create({ reaction });

        // messageDataStore.updateCount();

        router.body = {
            success: true,
            reaction: result.get({ plain: true }),
        };
    }

    async handleDeleteReactionRequest(router, ctx, next) {
        const { identity, reaction } = router.request.body;

        await reactionDataStore.delete({
            reaction,
        });

        // messageDataStore.updateCount();

        router.body = {
            success: true,
        };
    }
}
