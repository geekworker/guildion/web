import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import safe2json from '@extension/safe2json';
import { CategoryDataStore } from '@datastore';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const categoryDataStore = CategoryDataStore.instance;

export default class CategoryHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new CategoryHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetCategoriesRequest(router, ctx, next) {
        let { _locale } = router.request.body;

        const locale = getLocale(_locale);

        const categories = await models.Category.findAll({
            where: {
                // locale,
                permission: true,
            },
            order: [['guild_count', 'DESC']],
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        router.body = {
            success: true,
            categories: categories.map(category =>
                category.get({ plain: true })
            ),
        };
    }

    async handleInitCategoriesRequest(router, ctx, next) {
        await categoryDataStore.init_categories();

        router.body = {
            success: true,
        };
    }
}
