import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import {
    StreamDataStore,
    UserDataStore,
    GuildDataStore,
    MessageDataStore,
} from '@datastore';
import { ApiError } from '@extension/Error';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const userDataStore = UserDataStore.instance;
const streamDataStore = StreamDataStore.instance;
const guildDataStore = GuildDataStore.instance;
const messageDataStore = MessageDataStore.instance;

export default class SearchHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new SearchHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleSearchGuildRequest(router, ctx, next) {
        const { keyword, limit, offset, user } = router.request.body;

        const guilds = await guildDataStore.search({
            keyword,
            limit,
            offset,
        });

        await guildDataStore.setSearchIndexIncludes(guilds);

        router.body = {
            guilds: guilds.map(val => val.get({ plain: true })),
            success: true,
        };
    }

    async handleSearchMessageRequest(router, ctx, next) {
        const {
            messageable_id,
            messageable_type,
            keyword,
            limit,
            offset,
            is_static,
        } = router.request.body;

        const messages = await messageDataStore.search({
            messageable_id,
            messageable_type,
            keyword,
            limit,
            offset,
            is_static,
        });

        router.body = {
            messages: messages.map(val => val.get({ plain: true })),
            success: true,
        };
    }
}
