import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class NotificationHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new NotificationHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }
}
