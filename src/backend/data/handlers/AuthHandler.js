import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import models from '@models';
import { AuthDataStore, SessionDataStore, UserDataStore } from '@datastore';
import querystring from 'querystring';
import safe2json from '@extension/safe2json';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import uuidv4 from 'uuid/v4';
import Promise from 'bluebird';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const sessionDataStore = SessionDataStore.instance;
const authDataStore = AuthDataStore.instance;
const userDataStore = UserDataStore.instance;

export default class AuthHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new AuthHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleAuthenticateRequest(router, ctx, next) {
        const { email, password, devise } = router.request.body;

        const identity = await authDataStore.authenticate({
            email,
            password,
        });

        const accessToken = await sessionDataStore.setAccessToken({
            identity,
            devise,
            isOneTime: false,
        });

        router.body = {
            success: true,
            user: identity.User.get({ plain: true }),
            identity: identity.get({ plain: true }),
            accessToken,
        };
    }
}
