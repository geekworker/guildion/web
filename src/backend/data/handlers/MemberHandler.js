import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import { MemberDataStore } from '@datastore';
import safe2array from '@extension/safe2array';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const memberDataStore = MemberDataStore.instance;

export default class MemberHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new MemberHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetRequest(router, ctx, next) {
        const { id, identity } = router.request.body;

        const result = await memberDataStore.getMember({
            member: { id },
        });

        router.body = {
            success: true,
            member: result.get({ plain: true }),
        };
    }

    async handleUpdateRequest(router, ctx, next) {
        const { member, identity } = router.request.body;

        const result = await memberDataStore.update({
            member,
        });

        router.body = {
            success: true,
            member: result.get({ plain: true }),
        };
    }

    async handleAllUpdateRequest(router, ctx, next) {
        const { identity } = router.request.body;

        const results = await memberDataStore.all_update({
            current_user: identity.User,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleUpdateNotificationRequest(router, ctx, next) {
        const { member, identity } = router.request.body;

        const result = await memberDataStore.updateNotification({
            member,
        });

        router.body = {
            success: true,
            member: result.get({ plain: true }),
        };
    }

    async handleGetGuildMembersRequest(router, ctx, next) {
        const { guild, identity } = router.request.body;

        const results = await memberDataStore.getAllGuildMembers({
            guild,
            current_user: identity.User,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetCurrentGuildMembersRequest(router, ctx, next) {
        const { guild, limit, offset, identity } = router.request.body;

        const results = await memberDataStore.getCurrentGuildMembers({
            guild,
            limit,
            offset,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetIncrementGuildMembersRequest(router, ctx, next) {
        const { guild, limit, offset, identity, last_id } = router.request.body;

        const results = await memberDataStore.getIncrementGuildMembers({
            guild,
            limit,
            offset,
            last_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetDecrementGuildMembersRequest(router, ctx, next) {
        const {
            guild,
            limit,
            offset,
            identity,
            first_id,
        } = router.request.body;

        const results = await memberDataStore.getDecrementGuildMembers({
            guild,
            limit,
            offset,
            first_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetCurrentTextChannelMembersRequest(router, ctx, next) {
        const { channel, limit, offset, identity } = router.request.body;

        const results = await memberDataStore.getCurrentTextChannelMembers({
            channel,
            limit,
            offset,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetIncrementTextChannelMembersRequest(router, ctx, next) {
        const {
            channel,
            limit,
            offset,
            identity,
            last_id,
        } = router.request.body;

        const results = await memberDataStore.getIncrementTextChannelMembers({
            channel,
            limit,
            offset,
            last_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetDecrementTextChannelMembersRequest(router, ctx, next) {
        const {
            channel,
            limit,
            offset,
            identity,
            first_id,
        } = router.request.body;

        const results = await memberDataStore.getDecrementTextChannelMembers({
            channel,
            limit,
            offset,
            first_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetCurrentStreamChannelMembersRequest(router, ctx, next) {
        const { channel, limit, offset, identity } = router.request.body;

        const results = await memberDataStore.getCurrentStreamChannelMembers({
            channel,
            limit,
            offset,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetIncrementStreamChannelMembersRequest(router, ctx, next) {
        const {
            channel,
            limit,
            offset,
            identity,
            last_id,
        } = router.request.body;

        const results = await memberDataStore.getIncrementStreamChannelMembers({
            channel,
            limit,
            offset,
            last_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetDecrementStreamChannelMembersRequest(router, ctx, next) {
        const {
            channel,
            limit,
            offset,
            identity,
            first_id,
        } = router.request.body;

        const results = await memberDataStore.getDecrementStreamChannelMembers({
            channel,
            limit,
            offset,
            first_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetCurrentDMChannelMembersRequest(router, ctx, next) {
        const { channel, limit, offset, identity } = router.request.body;

        const results = await memberDataStore.getCurrentDMChannelMembers({
            channel,
            limit,
            offset,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetIncrementDMChannelMembersRequest(router, ctx, next) {
        const {
            channel,
            limit,
            offset,
            identity,
            last_id,
        } = router.request.body;

        const results = await memberDataStore.getIncrementDMChannelMembers({
            channel,
            limit,
            offset,
            last_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetDecrementDMChannelMembersRequest(router, ctx, next) {
        const {
            channel,
            limit,
            offset,
            identity,
            first_id,
        } = router.request.body;

        const results = await memberDataStore.getDecrementDMChannelMembers({
            channel,
            limit,
            offset,
            first_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetStreamInvitableMembersRequest(router, ctx, next) {
        const { channel, limit, offset, identity } = router.request.body;

        const results = await memberDataStore.getStreamChannelInvitableMembers({
            channel,
            current_user: identity.User,
            limit,
            offset,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetTextInvitableMembersRequest(router, ctx, next) {
        const { channel, limit, offset, identity } = router.request.body;

        const results = await memberDataStore.getTextChannelInvitableMembers({
            channel,
            current_user: identity.User,
            limit,
            offset,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetDMInvitableMembersRequest(router, ctx, next) {
        const { channel, identity, limit, offset } = router.request.body;

        const results = await memberDataStore.getDMChannelInvitableMembers({
            channel,
            current_user: identity.User,
            limit,
            offset,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetCurrentRoleMembersRequest(router, ctx, next) {
        const { identity, role, limit } = router.request.body;

        const results = await memberDataStore.getCurrentRoleMembers({
            current_user: identity.User,
            role,
            limit,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetIncrementRoleMembersRequest(router, ctx, next) {
        const { identity, role, limit, last_id } = router.request.body;

        const results = await memberDataStore.getIncrementRoleMembers({
            current_user: identity.User,
            role,
            limit,
            last_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetDecrementRoleMembersRequest(router, ctx, next) {
        const { identity, role, limit, first_id } = router.request.body;

        const results = await memberDataStore.getDecrementRoleMembers({
            current_user: identity.User,
            role,
            limit,
            first_id,
        });

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }

    async handleTransferOwnerRequest(router, ctx, next) {
        const { guild, target, identity } = router.request.body;

        const result = await memberDataStore.transferOwner({
            guild,
            target,
            owner: identity.user,
        });

        router.body = {
            success: true,
            guild: result.get({ plain: true }),
        };
    }

    async handleUpdateMemberIndexRequest(router, ctx, next) {
        const { member, identity } = router.request.body;

        const result = await memberDataStore.index_update({
            member,
            current_user: identity.User,
        });

        router.body = {
            success: true,
            member: result.get({ plain: true }),
        };
    }

    async handleUpdateMemberIndexesRequest(router, ctx, next) {
        const { members, identity } = router.request.body;

        const _members = safe2array(members);

        const results = await Promise.all(
            _members.map(member =>
                memberDataStore.index_update({
                    member,
                    current_user: identity.User,
                })
            )
        );

        router.body = {
            success: true,
            members: results.map(result => result.get({ plain: true })),
        };
    }
}
