import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import { DMChannelDataStore, MemberDataStore } from '@datastore';
import safe2array from '@extension/safe2array';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const dmChannelDataStore = DMChannelDataStore.instance;
const memberDataStore = MemberDataStore.instance;

export default class DMChannelHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new DMChannelHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetRequest(router, ctx, next) {
        const { identity, uid } = router.request.body;

        const result = await dmChannelDataStore.getChannel({
            channel: { uid },
            current_user: identity.User,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleFindOrCreateRequest(router, ctx, next) {
        const { identity, guild, target } = router.request.body;

        const result = await dmChannelDataStore.findOrCreateDMChannel({
            guild,
            user: identity.User,
            target,
        });

        const entries = await models.DMChannelEntry.findAll({
            where: {
                channel_id: Number(result.id) || 0,
            },
        });

        await Promise.all(entries.map(val => val.update({ permission: true })));

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleGetRolesRequest(router, ctx, next) {
        const { identity, uid } = router.request.body;

        const result = await models.DMChannel.findOne({
            where: {
                uid,
                permission: true,
            },
        });

        const roles = await models.DMChannelRole.findAll({
            where: {
                channel_id: result.id,
                permission: true,
            },
            include: [
                {
                    model: models.Role,
                },
                {
                    model: models.DMChannel,
                },
            ],
        });

        router.body = {
            success: true,
            roles: roles.map(role => role.get({ plain: true })),
        };
    }

    async handleCreateRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        channel.Members = safe2array(channel.Members);

        const result = await dmChannelDataStore.create({
            user: identity.User,
            section: channel.Section,
            members: channel.Members,
            channel,
        });

        await memberDataStore.updateDMChannelMembersPermission({
            current_user: identity.User,
            channel: result,
            members: channel.Members,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleUpdateRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        channel.Members = safe2array(channel.Members);

        const result = await dmChannelDataStore.update({
            user: identity.User,
            section: channel.Section,
            members: channel.Members,
            channel,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleUpdateEntryRequest(router, ctx, next) {
        const { identity, entry } = router.request.body;

        const result = await dmChannelDataStore.updateEntry({ entry });

        router.body = {
            success: true,
            entry: result.get({ plain: true }),
        };
    }

    async handleSwitchPermissionRequest(router, ctx, next) {
        const { identity, channel, permission } = router.request.body;

        const result = await dmChannelDataStore.switchPermission({
            user: identity.User,
            channel,
            permission,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleJoinRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const member = await models.Member.findOne({
            where: {
                user_id: identity.UserId,
                guild_id: Number(channel.GuildId) || 0,
            },
        });

        const result = await dmChannelDataStore.join({ channel, member });

        await memberDataStore.updateDMChannelMembersPermission({
            current_user: identity.User,
            channel,
            members: [member],
        });

        router.body = {
            success: true,
            entry: result.get({ plain: true }),
        };
    }

    async handleLeaveRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const member = await models.Member.findOne({
            where: {
                user_id: identity.UserId,
                guild_id: Number(channel.GuildId) || 0,
            },
        });

        const result = await dmChannelDataStore.leave({ channel, member });

        router.body = {
            success: true,
        };
    }

    async handleAddRoleRequest(router, ctx, next) {
        const { identity, channel, role } = router.request.body;

        const result = await dmChannelDataStore.add_role({ channel, role });

        const members = await memberDataStore.getAllDMChannelMembers({
            current_user: identity.User,
            channel,
            role_ignore: true,
        });

        await memberDataStore.updateDMChannelMembersPermission({
            current_user: identity.User,
            channel,
            members,
        });

        router.body = {
            success: true,
            channel_role: result.get({ plain: true }),
        };
    }

    async handleUpdateRoleRequest(router, ctx, next) {
        const { identity, channel_role } = router.request.body;

        const result = await dmChannelDataStore.edit_role({
            channelRole: channel_role,
        });

        const channel = await models.DMChannel.findOne({
            where: {
                id: Number(channel_role.ChannelId) || 0,
            },
        });

        const members = await memberDataStore.getAllDMChannelMembers({
            current_user: identity.User,
            channel,
            role_ignore: true,
        });

        await memberDataStore.updateDMChannelMembersPermission({
            current_user: identity.User,
            channel,
            members,
        });

        router.body = {
            success: true,
            channel_role: result.get({ plain: true }),
        };
    }

    async handleRemoveRoleRequest(router, ctx, next) {
        const { identity, channel, role } = router.request.body;

        const result = await dmChannelDataStore.remove_role({ channel, role });

        const members = await memberDataStore.getAllDMChannelMembers({
            current_user: identity.User,
            channel,
            role_ignore: true,
        });

        await memberDataStore.updateDMChannelMembersPermission({
            current_user: identity.User,
            channel,
            members,
        });

        router.body = {
            success: true,
        };
    }

    async handleInvitesRequest(router, ctx, next) {
        const { identity, channel, members } = router.request.body;

        const _members = safe2array(members);

        const results = await Promise.all(
            _members.map(member => dmChannelDataStore.join({ channel, member }))
        );

        await memberDataStore.updateDMChannelMembersPermission({
            current_user: identity.User,
            channel,
            members: _members,
        });

        router.body = {
            success: true,
            entries: results.map(entry => entry.get({ plain: true })),
        };
    }

    async handleKickRequest(router, ctx, next) {
        const { identity, channel, member } = router.request.body;

        const result = await dmChannelDataStore.leave({ channel, member });

        router.body = {
            success: true,
        };
    }

    async handleKicksRequest(router, ctx, next) {
        const { identity, channel, members } = router.request.body;

        const results = await Promise.all(
            members.map(member => dmChannelDataStore.leave({ channel, member }))
        );

        router.body = {
            success: true,
        };
    }

    async handleSectionIndexUpdateRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const result = await dmChannelDataStore.section_index_update({
            user: identity.User,
            channel,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleDeleteRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const result = await dmChannelDataStore.delete({
            user: identity.User,
            channel,
        });

        router.body = {
            success: true,
        };
    }

    async handleReadsRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const member = await models.Member.findOne({
            where: {
                guild_id: Number(channel.GuildId) || 0,
                user_id: identity.UserId,
            },
        });

        const reads = await dmChannelDataStore.reads({ channel, member });

        router.body = {
            success: true,
            reads: reads.map(read => read.get({ plain: true })),
        };
    }
}
