import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import {
    GuildDataStore,
    TextChannelDataStore,
    DMChannelDataStore,
    StreamChannelDataStore,
    RoleDataStore,
    MemberDataStore,
} from '@datastore';
import safe2array from '@extension/safe2array';
import tt from 'counterpart';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const guildDataStore = GuildDataStore.instance;
const textChannelDataStore = TextChannelDataStore.instance;
const dmChannelDataStore = DMChannelDataStore.instance;
const streamChannelDataStore = StreamChannelDataStore.instance;
const roleDataStore = RoleDataStore.instance;
const memberDataStore = MemberDataStore.instance;

export default class GuildHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new GuildHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetUserGuildsRequest(router, ctx, next) {
        const { identity, limit, offset } = router.request.body;

        const guilds = await guildDataStore.getUserGuilds({
            user: identity.User,
        });

        router.body = {
            success: true,
            guilds: guilds.map(guild => guild.get({ plain: true })),
        };
    }

    async handleGetHeroGuildsRequest(router, ctx, next) {
        const { limit, offset } = router.request.body;

        const guilds = await guildDataStore.getGuildsForHero({
            limit,
            offset,
        });

        router.body = {
            success: true,
            guilds: guilds.map(guild => guild.get({ plain: true })),
        };
    }

    async handleGetWelcomeGuildsRequest(router, ctx, next) {
        const { limit, offset } = router.request.body;

        const guilds = await guildDataStore.getGuildsForWelcome({
            limit,
            offset,
        });

        router.body = {
            success: true,
            guilds: guilds.map(guild => guild.get({ plain: true })),
        };
    }

    async handleGetGuildRequest(router, ctx, next) {
        const { identity, uid } = router.request.body;

        const guild = await guildDataStore.getGuild({
            user: identity.User,
            guild: { uid },
        });

        router.body = {
            success: true,
            guild: guild.get({ plain: true }),
        };
    }

    async handleGetGuildInfoRequest(router, ctx, next) {
        const { uid } = router.request.body;

        const guild = await guildDataStore.getGuildInfo({
            guild: { uid },
        });

        router.body = {
            success: true,
            guild: guild.get({ plain: true }),
        };
    }

    async handleGetGuildWithChannelRequest(router, ctx, next) {
        const { identity, guild_uid, channel_uid } = router.request.body;

        const guild = await guildDataStore.getGuild({
            user: identity.User,
            guild: { uid: guild_uid },
        });

        if (!guild) {
            router.body = {
                success: true,
            };
            return;
        }

        const member = guild.Members.find(
            member => member.UserId == identity.UserId
        );

        if (!member) {
            router.body = {
                success: true,
            };
            return;
        }

        var channel = null;
        var channel_type = null;

        const [text_channel, stream_channel, dm_channel] = await Promise.all([
            models.TextChannel.findOne({
                where: {
                    guild_id: Number(guild.id) || 0,
                    uid: channel_uid,
                    permission: true,
                },
                include: models.TextChannel.guild_show_include(models, {
                    user_id: Number(identity.UserId) || 0,
                    member_id: Number(member.id) || 0,
                }),
            }),
            models.StreamChannel.findOne({
                where: {
                    guild_id: Number(guild.id) || 0,
                    uid: channel_uid,
                    permission: true,
                },
                include: models.StreamChannel.guild_show_include(models, {
                    user_id: Number(identity.UserId) || 0,
                    member_id: Number(member.id) || 0,
                }),
            }),
            models.DMChannel.findOne({
                where: {
                    guild_id: Number(guild.id) || 0,
                    uid: channel_uid,
                    permission: true,
                },
                include: models.DMChannel.guild_show_include(models, {
                    user_id: Number(identity.UserId) || 0,
                    member_id: Number(member.id) || 0,
                }),
            }),
        ]);

        if (!!text_channel) {
            channel = text_channel;
            channel_type = 'TextChannel';
        } else if (!!stream_channel) {
            channel = stream_channel;
            channel_type = 'StreamChannel';
        } else if (!!dm_channel) {
            channel = dm_channel;
            channel_type = 'DMChannel';
        }

        if (!channel) {
            router.body = {
                success: true,
            };
            return;
        }

        router.body = {
            success: true,
            guild: guild.get({ plain: true }),
            member: member.get({ plain: true }),
            channel: channel.get({ plain: true }),
            channel_type,
        };
    }

    async handleGetGuildChannelsRequest(router, ctx, next) {
        const { identity, uid } = router.request.body;

        const guild = await guildDataStore.getGuildChannels({
            user: identity.User,
            guild: { uid },
        });

        router.body = {
            success: true,
            guild: guild.get({ plain: true }),
        };
    }

    async handleGetGuildRolesRequest(router, ctx, next) {
        const { identity, uid, id } = router.request.body;

        const roles = await guildDataStore.getGuildRoles({
            user: identity.User,
            guild: { uid, id },
        });

        router.body = {
            success: true,
            roles: roles.map(role => role.get({ plain: true })),
        };
    }

    async handleGuildCreateRequest(router, ctx, next) {
        const { identity, guild, template, _locale } = router.request.body;

        const result = await this.create_base_guild({
            identity,
            guild,
            _locale,
        });

        router.body = {
            success: true,
            guild: result.get({ plain: true }),
        };
    }

    async handleGuildUpdateRequest(router, ctx, next) {
        const { identity, guild } = router.request.body;

        if (!guild) return;

        guild.Owner = identity.User;
        guild.OwnerId = Number(identity.UserId) || 0;
        guild.owner_id = Number(identity.UserId) || 0;
        guild.Tags = safe2array(guild.Tags);

        const result = await guildDataStore.update({
            user: identity.User,
            guild,
        });

        router.body = {
            success: true,
            guild: result.get({ plain: true }),
        };
    }

    async handleGuildDeleteRequest(router, ctx, next) {
        const { identity, guild } = router.request.body;

        if (!guild) return;

        guild.Owner = identity.User;
        guild.OwnerId = Number(identity.UserId) || 0;
        guild.owner_id = Number(identity.UserId) || 0;
        guild.Tags = safe2array(guild.Tags);

        const result = await guildDataStore.delete({
            user: identity.User,
            guild,
        });

        router.body = {
            success: true,
        };
    }

    async handleCreateSectionRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const result = await guildDataStore.create_section({
            user: identity.User,
            guild: channel.Guild,
            channel,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleUpdateSectionRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const result = await guildDataStore.update_section({
            user: identity.User,
            guild: channel.Guild,
            channel,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleUpdateIndexRequest(router, ctx, next) {
        const { guild, identity } = router.request.body;

        const result = await guildDataStore.index_update({
            guild,
            current_user: identity.User,
        });

        router.body = {
            success: true,
            guild: result.get({ plain: true }),
        };
    }

    async handleUpdateIndexesRequest(router, ctx, next) {
        const { guilds, identity } = router.request.body;

        const _guilds = safe2array(guilds);

        const results = await Promise.all(
            _guilds.map(guild =>
                guildDataStore.index_update({
                    guild,
                    current_user: identity.User,
                })
            )
        );

        router.body = {
            success: true,
            guilds: results.map(result => result.get({ plain: true })),
        };
    }

    async handleSectionIndexUpdateRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const result = await guildDataStore.section_index_update({
            user: identity.User,
            channel,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleSectionIndexesUpdateRequest(router, ctx, next) {
        const { identity, channels } = router.request.body;

        const _channels = safe2array(channels);

        console.log(_channels);

        const results = await Promise.all(
            _channels.map(channel =>
                guildDataStore.section_index_update({
                    user: identity.User,
                    channel,
                })
            )
        );

        router.body = {
            success: true,
            channels: results.map(result => result.get({ plain: true })),
        };
    }

    async handleDeleteSectionRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const result = await guildDataStore.delete_section({
            user: identity.User,
            channel,
        });

        router.body = {
            success: true,
        };
    }

    async handleJoinRequest(router, ctx, next) {
        const { identity, guild } = router.request.body;

        const result = await guildDataStore.join({
            guild,
            user: identity.User,
        });

        await this.afterJoinEntryCreate({ guild, member: result, identity });
        await roleDataStore.set_default_roles({ member: result });

        result.index = await memberDataStore.setLastIndex(result);

        router.body = {
            success: true,
            member: result.get({ plain: true }),
        };
    }

    async handleLeaveRequest(router, ctx, next) {
        const { identity, guild } = router.request.body;

        const result = await guildDataStore.leave({
            guild,
            user: identity.User,
        });

        router.body = {
            success: true,
        };
    }

    async handleKickRequest(router, ctx, next) {
        const { identity, guild, target } = router.request.body;

        const result = await guildDataStore.leave({ guild, user: target });

        router.body = {
            success: true,
        };
    }

    async handleGetCurrentMemberRequestsRequest(router, ctx, next) {
        const { identity, guild, limit } = router.request.body;

        const results = await guildDataStore.getCurrentMemberRequests({
            user: identity.User,
            guild,
            limit,
        });

        router.body = {
            success: true,
            requests: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetIncrementMemberRequestsRequest(router, ctx, next) {
        const { identity, guild, limit, last_id } = router.request.body;

        const results = await guildDataStore.getIncrementMemberRequests({
            user: identity.User,
            guild,
            limit,
            last_id,
        });

        router.body = {
            success: true,
            requests: results.map(result => result.get({ plain: true })),
        };
    }

    async handleGetDecrementMemberRequestsRequest(router, ctx, next) {
        const { identity, guild, limit, first_id } = router.request.body;

        const results = await guildDataStore.getDecrementMemberRequests({
            user: identity.User,
            guild,
            limit,
            first_id,
        });

        router.body = {
            success: true,
            requests: results.map(result => result.get({ plain: true })),
        };
    }

    async handleMakeMemberRequestRequest(router, ctx, next) {
        const { identity, guild } = router.request.body;

        const result = await guildDataStore.makeMemberRequest({
            user: identity.User,
            guild,
        });

        router.body = {
            success: true,
            request: result.get({ plain: true }),
        };
    }

    async handleAcceptMemberRequestRequest(router, ctx, next) {
        const { identity, guild, target } = router.request.body;

        const result = await guildDataStore.acceptFriendRequest({
            user: target,
            guild,
        });

        await this.afterJoinEntryCreate({ guild, member: result, identity });
        await roleDataStore.set_default_roles({ member: result });

        router.body = {
            success: true,
            member: result.get({ plain: true }),
        };
    }

    async handleDenyMemberRequestRequest(router, ctx, next) {
        const { identity, guild, target } = router.request.body;

        const result = await guildDataStore.denyFriendRequest({
            user: target,
            guild,
        });

        router.body = {
            success: true,
        };
    }

    async handleBlockRequest(router, ctx, next) {
        const { identity, target, guild } = router.request.body;

        const result = await guildDataStore.block({
            target,
            user: identity.User,
            guild,
        });
        await guildDataStore.leave({ guild, user: target });

        router.body = {
            success: true,
        };
    }

    async handleUnBlockRequest(router, ctx, next) {
        const { identity, target, guild } = router.request.body;

        const result = await guildDataStore.unblock({
            target,
            user: identity.User,
            guild,
        });

        router.body = {
            success: true,
        };
    }

    async handleReportRequest(router, ctx, next) {
        const { identity, description, guild } = router.request.body;

        const result = await guildDataStore.report({
            user: identity.User,
            guild,
            description,
        });

        router.body = {
            success: true,
        };
    }

    async handleGetBlocksRequest(router, ctx, next) {
        const { identity, limit, offset } = router.request.body;

        const blocks = await models.User.findAll({
            where: {
                permission: true,
            },
            include: [
                {
                    model: models.GuildBlock,
                    where: {
                        user_id: Number(identity.UserId) || 0,
                    },
                },
            ],
        });

        router.body = {
            success: true,
            users: blocks.map(val => val.get({ plain: true })),
        };
    }

    async afterJoinEntryCreate({ guild, member, identity }) {
        const [text_channels, stream_channels] = await Promise.all([
            models.TextChannel.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    is_private: false,
                    permission: true,
                },
            }),
            models.StreamChannel.findAll({
                where: {
                    guild_id: Number(guild.id) || 0,
                    is_private: false,
                    is_dm: false,
                    permission: true,
                },
            }),
        ]);

        Promise.all(
            text_channels.map(channel =>
                textChannelDataStore.join({ channel, member })
            )
        );
        Promise.all(
            stream_channels.map(channel =>
                streamChannelDataStore.join({ channel, member })
            )
        );

        const section = await models.SectionChannel.findOne({
            where: {
                guild_id: Number(guild.id) || 0,
                GuildId: Number(guild.id) || 0,
                is_dm: true,
                permission: true,
            },
        });

        const dm_channel1 = await dmChannelDataStore.create({
            section: section,
            user: member.User,
            channel: models.DMChannel.build({
                guild_id: Number(guild.id) || 0,
                GuildId: Number(guild.id) || 0,
                name: member.nickname,
                is_private: true,
            }),
            members: [member],
        });

        const stream_channel1 = await streamChannelDataStore.create({
            section: section,
            user: member.User,
            channel: models.StreamChannel.build({
                guild_id: Number(guild.id) || 0,
                GuildId: Number(guild.id) || 0,
                name: member.nickname,
                is_dm: true,
                is_private: true,
            }),
            members: [member],
        });

        await dm_channel1.update({ is_im: true });
        await stream_channel1.update({ is_im: true });
    }

    async create_base_guild({ identity, guild, name, _locale }) {
        if (!guild) {
            guild = { name };
        }
        guild.Owner = identity.User;
        guild.OwnerId = Number(identity.UserId) || 0;
        guild.owner_id = Number(identity.UserId) || 0;
        guild.Tags = safe2array(guild.Tags);

        const result = await guildDataStore.create({
            user: identity.User,
            guild,
        });

        const member = await models.Member.create({
            guild_id: Number(result.id) || 0,
            GuildId: Number(result.id) || 0,
            UserId: Number(identity.UserId) || 0,
            user_id: Number(identity.UserId) || 0,
            nickname: identity.User.nickname,
            description: identity.User.description,
            picture_small: identity.User.picture_small,
            picture_large: identity.User.picture_large,
            uid: String.prototype.getUniqueString(),
            permission: true,
        });

        member.index = await memberDataStore.setLastIndex(member);

        const section1 = await guildDataStore.create_section({
            user: identity.User,
            channel: models.SectionChannel.build({
                guild_id: Number(result.id) || 0,
                GuildId: Number(result.id) || 0,
                name: tt('guild.home', { locale: _locale }),
            }),
            guild: result,
        });
        const text_channel1 = await textChannelDataStore.create({
            section: section1,
            user: identity.User,
            channel: models.TextChannel.build({
                guild_id: Number(result.id) || 0,
                GuildId: Number(result.id) || 0,
                name: tt('guild.lounge', { locale: _locale }),
            }),
        });
        const stream_channel1 = await streamChannelDataStore.create({
            section: section1,
            user: identity.User,
            channel: models.StreamChannel.build({
                guild_id: Number(result.id) || 0,
                GuildId: Number(result.id) || 0,
                name: tt('guild.watch_room', { locale: _locale }),
            }),
        });
        const section2 = await models.SectionChannel.create({
            guild_id: Number(result.id) || 0,
            GuildId: Number(result.id) || 0,
            uid: models.SectionChannel.generate_uid(),
            name: tt('guild.dm_section', { locale: _locale }),
            index: 999999,
            is_dm: true,
            permission: true,
        });
        const dm_channel1 = await dmChannelDataStore.create({
            section: section2,
            user: identity.User,
            channel: models.DMChannel.build({
                guild_id: Number(result.id) || 0,
                GuildId: Number(result.id) || 0,
                name: identity.User.nickname,
                is_private: true,
            }),
            members: [member],
        });
        const stream_channel2 = await streamChannelDataStore.create({
            section: section2,
            user: identity.User,
            channel: models.StreamChannel.build({
                guild_id: Number(result.id) || 0,
                GuildId: Number(result.id) || 0,
                name: identity.User.nickname,
                is_dm: true,
                is_private: true,
            }),
            members: [member],
        });
        const role1 = await roleDataStore.create({
            role: models.Role.build({
                guild_id: Number(result.id) || 0,
                GuildId: Number(result.id) || 0,
                name: tt('guild.everyone', { locale: _locale }),
                color: '676C7B',
                priority: 0,
                mentionable: true,
                admin: false,
                guild_managable: false,
                roles_managable: false,
                channels_managable: false,
                channels_invitable: true,
                kickable: true,
                banable: true,
                member_acceptable: false,
                member_managable: true,
                members_managable: false,
                channels_viewable: true,
                message_sendable: true,
                messages_managable: false,
                message_embeddable: true,
                message_attachable: true,
                messages_readable: true,
                message_mentionable: true,
                message_reactionable: true,
                stream_connectable: true,
                stream_speekable: true,
                stream_livestreamable: true,
                movie_selectable: true,
                stream_mutable: false,
                stream_deafenable: false,
                stream_movable: true,
                folders_viewable: true,
                folder_creatable: true,
                folders_managable: false,
                files_viewable: true,
                file_creatable: true,
                files_managable: false,
                files_downloadable: false,
                permission: true,
            }),
            _locale,
        });
        await dm_channel1.update({ is_im: true });
        await stream_channel2.update({ is_im: true });
        await role1.update({ is_default: true, is_everyone: true });
        const created = await models.MemberRole.create({
            member_id: Number(member.dataValues.id) || 0,
            MemberId: Number(member.dataValues.id) || 0,
            role_id: Number(role1.dataValues.id) || 0,
            RoleId: Number(role1.dataValues.id) || 0,
            position: tt('g.owner', { locale: _locale }),
            permission: true,
        });
        return result;
    }

    async temporaryJoin({ guild, identity }) {
        const result = await guildDataStore.join({
            guild,
            user: identity.User,
            temporary: true,
        });
        result.index = await memberDataStore.setLastIndex(result);
        await this.afterJoinEntryCreate({ guild, member: result, identity });
    }
}
