import HandlerImpl from '@handlers/HandlerImpl';
import models from '@models';
import Cookies from 'js-cookie';
import jwt from 'jsonwebtoken';
import validator from 'validator';
import badDomains from '@constants/bad-domains';
import { SessionDataStore } from '@datastore';
import GuildHandler from '@handlers/GuildHandler';
import safe2json from '@extension/safe2json';
import safe2array from '@extension/safe2array';
import querystring from 'querystring';
import uuidv4 from 'uuid/v4';
import countryCode from '@constants/countryCode';
import { ApiError } from '@extension/Error';
import tt from 'counterpart';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const sessionDataStore = SessionDataStore.instance;
const guildHandler = GuildHandler.instance;

export default class SessionHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new SessionHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleRegisterRequest(router, ctx, next) {
        const {
            nickname,
            email,
            password,
            locale,
            country_code,
            timezone,
            enable_mail_notification,
            devise,
        } = router.request.body;

        const i_c = await models.Identity.findOne({
            where: {
                email,
            },
        });

        const { identity, user } = await sessionDataStore.register({
            nickname,
            email,
            password,
            locale,
            country_code,
            timezone,
            enable_mail_notification,
        });

        identity.setValue('User', user);

        const accessToken = await sessionDataStore.setAccessToken({
            identity,
            devise,
            is_one_time: false,
        });

        const guild = await guildHandler.create_base_guild({
            identity,
            name: tt('guild.default_name', { locale, data: user.nickname }),
            _locale: locale,
        });
        const updated_guild = await guild.update({
            is_public: false,
            is_private: true,
        });

        router.body = {
            identity: identity.get({ plain: true }),
            user: user.get({ plain: true }),
            accessToken,
            success: true,
        };
    }

    async handleResendConfirmEmail(router, ctx, next) {
        const { email, _locale } = router.request.body;

        const identity = await sessionDataStore.resendConfirmEmail({
            email,
            locale: _locale,
        });

        router.body = {
            success: true,
        };
    }

    async handleConfirmEmail(router, ctx, next) {
        const { email, code, devise } = router.request.body;

        const identity = await sessionDataStore.confirmEmail({
            email,
            code,
        });

        const accessToken = await sessionDataStore.setAccessToken({
            identity,
            devise: devise,
            is_one_time: false,
        });

        router.body = {
            identity: identity.get({ plain: true }),
            accessToken,
            success: true,
        };
    }

    async handleCheckAccessTokenRequest(router, ctx, next) {
        const { accessToken, devise } = router.request.body;

        const identity = await sessionDataStore
            .checkAccessToken({
                access_token: accessToken,
                devise,
            })
            .catch(e => {
                throw new ApiError({
                    error: e,
                    tt_key: 'errors.invalid_response_from_server',
                });
            });

        if (!identity) {
            router.body = {
                success: true,
            };
            return;
        }

        router.body = {
            success: true,
            identity: identity.get({ plain: true }),
            user: identity.User.get({ plain: true }),
        };
    }

    async handleGenerateAccessTokenRequest(router, ctx, next) {
        const { accessToken, devise, is_one_time } = router.request.body;

        if (!accessToken) {
            router.body = {
                success: true,
            };
            return;
        }

        const identity = await sessionDataStore
            .checkAccessToken({
                access_token: accessToken,
                devise,
                deleting: false,
            })
            .catch(e => {
                throw new ApiError({
                    error: e,
                    tt_key: 'errors.invalid_response_from_server',
                });
            });

        if (!identity) {
            router.body = {
                success: true,
            };
            return;
        }

        const newAccessToken = await sessionDataStore.setAccessToken({
            identity,
            devise,
            is_one_time: false,
        });

        router.body = {
            success: true,
            accessToken: newAccessToken,
        };
    }

    async handleStopMailNotificationRequest(router, ctx, next) {
        const { token } = router.query;

        const decoded = await sessionDataStore.verifyToken(
            token,
            'mail_notification'
        );

        const identity = await models.Identity.findOne({
            where: {
                email: decoded.email,
            },
        });

        if (!identity)
            throw new ApiError({
                error: e,
                tt_key: 'errors.not_exists',
                tt_params: { content: 'User' },
            });
        if (!identity.permission)
            throw new ApiError({
                error: e,
                tt_key: 'errors.is_not_permitted',
                tt_params: { data: identity.username },
            });

        const val = await identity.update({
            enable_mail_notification: false,
        });

        router.redirect(`/?success_key=g.mail_stop`);
    }
}
