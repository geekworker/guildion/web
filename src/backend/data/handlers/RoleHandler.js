import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { RoleDataStore } from '@datastore';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const roleDataStore = RoleDataStore.instance;

export default class RoleHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new RoleHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetRequest(router, ctx, next) {
        let { identity, uid } = router.request.body;

        const result = await models.Role.findOne({
            where: { uid },
            include: [...models.Role.show_include(models)],
        });

        router.body = {
            success: true,
            role: result.get({ plain: true }),
        };
    }

    async handleGetMemberRolesRequest(router, ctx, next) {
        let { identity, member } = router.request.body;

        const results = await models.MemberRole.findAll({
            where: { member_id: Number(member.id) || 0 },
            include: [
                {
                    model: models.Member,
                },
                {
                    model: models.Role,
                },
            ],
        });

        router.body = {
            success: true,
            member_roles: results.map(result => result.get({ plain: true })),
        };
    }

    async handleCreateRequest(router, ctx, next) {
        let { identity, role, _locale } = router.request.body;

        const guild = await models.Guild.findOne({
            where: {
                id: Number(role.GuildId),
            },
        });

        const result = await roleDataStore.create({
            role,
            _locale: guild.locale,
            guild,
        });

        router.body = {
            success: true,
            role: result.get({ plain: true }),
        };
    }

    async handleUpdateRequest(router, ctx, next) {
        let { identity, role } = router.request.body;

        const result = await roleDataStore.update({ role });

        router.body = {
            success: true,
            role: result.get({ plain: true }),
        };
    }

    async handleDeleteRequest(router, ctx, next) {
        let { identity, role } = router.request.body;

        const result = await roleDataStore.delete({ role });

        router.body = {
            success: true,
        };
    }

    async handleMemberRoleCreateRequest(router, ctx, next) {
        let { identity, member_role } = router.request.body;

        const result = await roleDataStore.create_member_role({ member_role });

        router.body = {
            success: true,
            member_role: result.get({ plain: true }),
        };
    }

    async handleMemberRoleUpdateRequest(router, ctx, next) {
        let { identity, member_role } = router.request.body;

        const result = await roleDataStore.update_member_role({ member_role });

        router.body = {
            success: true,
            member_role: result.get({ plain: true }),
        };
    }

    async handleMemberRoleDeleteRequest(router, ctx, next) {
        let { identity, member_role } = router.request.body;

        const result = await roleDataStore.delete_member_role({ member_role });

        router.body = {
            success: true,
        };
    }
}
