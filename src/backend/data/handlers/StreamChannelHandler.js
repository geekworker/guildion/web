import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import {
    StreamChannelDataStore,
    StreamDataStore,
    FileDataStore,
    FolderDataStore,
    MessageDataStore,
    MemberDataStore,
} from '@datastore';
import safe2array from '@extension/safe2array';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const streamChannelDataStore = StreamChannelDataStore.instance;
const streamDataStore = StreamDataStore.instance;
const fileDataStore = FileDataStore.instance;
const folderDataStore = FolderDataStore.instance;
const messageDataStore = MessageDataStore.instance;
const memberDataStore = MemberDataStore.instance;

export default class StreamChannelHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new StreamChannelHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetStreamChannelRequest(router, ctx, next) {
        const { uid, identity } = router.request.body;

        const channel = await streamChannelDataStore.getChannel({
            channel: { uid },
            current_user: identity.User,
        });

        router.body = {
            success: true,
            channel: channel.get({ plain: true }),
        };
    }

    async handleFindOrCreateRequest(router, ctx, next) {
        var {
            identity,
            guild,
            target,
            stream,
            movie_only,
            voice_only,
        } = router.request.body;

        const result = await streamChannelDataStore.findOrCreateStreamChannel({
            guild,
            user: identity.User,
            target,
            movie_only,
            voice_only,
        });

        if (!!stream) {
            stream.channel_id = result.id;
            stream.ChannelId = result.id;

            const current_member = await models.Member.findOne({
                where: {
                    user_id: identity.UserId,
                    UserId: identity.UserId,
                    guild_id: result.GuildId,
                    GuildId: result.GuildId,
                },
            });

            if (
                stream.streamable_type == 'File' &&
                stream.Streamable &&
                (stream.streamable_id == null || stream.streamable_id == 0)
            ) {
                const file = await fileDataStore.create({
                    file: stream.Streamable,
                    current_user: identity.User,
                });
                stream.streamable_id = file.id;
                stream.Streamable = file;
            } else if (
                stream.Files &&
                stream.Files.length > 0 &&
                (stream.streamable_id == null || stream.streamable_id == 0)
            ) {
                stream.Files = safe2array(stream.Files);
                stream.Files = await Promise.all(
                    stream.Files.map(file =>
                        fileDataStore.create({
                            file,
                            current_user: identity.User,
                        })
                    )
                );

                const references = await folderDataStore.create_temporary({
                    current_member,
                    files: stream.Files,
                });

                stream.streamable_id = references[0].id;
                stream.streamable_type == 'FileReference';
            }

            let before_streams = await models.Stream.findAll({
                where: {
                    channel_id: Number(stream.ChannelId) || 0,
                    is_live: true,
                },
                order: [['id', 'DESC']],
                limit: 1,
            });

            let live;
            if (before_streams.length == 0) {
                live = await streamDataStore.startLive({
                    stream,
                });
            } else {
                live = await streamDataStore.switchLive({
                    stream,
                    before_stream: before_streams[0],
                });
            }

            const participant = await streamDataStore.join({
                member: current_member,
                stream: live,
            });

            if (live.streamable_id && live.streamable_type) {
                await messageDataStore.create_channelog({
                    message: {
                        MessageableId: result.id,
                        messageable_type: 'StreamChannel',
                    },
                    channelog: {
                        ChannelogableId: Number(live.id) || 0,
                        channelogable_type: 'Stream',
                        action: 'start_stream',
                    },
                    current_user: identity.User,
                });
            }
        }

        const entries = await models.StreamChannelEntry.findAll({
            where: {
                channel_id: Number(result.id) || 0,
            },
        });

        await Promise.all(entries.map(val => val.update({ permission: true })));

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleGetRolesRequest(router, ctx, next) {
        const { identity, uid } = router.request.body;

        const result = await models.StreamChannel.findOne({
            where: {
                uid,
                permission: true,
            },
        });

        const roles = await models.StreamChannelRole.findAll({
            where: {
                channel_id: result.id,
                permission: true,
            },
            include: [
                {
                    model: models.Role,
                },
                {
                    model: models.StreamChannel,
                },
            ],
        });

        router.body = {
            success: true,
            roles: roles.map(role => role.get({ plain: true })),
        };
    }

    async handleGetParticipantsRequest(router, ctx, next) {
        const { identity, uid } = router.request.body;

        const result = await models.StreamChannel.findOne({
            where: {
                uid,
                permission: true,
            },
        });

        const stream = await models.Stream.findOne({
            where: {
                channel_id: result.id,
                is_live: true,
                permission: true,
            },
        });

        const participants = await models.Participant.findAll({
            where: {
                stream_id: stream.id,
                is_live: true,
                permission: true,
            },
            include: [
                {
                    model: models.Member,
                    include: [
                        {
                            model: models.User,
                        },
                    ],
                },
            ],
        });

        router.body = {
            success: true,
            participants: participants.map(participant =>
                participant.get({ plain: true })
            ),
        };
    }

    async handleGetStreamChannelLiveStreamRequest(router, ctx, next) {
        const { uid, identity } = router.request.body;

        const channel = await models.StreamChannel.findOne({
            where: {
                uid: uid,
                permission: true,
            },
        });

        if (!channel) {
            router.body = {
                success: true,
            };
            return;
        }

        const stream = await models.Stream.findAll({
            where: {
                is_live: true,
                channel_id: channel.id,
            },
            order: [['id', 'DESC']],
            limit: 1,
        });

        if (!stream.length == 0) {
            router.body = {
                success: true,
            };
            return;
        }

        router.body = {
            success: true,
            stream: stream[0].get({ plain: true }),
        };
    }

    async handleCreateRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        channel.Members = safe2array(channel.Members);

        const result = await streamChannelDataStore.create({
            user: identity.User,
            section: channel.Section,
            members: channel.Members,
            channel,
        });

        await memberDataStore.updateStreamChannelMembersPermission({
            current_user: identity.User,
            channel: result,
            members: channel.Members,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleUpdateRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        channel.Members = safe2array(channel.Members);

        const result = await streamChannelDataStore.update({
            user: identity.User,
            section: channel.Section,
            members: channel.Members,
            channel,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleSwitchLoopRequest(router, ctx, next) {
        const { identity, channel, is_loop } = router.request.body;

        const result = await streamChannelDataStore.switchLoop({
            channel,
            is_loop,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleSectionIndexUpdateRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const result = await streamChannelDataStore.section_index_update({
            user: identity.User,
            channel,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleSectionIndexesUpdateRequest(router, ctx, next) {
        const { identity, channels } = router.request.body;

        const _channels = safe2array(channels);

        const results = await Promise.all(
            _channels.map(channel =>
                streamChannelDataStore.section_index_update({
                    user: identity.User,
                    channel,
                })
            )
        );

        router.body = {
            success: true,
            channels: results.map(result => result.get({ plain: true })),
        };
    }

    async handleDeleteRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const result = await streamChannelDataStore.delete({
            user: identity.User,
            channel,
        });

        router.body = {
            success: true,
        };
    }

    async handleUpdateEntryRequest(router, ctx, next) {
        const { identity, entry } = router.request.body;

        const result = await streamChannelDataStore.updateEntry({ entry });

        router.body = {
            success: true,
            entry: result.get({ plain: true }),
        };
    }

    async handleSwitchPermissionRequest(router, ctx, next) {
        const { identity, channel, permission } = router.request.body;

        const result = await streamChannelDataStore.switchPermission({
            user: identity.User,
            channel,
            permission,
        });

        router.body = {
            success: true,
            channel: result.get({ plain: true }),
        };
    }

    async handleJoinRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const member = await models.Member.findOne({
            where: {
                user_id: identity.UserId,
                guild_id: Number(channel.GuildId) || 0,
            },
        });

        const result = await streamChannelDataStore.join({ channel, member });

        await memberDataStore.updateStreamChannelMembersPermission({
            current_user: identity.User,
            channel,
            members: [member],
        });

        router.body = {
            success: true,
            entry: result.get({ plain: true }),
        };
    }

    async handleLeaveRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const member = await models.Member.findOne({
            where: {
                user_id: identity.UserId,
                guild_id: Number(channel.GuildId) || 0,
            },
        });

        const result = await streamChannelDataStore.leave({ channel, member });

        router.body = {
            success: true,
        };
    }

    async handleAddRoleRequest(router, ctx, next) {
        const { identity, channel, role } = router.request.body;

        const result = await streamChannelDataStore.add_role({ channel, role });

        const members = await memberDataStore.getAllStreamChannelMembers({
            current_user: identity.User,
            channel,
            role_ignore: true,
        });

        await memberDataStore.updateStreamChannelMembersPermission({
            current_user: identity.User,
            channel,
            members,
        });

        router.body = {
            success: true,
            channel_role: result.get({ plain: true }),
        };
    }

    async handleUpdateRoleRequest(router, ctx, next) {
        const { identity, channel_role } = router.request.body;

        const result = await streamChannelDataStore.edit_role({
            channelRole: channel_role,
        });

        const channel = await models.StreamChannel.findOne({
            where: {
                id: Number(channel_role.ChannelId) || 0,
            },
        });

        const members = await memberDataStore.getAllStreamChannelMembers({
            current_user: identity.User,
            channel,
            role_ignore: true,
        });

        await memberDataStore.updateStreamChannelMembersPermission({
            current_user: identity.User,
            channel,
            members,
        });

        router.body = {
            success: true,
            channel_role: result.get({ plain: true }),
        };
    }

    async handleRemoveRoleRequest(router, ctx, next) {
        const { identity, channel, role } = router.request.body;

        const result = await streamChannelDataStore.remove_role({
            channel,
            role,
        });

        const members = await memberDataStore.getAllStreamChannelMembers({
            current_user: identity.User,
            channel,
            role_ignore: true,
        });

        await memberDataStore.updateStreamChannelMembersPermission({
            current_user: identity.User,
            channel,
            members,
        });

        router.body = {
            success: true,
        };
    }

    async handleInvitesRequest(router, ctx, next) {
        const { identity, channel, members } = router.request.body;

        const _members = safe2array(members);

        const results = await Promise.all(
            _members.map(member =>
                streamChannelDataStore.join({ channel, member })
            )
        );

        await memberDataStore.updateStreamChannelMembersPermission({
            current_user: identity.User,
            channel,
            members: _members,
        });

        router.body = {
            success: true,
            entries: results.map(entry => entry.get({ plain: true })),
        };
    }

    async handleKickRequest(router, ctx, next) {
        const { identity, channel, member } = router.request.body;

        const result = await streamChannelDataStore.leave({ channel, member });

        router.body = {
            success: true,
        };
    }

    async handleKicksRequest(router, ctx, next) {
        const { identity, channel, members } = router.request.body;

        const results = await Promise.all(
            members.map(member =>
                streamChannelDataStore.leave({ channel, member })
            )
        );

        router.body = {
            success: true,
        };
    }

    async handleReadsRequest(router, ctx, next) {
        const { identity, channel } = router.request.body;

        const member = await models.Member.findOne({
            where: {
                guild_id: Number(channel.GuildId) || 0,
                user_id: identity.UserId,
            },
        });

        const reads = await streamChannelDataStore.reads({ channel, member });

        router.body = {
            success: true,
            reads: reads.map(read => read.get({ plain: true })),
        };
    }
}
