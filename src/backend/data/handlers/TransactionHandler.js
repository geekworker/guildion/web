import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import data_config from '@constants/data_config';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import safe2json from '@extension/safe2json';
import safe2array from '@extension/safe2array';
import { TransactionDataStore } from '@datastore';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const transactionDataStore = TransactionDataStore.instance;

export default class TransactionHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new TransactionHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }
}
