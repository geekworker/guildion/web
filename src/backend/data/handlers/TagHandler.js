import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import safe2json from '@extension/safe2json';
import { getLocale, fallbackLocale } from '@locales';
import { TagDataStore } from '@datastore';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const tagDataStore = TagDataStore.instance;

export default class TagHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new TagHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetTagsRequest(router, ctx, next) {
        const { _locale, limit, offset } = router.request.body;

        const locale = getLocale(_locale);

        const tags = await models.Tag.findAll({
            where: {
                locale,
                permission: true,
                guild_count: {
                    $gte: data_config.Tag.min_guild_count_limit,
                },
            },
            order: [['guild_count', 'DESC']],
            offset: Number(offset || 0),
            limit: Number(limit || data_config.fetch_data_limit('S')),
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        router.body = {
            success: true,
            tags: tags.map(tag => tag.get({ plain: true })),
        };
    }
}
