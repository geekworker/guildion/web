import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import data_config from '@constants/data_config';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import safe2json from '@extension/safe2json';
import safe2array from '@extension/safe2array';
import Promise from 'bluebird';
import {
    FileDataStore,
    FolderDataStore,
    MemberDataStore,
    GuildDataStore,
    UserDataStore,
} from '@datastore';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const fileDataStore = FileDataStore.instance;
const folderDataStore = FolderDataStore.instance;
const memberDataStore = MemberDataStore.instance;
const guildDataStore = GuildDataStore.instance;
const userDataStore = UserDataStore.instance;

export default class FileHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new FileHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetRequest(router, ctx, next) {
        const { id, uid } = router.request.body;

        const file = await models.File.findOne({
            where: {
                $or: [
                    {
                        id: Number(id) || 0,
                    },
                    {
                        uid: uid,
                    },
                ],
                permission: true,
            },
            include: [...models.File.show_include(models)],
        });

        router.body = {
            success: true,
            file: file.get({ plain: true }),
        };
    }

    async handleGetCurrentUserFilesRequest(router, ctx, next) {
        const { limit, identity } = router.request.body;

        const files = await fileDataStore.getCurrentUserFiles({
            limit,
            current_user: identity.User,
        });

        router.body = {
            success: true,
            files: files.map(file => file.get({ plain: true })),
        };
    }

    async handleGetIncrementUserFilesRequest(router, ctx, next) {
        const { limit, identity, last_id } = router.request.body;

        const files = await fileDataStore.getIncrementUserFiles({
            limit,
            current_user: identity.User,
            last_id,
        });

        router.body = {
            success: true,
            files: files.map(file => file.get({ plain: true })),
        };
    }

    async handleGetDecrementUserFilesRequest(router, ctx, next) {
        const { limit, identity, first_id } = router.request.body;

        const files = await fileDataStore.getDecrementUserFiles({
            limit,
            current_user: identity.User,
            first_id,
        });

        router.body = {
            success: true,
            files: files.map(file => file.get({ plain: true })),
        };
    }

    async handleConvertedFileRequest(router, ctx, next) {
        const { context } = router.request.body;

        router.body = {
            success: true,
        };
    }

    async handleCreateFilesRequest(router, ctx, next) {
        const { files, identity } = router.request.body;

        let _files = safe2array(files);

        if (!_files || _files.length == 0) {
            router.body = {
                success: true,
            };
            return;
        }

        const results = await Promise.all(
            _files.map(file =>
                fileDataStore.create({
                    file,
                    current_user: identity.User,
                })
            )
        );

        Promise.all(
            results.map(async file => {
                const member = await memberDataStore.updateCount({
                    id: file.MemberId,
                });
                await guildDataStore.updateCount({ id: member.GuildId });
                await userDataStore.updateCount(identity.User);
            })
        );

        router.body = {
            success: true,
            files: results.map(file => file.get({ plain: true })),
        };
    }

    async handleCreateFileRequest(router, ctx, next) {
        const { file, identity } = router.request.body;

        const result = await fileDataStore.create({
            file,
            current_user: identity.User,
        });

        memberDataStore.updateCount({ id: result.MemberId }).then(member => {
            guildDataStore.updateCount({ id: member.GuildId });
            userDataStore.updateCount(identity.User);
        });

        router.body = {
            success: true,
            file: result.get({ plain: true }),
        };
    }

    async handleUpdateFileRequest(router, ctx, next) {
        const { file, identity } = router.request.body;

        const result = await fileDataStore.update({
            file,
            current_user: identity.User,
        });

        memberDataStore.updateCount({ id: result.MemberId }).then(member => {
            guildDataStore.updateCount({ id: member.GuildId });
            userDataStore.updateCount(identity.User);
        });

        router.body = {
            success: true,
            file: result.get({ plain: true }),
        };
    }

    async handleDeleteFileRequest(router, ctx, next) {
        const { file, identity } = router.request.body;

        const result = await fileDataStore.delete({
            file,
            current_user: identity.User,
        });

        memberDataStore
            .updateCount({ id: Number(file.MemberId) || 0 })
            .then(member => {
                guildDataStore.updateCount({ id: member.GuildId });
                userDataStore.updateCount(identity.User);
            });

        router.body = {
            success: true,
            // file: result.get({ plain: true }),
        };
    }

    async handleDeleteFilesRequest(router, ctx, next) {
        const { files, identity } = router.request.body;

        let _files = safe2array(files);

        if (!_files || _files.length == 0) {
            router.body = {
                success: true,
            };
            return;
        }

        const results = await Promise.all(
            _files.map(file =>
                fileDataStore.delete({
                    file,
                    current_user: identity.User,
                })
            )
        );

        Promise.all(
            _files.map(async file => {
                const member = await memberDataStore.updateCount({
                    id: Number(file.MemberId),
                });
                await guildDataStore.updateCount({ id: member.GuildId });
                await userDataStore.updateCount(identity.User);
            })
        );

        router.body = {
            success: true,
        };
    }

    async handleCreateFileReferenceRequest(router, ctx, next) {
        const { reference, identity } = router.request.body;

        const result = await fileDataStore.file_reference_create({
            file_reference: reference,
            current_user: identity.User,
        });

        folderDataStore.updateCount({ id: Number(reference.FolderId) || 0 });

        router.body = {
            success: true,
            reference: result.get({ plain: true }),
        };
    }

    async handleCreateFileReferencesRequest(router, ctx, next) {
        const { references, identity } = router.request.body;

        let _references = safe2array(references);

        if (!_references || _references.length == 0) {
            router.body = {
                success: true,
            };
            return;
        }

        const results = await Promise.map(
            _references,
            reference =>
                fileDataStore.file_reference_create({
                    file_reference: reference,
                    current_user: identity.User,
                }),
            { concurrency: 1 }
        );

        const folder_id = _references[0].FolderId;
        await folderDataStore.updateCount({ id: Number(folder_id) || 0 });

        router.body = {
            success: true,
            references: results.map(reference =>
                reference.get({ plain: true })
            ),
        };
    }

    async handleUpdateFileReferenceRequest(router, ctx, next) {
        const { reference, identity } = router.request.body;

        const result = await fileDataStore.file_reference_update({
            file_reference: reference,
            current_user: identity.User,
        });

        router.body = {
            success: true,
            reference: result.get({ plain: true }),
        };
    }

    async handleDeleteFileReferenceRequest(router, ctx, next) {
        const { reference, identity } = router.request.body;

        const result = await fileDataStore.file_reference_delete({
            file_reference: reference,
            current_user: identity.User,
        });

        folderDataStore.updateCount({ id: Number(reference.FolderId) || 0 });

        router.body = {
            success: true,
            // reference: result.get({ plain: true }),
        };
    }

    async handleDeleteFileReferencesRequest(router, ctx, next) {
        const { references, identity } = router.request.body;

        let _references = safe2array(references);

        if (!_references || _references.length == 0) {
            router.body = {
                success: true,
            };
            return;
        }

        const results = await Promise.all(
            _references.map(reference =>
                fileDataStore.file_reference_delete({
                    file_reference: reference,
                    current_user: identity.User,
                })
            )
        );

        const folder_id = _references[0].FolderId;
        await folderDataStore.updateCount({ id: Number(folder_id) || 0 });

        router.body = {
            success: true,
            // reference: result.get({ plain: true }),
        };
    }

    async handleUpdateFileReferenceIndexRequest(router, ctx, next) {
        const { reference, identity } = router.request.body;

        const result = await fileDataStore.index_update({
            file_reference: reference,
            current_user: identity.User,
        });

        folderDataStore.updateCount({ id: Number(reference.FolderId) || 0 });

        router.body = {
            success: true,
            reference: result.get({ plain: true }),
        };
    }

    async handleUpdateFileReferenceIndexesRequest(router, ctx, next) {
        const { references, identity } = router.request.body;

        const _references = safe2array(references);

        const results = await Promise.all(
            _references.map(reference =>
                fileDataStore.index_update({
                    file_reference: reference,
                    current_user: identity.User,
                })
            )
        );

        router.body = {
            success: true,
            references: results.map(result => result.get({ plain: true })),
        };
    }
}
