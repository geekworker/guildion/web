import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import data_config from '@constants/data_config';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import safe2json from '@extension/safe2json';
import safe2array from '@extension/safe2array';
import { FolderDataStore, FileDataStore } from '@datastore';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const folderDataStore = FolderDataStore.instance;
const fileDataStore = FileDataStore.instance;

export default class FolderHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new FolderHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleCreateFolderRequest(router, ctx, next) {
        const { folder, identity } = router.request.body;

        folder.FileReferences = safe2array(folder.FileReferences);

        const result = await folderDataStore.create({
            folder,
            current_user: identity.User,
        });

        if (result.FileReferences) {
            const results = await Promise.all(
                result.FileReferences.map(reference =>
                    fileDataStore.file_reference_create({
                        file_reference: reference,
                        current_user: identity.User,
                    })
                )
            );
            result.setValue('FileReferences', results);
        }

        router.body = {
            success: true,
            folder: result.get({ plain: true }),
        };
    }

    async handleUpdateFolderRequest(router, ctx, next) {
        const { folder, identity } = router.request.body;

        const result = await folderDataStore.update({
            folder,
            current_user: identity.User,
        });

        router.body = {
            success: true,
            folder: result.get({ plain: true }),
        };
    }

    async handleDeleteFolderRequest(router, ctx, next) {
        const { folder, identity } = router.request.body;

        const result = await folderDataStore.delete({
            folder,
            current_user: identity.User,
        });

        router.body = {
            success: true,
            // folder: result.get({ plain: true }),
        };
    }

    async handleGetFolderRequest(router, ctx, next) {
        const { id, uid, identity } = router.request.body;

        const folder = await models.Folder.findOne({
            where: {
                $or: [
                    {
                        id: Number(id) || 0,
                    },
                    {
                        uid: uid,
                    },
                ],
                permission: true,
            },
            include: [
                // ...models.Folder.show_include(models),
            ],
        });

        // folder.setValue(
        //     'FileReferences',
        //     await models.FileReference.findAll({
        //         where: {
        //             folder_id: folder.id,
        //             permission: true,
        //         },
        //         include: [
        //             {
        //                 model: models.File,
        //                 include: [
        //                     {
        //                         model: models.Member,
        //                         include: [
        //                             {
        //                                 model: models.User,
        //                             }
        //                         ],
        //                     }
        //                 ],
        //             },
        //         ],
        //     })
        // );

        router.body = {
            success: true,
            folder: folder.get({ plain: true }),
        };
    }

    async handleGetGuildFoldersRequest(router, ctx, next) {
        const { guild_id, identity, is_playlist } = router.request.body;

        const folders = await models.Folder.findAll({
            where: {
                guild_id: Number(guild_id) || 0,
                is_playlist: Number.prototype.castBool(is_playlist),
                temporary: false,
                permission: true,
            },
            order: [['index', 'ASC']],
            include: [...models.Folder.index_include(models)],
        });

        const _folders = await Promise.all(
            folders.map(async folder => {
                const references = await models.FileReference.findAll({
                    where: {
                        folder_id: folder.id,
                        permission: true,
                    },
                    include: [
                        {
                            model: models.File,
                        },
                    ],
                    order: [['index', 'DESC']],
                    limit: 5,
                });
                folder.setValue('FileReferences', references.reverse());
                return folder;
            })
        );

        router.body = {
            success: true,
            folders: _folders.map(folder => folder.get({ plain: true })),
        };
    }

    async handleGetCurrentFolderFilesRequest(router, ctx, next) {
        const { folder_id, limit, identity } = router.request.body;

        const references = await folderDataStore.getCurrentFolderFiles({
            folder_id,
            current_user: identity.User,
            limit,
        });

        router.body = {
            success: true,
            references: references.map(val => val.get({ plain: true })),
        };
    }

    async handleGetIncrementFolderFilesRequest(router, ctx, next) {
        const { folder_id, identity, last_id, limit } = router.request.body;

        const references = await folderDataStore.getIncrementFolderFiles({
            folder_id,
            current_user: identity.User,
            last_id,
            limit,
        });

        router.body = {
            success: true,
            references: references.map(val => val.get({ plain: true })),
        };
    }

    async handleGetDecrementFolderFilesRequest(router, ctx, next) {
        const { folder_id, identity, first_id, limit } = router.request.body;

        const references = await folderDataStore.getDecrementFolderFiles({
            folder_id,
            current_user: identity.User,
            first_id,
            limit,
        });

        router.body = {
            success: true,
            references: references.map(val => val.get({ plain: true })),
        };
    }

    async handleGetCurrentGuildFilesRequest(router, ctx, next) {
        const { guild_id, limit, identity, is_playlist } = router.request.body;

        const files = await folderDataStore.getCurrentGuildFiles({
            guild_id,
            current_user: identity.User,
            limit,
            is_playlist,
        });

        router.body = {
            success: true,
            files: files.map(val => val.get({ plain: true })),
        };
    }

    async handleGetIncrementGuildFilesRequest(router, ctx, next) {
        const {
            guild_id,
            identity,
            last_id,
            limit,
            is_playlist,
        } = router.request.body;

        const files = await folderDataStore.getIncrementGuildFiles({
            guild_id,
            current_user: identity.User,
            last_id,
            limit,
            is_playlist,
        });

        router.body = {
            success: true,
            files: files.map(file => file.get({ plain: true })),
        };
    }

    async handleGetDecrementGuildFilesRequest(router, ctx, next) {
        const {
            guild_id,
            identity,
            first_id,
            limit,
            is_playlist,
        } = router.request.body;

        const files = await folderDataStore.getDecrementGuildFiles({
            guild_id,
            current_user: identity.User,
            first_id,
            limit,
            is_playlist,
        });

        router.body = {
            success: true,
            files: files.map(file => file.get({ plain: true })),
        };
    }

    async handleGetCurrentGuildMyFilesRequest(router, ctx, next) {
        const { guild_id, limit, identity } = router.request.body;

        const files = await folderDataStore.getCurrentGuildMyFiles({
            guild_id,
            current_user: identity.User,
            limit,
        });

        router.body = {
            success: true,
            files: files.map(file => file.get({ plain: true })),
        };
    }

    async handleGetIncrementGuildMyFilesRequest(router, ctx, next) {
        const { guild_id, identity, last_id, limit } = router.request.body;

        const files = await folderDataStore.getIncrementGuildMyFiles({
            guild_id,
            current_user: identity.User,
            last_id,
            limit,
        });

        router.body = {
            success: true,
            files: files.map(file => file.get({ plain: true })),
        };
    }

    async handleGetDecrementGuildMyFilesRequest(router, ctx, next) {
        const { guild_id, identity, first_id, limit } = router.request.body;

        const files = await folderDataStore.getDecrementGuildMyFiles({
            guild_id,
            current_user: identity.User,
            first_id,
            limit,
        });

        router.body = {
            success: true,
            files: files.map(file => file.get({ plain: true })),
        };
    }

    async handleUpdateFolderIndexRequest(router, ctx, next) {
        const { folder, identity } = router.request.body;

        const result = await folderDataStore.index_update({
            folder,
            current_user: identity.User,
        });

        router.body = {
            success: true,
            folder: result.get({ plain: true }),
        };
    }

    async handleUpdateFolderIndexesRequest(router, ctx, next) {
        const { folders, identity } = router.request.body;

        const _folders = safe2array(folders);

        const results = await Promise.all(
            _folders.map(folder =>
                folderDataStore.index_update({
                    folder,
                    current_user: identity.User,
                })
            )
        );

        router.body = {
            success: true,
            folders: results.map(result => result.get({ plain: true })),
        };
    }
}
