import HandlerImpl from '@handlers/HandlerImpl';
import { Set, Map, fromJS, List } from 'immutable';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import models from '@models';
import { getLocale, fallbackLocale } from '@locales';
import { ApiError } from '@extension/Error';
import {
    MessageDataStore,
    FileDataStore,
    FolderDataStore,
    GuildDataStore,
    MemberDataStore,
    UserDataStore,
    NotificationDataStore,
    TextChannelDataStore,
    StreamChannelDataStore,
    DMChannelDataStore,
} from '@datastore';
import safe2array from '@extension/safe2array';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const messageDataStore = MessageDataStore.instance;
const fileDataStore = FileDataStore.instance;
const folderDataStore = FolderDataStore.instance;
const guildDataStore = GuildDataStore.instance;
const memberDataStore = MemberDataStore.instance;
const userDataStore = UserDataStore.instance;
const notificationDataStore = NotificationDataStore.instance;
const textChannelDataStore = TextChannelDataStore.instance;
const streamChannelDataStore = StreamChannelDataStore.instance;
const dmChannelDataStore = DMChannelDataStore.instance;

export default class MessageHandler extends HandlerImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new MessageHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async handleGetMessageRequest(router, ctx, next) {
        const { id, uid, identity } = router.request.body;

        const message = await models.Message.findOne({
            where: {
                $or: [
                    {
                        id: Number(id) || 0,
                    },
                    {
                        uid: uid,
                    },
                ],
                permission: true,
            },
            include: [...models.Message.index_include(models)],
        });

        router.body = {
            success: true,
            message: message.get({ plain: true }),
        };
    }

    async handleGetCurrentMessagesRequest(router, ctx, next) {
        const {
            messageable_type,
            messageable_id,
            limit,
            identity,
            is_static,
        } = router.request.body;

        const _messages = await messageDataStore.getCurrentMessages({
            messageable_type,
            messageable_id,
            current_user: identity.User,
            limit,
            is_static,
        });
        const messages = await messageDataStore.filterValidMessages({
            user: identity.User,
            targets: _messages,
        });

        router.body = {
            success: true,
            messages: messages.map(message => message.get({ plain: true })),
        };
    }

    async handleGetIncrementMessagesRequest(router, ctx, next) {
        const {
            messageable_type,
            messageable_id,
            identity,
            last_id,
            limit,
            is_static,
        } = router.request.body;

        const _messages = await messageDataStore.getIncrementMessages({
            messageable_type,
            messageable_id,
            current_user: identity.User,
            last_id,
            limit,
            is_static,
        });
        const messages = await messageDataStore.filterValidMessages({
            user: identity.User,
            targets: _messages,
        });

        router.body = {
            success: true,
            messages: messages.map(message => message.get({ plain: true })),
        };
    }

    async handleGetDecrementMessagesRequest(router, ctx, next) {
        const {
            messageable_type,
            messageable_id,
            identity,
            first_id,
            limit,
            is_static,
        } = router.request.body;

        const _messages = await messageDataStore.getDecrementMessages({
            messageable_type,
            messageable_id,
            current_user: identity.User,
            first_id,
            limit,
            is_static,
        });
        const messages = await messageDataStore.filterValidMessages({
            user: identity.User,
            targets: _messages,
        });

        router.body = {
            success: true,
            messages: messages.map(message => message.get({ plain: true })),
        };
    }

    async handleCreateMessageRequest(router, ctx, next) {
        const { message, identity } = router.request.body;

        message.Files = safe2array(message.Files);
        const result = await messageDataStore.create({
            message,
            current_user: identity.User,
        });

        message.id = result.id;

        const attachments = await fileDataStore.create_files_from_message({
            message,
            current_user: identity.User,
        });

        if (!!attachments) {
            result.setValue(
                'Files',
                attachments.map(attachment => attachment.File)
            );
        }

        memberDataStore.updateCount({ id: result.SenderId }).then(member => {
            userDataStore.updateCount(identity.User);
            guildDataStore.updateCount({ id: member.GuildId });
        });

        textChannelDataStore.updateCountFromMessage(result);
        streamChannelDataStore.updateCountFromMessage(result);
        dmChannelDataStore.updateCountFromMessage(result);

        notificationDataStore.onCreateMessage({
            current_user: identity.User,
            message: result,
        });

        const updated = await models.Message.findOne({
            where: {
                id: result.id,
            },
            include: [...models.Message.index_include(models)],
        });

        router.body = {
            success: true,
            message: updated.get({ plain: true }),
        };
    }

    async handleUpdateMessageRequest(router, ctx, next) {
        const { message, identity } = router.request.body;

        message.Files = safe2array(message.Files);
        const result = await messageDataStore.update({
            message,
            current_user: identity.User,
        });

        memberDataStore.updateCount({ id: result.SenderId }).then(member => {
            userDataStore.updateCount(identity.User);
            guildDataStore.updateCount({ id: member.GuildId });
        });

        router.body = {
            success: true,
            message: result.get({ plain: true }),
        };
    }

    async handleUpdateMessageStaticRequest(router, ctx, next) {
        const { message, identity, is_static } = router.request.body;

        const result = await messageDataStore.updateStatic({
            message,
            current_user: identity.User,
            is_static,
        });

        router.body = {
            success: true,
            message: result.get({ plain: true }),
        };
    }

    async handleDeleteMessageRequest(router, ctx, next) {
        const { message, identity } = router.request.body;

        message.Files = safe2array(message.Files);
        await Promise.all(
            message.Files.map(file =>
                fileDataStore.delete({ file, current_user: identity.User })
            )
        );
        const result = await messageDataStore.delete({
            message,
            current_user: identity.User,
        });

        memberDataStore.updateCount({ id: message.SenderId }).then(member => {
            userDataStore.updateCount(identity.User);
            guildDataStore.updateCount({ id: member.GuildId });
        });

        router.body = {
            success: true,
        };
    }

    async handleReadsRequest(router, ctx, next) {
        const { identity, messages } = router.request.body;
        delete messages[0]['Reads'];
        delete messages[0]['Files'];
        delete messages[0]['Sender'];
        const _messages = safe2array(messages);

        const sender = await models.Member.findOne({
            where: { id: Number(_messages[0].SenderId) || 0 },
        });

        const member = await models.Member.findOne({
            where: {
                guild_id: sender.GuildId,
                user_id: identity.UserId,
            },
        });

        const reads = await Promise.all(
            _messages.map(message => messageDataStore.read({ message, member }))
        );

        router.body = {
            success: true,
            // reads: reads.map(read => read.get({ plain: true })),
        };
    }

    async handleReadRequest(router, ctx, next) {
        const { identity, message } = router.request.body;

        const sender = await models.Member.findOne({
            where: { id: Number(message.SenderId) || 0 },
        });

        const member = await models.Member.findOne({
            where: {
                guild_id: sender.GuildId,
                user_id: identity.UserId,
            },
        });

        const read = await messageDataStore.read({ message, member });

        router.body = {
            success: true,
            // read: read.get({ plain: true }),
        };
    }

    async handleUnReadsRequest(router, ctx, next) {
        const { identity, messages } = router.request.body;

        delete messages[0]['Reads'];
        delete messages[0]['Files'];
        delete messages[0]['Sender'];
        const _messages = safe2array(messages);

        const sender = await models.Member.findOne({
            where: { id: Number(_messages[0].SenderId) || 0 },
        });

        const member = await models.Member.findOne({
            where: {
                guild_id: sender.GuildId,
                user_id: identity.UserId,
            },
        });

        const reads = await Promise.all(
            _messages.map(message =>
                messageDataStore.unread({ message, member })
            )
        );

        router.body = {
            success: true,
            // reads: reads.map(read => read.get({ plain: true })),
        };
    }

    async handleUnReadRequest(router, ctx, next) {
        const { identity, message } = router.request.body;

        const sender = await models.Member.findOne({
            where: { id: Number(message.SenderId) || 0 },
        });

        const member = await models.Member.findOne({
            where: {
                guild_id: sender.GuildId,
                user_id: identity.UserId,
            },
        });

        const read = await messageDataStore.unread({ message, member });

        router.body = {
            success: true,
            // read: read.get({ plain: true }),
        };
    }
}
