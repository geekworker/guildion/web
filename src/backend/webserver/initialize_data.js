import CategoryDataStore from '@datastore/CategoryDataStore';
import Gateway from '@network/gateway';
import Translator from '@infrastructure/Translator'

CategoryDataStore.instance.init_categories();
Gateway.instance.init_api();

module.exports = {};
