import path from 'path';
import Koa from 'koa';
import mount from 'koa-mount';
import helmet from 'koa-helmet';
import koa_logger from 'koa-logger';
import requestTime from '@utils/requesttimings';
import StatsLoggerClient from '@utils/StatsLoggerClient';
import hardwareStats from '@utils/hardwarestats';
import cluster from 'cluster';
import os from 'os';
import prod_logger from '@utils/prod_logger';
import favicon from 'koa-favicon';
import staticCache from 'koa-static-cache';
import RootMiddlewares from '@middlewares';
import AuthMiddleware from '@middlewares/auth';
import isBot from 'koa-isbot';
import csrf from 'koa-csrf';
import minimist from 'minimist';
import koaLocale from 'koa-locale';
import { getSupportedLocales } from '@network/misc';
import useRedirects from '@network/redirect';
import session from 'koa-session';
import send from 'koa-send';
import sitemap from '@network/sitemap';
import config from '@constants/config';
import helmet_config from '@constants/helmet_config';

if (cluster.isMaster) console.log('application server starting, please wait.');

// import uploadImage from 'server/upload-image' //medium-editor

const app = new Koa();
app.name = config.APP_NAME;
const NODE_ENV = process.env.NODE_ENV || 'development';
// cache of a thousand days
const cacheOpts = { maxAge: 86400000, gzip: true, buffer: true };

// Serve static assets without fanfare
app.use(favicon(path.join(__dirname, '../../assets/images/brands/logo.png')));

app.use(
    mount(
        '/favicons',
        staticCache(
            path.join(__dirname, '../../assets/images/favicons'),
            cacheOpts
        )
    )
);
app.use(
    mount(
        '/images',
        staticCache(path.join(__dirname, '../../assets/images'), cacheOpts)
    )
);
app.use(
    mount(
        '/icons',
        staticCache(path.join(__dirname, '../../assets/icons'), cacheOpts)
    )
);
app.use(
    mount(
        '/notifications',
        staticCache(
            path.join(__dirname, '../../assets/notifications'),
            cacheOpts
        )
    )
);

app.use(
    mount('/.well-known/apple-app-site-association', function*(next) {
        yield send(
            this,
            path.join(
                __dirname,
                '../../assets/well-known/apple-app-site-association.json'
            )
        );
    })
);

app.use(
    mount('/apple-app-site-association', function*(next) {
        yield send(
            this,
            path.join(__dirname, '../../assets/apple-app-site-association.json')
        );
    })
);

app.use(
    mount('/manifest.json', function*(next) {
        yield send(
            this,
            path.join(__dirname, '../../assets/static/manifest.json')
        );
    })
);

app.use(
    mount('/OneSignalSDKWorker.js', function*(next) {
        yield send(
            this,
            path.join(__dirname, '../../assets/static/OneSignalSDKWorker.js')
        );
    })
);

app.use(
    mount('/OneSignalSDKUpdaterWorker.js', function*(next) {
        yield send(
            this,
            path.join(
                __dirname,
                '../../assets/static/OneSignalSDKUpdaterWorker.js'
            )
        );
    })
);
app.use(
    mount('/robots.txt', function*(next) {
        yield send(
            this,
            path.join(__dirname, '../../assets/static/robots.txt')
        );
    })
);

app.use(
    mount('/sitemap.xml', function*(next) {
        let xml = yield sitemap();
        this.type = 'text/xml';
        this.body = xml;
    })
);

// Proxy asset folder to webpack development server in development mode
if (NODE_ENV === 'development') {
    const webpack_dev_port = process.env.PORT
        ? parseInt(process.env.PORT) + 1
        : 8081;
    const proxyhost = 'http://0.0.0.0:' + webpack_dev_port;
    console.log('proxying to webpack dev server at ' + proxyhost);
    const proxy = require('koa-proxy')({
        host: proxyhost,
        map: filePath => 'assets/' + filePath,
    });
    app.use(mount('/assets', proxy));
} else {
    app.use(
        mount(
            '/assets',
            staticCache(path.join(__dirname, '../../../dist'), cacheOpts)
        )
    );
}

app.use(isBot());

// set number of processes equal to number of cores
// (unless passed in as an env var)
const numProcesses = process.env.NUM_PROCESSES || os.cpus().length;

const statsLoggerClient = new StatsLoggerClient(process.env.STATSD_IP);

app.use(requestTime(statsLoggerClient));

csrf(app);

koaLocale(app);

function convertEntriesToArrays(obj) {
    return Object.keys(obj).reduce((result, key) => {
        result[key] = obj[key].split(/\s+/);
        return result;
    }, {});
}

// some redirects and health status
app.use(function*(next) {
    if (this.method === 'GET' && this.url === '/.well-known/healthcheck.json') {
        this.status = 200;
        this.body = {
            status: 'ok',
            docker_tag: process.env.DOCKER_TAG ? process.env.DOCKER_TAG : false,
            source_commit: process.env.SOURCE_COMMIT
                ? process.env.SOURCE_COMMIT
                : false,
        };
        return;
    }

    yield next;
});

// load production middleware
if (NODE_ENV === 'production') {
    app.use(require('koa-conditional-get')());
    app.use(require('koa-etag')());
    app.use(require('koa-compressor')());
}

// Logging
if (NODE_ENV === 'production') {
    app.use(prod_logger());
} else {
    app.use(koa_logger());
}

app.use(
    helmet({
        hsts: false,
    })
);

app.use(
    mount(
        '/static',
        staticCache(path.join(__dirname, '../../assets/static'), cacheOpts)
    )
);

app.use(
    mount('/robots.txt', function*() {
        this.set('Cache-Control', 'public, max-age=86400000');
        this.type = 'text/plain';
        this.body = 'User-agent: *\nAllow: /';
    })
);

app.use(session({}, app));

RootMiddlewares(app);
AuthMiddleware(app);
useRedirects(app);

require('@webserver/initialize_data');

// helmet wants some things as bools and some as lists, makes env.difficult.
// our env.uses strings, this splits them to lists on whitespace.

//FIXME: this will be related the error of type error 'r' is not function
if (NODE_ENV === 'production') {
    const helmetConfig = {
        directives: convertEntriesToArrays(helmet_config.directives),
        reportOnly: helmet_config.reportOnly,
        setAllHeaders: helmet_config.setAllHeaders,
    };
    helmetConfig.directives.reportUri = helmetConfig.directives.reportUri[0];
    if (helmetConfig.directives.reportUri === '-') {
        delete helmetConfig.directives.reportUri;
    }
    app.use(helmet.contentSecurityPolicy(helmetConfig));
}

if (NODE_ENV !== 'test') {
    app.use(function*() {
        const appRender = require('./app_render');

        let resolvedAssets = false;
        let supportedLocales = false;

        if (process.env.NODE_ENV === 'production') {
            resolvedAssets = require(path.join(
                __dirname,
                '../../..',
                '/tmp/webpack-stats-prod.json'
            ));
            supportedLocales = getSupportedLocales();
        }

        // Load the pinned posts and store them on the ctx for later use. Since
        // we're inside a generator, we can't `await` here, so we pass a promise
        // so `src/server/app_render.jsx` can `await` on it.
        yield appRender(this, supportedLocales, resolvedAssets);
        // if (app_router.dbStatus.ok) recordWebEvent(this, 'page_load');
    });

    const argv = minimist(process.argv.slice(2));

    const port = process.env.PORT ? parseInt(process.env.PORT) : 8080;

    if (NODE_ENV === 'production') {
        if (cluster.isMaster) {
            for (var i = 0; i < numProcesses; i++) {
                cluster.fork();
            }
            // if a worker dies replace it so application keeps running
            cluster.on('exit', function(worker) {
                console.log(
                    'error: worker %d died, starting a new one',
                    worker.id
                );
                cluster.fork();
            });
        } else {
            app.listen(port);
            if (process.send) process.send('online');
            console.log(`Worker process started for port ${port}`);
        }
    } else {
        // spawn a single thread if not running in production mode
        app.listen(port);
        if (process.send) process.send('online');
        console.log(`Application started on port ${port}`);
    }
}

// set PERFORMANCE_TRACING to the number of seconds desired for
// logging hardware stats to the console
if (process.env.PERFORMANCE_TRACING)
    setInterval(hardwareStats, 1000 * process.env.PERFORMANCE_TRACING);

module.exports = app;
