import { ClientError, ApiError } from '@extension/Error';
import { ValidationEntity, ValidationEntities } from '@entity/ValidationEntity';
import models from '@models';
import badDomains from '@constants/bad-domains';
import data_config from '@constants/data_config';
import validator from 'validator';
import { ACTION_TYPE } from '@entity';
import {
    apiUnwrapData,
    apiCheckEmailFormat,
    apiCheckEmailIsBadDomain,
    apiNicknameNotContainMentionFormat,
    apiNicknameNotContainDotFormat,
    apiNicknameNotContainSlashFormat,
} from '@validations';

export const apiCheckUserIsValid = new ValidationEntity({
    validate: async arg => {
        const { user } = arg;
        if (!user.id /*|| !user.username*/) return false;
        const base = await models.User.findOne({
            where: {
                id: Number(user.id) || 0,
                //username: user.username,
            },
        });
        return (
            !!base && base.id == user.id /* && base.username == user.username*/
        );
    },
    error: arg => {
        throw new ApiError({
            error: new Error('user is not correct format'),
            tt_key: 'errors.is_not_correct',
            tt_params: { data: 'g.user' },
        });
    },
});

export const apiCheckNicknameLength = new ValidationEntity({
    validate: async arg => {
        const { nickname } = arg;
        if (!nickname) return false;
        return (
            `${nickname}`.length > data_config.User.nickname_min_limit &&
            `${nickname}`.length < data_config.User.nickname_max_limit
        );
    },
    error: arg => {
        throw new ApiError({
            error: new Error('nickname is not correct format'),
            tt_key: 'errors.is_text_min_max_limit',
            tt_params: {
                data: 'g.nickname',
                min: data_config.User.nickname_min_limit,
                max: data_config.User.nickname_max_limit,
            },
        });
    },
});

function validURL(str) {
    var pattern = new RegExp(
        '^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$',
        'i'
    ); // fragment locator
    return !!pattern.test(str);
}

export const apiSyncUserValidates = new ValidationEntities({
    items: [apiUnwrapData('user'), apiCheckUserIsValid],
});

export const apiUserCreateValidates = new ValidationEntities({
    items: [
        apiUnwrapData('nickname'),
        apiCheckNicknameLength,
        apiNicknameNotContainMentionFormat,
        apiNicknameNotContainDotFormat,
        apiNicknameNotContainSlashFormat,
    ],
});

export const apiUserUpdateValidates = new ValidationEntities({
    items: [
        apiUnwrapData('nickname'),
        apiCheckNicknameLength,
        apiNicknameNotContainMentionFormat,
        apiNicknameNotContainDotFormat,
        apiNicknameNotContainSlashFormat,
    ],
});
