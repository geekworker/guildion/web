import { ClientError, ApiError } from '@extension/Error';
import { ValidationEntity, ValidationEntities } from '@entity/ValidationEntity';
import models from '@models';
import badDomains from '@constants/bad-domains';
import data_config from '@constants/data_config';
import validator from 'validator';

export const apiUnwrapData = key =>
    new ValidationEntity({
        validate: async arg => !!arg[key],
        error: arg => {
            throw new ApiError({
                error: new Error(`${key} is required`),
                tt_key: 'errors.is_required',
                tt_params: { data: `g.${key}` },
            });
        },
    });

export const apiNicknameNotContainMentionFormat = new ValidationEntity({
    validate: async arg => {
        const { nickname } = arg;
        if (!nickname) return false;
        return !nickname.includes('@');
    },
    error: arg => {
        throw new ApiError({
            error: new Error('nickname should_not_contain_keyword @'),
            tt_key: 'errors.should_not_contain_keyword',
            tt_params: { data: 'g.nickname', keyword: '@' },
        });
    },
});

export const apiNicknameNotContainDotFormat = new ValidationEntity({
    validate: async arg => {
        const { nickname } = arg;
        if (!nickname) return false;
        return !nickname.includes('.');
    },
    error: arg => {
        throw new ApiError({
            error: new Error('nickname should_not_contain_keyword .'),
            tt_key: 'errors.should_not_contain_keyword',
            tt_params: { data: 'g.nickname', keyword: '.' },
        });
    },
});

export const apiNicknameNotContainSlashFormat = new ValidationEntity({
    validate: async arg => {
        const { nickname } = arg;
        if (!nickname) return false;
        return !nickname.includes('/');
    },
    error: arg => {
        throw new ApiError({
            error: new Error('nickname should_not_contain_keyword /'),
            tt_key: 'errors.should_not_contain_keyword',
            tt_params: { data: 'g.nickname', keyword: '/' },
        });
    },
});

export const apiCheckEmailFormat = new ValidationEntity({
    validate: async arg => {
        const { email } = arg;
        if (!email) return false;
        return validator.isEmail(email);
    },
    error: arg => {
        throw new ApiError({
            error: new Error('Email is not correct format'),
            tt_key: 'errors.is_not_correct',
            tt_params: { data: arg.email },
        });
    },
});

export const apiCheckEmailIsBadDomain = new ValidationEntity({
    validate: async arg => {
        const { email } = arg;
        if (!email) return false;
        return !badDomains.includes(email.split('@')[1]);
    },
    error: arg => {
        throw new ApiError({
            error: new Error('Email is blacklisted'),
            tt_key: 'errors.is_not_correct',
            tt_params: { data: 'Email' },
        });
    },
});

export const apiCheckPhoneNumberFormat = new ValidationEntity({
    validate: async arg => {
        const { phone_number } = arg;
        if (!phone_number) return false;
        return validator.isMobilePhone(phone_number);
    },
    error: arg => {
        throw new ApiError({
            error: new Error('phone number is not correct format'),
            tt_key: 'errors.is_not_correct',
            tt_params: { data: arg.phone_number },
        });
    },
});

export const apiCheckUserIsVerfied = new ValidationEntity({
    validate: async arg => {
        let { user, UserId } = arg;
        user =
            user ||
            (await models.User.findOne({
                where: {
                    id: UserId,
                },
            }));
        if (!user) return false;
        return user.verified;
    },
    error: arg => {
        throw new ApiError({
            error: new Error('require telephone confirm'),
            tt_key: 'g.do_it',
            tt_params: { data: 'g.phone_verify' },
        });
    },
});

export const apiCheckHalfWidthAlphanumeric = key =>
    new ValidationEntity({
        validate: async arg => {
            if (!arg[key]) return false;
            return `${arg[key]}`.match(/^[A-Za-z0-9]*$/);
        },
        error: arg => {
            throw new ApiError({
                error: new Error(`${key} should be HalfWidthAlphanumeric`),
                tt_key: 'errors.only_halfwidthalphanumeric',
                tt_params: { data: `g.${key}` },
            });
        },
    });

export const apiCheckHalfWidthAlphanumericOrSimbol = key =>
    new ValidationEntity({
        validate: async arg => {
            if (!arg[key]) return false;
            return `${arg[key]}`.match(/^[a-zA-Z0-9!-/:-@¥[-`{-~]*$/);
        },
        error: arg => {
            throw new ApiError({
                error: new Error(
                    `${key} should be HalfWidthAlphanumericOrSimbol`
                ),
                tt_key: 'errors.only_halfwidthalphanumeric_or_simbol',
                tt_params: { data: `g.${key}` },
            });
        },
    });

export const apiCheckSpace = key =>
    new ValidationEntity({
        validate: async arg => {
            if (!arg[key]) return false;
            return !`${arg[key]}`.match(/[\s]+/);
        },
        error: arg => {
            throw new ApiError({
                error: new Error(`${key} should not have space characters`),
                tt_key: 'errors.has_space',
                tt_params: { data: `g.${key}` },
            });
        },
    });

export const apiCheckInteger = key =>
    new ValidationEntity({
        validate: async arg => {
            if (!arg[key]) return false;
            return !`${arg[key]}`.match(/[+-]?\d+/);
        },
        error: arg => {
            throw new ApiError({
                error: new Error(`${key} is_not_correct`),
                tt_key: 'errors.is_not_correct',
                tt_params: { data: `g.${key}` },
            });
        },
    });

export const apiEmojiIsValid = key =>
    new ValidationEntity({
        validate: async arg => {
            if (!arg[key]) return false;
            if (arg[key].length == 0) return false;
            const regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
            return `${arg[key]}`.match(regex);
        },
        error: arg => {
            throw new ApiError({
                error: new Error(`${key} is not emoji`),
                tt_key: 'errors.only_emoji',
                tt_params: { data: `g.${key}` },
            });
        },
    });
