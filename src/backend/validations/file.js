import { ClientError, ApiError } from '@extension/Error';
import { ValidationEntity, ValidationEntities } from '@entity/ValidationEntity';
import models from '@models';
import badDomains from '@constants/bad-domains';
import data_config from '@constants/data_config';
import validator from 'validator';
import { ACTION_TYPE } from '@entity';
import {
    apiUnwrapData,
    apiCheckEmailFormat,
    apiCheckEmailIsBadDomain,
    apiCheckPhoneNumberFormat,
    apiCheckHalfWidthAlphanumeric,
    apiCheckHalfWidthAlphanumericOrSimbol,
    apiCheckSpace,
} from '@validations';

export const apiFileCreateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiFileUpdateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiFileDeleteValidates = new ValidationEntities({
    items: [
    ],
});

export const apiAttachmentCreateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiAttachmentUpdateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiAttachmentDeleteValidates = new ValidationEntities({
    items: [
    ],
});

export const apiFileReferenceCreateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiFileReferenceUpdateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiFileReferenceDeleteValidates = new ValidationEntities({
    items: [
    ],
});
