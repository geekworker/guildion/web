import { ClientError, ApiError } from '@extension/Error';
import { ValidationEntity, ValidationEntities } from '@entity/ValidationEntity';
import models from '@models';
import badDomains from '@constants/bad-domains';
import data_config from '@constants/data_config';
import validator from 'validator';
import { ACTION_TYPE } from '@entity';
import {
    apiUnwrapData,
    apiCheckEmailFormat,
    apiCheckEmailIsBadDomain,
    apiCheckPhoneNumberFormat,
    apiCheckHalfWidthAlphanumeric,
    apiCheckHalfWidthAlphanumericOrSimbol,
    apiCheckSpace,
} from '@validations';

export const apiStreamChannelCreateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiStreamChannelUpdateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiStreamChannelDeleteValidates = new ValidationEntities({
    items: [
    ],
});
