import { ClientError, ApiError } from '@extension/Error';
import { ValidationEntity, ValidationEntities } from '@entity/ValidationEntity';
import models from '@models';
import badDomains from '@constants/bad-domains';
import data_config from '@constants/data_config';
import validator from 'validator';
import { ACTION_TYPE } from '@entity';
import {
    apiUnwrapData,
    apiCheckEmailFormat,
    apiCheckEmailIsBadDomain,
    apiCheckPhoneNumberFormat,
    apiCheckHalfWidthAlphanumeric,
    apiCheckHalfWidthAlphanumericOrSimbol,
    apiCheckSpace,
    apiNicknameNotContainMentionFormat,
    apiNicknameNotContainDotFormat,
    apiNicknameNotContainSlashFormat,
} from '@validations';

export const apiCheckIdentityEmailExists = new ValidationEntity({
    validate: async arg => {
        const { email } = arg;
        if (!email) return false;
        const result = await models.Identity.findOne({
            where: { email },
            raw: false,
        });
        return !result;
    },
    error: arg => {
        throw new ApiError({
            error: new Error(`is_not_correct_auth`),
            tt_key: 'errors.is_not_correct_auth',
        });
    },
});

export const apiCheckEmailIsVerified = new ValidationEntity({
    validate: async arg => {
        const { identity } = arg;
        return identity.email_is_verified;
    },
    error: arg => {
        throw new ApiError({
            error: new Error('Email verified required'),
            tt_key: 'errors.is_required',
            tt_params: { data: 'Email verify' },
        });
    },
});

export const apiCheckPassword = new ValidationEntity({
    validate: async arg => {
        const { password } = arg;
        if (!password) return false;
        return (
            `${password}`.length >= data_config.Identity.password_min_limit &&
            `${password}`.length <= data_config.Identity.password_max_limit
        );
    },
    error: arg => {
        throw new ApiError({
            error: new Error('password is not correct format'),
            tt_key: 'errors.is_text_min_max_limit',
            tt_params: {
                data: 'g.password',
                min: data_config.Identity.password_min_limit,
                max: data_config.Identity.password_max_limit,
            },
        });
    },
});

export const apiCheckIdentityUserNameExists = new ValidationEntity({
    validate: async arg => {
        const { username } = arg;
        if (!username) return false;
        const result = await models.Identity.findOne({
            where: { username },
            raw: false,
        });
        return !!result;
    },
    error: arg => {
        throw new ApiError({
            error: new Error(
                `the user of having ${arg.username} is not exists`
            ),
            tt_key: 'errors.not_exists',
            tt_params: { content: arg.username },
        });
    },
});

export const apiCheckEmail = new ValidationEntity({
    validate: async arg => {
        const { email } = arg;
        if (!email) return false;
        return (
            `${email}`.length > data_config.Identity.email_min_limit &&
            `${email}`.length < data_config.Identity.email_max_limit
        );
    },
    error: arg => {
        throw new ApiError({
            error: new Error('email is not correct format'),
            tt_key: 'errors.is_text_min_max_limit',
            tt_params: {
                data: 'g.email',
                min: data_config.Identity.email_min_limit,
                max: data_config.Identity.email_max_limit,
            },
        });
    },
});

export const apiCheckNicknameLength = new ValidationEntity({
    validate: async arg => {
        const { nickname } = arg;
        if (!nickname) return false;
        return (
            `${nickname}`.length > data_config.User.nickname_min_limit &&
            `${nickname}`.length < data_config.User.nickname_max_limit
        );
    },
    error: arg => {
        throw new ApiError({
            error: new Error('nickname is not correct format'),
            tt_key: 'errors.is_text_min_max_limit',
            tt_params: {
                data: 'g.nickname',
                min: data_config.User.nickname_min_limit,
                max: data_config.User.nickname_max_limit,
            },
        });
    },
});

export const apiSessionRegisterValidates = new ValidationEntities({
    items: [
        apiUnwrapData('email'),
        apiUnwrapData('nickname'),
        apiCheckSpace('password'),
        apiCheckEmailFormat,
        apiCheckEmailIsBadDomain,
        apiCheckPassword,
        apiCheckIdentityEmailExists,
        apiCheckNicknameLength,
        apiNicknameNotContainMentionFormat,
        apiNicknameNotContainDotFormat,
        apiNicknameNotContainSlashFormat,
    ],
});

export const apiReSendConfirmEmailValidates = new ValidationEntities({
    items: [
        apiUnwrapData('email'),
        apiCheckEmail,
        apiCheckEmailFormat,
        apiCheckEmailIsBadDomain,
    ],
});

export const apiResetPasswordEmailValidates = new ValidationEntities({
    items: [
        // apiCheckIdentityFromSNS,
        apiUnwrapData('email'),
        apiUnwrapData('username'),
        // apiCheckHalfWidthAlphanumeric('username'),
        apiCheckSpace('username'),
        apiCheckEmail,
        apiCheckEmailFormat,
        apiCheckEmailIsBadDomain,
        apiCheckIdentityUserNameExists,
    ],
});
