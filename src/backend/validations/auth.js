import { ClientError, ApiError } from '@extension/Error';
import { ValidationEntity, ValidationEntities } from '@entity/ValidationEntity';
import models from '@models';
import badDomains from '@constants/bad-domains';
import data_config from '@constants/data_config';
import validator from 'validator';
import { ACTION_TYPE } from '@entity';
import {
    apiUnwrapData,
    apiCheckEmailFormat,
    apiCheckEmailIsBadDomain,
    apiCheckPhoneNumberFormat,
    apiCheckHalfWidthAlphanumeric,
    apiCheckHalfWidthAlphanumericOrSimbol,
    apiCheckSpace,
} from '@validations';

export const apiCheckIdentityEmailExists = new ValidationEntity({
    validate: async arg => {
        const { email } = arg;
        if (!email) return false;
        const result = await models.Identity.findOne({
            where: { email },
            raw: false,
        });
        return !!result;
    },
    error: arg => {
        throw new ApiError({
            error: new Error(`is_not_correct_auth`),
            tt_key: 'errors.is_not_correct_auth',
        });
    },
});

export const apiCheckIdentityFinishSinup = new ValidationEntity({
    validate: async arg => {
        const { email } = arg;
        if (!email) return false;
        const result = await models.Identity.findOne({
            where: { email },
            raw: false,
        });
        return !!result.UserId;
    },
    error: arg => {
        throw new ApiError({
            error: new Error(`is_not_correct_auth`),
            tt_key: 'errors.is_not_correct_auth',
        });
    },
});

export const apiAuthenticateIdentityValidates = new ValidationEntities({
    items: [
        apiCheckIdentityEmailExists,
        apiCheckIdentityFinishSinup,
    ],
});
