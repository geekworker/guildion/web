import { ClientError, ApiError } from '@extension/Error';
import { ValidationEntity, ValidationEntities } from '@entity/ValidationEntity';
import models from '@models';
import badDomains from '@constants/bad-domains';
import data_config from '@constants/data_config';
import validator from 'validator';
import { ACTION_TYPE } from '@entity';
import {
    apiUnwrapData,
    apiCheckEmailFormat,
    apiCheckEmailIsBadDomain,
    apiCheckPhoneNumberFormat,
    apiCheckHalfWidthAlphanumeric,
    apiCheckHalfWidthAlphanumericOrSimbol,
    apiCheckSpace,
} from '@validations';

export const apiCheckMemberExists = new ValidationEntity({
    validate: async arg => {
        let { guild, user } = arg;
        if (!guild || !user || !user.id) return false;
        const result = await models.Guild.findOne({
            where: {
                $or: [
                    {
                        id: Number(guild.id) || 0,
                    },
                    {
                        uid: guild.uid,
                    }
                ],
            },
        });

        if (!result) return false;

        const member = await models.Member.findOne({
            where: {
                guild_id: Number(result.id) || 0,
                user_id: Number(user.id) || 0,
            },
        })

        return (Number.prototype.castBool(result.is_public) && !Number.prototype.castBool(result.is_private)) || !!member;
    },
    error: arg => {
        throw new ApiError({
            error: new Error(`is_not_correct_auth`),
            tt_key: 'errors.is_not_correct_auth',
        });
    },
});

export const apiGuildShowFetchValidates = new ValidationEntities({
    items: [
        apiUnwrapData('user'),
        apiUnwrapData('guild'),
        // apiCheckMemberExists,
    ],
});

export const apiGuildCreateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiGuildUpdateValidates = new ValidationEntities({
    items: [
    ],
});

export const apiGuildDeleteValidates = new ValidationEntities({
    items: [
    ],
});

export const apiGuildJoinValidates = new ValidationEntities({
    items: [
    ],
});

export const apiGuildLeaveValidates = new ValidationEntities({
    items: [
    ],
});
