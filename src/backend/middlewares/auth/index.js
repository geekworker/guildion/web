import { handleApiError } from '@extension/Error';
import Gateway from '@network/gateway';
import koa_router from 'koa-router';
import koa_body from 'koa-body';
import crypto from 'crypto';
import models from '@models';
import { esc, escAttrs } from '@models';
import coBody from 'co-body';

const gateway = Gateway.instance;

export default function AuthMiddleware(app) {
    const router = koa_router({ prefix: '/auth' });
    app.use(router.routes());
    const koaBody = koa_body();
}
