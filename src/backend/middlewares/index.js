import koa_router from 'koa-router';
import koa_body from 'koa-body';
import crypto from 'crypto';
import models from '@models';
import { esc, escAttrs } from '@models';
import coBody from 'co-body';
import { handleApiError } from '@extension/Error';
import Gateway from '@network/gateway';
import log from '@extension/log';
import V1Middleware from '@middlewares/v1';
import _ from 'lodash';

const gateway = Gateway.instance;

export default function RootMiddlewares(app) {
    const router = koa_router({ prefix: '/api' });
    app.use(router.routes());
    const koaBody = koa_body();

    router.use('/', _.cloneDeep(V1Middleware().routes()));
}
