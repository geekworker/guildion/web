import koa_router from 'koa-router';
import koa_body from 'koa-body';
import crypto from 'crypto';
import models from '@models';
import { esc, escAttrs } from '@models';
import coBody from 'co-body';
import { handleApiError } from '@extension/Error';
import Gateway from '@network/gateway';
import log from '@extension/log';
import { StreamHandler } from '@handlers';

const gateway = Gateway.instance;
const streamHandler = StreamHandler.instance;

export default function V1StreamMiddleware() {
    const router = koa_router({
        /*prefix: '/'*/
    });
    const koaBody = koa_body();

    router.post('/stream', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield streamHandler
            .handleGetStreamRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/stream/start', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield streamHandler
            .handleStartLiveRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/stream/switch', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield streamHandler
            .handleSwitchLiveRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/stream/end', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield streamHandler
            .handleEndLiveRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/stream/join', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield streamHandler
            .handleJoinRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/stream/leave', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield streamHandler
            .handleLeaveRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    return router;
}
