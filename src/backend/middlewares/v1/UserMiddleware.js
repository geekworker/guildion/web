import koa_router from 'koa-router';
import koa_body from 'koa-body';
import crypto from 'crypto';
import models from '@models';
import { esc, escAttrs } from '@models';
import coBody from 'co-body';
import { handleApiError } from '@extension/Error';
import Gateway from '@network/gateway';
import log from '@extension/log';
import { UserHandler } from '@handlers';

const gateway = Gateway.instance;
const userHandler = UserHandler.instance;

export default function V1UserMiddleware() {
    const router = koa_router({
        /*prefix: '/'*/
    });
    const koaBody = koa_body();

    router.post('/user/sync', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: false,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleSyncUserRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/devise/create', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: false,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleRegisterUserDeviseRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/devise/update', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleUpdateUserDeviseRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/devise/logout', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleUserDeviseLogoutRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/members', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleGetUserMembersRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/members/current', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleGetCurrentUserMembersRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/members/increment', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleGetIncrementUserMembersRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/members/decrement', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleGetDecrementUserMembersRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/active', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleActiveRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/inactive', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleInActiveRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/update', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleUpdateUserRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/delete', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleDeleteUserRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/block', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleBlockRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/mute', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleMuteRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/unblock', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleUnBlockRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/unmute', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleUnMuteRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/report', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleReportRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/blocks', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleGetBlocksRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/mutes', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleGetMutesRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/user/relation/stats', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield userHandler
            .handleGetUserBlockStatusRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    return router;
}
