import koa_router from 'koa-router';
import koa_body from 'koa-body';
import crypto from 'crypto';
import models from '@models';
import { esc, escAttrs } from '@models';
import coBody from 'co-body';
import { handleApiError } from '@extension/Error';
import Gateway from '@network/gateway';
import log from '@extension/log';
import { MessageHandler } from '@handlers';

const gateway = Gateway.instance;
const messageHandler = MessageHandler.instance;

export default function V1MessageMiddleware() {
    const router = koa_router({
        /*prefix: '/'*/
    });
    const koaBody = koa_body({
        jsonLimit: '10mb',
        formLimit: '10mb',
        textLimit: '10mb',
    });

    router.post('/channel/message', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleGetMessageRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/messages/current', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleGetCurrentMessagesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/messages/increment', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleGetIncrementMessagesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/messages/decrement', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleGetDecrementMessagesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/message/create', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleCreateMessageRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/message/update', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleUpdateMessageRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/message/update/static', koaBody, function*(
        ctx,
        next
    ) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleUpdateMessageStaticRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/message/delete', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleDeleteMessageRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/messages/read', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleReadsRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/message/read', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleReadRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/messages/unread', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleUnReadsRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/channel/message/unread', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield messageHandler
            .handleUnReadRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    return router;
}
