import koa_router from 'koa-router';
import koa_body from 'koa-body';
import crypto from 'crypto';
import models from '@models';
import { esc, escAttrs } from '@models';
import coBody from 'co-body';
import { handleApiError } from '@extension/Error';
import Gateway from '@network/gateway';
import log from '@extension/log';
import { FolderHandler } from '@handlers';

const gateway = Gateway.instance;
const folderHandler = FolderHandler.instance;

export default function V1FolderMiddleware() {
    const router = koa_router({
        /*prefix: '/'*/
    });
    const koaBody = koa_body({
        jsonLimit: '10mb',
        formLimit: '10mb',
        textLimit: '10mb',
    });

    router.post('/folder', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetFolderRequest(results.router, results.ctx, results.next)
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/folder/create', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleCreateFolderRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/folder/update', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleUpdateFolderRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/folder/delete', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleDeleteFolderRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/guild/folders', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetGuildFoldersRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/folder/references/current', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetCurrentFolderFilesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/folder/references/increment', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetIncrementFolderFilesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/folder/references/decrement', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetDecrementFolderFilesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/guild/files/current', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetCurrentGuildFilesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/guild/files/increment', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetIncrementGuildFilesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/guild/files/decrement', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetDecrementGuildFilesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/member/files/current', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetCurrentGuildMyFilesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/member/files/increment', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetIncrementGuildMyFilesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/member/files/decrement', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleGetDecrementGuildMyFilesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/folder/index/update', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleUpdateFolderIndexRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    router.post('/folders/index/update', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: true,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield folderHandler
            .handleUpdateFolderIndexesRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    return router;
}
