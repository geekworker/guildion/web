import koa_router from 'koa-router';
import koa_body from 'koa-body';
import crypto from 'crypto';
import models from '@models';
import { esc, escAttrs } from '@models';
import coBody from 'co-body';
import { handleApiError } from '@extension/Error';
import Gateway from '@network/gateway';
import log from '@extension/log';
import _ from 'lodash';
import V1AuthMiddleware from '@middlewares/v1/AuthMiddleware';
import V1CategoryMiddleware from '@middlewares/v1/CategoryMiddleware';
import V1DocumentMiddleware from '@middlewares/v1/DocumentMiddleware';
import V1StreamChannelMiddleware from '@middlewares/v1/StreamChannelMiddleware';
import V1TextChannelMiddleware from '@middlewares/v1/TextChannelMiddleware';
import V1FileMiddleware from '@middlewares/v1/FileMiddleware';
import V1FolderMiddleware from '@middlewares/v1/FolderMiddleware';
import V1GuildMiddleware from '@middlewares/v1/GuildMiddleware';
import V1MemberMiddleware from '@middlewares/v1/MemberMiddleware';
import V1MessageMiddleware from '@middlewares/v1/MessageMiddleware';
import V1NotificationMiddleware from '@middlewares/v1/NotificationMiddleware';
import V1DMChannelMiddleware from '@middlewares/v1/DMChannelMiddleware';
import V1SearchMiddleware from '@middlewares/v1/SearchMiddleware';
import V1SessionMiddleware from '@middlewares/v1/SessionMiddleware';
import V1StreamMiddleware from '@middlewares/v1/StreamMiddleware';
import V1TagMiddleware from '@middlewares/v1/TagMiddleware';
import V1TransactionMiddleware from '@middlewares/v1/TransactionMiddleware';
import V1UserMiddleware from '@middlewares/v1/UserMiddleware';
import V1ReactionMiddleware from '@middlewares/v1/ReactionMiddleware';
import V1RoleMiddleware from '@middlewares/v1/RoleMiddleware';

const gateway = Gateway.instance;

export default function V1Middleware() {
    const router = koa_router({ prefix: '/v1' });
    const koaBody = koa_body();

    router.use('/', _.cloneDeep(V1AuthMiddleware().routes()));
    router.use('/', _.cloneDeep(V1CategoryMiddleware().routes()));
    router.use('/', _.cloneDeep(V1DocumentMiddleware().routes()));
    router.use('/', _.cloneDeep(V1StreamChannelMiddleware().routes()));
    router.use('/', _.cloneDeep(V1TextChannelMiddleware().routes()));
    router.use('/', _.cloneDeep(V1FileMiddleware().routes()));
    router.use('/', _.cloneDeep(V1FolderMiddleware().routes()));
    router.use('/', _.cloneDeep(V1GuildMiddleware().routes()));
    router.use('/', _.cloneDeep(V1MemberMiddleware().routes()));
    router.use('/', _.cloneDeep(V1MessageMiddleware().routes()));
    router.use('/', _.cloneDeep(V1NotificationMiddleware().routes()));
    router.use('/', _.cloneDeep(V1DMChannelMiddleware().routes()));
    router.use('/', _.cloneDeep(V1SearchMiddleware().routes()));
    router.use('/', _.cloneDeep(V1SessionMiddleware().routes()));
    router.use('/', _.cloneDeep(V1StreamMiddleware().routes()));
    router.use('/', _.cloneDeep(V1TagMiddleware().routes()));
    router.use('/', _.cloneDeep(V1TransactionMiddleware().routes()));
    router.use('/', _.cloneDeep(V1UserMiddleware().routes()));
    router.use('/', _.cloneDeep(V1ReactionMiddleware().routes()));
    router.use('/', _.cloneDeep(V1RoleMiddleware().routes()));

    router.post('/csp_violation', function*() {
        let params;
        try {
            params = yield coBody(this);
        } catch (error) {
            console.log('-- /csp_violation error -->', error);
        }
        if (params && params['csp-report']) {
            const csp_report = params['csp-report'];
            const value = `${csp_report['document-uri']} : ${
                csp_report['blocked-uri']
            }`;
            console.log(
                '-- /csp_violation -->',
                value,
                '--',
                this.req.headers['user-agent']
            );
        } else {
            console.log(
                '-- /csp_violation [no csp-report] -->',
                params,
                '--',
                this.req.headers['user-agent']
            );
        }
        this.body = '';
    });

    return router;
}
