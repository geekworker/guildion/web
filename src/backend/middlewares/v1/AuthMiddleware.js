import koa_router from 'koa-router';
import koa_body from 'koa-body';
import crypto from 'crypto';
import models from '@models';
import { esc, escAttrs } from '@models';
import coBody from 'co-body';
import { handleApiError } from '@extension/Error';
import Gateway from '@network/gateway';
import log from '@extension/log';
import { AuthHandler } from '@handlers';

const gateway = Gateway.instance;
const authHandler = AuthHandler.instance;

export default function V1AuthMiddleware() {
    const router = koa_router({
        /*prefix: '/'*/
    });
    const koaBody = koa_body();

    router.post('/authenticate', koaBody, function*(ctx, next) {
        const results = yield gateway.run(this, ctx, next, {
            accessTokenRequired: false,
        });
        if (!!results.error) {
            yield handleApiError(
                results.router,
                results.ctx,
                results.next,
                results.error
            );
            return;
        }
        yield authHandler
            .handleAuthenticateRequest(
                results.router,
                results.ctx,
                results.next
            )
            .catch(
                async e =>
                    await handleApiError(
                        results.router,
                        results.ctx,
                        results.next,
                        e
                    )
            );
    });

    return router;
}
