function safe2plain(instance) {
    if (!instance || !instance.dataValues) return instance;
    const plain = instance.get({ plain: true });
    Object.keys(plain).map(key => {
        if (plain[key] instanceof Array && plain[key].length > 0 && !!plain[key][0] && !!plain[key][0].dataValues) {
            plain[key] = plain[key].map(val => safe2plain(val));
        } else if (!!plain[key] && !!plain[key].dataValues) {
            plain[key] = safe2plain(plain[key]);
        }
    })
    return plain;
}

module.exports = safe2plain;
