'use strict';

Array.prototype.unique_by_id = function(self) {
    const results = [];
    const map = new Map();
    for (const item of self) {
        if (!map.has(item.id)) {
            map.set(item.id, true);
            results.push(item);
        }
    }
    return results;
};

Array.prototype.unique_by_key = function(self, key) {
    const results = [];
    const map = new Map();
    for (const item of self) {
        if (!map.has(item[key])) {
            map.set(item[key], true);
            results.push(item);
        }
    }
    return results;
};

module.exports = new Array();
