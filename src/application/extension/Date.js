
Date.prototype.getHoursAgo = hour => {
    const now = Date.now();
    return new Date(now + hour * 60 * 60 * 1000);
};

Date.prototype.getHoursAgoFrom = (hour) => {
    return new Date(this + hour * 60 * 60 * 1000);
};

module.exports = new Date();
