Number.prototype.castBool = self => {
    return self == 1 || self == true || self == 'true';
};

Number.prototype.toBool = () => {
    return this == 1 || this == true || this == 'true';
};

Number.prototype.bytesToSize = (bytes) => {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

module.exports = 0;
