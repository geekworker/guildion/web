const safe2json = val => {
    if (!val) return val;
    if (typeof val.toJSON == 'function') return val.toJSON.bind(val);
    if (!!val.dataValues) return val.get({ plain: true });
    return val;
};

module.exports = safe2json;
