const times = x => f => {
    if (x > 0) {
        f();
        times(x - 1)(f);
    }
};

const safe2array = val => {
    if (!val) return [];
    if (Array.isArray(val) && val.length == 1) {
        let isConvertable = [];
        let keies = [];
        let count = 0;
        let hash = val[0];
        for (let key in hash) {
            keies.push(key);
            isConvertable.push(Array.isArray(hash[key]));
            if (Array.isArray(hash[key])) {
                count = hash[key].length
            } else if (Object.keys(hash[key]).length > 0 && Array.isArray(hash[key][Object.keys(hash[key])[0]])) {
                hash[key] = safe2array([hash[key]]);
            }
        }

        if (isConvertable.filter(c => c == true).length == 0) return val;

        let i = 0;
        let newValue = [];

        times(count)(() => {
            let newHash = {};
            for (let key in keies) {
                if (isNaN(key)) break;
                newHash[keies[key]] = hash[keies[key]][i];

                if (Array.isArray(safe2array(hash[keies[key]]))) {
                    newHash[keies[key]] = safe2array(hash[keies[key]])[i]
                }
            }
            newValue.push(newHash);
            i += 1;
        });
        return newValue;
    } else {
        return val;
    }
};

module.exports = safe2array;

/* Sample Data
[ { text: [ '', '' ],
    user_count: [ '0', '0' ],
    id: [ '50', '133' ],
    updated_at: [ '2020-05-17 08:48:44 +0000', '2020-05-17 08:48:44 +0000' ],
    valid: [ '0', '0' ],
    is_private: [ '0', '0' ],
    stream_count: [ '0', '0' ],
    created_at: [ '2020-05-17 08:48:44 +0000', '2020-05-17 08:48:44 +0000' ],
    permission: [ '0', '0' ],
    picture: [ '', '' ],
    view_count: [ '0', '0' ],
    CategoryId: [ '0', '0' ] } ]
*/
