const available_locales = ['ja', 'en'];

const available_locale_labels = ['日本語', 'English'];

const id_prefix = '-hreflang';

const makeId = lang => `${lang}${id_prefix}`;

module.exports = {
    available_locales,
    available_locale_labels,
    id_prefix,
    makeId,
};
