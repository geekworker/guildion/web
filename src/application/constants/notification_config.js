import config from '@constants/config';

export const notification_pickup_cron = '00 00 13 * * *';
export const notification_pickup_opinion_cron = '00 00 20 * * *';
export const summary_limit = 5;
export const random_limit = 10;

export const notification_unsubscribe_url =
    config.CURRENT_APP_URL + '/user/mail/notification';

module.exports = {
    notification_pickup_cron,
    notification_pickup_opinion_cron,
    summary_limit,
    random_limit,
    notification_unsubscribe_url,
};
