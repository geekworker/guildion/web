const SMS_VERIFY_LENGTH = '6';
const EMAIL_CONFIRM_MODE = false;

module.exports = {
    SMS_VERIFY_LENGTH,
    EMAIL_CONFIRM_MODE,
};
