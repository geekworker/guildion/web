module.exports = process.env.NODE_ENV == 'development' ? {
    directives: {
        childSrc: "'self' player.twitch.tv www.youtube.com staticxx.facebook.com w.soundcloud.com player.vimeo.com",
        connectSrc: "'self' file: data: blob: filesystem: api.blocktrades.us guildion.s3.amazonaws.com guildion.s3-ap-northeast-1.amazonaws.com upload.twitter.com/",
        defaultSrc: "'self' www.youtube.com staticxx.facebook.com player.vimeo.com fonts.gstatic.com guildion.s3-ap-northeast-1.amazonaws.com",
        fontSrc: "'self' data: fonts.gstatic.com",
        frameAncestors: "'none'",
        imgSrc: "* data: filesystem: file: blob: ws: wss:",
        objectSrc: "'none'",
        pluginTypes: "application/pdf",
        scriptSrc: "'self' 'unsafe-eval' 'unsafe-inline' www.google-analytics.com connect.facebook.net",
        styleSrc: "'self' 'unsafe-inline' fonts.googleapis.com",
        reportUri: "/api/v1/csp_violation"
    },
    reportOnly: false,
    setAllHeaders: true,
} : {
    directives: {
        childSrc: "'self' googleads.g.doubleclick.net onesignal.com",
        connectSrc: "'self' file: data: blob: filesystem: onesignal.com www.google-analytics.com www.google-analytics.com",
        defaultSrc: "'self' blob: www.youtube.com staticxx.facebook.com player.vimeo.com fonts.gstatic.com d1jpmyfyzkodnr.cloudfront.net",
        fontSrc: "'self' data: fonts.gstatic.com",
        frameAncestors: "'none'",
        imgSrc: "* data: filesystem: file: blob: ws: wss:",
        objectSrc: "'none'",
        pluginTypes: "application/pdf",
        scriptSrc: "'self' 'unsafe-eval' 'unsafe-inline' www.googletagmanager.com www.googletagservices.com pagead2.googlesyndication.com www.googletagservices.com google-analytics.com pagead2.googlesyndication.com onesignal.com onesignal.com cdn.onesignal.com adservice.google.co.jp d1jpmyfyzkodnr.cloudfront.net",
        styleSrc: "'self' 'unsafe-inline' fonts.googleapis.com onesignal.com cdn.onesignal.com",
        reportUri: "/api/v1/csp_violation"
    },
    reportOnly: false,
    setAllHeaders: true
};
