import { Enum, defineEnum } from '@extension/Enum';

module.exports = defineEnum({
    other: {
        rawValue: "other",
        value: "other",
    },
    txt: {
        rawValue: "text/plain",
        value: "txt",
    },
    csv: {
        rawValue: "text/csv",
        value: "csv",
    },
    html: {
        rawValue: "text/html",
        value: "html",
    },
    css: {
        rawValue: "text/css",
        value: "css",
    },
    javascript: {
        rawValue: "text/javascript",
        value: "javascript",
    },
    exe: {
        rawValue: "application/octet-stream",
        value: "exe",
    },
    json: {
        rawValue: "application/json",
        value: "json",
    },
    pdf: {
        rawValue: "application/pdf",
        value: "pdf",
    },
    xls: {
        rawValue: "application/vnd.ms-excel",
        value: "xls",
    },
    xlsx: {
        rawValue: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        value: "xlsx",
    },
    ppt: {
        rawValue: "application/vnd.ms-powerpoint",
        value: "ppt",
    },
    pptx: {
        rawValue: "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        value: "pptx",
    },
    doc: {
        rawValue: "application/msword",
        value: "doc",
    },
    docx: {
        rawValue: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        value: "docx",
    },

    bitmap: {
        rawValue: "image/bmp",
        value: "bitmap",
    },
    svg: {
        rawValue: "image/svg+xml",
        value: "svg",
    },
    zip: {
        rawValue: "application/zip",
        value: "zip",
    },
    lzh: {
        rawValue: "application/x-lzh",
        value: "lzh",
    },
    tar: {
        rawValue: "application/x-tar",
        value: "tar",
    },

    ai: {
        rawValue: "application/postscript",
        value: "ai",
        is_image: true,
    },
    bmp: {
        rawValue: "image/x-bmp",
        value: "bmp",
        is_image: true,
    },
    rle: {
        rawValue: "image/x-bmp",
        value: "rle",
        is_image: true,
    },
    dib: {
        rawValue: "image/x-bmp",
        value: "dib",
        is_image: true,
    },

    cgm: {
        rawValue: "image/cgm",
        value: "cgm",
        is_image: true,
    },
    dwf: {
        rawValue: "drawing/x-dwf",
        value: "dwf",
        is_image: true,
    },
    epsf: {
        value: "epsf",
        rawValue: "application/postscript",
        is_image: true,
    },
    eps: {
        value: "eps",
        rawValue: "application/postscript",
        is_image: true,
    },
    ps: {
        value: "ps",
        rawValue: "application/postscript",
        is_image: true,
    },
    fif: {
        value: "fif",
        rawValue: "image/fif",
        is_image: true,
    },
    fpx: {
        value: "fpx",
        rawValue: "image/fpx", // "image/x-fpx"
        is_image: true,
    },
    gif: {
        value: "gif",
        rawValue: "image/gif",
        is_image: true,
    },
    jpg: {
        value: "jpg",
        rawValue: "image/jpeg",
        is_image: true,
    },
    jpeg: {
        value: "jpeg",
        rawValue: "image/jpeg",
        is_image: true,
    },
    jpe: {
        value: "jpe",
        rawValue: "image/jpeg",
        is_image: true,
    },
    jfif: {
        value: "jfif",
        rawValue: "image/jpeg",
        is_image: true,
    },
    jfi: {
        value: "jfi",
        rawValue: "image/jpeg",
        is_image: true,
    },
    pcd: {
        value: "pcd",
        rawValue: "image/pcd", // "image/x-photo-cd"
        is_image: true,
    },
    pict: {
        value: "pict",
        rawValue: "image/pict",
        is_image: true,
    },
    pct: {
        value: "pct",
        rawValue: "image/pict",
        is_image: true,
    },
    png: {
        value: "png",
        rawValue: "image/x-png", // "image/png"
        is_image: true,
    },
    tga: {
        value: "tga",
        rawValue: "image/x-targa",
        is_image: true,
    },
    tpic: {
        value: "tpic",
        rawValue: "image/x-targa",
        is_image: true,
    },
    vda: {
        value: "vda",
        rawValue: "image/x-targa",
        is_image: true,
    },
    vst: {
        value: "vst",
        rawValue: "image/x-targa",
        is_image: true,
    },
    tiff: {
        value: "tiff",
        rawValue: "image/tiff", // "image/x-tiff"
        is_image: true,
    },
    tif: {
        value: "tif",
        rawValue: "image/tiff", // "image/x-tiff"
        is_image: true,
    },
    wrl: {
        value: "wrl",
        rawValue: "model/vrml", // "x-world/x-vrml"
        is_image: true,
    },
    xbm: {
        value: "xbm",
        rawValue: "image/x-bitmap",
        is_image: true,
    },
    xpm: {
        value: "xpm",
        rawValue: "image/x-xpixmap",
        is_image: true,
    },


    aiff: {
      value: "aiff",
      rawValue: "audio/aiff", // "audio/x-aiff"
      is_sound: true,
    },
    aif: {
      value: "aif",
      rawValue: "audio/aiff", // "audio/x-aiff"
      is_sound: true,
    },
    au: {
        value: "au",
        rawValue: "audio/basic",
        is_sound: true,
    },
    snd: {
        value: "snd",
        rawValue: "audio/basic",
        is_sound: true,
    },
    kar: {
        value: "kar",
        rawValue: "audio/midi", // "audio/x-midi"
        is_sound: true,
    },
    midi: {
        value: "midi",
        rawValue: "audio/midi", // "audio/x-midi"
        is_sound: true,
    },
    mid: {
        value: "mid",
        rawValue: "audio/midi", // "audio/x-midi"
        is_sound: true,
    },
    smf: {
        value: "smf",
        rawValue: "audio/midi", // "audio/x-midi"
        is_sound: true,
    },
    m1a: {
        value: "m1a",
        rawValue: "audio/mpeg", // "audio/x-mpeg"
        is_sound: true,
    },
    m2a: {
        value: "m2a",
        rawValue: "audio/mpeg", // "audio/x-mpeg"
        is_sound: true,
    },
    mp2: {
        value: "mp2",
        rawValue: "audio/mpeg", // "audio/x-mpeg"
        is_sound: true,
    },
    mp3: {
        value: "mp3",
        rawValue: "audio/mpeg", // "audio/x-mpeg"
        is_sound: true,
    },
    mpa: {
        value: "mpa",
        rawValue: "audio/mpeg", // "audio/x-mpeg"
        is_sound: true,
    },
    mpega: {
        value: "mpega",
        rawValue: "audio/mpeg", // "audio/x-mpeg"
        is_sound: true,
    },
    rpm: {
        value: "rpm",
        rawValue: "audio/x-pn-realaudio-plugin",
        is_sound: true,
    },
    // MARK: only swa is sound
    swa: {
        value: "swa",
        rawValue: "application/x-director",
        is_sound: true,
    },
    dcr: {
        value: "dcr",
        rawValue: "application/x-director",
        is_sound: true,
    },
    dir: {
        value: "dir",
        rawValue: "application/x-director",
        is_sound: true,
    },
    dxr: {
        value: "dxr",
        rawValue: "application/x-director",
        is_sound: true,
    },
    vqf: {
        value: "vqf",
        rawValue: "audio/x-twinvq",
        is_sound: true,
    },
    wav: {
        value: "wav",
        rawValue: "audio/wav", // "audio/x-wav"
        is_sound: true,
    },


    aab: {
        value: "aab",
        rawValue: "application/x-authorware-bin",
        is_movie: true,
    },
    aam: {
        value: "aam",
        rawValue: "application/x-authorware-map",
        is_movie: true,
    },
    aas: {
        value: "aas",
        rawValue: "application/x-authorware-seg",
        is_movie: true,
    },
    asf: {
        value: "asf",
        rawValue: "video/x-ms-asf",
        is_movie: true,
    },
    avi: {
        value: "avi",
        rawValue: "vide/x-msvideo",
        is_movie: true,
    },
    flc: {
        value: "flc",
        rawValue: "video/flc",
        is_movie: true,
    },
    fli: {
        value: "fli",
        rawValue: "video/flc",
        is_movie: true,
    },
    moov: {
        value: "moov",
        rawValue: "video/quicktime",
        is_movie: true,
    },
    mov: {
        value: "mov",
        rawValue: "video/quicktime",
        is_movie: true,
    },
    qt: {
        value: "qt",
        rawValue: "video/quicktime",
        is_movie: true,
    },
    mp4: {
        value: "mp4",
        rawValue: "video/mp4",
        is_movie: true,
    },
    // MARK: _2s is equal 2s
    mpeg: {
        value: "mpeg",
        rawValue: "vide/mpeg", // "video/x-mpeg"
        is_movie: true,
    },
    mpg: {
        value: "mpg",
        rawValue: "vide/mpeg", // "video/x-mpeg"
        is_movie: true,
    },
    mpe: {
        value: "mpe",
        rawValue: "vide/mpeg", // "video/x-mpeg"
        is_movie: true,
    },
    mpv: {
        value: "mpv",
        rawValue: "vide/mpeg", // "video/x-mpeg"
        is_movie: true,
    },
    mng: {
        value: "mng",
        rawValue: "vide/mpeg", // "video/x-mpeg"
        is_movie: true,
    },
    m1s: {
        value: "m1s",
        rawValue: "vide/mpeg", // "video/x-mpeg"
        is_movie: true,
    },
    m1v: {
        value: "m1v",
        rawValue: "vide/mpeg", // "video/x-mpeg"
        is_movie: true,
    },
    _2s: {
        value: "2s",
        rawValue: "vide/mpeg", // "video/x-mpeg"
        is_movie: true,
    },
    m2v: {
        value: "m2v",
        rawValue: "vide/mpeg", // "video/x-mpeg"
        is_movie: true,
    },

    rm: {
        value: "rm",
        rawValue: "audio/x-pn-realaudio",
        is_movie: true,
    },
    spl: {
        value: "spl",
        rawValue: "application/futuresplash",
        is_movie: true,
    },
    swf: {
        value: "swf",
        rawValue: "application/x-shockwave-flash",
        is_movie: true,
    },
    vdo: {
        value: "vdo",
        rawValue: "video/vdo",
        is_movie: true,
    },
    viv: {
        value: "viv",
        rawValue: "video/vnd.vivo",
        is_movie: true,
    },
    vivo: {
        value: "vivo",
        rawValue: "video/vnd.vivo",
        is_movie: true,
    },
    xdm: {
        value: "xdm",
        rawValue: "application/x-xdma",
        is_movie: true,
    },
    xdma: {
        value: "xdma",
        rawValue: "application/x-xdma",
        is_movie: true,
    },
});
