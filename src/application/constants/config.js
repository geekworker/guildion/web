export const APP_NAME = 'Guildion';
export const APP_NAME_LATIN = 'Guildion';
export const APP_NAME_UPPERCASE = 'GUILDION';
export const APP_ICON = 'Guildion';
export const HOST_NAME = 'Guildion';
export const APP_DOMAIN = 'guildion.us'; // '3.209.221.175';
export const APP_URL = 'https://guildion.us'; // 'http://3.209.221.175';
export const LOCAL_APP_URL = 'http://localhost:8080';
export const CURRENT_APP_URL =
    process.env.NODE_ENV == 'production' ? APP_URL : LOCAL_APP_URL;
export const APP_HOST = 'guildion.us';
export const APP_IP = 'http://54.150.73.24';
export const INC_FULL_NAME = '株式会社Selfinity';
export const IMG_MAIN_PROXY = process.env.AWS_CLOUDFRONT_URL;
export const IOS_SCHEME = 'guildion://';

export const IOS_DOWNLOAD_URL =
    'https://apps.apple.com/jp/app/nowful-realtime-community/id1515505548';

module.exports = {
    APP_NAME,
    APP_NAME_LATIN,
    APP_NAME_UPPERCASE,
    APP_ICON,
    APP_URL,
    LOCAL_APP_URL,
    APP_DOMAIN,
    INC_FULL_NAME,
    APP_HOST,
    APP_IP,
    IMG_MAIN_PROXY,
    CURRENT_APP_URL,
    IOS_SCHEME,
    IOS_DOWNLOAD_URL,
};
