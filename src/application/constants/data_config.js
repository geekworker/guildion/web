const config = require('@constants/config');

const fetch_data_limit = size => {
    switch (size) {
        case 'S':
            return 10;
        case 'M':
            return 50;
        case 'L':
            return 100;
        case 'XL':
            return 500;
    }
};

const fetch_data_raw_limit = size => {
    switch (size) {
        case 'S':
            return 3;
        case 'M':
            return 6;
        case 'L':
            return 9;
        case 'XL':
            return 12;
    }
};

const fetch_data_offset = size => {
    switch (size) {
        case 'S':
            return 10;
        case 'M':
            return 50;
        case 'L':
            return 100;
    }
};

const concurrency_size = size => {
    switch (size) {
        case 'S':
            return 6;
        case 'M':
            return 10;
        case 'L':
            return 12;
        case 'XL':
            return 20;
    }
};

const uuid_size = size => {
    switch (size) {
        case 'S':
            return 8;
        case 'M':
            return 16;
        case 'L':
            return 24;
    }
};

const max_decimal_range = 65;
const min_decimal_range = 4;

const Image = {
    default_user_images: [
        config.IMG_MAIN_PROXY + '/public/images/profiles/blue.png',
        config.IMG_MAIN_PROXY + '/public/images/profiles/green.png',
        config.IMG_MAIN_PROXY + '/public/images/profiles/pink.png',
        config.IMG_MAIN_PROXY + '/public/images/profiles/purple.png',
        config.IMG_MAIN_PROXY + '/public/images/profiles/yellow.png',
    ],
    default_user_image: () =>
        Image.default_user_images[
            Math.floor(
                Math.random() * Math.floor(Image.default_user_images.length)
            )
        ],
    default_guild_images: [
        config.IMG_MAIN_PROXY + '/public/images/guilds/blue.png',
        config.IMG_MAIN_PROXY + '/public/images/guilds/green.png',
        config.IMG_MAIN_PROXY + '/public/images/guilds/pink.png',
        config.IMG_MAIN_PROXY + '/public/images/guilds/purple.png',
        config.IMG_MAIN_PROXY + '/public/images/guilds/yellow.png',
    ],
    default_guild_image: () =>
        Image.default_guild_images[
            Math.floor(
                Math.random() * Math.floor(Image.default_guild_images.length)
            )
        ],
    default_opg_image:
        config.IMG_MAIN_PROXY + '/public/images/brands/brand-logo.png',
    logo_image: config.IMG_MAIN_PROXY + '/public/images/brands/brand-logo',
    mini_logo_image: config.IMG_MAIN_PROXY + '/public/images/brands/logo.png',
};

const User = {
    nickname_max_limit: 50,
    nickname_min_limit: 1,
};

const Member = {
    nickname_max_limit: 50,
    nickname_min_limit: 1,
};

const Identity = {
    password_min_limit: 8,
    password_max_limit: 120,
    email_max_limit: 120,
    email_min_limit: 0,
};

const Guild = {
    tags_max_limit: 5,
    bump_hour_limit: 2,
};

const Tag = {
    min_guild_count_limit: 1,
};

module.exports = {
    fetch_data_limit,
    fetch_data_raw_limit,
    fetch_data_offset,
    concurrency_size,
    uuid_size,
    max_decimal_range,
    min_decimal_range,
    Image,
    User,
    Member,
    Identity,
    Guild,
    Tag,
};
