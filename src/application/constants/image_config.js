const config = require('@constants/config');

export const LOGO_PATH =
    config.IMG_MAIN_PROXY + '/public/images/brands/logo.png';
export const MINI_LOGO_PATH =
    config.IMG_MAIN_PROXY + '/public/images/brands/logo.png';
export const LOGO_HEADER_PATH =
    config.IMG_MAIN_PROXY + '/public/images/brands/logo-header.png';
export const LOGO_HEADER_TEXT_WHITE_PATH =
    config.IMG_MAIN_PROXY + '/public/images/brands/logo-header-text-white.png';
export const BRAND_LOGO_PATH =
    config.IMG_MAIN_PROXY + '/public/images/brands/brand-logo.png';
export const WHITE_LOGO_PATH =
    config.IMG_MAIN_PROXY + '/public/images/brands/white-logo.png';
export const OVERVIEW_PATH =
    config.IMG_MAIN_PROXY + '/public/images/brands/ja/overview.png';
export const OVERVIEW_DYNAMIC_WHITE_PATH =
    config.IMG_MAIN_PROXY +
    '/public/images/brands/ja/overview-dynamic_white.png';

export const NO_IMAGE_PATH =
    config.IMG_MAIN_PROXY + '/public/images/noimage.png';

module.exports = {
    LOGO_PATH,
    MINI_LOGO_PATH,
    LOGO_HEADER_PATH,
    LOGO_HEADER_TEXT_WHITE_PATH,
    BRAND_LOGO_PATH,
    WHITE_LOGO_PATH,
    OVERVIEW_PATH,
    OVERVIEW_DYNAMIC_WHITE_PATH,
    NO_IMAGE_PATH,
};
