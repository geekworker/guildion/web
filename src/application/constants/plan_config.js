import { Enum, defineEnum } from '@extension/Enum';
import tt from 'counterpart';
require('@extension/Number');

export const FREE_PLAN_STORAGE_BYTE = 524288000;
export const FREE_PLAN_MAX_PARTICIPANT_COUNT = 30;

export const SUBSCRIPTION_SPAN = defineEnum({
    Month1: {
        rawValue: 0,
        value: 'month1',
        suffix: () => tt('g.suffix_month'),
        count: 1,
        price: plan => {
            switch (plan) {
                case SUBSCRIPTION_PLAN.VolumeMini:
                    return 300;
                case SUBSCRIPTION_PLAN.VolumeStandardMini:
                    return 600;
                case SUBSCRIPTION_PLAN.VolumeStandard:
                    return 1500;
                case SUBSCRIPTION_PLAN.VolumeStandardBig:
                    return 2500;
            }
        },
        priceString: plan => {
            return `¥${SUBSCRIPTION_SPAN.Month1.price(plan).toLocaleString()}`;
        },
        monthPriceString: plan => {
            return '';
        },
        discount: plan => 0,
        discountString: plan => {
            switch (plan) {
                case SUBSCRIPTION_PLAN.VolumeMini:
                    return tt('premium.super_discount');
                case SUBSCRIPTION_PLAN.VolumeStandardMini:
                    return tt('premium.good_discount');
                case SUBSCRIPTION_PLAN.VolumeStandard:
                    return tt('premium.nice_discount');
                case SUBSCRIPTION_PLAN.VolumeStandardBig:
                    return tt('premium.nice_discount');
            }
        },
    },
    Month3: {
        rawValue: 1,
        value: 'month3',
        suffix: () => tt('g.suffix_months'),
        count: 3,
        price: plan => {
            switch (plan) {
                case SUBSCRIPTION_PLAN.VolumeMini:
                    return 750;
                case SUBSCRIPTION_PLAN.VolumeStandardMini:
                    return 1600;
                case SUBSCRIPTION_PLAN.VolumeStandard:
                    return 4000;
                case SUBSCRIPTION_PLAN.VolumeStandardBig:
                    return 6000;
            }
        },
        priceString: plan => {
            return `¥${SUBSCRIPTION_SPAN.Month3.price(plan).toLocaleString()}`;
        },
        monthPriceString: plan => {
            return `¥${(
                SUBSCRIPTION_SPAN.Month3.price(plan) /
                SUBSCRIPTION_SPAN.Month3.count
            )
                .toFixed(0)
                .toLocaleString()}/${tt('g.month')}`;
        },
        discount: plan => {
            switch (plan) {
                case SUBSCRIPTION_PLAN.VolumeMini:
                    return 17;
                case SUBSCRIPTION_PLAN.VolumeStandardMini:
                    return 12;
                case SUBSCRIPTION_PLAN.VolumeStandard:
                    return 12;
                case SUBSCRIPTION_PLAN.VolumeStandardBig:
                    return 20;
            }
        },
        discountString: plan => {
            switch (plan) {
                case SUBSCRIPTION_PLAN.VolumeMini:
                    return tt('premium.discount', {
                        data: `${SUBSCRIPTION_SPAN.Month3.discount(plan)}%`,
                    });
                case SUBSCRIPTION_PLAN.VolumeStandardMini:
                    return tt('premium.discount', {
                        data: `${SUBSCRIPTION_SPAN.Month3.discount(plan)}%`,
                    });
                case SUBSCRIPTION_PLAN.VolumeStandard:
                    return tt('premium.discount', {
                        data: `${SUBSCRIPTION_SPAN.Month3.discount(plan)}%`,
                    });
                case SUBSCRIPTION_PLAN.VolumeStandardBig:
                    return tt('premium.discount', {
                        data: `${SUBSCRIPTION_SPAN.Month3.discount(plan)}%`,
                    });
            }
        },
    },
    Year: {
        rawValue: 2,
        value: 'year',
        suffix: () => tt('g.suffix_months'),
        count: 12,
        price: plan => {
            switch (plan) {
                case SUBSCRIPTION_PLAN.VolumeMini:
                    return 2800;
                case SUBSCRIPTION_PLAN.VolumeStandardMini:
                    return 5600;
                case SUBSCRIPTION_PLAN.VolumeStandard:
                    return 12000;
                case SUBSCRIPTION_PLAN.VolumeStandardBig:
                    return 22500;
            }
        },
        priceString: plan => {
            return `¥${SUBSCRIPTION_SPAN.Year.price(plan).toLocaleString()}`;
        },
        monthPriceString: plan => {
            return `¥${(
                SUBSCRIPTION_SPAN.Year.price(plan) /
                SUBSCRIPTION_SPAN.Year.count
            )
                .toFixed(0)
                .toLocaleString()}/${tt('g.month')}`;
        },
        discount: plan => {
            switch (plan) {
                case SUBSCRIPTION_PLAN.VolumeMini:
                    return 23;
                case SUBSCRIPTION_PLAN.VolumeStandardMini:
                    return 23;
                case SUBSCRIPTION_PLAN.VolumeStandard:
                    return 23;
                case SUBSCRIPTION_PLAN.VolumeStandardBig:
                    return 25;
            }
        },
        discountString: plan => {
            switch (plan) {
                case SUBSCRIPTION_PLAN.VolumeMini:
                    return tt('premium.discount', {
                        data: `${SUBSCRIPTION_SPAN.Year.discount(plan)}%`,
                    });
                case SUBSCRIPTION_PLAN.VolumeStandardMini:
                    return tt('premium.discount', {
                        data: `${SUBSCRIPTION_SPAN.Year.discount(plan)}%`,
                    });
                case SUBSCRIPTION_PLAN.VolumeStandard:
                    return tt('premium.discount', {
                        data: `${SUBSCRIPTION_SPAN.Year.discount(plan)}%`,
                    });
                case SUBSCRIPTION_PLAN.VolumeStandardBig:
                    return tt('premium.discount', {
                        data: `${SUBSCRIPTION_SPAN.Year.discount(plan)}%`,
                    });
            }
        },
    },
});

export const SUBSCRIPTION_PLAN = defineEnum({
    VolumeMini: {
        rawValue: 0,
        value: 'volumeMini',
        name: () => tt('premium.plans.mini'),
        storage_byte: 5368709120,
        participant_max_limit: 10,
        enable_original_reaction: true,
        func_value: plan_function => {
            switch (plan_function) {
                case PLAN_FUNCTION.Volume:
                    return Number.prototype.bytesToSize(
                        SUBSCRIPTION_PLAN.VolumeMini.storage_byte
                    );
                case PLAN_FUNCTION.Participant:
                    return `${
                        SUBSCRIPTION_PLAN.VolumeMini.participant_max_limit
                    }`;
                case PLAN_FUNCTION.Reaction:
                    return '○';
            }
        },
    },
    VolumeStandardMini: {
        rawValue: 1,
        value: 'volumeStandardMini',
        name: () => tt('premium.plans.standard_mini'),
        storage_byte: 16106127360,
        participant_max_limit: 20,
        enable_original_reaction: true,
        func_value: plan_function => {
            switch (plan_function) {
                case PLAN_FUNCTION.Volume:
                    return Number.prototype.bytesToSize(
                        SUBSCRIPTION_PLAN.VolumeStandardMini.storage_byte
                    );
                case PLAN_FUNCTION.Participant:
                    return `${
                        SUBSCRIPTION_PLAN.VolumeStandardMini
                            .participant_max_limit
                    }`;
                case PLAN_FUNCTION.Reaction:
                    return '○';
            }
        },
    },
    VolumeStandard: {
        rawValue: 2,
        value: 'volumeStandard',
        name: () => tt('premium.plans.standard'),
        storage_byte: 53687091200,
        participant_max_limit: 50,
        enable_original_reaction: true,
        func_value: plan_function => {
            switch (plan_function) {
                case PLAN_FUNCTION.Volume:
                    return Number.prototype.bytesToSize(
                        SUBSCRIPTION_PLAN.VolumeStandard.storage_byte
                    );
                case PLAN_FUNCTION.Participant:
                    return `${
                        SUBSCRIPTION_PLAN.VolumeStandard.participant_max_limit
                    }`;
                case PLAN_FUNCTION.Reaction:
                    return '○';
            }
        },
    },
    VolumeStandardBig: {
        rawValue: 3,
        value: 'volumeStandardBig',
        name: () => tt('premium.plans.standard_big'),
        storage_byte: 107374182400,
        participant_max_limit: 100,
        enable_original_reaction: true,
        func_value: plan_function => {
            switch (plan_function) {
                case PLAN_FUNCTION.Volume:
                    return Number.prototype.bytesToSize(
                        SUBSCRIPTION_PLAN.VolumeStandardBig.storage_byte
                    );
                case PLAN_FUNCTION.Participant:
                    return `${
                        SUBSCRIPTION_PLAN.VolumeStandardBig
                            .participant_max_limit
                    }`;
                case PLAN_FUNCTION.Reaction:
                    return '○';
            }
        },
    },
});

export const PLAN_FUNCTION = defineEnum({
    Volume: {
        rawValue: 0,
        value: 'Volume',
        string: () => tt('premium.functions.storage'),
    },
    Participant: {
        rawValue: 1,
        value: 'Participant',
        string: () => tt('premium.functions.participant'),
    },
    Reaction: {
        rawValue: 2,
        value: 'Reaction',
        string: () => tt('premium.functions.reaction'),
    },
});
