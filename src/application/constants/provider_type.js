import { Enum, defineEnum } from '@extension/Enum';

module.exports = defineEnum({
    other: {
        rawValue: "other",
        value: "other",
    },
    youtube: {
        rawValue: "youtube",
        value: "youtube",
        is_movie: true
    },
});
