export const IOS_TESTFLIGHT_URL = 'https://testflight.apple.com/join/t5YOhCWK';
export const IOS_TESTFLIGHT_MODE = true;

module.exports = {
    IOS_TESTFLIGHT_URL,
    IOS_TESTFLIGHT_MODE,
};
