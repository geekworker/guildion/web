import tt from 'counterpart';
import { available_locales } from '@constants/locale_config';
import { browserHistory } from 'react-router';

function removeEmpty(obj) {
    for (var propName in obj) {
        if (obj[propName] === null || obj[propName] === undefined) {
            delete obj[propName];
        }
    }
    return obj;
}

export const getValueWithLangs = (
    key,
    params = {
        en: available_locales.includes('en'),
        es: available_locales.includes('es'),
        ru: available_locales.includes('ru'),
        fr: available_locales.includes('fr'),
        it: available_locales.includes('it'),
        ko: available_locales.includes('ko'),
        zh: available_locales.includes('zh'),
        pl: available_locales.includes('pl'),
        ja: available_locales.includes('ja'),
    }
) => {
    if (!key || key == '') return;
    return removeEmpty({
        en: params.en ? tt(key, { locale: 'en' }) : null,
        es: params.es ? tt(key, { locale: 'es' }) : null,
        ru: params.ru ? tt(key, { locale: 'ru' }) : null,
        fr: params.fr ? tt(key, { locale: 'fr' }) : null,
        it: params.it ? tt(key, { locale: 'it' }) : null,
        ko: params.ko ? tt(key, { locale: 'ko' }) : null,
        zh: params.zh ? tt(key, { locale: 'zh' }) : null,
        pl: params.pl ? tt(key, { locale: 'pl' }) : null,
        ja: params.ja ? tt(key, { locale: 'ja' }) : null,
    });
};

export const fallbackLocale = 'en';

export const getLocale = locale => {
    if (!available_locales.includes(locale)) {
        locale = fallbackLocale;
    }

    return locale;
};

export const getCurrentLocalePathname = () => {
    if (!process.env.NODE_ENV) return null;
    const pathname = browserHistory.getCurrentLocation().pathname;
    return available_locales.filter(lang => pathname.startsWith(`/${lang}`))[0];
};

export const getCurrentLocalePathnameByPathname = pathname => {
    return available_locales.filter(lang => pathname.startsWith(`/${lang}`))[0];
};
