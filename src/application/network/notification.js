import config from '@constants/config';
import OneSignal from 'onesignal-node';
import { getValueWithLangs } from '@locales';
import { ClientError, ApiError } from '@extension/Error';

const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class Notification {
    static getTimer(key) {
        switch (key) {
            case 'S':
                return 4000;
            case 'M':
                return 8000;
            case 'L':
                return 12000;
        }
    }

    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new Notification(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async push({
        tt_key,
        icon = config.IMG_MAIN_PROXY + '/public/images/brands/brand-logo.png',
        url = '/',
        ids = [],
        title = null,
        body = null,
    }) {
        if (!tt_key || !icon || !url || !ids) return false;

        if (!!title) {
            title = { ja: title, en: title };
        }

        if (!!body) {
            body = { ja: body, en: body };
        }

        var notification =
            ids.length > 0
                ? new OneSignal.Notification({
                      url,
                      headings:
                          title ||
                          getValueWithLangs(
                              'notifications.' + tt_key + '.title'
                          ),
                      contents:
                          body ||
                          getValueWithLangs(
                              'notifications.' + tt_key + '.body'
                          ),
                      include_player_ids: ids,
                  })
                : new OneSignal.Notification({
                      url,
                      headings:
                          title ||
                          getValueWithLangs(
                              'notifications.' + tt_key + '.title'
                          ),
                      contents:
                          body ||
                          getValueWithLangs(
                              'notifications.' + tt_key + '.body'
                          ),
                  });

        if (process.env.NODE_ENV == 'development') {
            console.log(notification);
        }

        const client = new OneSignal.Client({
            userAuthKey: `${process.env.ONESIGNAL_USER_AUTH_KEY}`,
            app: {
                appAuthKey: `${process.env.ONESIGNAL_APP_AUTH_KEY}`,
                appId: `${process.env.ONESIGNAL_APP_ID}`,
            },
        });

        client.sendNotification(notification, (e, httpResponse, data) => {
            if (e) {
                throw new ApiError({
                    error: e,
                    tt_key: 'errors.invalid_response_from_server',
                });
            } else {
                return true;
            }
        });
    }
}

export var OneSignalWindow = [];
export var getOneSignalWindow = () => {
    try {
        if (process.env.BROWSER && OneSignalWindow instanceof Array) {
            OneSignalWindow = window.OneSignal || [];
            OneSignalWindow.push(function() {
                OneSignalWindow.init({
                    appId: process.env.ONESIGNAL_APP_ID,
                });
            });
            return OneSignalWindow;
        } else {
            return OneSignalWindow;
        }
    } catch (e) {
        return OneSignalWindow;
    }
};
