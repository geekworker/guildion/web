import models from '@models';
import config from '@constants/config';
import data_config from '@constants/data_config';

const generateDocumentLinks = async () => {
    const instances = await models.Document.findAll({
        where: {
            permission: true,
        },
        include: [
            {
                as: 'Group',
                model: models.DocumentGroup,
                where: {
                    permission: true,
                },
            },
        ],
    });

    return [
        ...instances.map((instance, index) => {
            return {
                url: `/document/${instance.Group.groupname}/${instance.uid}`,
                changefreq: 'monthly',
                priority: 0.4,
                links: [
                    {
                        lang: 'en',
                        url: `/en/document/${instance.Group.groupname}/${
                            instance.uid
                        }`,
                    },
                    {
                        lang: 'ja',
                        url: `/ja/document/${instance.Group.groupname}/${
                            instance.uid
                        }`,
                    },
                ],
            };
        }),
    ];
};

const generateDocumentGroupLinks = async () => {
    const instances = await models.DocumentGroup.findAll({
        where: {
            permission: true,
        },
        attributes: ['id', 'groupname'],
        raw: true,
    });

    return [
        ...instances.map((instance, index) => {
            return {
                url: `/document/${instance.groupname}`,
                changefreq: 'monthly',
                priority: 0.4,
                links: [
                    {
                        lang: 'en',
                        url: `/en/document/${instance.groupname}`,
                    },
                    {
                        lang: 'ja',
                        url: `/ja/document/${instance.groupname}`,
                    },
                ],
            };
        }),
    ];
};

const generateWelcomeGuildLinks = async () => {
    const instances = await models.Guild.findAll({
        where: {
            is_private: false,
            permission: true,
        },
    });

    return [
        ...instances.map((instance, index) => {
            return {
                url: `/guild/welcome/${instance.uid}`,
                changefreq: 'monthly',
                priority: 0.8,
                links: [
                    {
                        lang: 'en',
                        url: `/en/guild/welcome/${instance.uid}`,
                    },
                    {
                        lang: 'ja',
                        url: `/ja/guild/welcome/${instance.uid}`,
                    },
                ],
            };
        }),
    ];
};

const defaultLinks = [
    // { url: '/', changefreq: 'monthly', priority: 1.0 },
    {
        url: '/welcome',
        links: [
            {
                lang: 'en',
                url: `/en/welcome`,
            },
            {
                lang: 'ja',
                url: `/ja/welcome`,
            },
        ],
        img: [],
        video: [],
        changefreq: 'monthly',
        priority: 1.0,
    },
    {
        url: '/premium',
        changefreq: 'monthly',
        priority: 1.0,
        links: [
            {
                lang: 'en',
                url: `/en/premium`,
            },
            {
                lang: 'ja',
                url: `/ja/premium`,
            },
        ],
    },
    {
        url: '/downloads/ios',
        changefreq: 'monthly',
        priority: 1.0,
        links: [
            {
                lang: 'en',
                url: `/en/downloads/ios`,
            },
            {
                lang: 'ja',
                url: `/ja/downloads/ios`,
            },
        ],
    },
    {
        url: '/downloads/web',
        changefreq: 'monthly',
        priority: 1.0,
        links: [
            {
                lang: 'en',
                url: `/en/downloads/web`,
            },
            {
                lang: 'ja',
                url: `/ja/downloads/web`,
            },
        ],
    },
    {
        url: '/downloads/android',
        changefreq: 'monthly',
        priority: 1.0,
        links: [
            {
                lang: 'en',
                url: `/en/downloads/android`,
            },
            {
                lang: 'ja',
                url: `/ja/downloads/android`,
            },
        ],
    },
    {
        url: '/downloads/macbook',
        changefreq: 'monthly',
        priority: 1.0,
        links: [
            {
                lang: 'en',
                url: `/en/downloads/macbook`,
            },
            {
                lang: 'ja',
                url: `/ja/downloads/macbook`,
            },
        ],
    },
];

const generateUrls = async () =>
    Array.prototype.concat.apply(
        [],
        [
            defaultLinks,
            ...(await generateDocumentLinks()),
            ...(await generateDocumentGroupLinks()),
            ...(await generateWelcomeGuildLinks()),
        ]
    );

module.exports = async () => {
    const urls = await generateUrls();
    return '';
};
