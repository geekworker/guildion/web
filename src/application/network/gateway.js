import path from 'path';
import fs from 'fs';
import { esc } from '@models';
import models from '@models';
import { ApiError } from '@extension/Error';
import { SessionDataStore } from '@datastore';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const sessionDataStore = SessionDataStore.instance;

export default class Gateway {
    static emailRegex = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/;

    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new Gateway(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async init_api() {
        let developer = await models.Developer.findOne({
            where: {
                api_key: process.env.API_KEY,
                permission: true,
                verified: true,
            },
        });

        if (!developer) {
            developer = await models.Developer.create({
                api_key: process.env.API_KEY,
                password: process.env.API_PASSWORD,
                username: 'admin',
                email: 'admin@admin.com',
                email_is_verified: true,
                country_code: 'JP',
                locale: 'ja',
                permission: true,
                verified: true,
            });
        }

        developer = await developer.update({
            password: process.env.API_PASSWORD,
        });
    }

    async run(
        router,
        ctx,
        next,
        {
            accessTokenRequired = false,
            checkAPIRequired = false,
            onlyDevelopment = false,
        }
    ) {
        let error;
        if (onlyDevelopment && process.env.NODE_ENV != 'development') {
            router.status = 404;
            router.body = {};
            error = new Error('page not found');
            error.status = 404;
            return {
                router,
                ctx,
                next,
                error,
            };
        }

        if (accessTokenRequired) {
            router.request.body.identity = await this.checkAccessToken(
                router,
                ctx,
                next
            ).catch(e => {
                error = e;
            });
        }

        if (checkAPIRequired) {
            await this.checkApiKey(router, ctx, next).catch(e => {
                error = e;
            });
        }

        router.request.body.api_key = null;
        router.request.body.api_password = null;

        if (!!error) {
            return {
                router,
                ctx,
                next,
                error,
            };
        } else {
            return {
                router,
                ctx,
                next,
            };
        }
    }

    async isValid(router, ctx, next) {
        const results = await Promise.all([
            this.checkApiKey(router, ctx, next),
            // this.rateLimitReq(router, router.req),
        ]);
        return results.filter(val => !!val).length == results.length;
    }

    async checkApiKey(router, ctx, next) {
        const { api_key, api_password } = router.request.body;

        if (!api_key)
            throw new ApiError({
                error: new Error(``),
                tt_key: 'errors.invalid_response_from_server',
            });

        if (!api_password)
            throw new ApiError({
                error: new Error(``),
                tt_key: 'errors.invalid_response_from_server',
            });

        const developer = await models.Developer.findOne({
            where: {
                api_key,
            },
        }).catch(e => {
            throw new ApiError({
                error: e,
                tt_key: 'errors.invalid_response_from_server',
            });
        });

        if (!developer)
            throw new ApiError({
                error: new Error(``),
                tt_key: 'errors.invalid_response_from_server',
            });

        const result = await developer.authenticate(
            api_password,
            developer.dataValues.password_hash
        );

        if (!result)
            throw new ApiError({
                error: new Error(``),
                tt_key: 'errors.invalid_response_from_server',
            });

        return true;
    }

    async checkAccessToken(router, ctx, next) {
        const { accessToken, devise } = router.request.body;

        if (!accessToken || !devise)
            throw new ApiError({
                error: new Error(``),
                tt_key: 'errors.invalid_response_from_server',
            });

        const identity = await sessionDataStore
            .checkAccessToken({
                access_token: accessToken,
                devise,
                deleting: false,
            })
            .catch(e => {
                throw new ApiError({
                    error: e,
                    tt_key: 'errors.invalid_response_from_server',
                });
            });

        if (!identity)
            throw new ApiError({
                error: new Error(``),
                tt_key: 'errors.invalid_response_from_server',
            });

        return identity;
    }

    async getRemoteIp(req) {
        const remote_address =
            req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const ip_match = remote_address
            ? remote_address.match(/(\d+\.\d+\.\d+\.\d+)/)
            : null;
        return ip_match ? ip_match[1] : esc(remote_address);
    }

    async rateLimitReq(ctx, req) {
        const ip =
            req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const now = Date.now();

        // purge hits older than minutes_max
        this.ip_last_hit.forEach((v, k) => {
            const seconds = (now - v) / 1000;
            if (seconds > 1) {
                this.ip_last_hit.delete(ip);
            }
        });

        let result = false;
        // if ip is still in the map, abort
        if (this.ip_last_hit.has(ip)) {
            // console.log(`api rate limited for ${ip}: ${req}`);
            // throw new Error(`Rate limit reached: one call per ${minutes_max} minutes allowed.`);
            console.error(`Rate limit reached: one call per 1 second allowed.`);
            ctx.status = 429;
            ctx.body = 'Too Many Requests';
            result = true;
        }

        // record api hit
        this.ip_last_hit.set(ip, now);
        return !result;
    }

    async checkCSRF(ctx, csrf) {
        try {
            ctx.assertCSRF(csrf);
        } catch (e) {
            ctx.status = 403;
            ctx.body = 'invalid csrf token';
            console.log(
                '-- invalid csrf token -->',
                ctx.request.method,
                ctx.request.url,
                ctx.session.uid
            );
            return false;
        }
        return true;
    }

    async getSupportedLocales() {
        const locales = [];
        const files = fs.readdirSync(
            `${process.env.NODE_PATH}/application/locales`
        );
        for (const filename of files) {
            const match_res = filename.match(/(\w+)\.json?$/);
            if (match_res) locales.push(match_res[1]);
        }
        return locales;
    }
}
