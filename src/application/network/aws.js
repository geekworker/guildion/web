const AWS = require('aws-sdk');
import { ApiError } from '@extension/Error';
import data_config from '@constants/data_config';
import uuidv4 from 'uuid/v4';
const URL = require('url');

AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION,
    signatureVersion: 'v4',
    credentials: new AWS.Credentials(
        process.env.AWS_ACCESS_KEY_ID,
        process.env.AWS_SECRET_ACCESS_KEY
    ),
});

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_S3_REGION,
    signatureVersion: 'v4',
    credentials: new AWS.Credentials(
        process.env.AWS_ACCESS_KEY_ID,
        process.env.AWS_SECRET_ACCESS_KEY
    ),
});

const ec2 = new AWS.EC2({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION,
    credentials: new AWS.Credentials(
        process.env.AWS_ACCESS_KEY_ID,
        process.env.AWS_SECRET_ACCESS_KEY
    ),
});

const singleton = Symbol();
const singletonEnforcer = Symbol();

class AWSHandler {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new AWSHandler(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    static checkUploaded = (before, after) => {
        return false;
    };

    static getPureUrl = url => {
        return url.split(/[?#]/)[0];
    };

    static getUploadedUrl = filename => {
        return `${process.env.AWS_CLOUDFRONT_URL}/${filename}`;
    };

    static getFileName = url => {
        const parser = URL.parse(url);
        return parser.pathname
            .replace(`/${process.env.AWS_S3_BUCKET}`, '')
            .replace(`/`, '');
    };

    static getDirPath = path => {
        return path.startsWith("uploads/") ? path : "uploads/" + path
    };

    static deletable = url => {
        if (!url) return false;
        const parser = URL.parse(url);
        return (
            parser.host ==
                `${process.env.AWS_S3_BUCKET}.s3-${process.env.AWS_S3_REGION}.amazonaws.com` ||
            parser.host == `s3-${process.env.AWS_S3_REGION}.amazonaws.com` ||
            parser.host == process.env.AWS_CLOUDFRONT_DOMAIN
        ) && !parser.pathname.startsWith("public/") && !parser.pathname.startsWith("/public/")
    };

    static createChannelId = () => uuidv4();
    //String.prototype.makeId(data_config.stream_channel_id_limit);

    async uploadImage({ image, filename, type }) {
        const s3_params = {
            Bucket: process.env.AWS_S3_BUCKET,
            Key: filename,
            ContentType: type,
            Body: image,
            ACL: 'public-read',
        };

        return new Promise((resolve, reject) => {
            s3.putObject(s3_params, (err, result) => {
                if (err) reject(
                    new ApiError({
                        error: err,
                        tt_key: 'errors.invalid_response_from_server',
                    })
                )
                resolve(AWSHandler.getUploadedUrl(filename));
            });
        });
    }

    async deleteFile(url) {
        const s3_params = {
            Bucket: process.env.AWS_S3_BUCKET,
            Key: AWSHandler.getFileName(url),
        };

        return new Promise((resolve, reject) => {
            s3.deleteObject(s3_params, (err, result) => {
                if (err) reject(
                    new ApiError({
                        error: err,
                        tt_key: 'errors.invalid_response_from_server',
                    })
                )
                resolve(result);
            });
        });
    }

    async deleteFileFromKey(key) {
        const s3_params = {
            Bucket: process.env.AWS_S3_BUCKET,
            Key: key,
        };

        return new Promise((resolve, reject) => {
            s3.deleteObject(s3_params, (err, result) => {
                if (err) reject(
                    new ApiError({
                        error: err,
                        tt_key: 'errors.invalid_response_from_server',
                    })
                )
                resolve(result);
            });
        });
    }

    async getListObjects(path) {
        path = AWSHandler.getDirPath(path)
        const s3_params = {
            Bucket: process.env.AWS_S3_BUCKET,
            Prefix: path,
        };

        return new Promise((resolve, reject) => {
            s3.listObjectsV2(s3_params, (err, result) => {
                if (err) reject(
                    new ApiError({
                        error: err,
                        tt_key: 'errors.invalid_response_from_server',
                    })
                )
                resolve(result.Contents);
            });
        });
    }

    async deleteDirectory(path) {
        path = AWSHandler.getDirPath(path)
        const objects = await this.getListObjects(path)

        const deleted = await Promise.all(
            objects.map(obj => this.deleteFileFromKey(obj.Key))
        );
    }
}

module.exports = AWSHandler;
