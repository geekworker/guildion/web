import tt from 'counterpart';
import { browserHistory } from 'react-router';
import locale_config from '@constants/locale_config';

function getParam(name, url) {
    if (!process.env.BROWSER) return;
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return null;
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function getLocalePathname() {
    if (!process.env.BROWSER) return;
    const firstPath = window.location.pathname.split('/')[1];
    if (locale_config.available_locales.includes(firstPath.replace('/', '')))
        return firstPath.replace('/', '');
}

export function getEnvLocale(env) {
    env = env || process.env;
    return (
        getLocalePathname() ||
        tt.getLocale() ||
        env.LC_ALL ||
        env.LC_MESSAGES ||
        env.LANG ||
        env.LANGUAGE
    );
}

export function getOS() {
    if (!process.env.BROWSER) return;
    var userAgent = window.navigator.userAgent,
        platform = window.navigator.platform,
        macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
        windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
        iosPlatforms = ['iPhone', 'iPad', 'iPod'],
        os = null;

    if (macosPlatforms.indexOf(platform) !== -1) {
        os = 'Mac OS';
    } else if (iosPlatforms.indexOf(platform) !== -1) {
        os = 'iOS';
    } else if (windowsPlatforms.indexOf(platform) !== -1) {
        os = 'Windows';
    } else if (/Android/.test(userAgent)) {
        os = 'Android';
    } else if (!os && /Linux/.test(platform)) {
        os = 'Linux';
    }
    return os;
}
